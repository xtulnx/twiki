# ---+ Extensions
# ---++ LdapContrib
# This is the configuration used by the <em>LdapContrib</em> add-on, 
# the <em>LdapNgPlugin</em> and the <em>LdapContribAdminPlugin</em>.
# <p>
# Mandatory classes is the <em>LdapPasswdUser</em> as TWiki's <em>PasswordManager</em>,
# as well as <em>LdapUserMapping</em> as TWiki's <em>UserMappingManager</em>.
# </p>
# <p>
# If you use Apache for authentication, register <em>LdapApacheLogin</em> as the
# <em>LoginManager</em>. If you want to use LDAP for authentication, use <em>TemplateLogin</em>.
# </p>

# **BOOLEAN**
# Enable/disable debug output to the TWiki debug log.
$TWiki::cfg{Ldap}{Debug} = 0;

# ---+++ Connection settings

# **STRING**
# IP address (or hostname) of the LDAP server.
$TWiki::cfg{Ldap}{Host} = 'ldap.my.domain.com';

# **NUMBER**
# Port used when binding to the LDAP server.
$TWiki::cfg{Ldap}{Port} = 389;

# **NUMBER**
# Ldap protocol version to use when querying the server;
# Possible values are: 2, 3.
$TWiki::cfg{Ldap}{Version} = '3';

# **STRING**
# Base DN to use in searches.
$TWiki::cfg{Ldap}{Base} = 'dc=my,dc=domain,dc=com';

# **STRING**
# The DN to use when binding to the LDAP server; if undefined anonymous binding
# will be used. Example 'cn=proxyuser,dc=my,dc=domain,dc=com'.
$TWiki::cfg{Ldap}{BindDN} = '';

# **PASSWORD**
# The password used when binding to the LDAP server.
$TWiki::cfg{Ldap}{BindPassword} = 'secret';

# **BOOLEAN**
# Use SASL authentication when binding to the server; Note, when using SASL the 
# BindDN and BindPassword setting are used to configure the SASL access.
$TWiki::cfg{Ldap}{UseSASL} = 0;

# **STRING**
# List of SASL authentication mechanism to try; defaults to 'PLAIN CRAM-MD5
# EXTERNAL ANONYMOUS'.
$TWiki::cfg{Ldap}{SASLMechanism} = 'PLAIN CRAM-MD5 EXTERNAL ANONYMOUS';

# **STRING**
# If you use 'GSSAPI' mechanism for SASL authentication, you need to specify
# a user name with which you are authenticated.
$TWiki::cfg{Ldap}{GSSAPIuser} = '';

# **BOOLEAN**
# Use Transort Layer Security (TLS) to encrypt the connection to the LDAP server.
# You will need to specify the servers CA File using the TLSCAFile option.
$TWiki::cfg{Ldap}{UseTLS} = 0;

# **STRING**
# This defines the version of the SSL/TLS protocol to use. Possible values are:
# 'sslv2', 'sslv3',  'sslv2/3' or 'tlsv1'.
$TWiki::cfg{Ldap}{TLSSSLVersion} = 'tlsv1';

# **STRING**
# Specify how to verify the servers certificate. Possible values are: 'require', 'optional'
# or 'require'.
$TWiki::cfg{Ldap}{TLSVerify} = 'require';

# **STRING**
# Pathname of the directory containing CA certificates.
$TWiki::cfg{Ldap}{TLSCAPath} = '';

# **STRING**
# Filename containing the certificate of the CA which signed the server's certificate.
$TWiki::cfg{Ldap}{TLSCAFile} = '';

# **STRING**
# Client side certificate file.
$TWiki::cfg{Ldap}{TLSClientCert} = '';

# **STRING**
# Client side private key file.
$TWiki::cfg{Ldap}{TLSClientKey} = '';

# ---+++ User settings
# The options below configure how the wiki will extract account records from LDAP.

# **SELECTCLASS none,TWiki::Users::*User**
# Define a secondary password manager used to authenticate users that are 
# registered to the wiki natively. Note, that <b>this must not be TWiki::Users::LdapPasswdUser again!</b>
$TWiki::cfg{Ldap}{SecondaryPasswordManager} = 'none';
 
# **PERL**
# A list of distinguished names of users trees. 
# User accounts will be search for in the comma-separated list of user bases given here.
$TWiki::cfg{Ldap}{UserBase} = ['ou=people,dc=my,dc=domain,dc=com'];

# **SELECT sub,one**
# The scope of the search for users starting at UserBase. While "sub" search recursively
# a "one" will only search up to one level under the UserBase.
$TWiki::cfg{Ldap}{UserScope} = 'sub';

# **STRING**
# Filter to be used to find login accounts. Compare to GroupFilter below.
$TWiki::cfg{Ldap}{LoginFilter} = 'objectClass=posixAccount';

# **STRING**
# The user login name attribute. This is the attribute name that is
# used to login. 
$TWiki::cfg{Ldap}{LoginAttribute} = 'uid';

# **STRING**
# Regular expression pattern to limit which LoginNames can be written to, 
# and attempted read from, the LdapContrib database.
$TWiki::cfg{Ldap}{LoginPattern} = '^.+$';

# **STRING**
# The user mail attribute. This is the attribute name used to fetch
# users e-mail.
$TWiki::cfg{Ldap}{MailAttribute} = 'mail';

# **STRING**
# The user's wiki name attribute(s). This is the attributes to generate
# the WikiName from. Separate by comma.
$TWiki::cfg{Ldap}{WikiNameAttributes} = 'cn';

# **BOOLEAN**
# Enable/disable normalization of WikiUserNames after they have been created using 
# LDAP data by using the 'WikiNameAttributes'.
# If the WikiNameAttribute is set to 'mail' a trailing @my.domain.com
# is stripped. WARNING: if you switch this off you have to garantee that the WikiNames
# in the WikiNameAttribute are a proper WikiWord (camel-case, no spaces, no umlauts etc).
$TWiki::cfg{Ldap}{NormalizeWikiNames} = 1;

# **BOOLEAN**
# Enable/disable normalization of login names
$TWiki::cfg{Ldap}{NormalizeLoginNames} = 0;

# **BOOLEAN**
# Decide if LoginName should be case sensitive or not. If turned off, all LoginNames will
# be converted to lowercase before being read from or written to the cache.
$TWiki::cfg{Ldap}{CaseSensitiveLogin} = 0;

# **STRING**
# DEPRECATED. 
# Rename rules for !WikiNames. Used to prevent certain !WikiNames from being put in the
# cache, giving them a different !WikiName.
# This setting is deprecated - use <code>RewriteWikiNames</code> instead
$TWiki::cfg{Ldap}{WikiNameAliases} = '';

# **PERL**
# A hash mapping of rewrite rules. Rules are separated by commas. A rule has 
# the form 
# <pre>{
#   'pattern1' => 'substitute1', 
#   'pattern2' => 'substitute2' 
# }</pre>
# consists of a name pattern that has to match the wiki name to be rewritten
# and a substitute value that is used to replace the matched pattern. The
# substitute might contain $1, $2, ... , $5 to insert the first, second, ..., fifth
# bracket pair in the key pattern. (see perl manual for regular expressions).
# Example: '(.*)_users' => '$1'
$TWiki::cfg{Ldap}{RewriteWikiNames} = {
  '^(.*)@.*$' => '$1'
};

# **BOOLEAN**
# Allow/disallow changing the LDAP password using the ChangePassword feature
$TWiki::cfg{Ldap}{AllowChangePassword} = 0;

# **BOOLEAN**
# Take TWikiUserMapping rules stored in {UsersWebName}.{UsersTopicName} 
# (Typically Main.TWikiUsers) into consideration when building the database for the first time.
$TWiki::cfg{Ldap}{PreserveTWikiUserMapping} = 0;

# **BOOLEAN**
# Preserve the WikiName to login name mappings belonging to users deleted from LDAP when
# migrating from TWikiUserMapping (See {PreserveTWikiUserMapping} flag)
$TWiki::cfg{Ldap}{PreserveWikiNames} = 0;

# ---+++ Group settings
# The settings below configures the mapping and processing of LoginNames and WikiNames as
# well as the use of LDAP groups. 
# In any case you have to select the LdapUserMapping as the UserMappingManager in the
# Security Section section above.

# **BOOLEAN**
# Enable use of LDAP groups. If you switch this off the group-related settings
# have no effect. This flag is of use if you don't want to define groups in LDAP
# but still want to map LoginNames to WikiNames on the base of LDAP data.
$TWiki::cfg{Ldap}{MapGroups} = 1;

# **PERL**
# A list of distinguished names of the groups trees. All group definitions
# are used in the subtree under GroupBases. 
$TWiki::cfg{Ldap}{GroupBase} = ['ou=group,dc=my,dc=domain,dc=com'];

# **SELECT sub,one**
# The scope of the search for groups starting at GroupBase. While "sub" search recursively
# a "one" will only search up to one level under the GroupBase.
$TWiki::cfg{Ldap}{GroupScope} = 'sub';

# **STRING**
# Filter to be used to find groups. Compare to LoginFilter.
$TWiki::cfg{Ldap}{GroupFilter} = 'objectClass=posixGroup';

# **STRING**
# This is the name of the attribute that holds the name of the 
# group in a group record.
$TWiki::cfg{Ldap}{GroupAttribute} = 'cn';

# **STRING**
# Pattern to validate user login name. Prevents LDAP lookup for a name
# if the name does not match the pattern.
$TWiki::cfg{Ldap}{GroupPattern} = '^.+$';

# **STRING**
# This is the name of the attribute that holds the primary group attribute.
# This attribute is stored as part of the user record and refers to the
# primary group this user is in. Sometimes, this membership is not captured
# in the group record itself but in the user record to make it the primary group
# a user is in.
$TWiki::cfg{Ldap}{PrimaryGroupAttribute} = 'gidNumber';

# **STRING**
# The attribute that should be used to collect group members. This is the name of the
# attribute in a group record used to point to the user record. For example, in a possix setting this
# is the uid of the relevant posixAccount. If groups are implemented using the object class
# 'groupOfNames' the MemberAttribute will store a literal DN pointing to the account record. In this
# case you have to switch on the MemberIndirection flag below.
$TWiki::cfg{Ldap}{MemberAttribute} = 'memberUid';

# **STRING**
# This is the name of the attribute in a group record used to point to the inner group record.
# This value is often the same than MemberAttribute but may differ for some LDAP servers.
$TWiki::cfg{Ldap}{InnerGroupAttribute} = 'memberUid';

# **BOOLEAN**
# Flag indicating wether the MemberAttribute of a group stores a DN. 
$TWiki::cfg{Ldap}{MemberIndirection} = 0;

# **BOOLEAN**
# Flag indicating wether we fallback to WikiGroups. If this is switched on, 
# standard Wiki groups will be used as a fallback if a group definition of a given
# name was not found in the LDAP database. For best performance, turn this off to only
# use LDAP groups if you can.
$TWiki::cfg{Ldap}{WikiGroupsBackoff} = 1;

# **BOOLEAN**
# Enable/disable normalization of group names as they come from LDAP:
$TWiki::cfg{Ldap}{NormalizeGroupNames} = 0;

# **BOOLEAN**
# Decide if Groups should be case sensitive or not. If turned off, all Groups will
# be converted to lowercase before being read from or written to the cache.
$TWiki::cfg{Ldap}{CaseSensitiveGroup} = 0;

# **PERL**
# A hash mapping of rewrite rules. Rules are separated by commas. A rule has 
# the form 
# <pre>{
#   'pattern1' => 'substitute1', 
#   'pattern2' => 'substitute2' 
# }</pre>
# consists of a name pattern that has to match the group name to be rewritten
# and a substitute value that is used to replace the matched pattern. The
# substitute might contain $1, $2, ... , $5 to insert the first, second, ..., fifth
# bracket pair in the key pattern. (see perl manual for regular expressions).
# Example: '(.*)_users' => '$1'
$TWiki::cfg{Ldap}{RewriteGroups} = {
};

# **BOOLEAN**
# Flag indicating if groups that get the same name are merged. For exmaple, given two 
# ldap groups end up having the same name even though they have a different distinguished name
# or have been rewritten to match on the same group name (see RewriteGroups), then members
# of both groups are merged into one group of that name.
$TWiki::cfg{Ldap}{MergeGroups} = 0;

# ---+++ Performance settings
# The following settings are used to optimize performance in your environment. Please take care.

# **SELECT all,existing,off**
# Enable refreshing of the LDAP database.
# The refresh process is run the first time the database is created, and in defined intervals after this
# (See {MaxCacheAge}).
#
# <p><em>all</em> new LDAP users are added, the existing users are updated (DN and emails if changed), group definitions are updated.</p>
# <p><em>existing</em> new LDAP users are not added, the existing users are updated (DN and emails if changed), group definitions are updated.</p>
# <p><em>off</em> The possibility to refresh the LdapContrib database is disabled.</p>
$TWiki::cfg{Ldap}{Precache} = 'all';

# **NUMBER** 
# Time in seconds when cache data expires and is reloaded by the next user visiting
# the TWiki. Set to 0 to disable, ideal if you want a scheduled server job to do the work instead. 
$TWiki::cfg{Ldap}{MaxCacheAge} = 86400;

# **BOOLEAN**
# If Precache is set to 'all' or 'existing', the cache can be refreshed adding ?refreshldap=on
# or ?refreshldap=force to the URL. To prevent abuse, you can define that this action only
# can be invoked from the command line:
$TWiki::cfg{Ldap}{CLIOnlyRefresh} = 0;

# **NUMBER**
# Number of user objects to fetch in one paged result when building the username mappings;
# this is a speed optimization option, use this value with caution.
# Requires access to the 'control' LDAP extension as an LDAP client. Use '0' to disable it.
$TWiki::cfg{Ldap}{PageSize} = 500; 

# **BOOLEAN**
# Potentially backup the database after it's refreshed, given that the age of the
# previous backup is older than BackupFileAge. Stores 7 backup iterations. 
$TWiki::cfg{Ldap}{BackupCacheFile} = 0; 

# **NUMBER**
# Given that BackupCacheFile is true, what is the minimum back file age before we do another backup?
$TWiki::cfg{Ldap}{BackupFileAge} = 0; 

# ---+++ Misc settings 
# Other settings

# **STRING 50**
# Prevent certain names from being looked up in LDAP. Special values:
# <p><em>WIKIWORDS</em> prevents all CamelCased words from being looked up</p> 
# <p><em>EMAILS</em> prevents email addresses from being looked up</p>
$TWiki::cfg{Ldap}{Exclude} = 'WIKIWORDS, admin, guest';
