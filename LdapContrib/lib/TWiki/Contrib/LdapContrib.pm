# Module for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2006-2010 Michael Daum http://michaeldaumconsulting.com
# Copyright (C) 2006-2014 TWiki Contributors.
# Portions Copyright (C) 2006 Spanlink Communications
# Portions Copyright (C) 2013-2014 CERN 
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Contrib::LdapContrib;

use strict;
use warnings;

use Data::Dumper;
use Net::LDAP;
use Net::LDAP::Constant qw(LDAP_SUCCESS LDAP_SIZELIMIT_EXCEEDED LDAP_CONTROL_PAGED);
use Net::LDAP::Extension::SetPassword;
use DB_File::Lock;
use Fcntl qw(:flock O_RDWR O_CREAT O_RDONLY);
use File::Basename qw(basename);
use Storable qw(dclone);
use Time::Local;

require TWiki::Func;
require TWiki::Plugins;

use vars qw($VERSION $RELEASE %sharedLdapContrib);

$VERSION = '$Rev: 10335 (2012-05-05) $';
$RELEASE = '4.38';

=pod

---+++ TWiki::Contrib::LdapContrib

General LDAP services for TWiki. This class encapsulates the TWiki-specific means to 
integrate an LDAP directory service.  

Used by:
TWiki::LoginManager::LdapApacheLogin for authentication via $ENV{REMOTE_USER}.
TWiki::Users::LdapPasswdUser for authentication via TWiki::LoginManager::TemplateLogin.
TWiki::Users::LdapUserMapping for group definitions and login name to WikiName user mappings.

It is also used by some optional plug-ins:
TWiki::Plugins::LdapNgPlugin to interface general query services, as well as making WikiNames 
dynamic in your TWiki.
TWiki::Plugins::LdapContribAdminPlugin to provide an admin panel for the LdapContrib database.

---++++ Typical usage
<verbatim>
my $ldap = new TWiki::Contrib::LdapContrib;

my $result = $ldap->search(filter=>'mail=*@gmx*');
my $errorMsg = $ldap->getError();

my $count = $result->count();

my @entries = $result->sorted('sn');
my $entry = $result->entry(0);

my $value = $entry->get_value('cn');
my @emails = $entry->get_value('mail');
</verbatim>

---++++ Cache storage format

The cache stores a series of key-value pairs in a DB_File. The following
keys are used:

   * WIKINAMES - list of all wikiNames
   * LOGINNAMES - list of all loginNames
   * GROUPS - list of all groups
   * UNKWNUSERS - list of all usernames that could not be found in LDAP (to avoid future LDAP lookups in case caching is OFF)
   * UNKWNGROUPS - list of all group names that could not be found in LDAP (to avoid future LDAP lookups in case caching is OFF)
   * GROUPS::$groupName - list of all loginNames in group groupName (membership)
   * GROUP2UNCACHEDMEMBERSDN::$groupName - list of all DNs (when in memberIndirection mode) that could not be resolved to a user or group existing in the cache when $groupName was retreived from LDAP
   * EMAIL2U::$emailAddr - stores the loginName of an emailAddr
   * U2EMAIL::$loginName - stores the emailAddr of a loginName 
   * U2W::$loginName - stores the wikiName of a loginName
   * W2U::$wikiName - stores the loginName of a wikiName
   * DN2U::$dn - stores the LoginName and GroupName of a distinguishedName
   * U2DN::$loginName - stores the distinguishedName of a LoginName and GroupName
   * U2CREATED:$loginName - stores the create date of the entry
   * U2UPDATED:$loginName - stores the update date of the entry (meaning wikiName, dn or emails for the loginName is altered)
   * CACHEREFRESHPROCESS - stores the process id of process potentially refreshing the cache. Used to ensure that only one process refreshes the cache at any given time.
   * LASTUPDATED - a timestamp that's stored each time the cache is refreshed.

=cut

=pod

---++++ writeDebug($msg) 

Static Method to write a debug messages. 

=cut

sub writeDebug {
  my ($this, $msg, $callerFile, $callerLine) = @_;
  $callerFile = (defined $callerFile ? basename($callerFile) : 'callerFile');
  $callerLine ||= 'callerLine';

  if ($this->{debug}) {
    my $ip = $ENV{'REMOTE_ADDR'} || 'ip';
    my $id = $ENV{'REMOTE_USER'} || 'ssoid';
    my $topic = $this->{session}->{topicName} || 'topic';
    my $web = $this->{session}->{webName} || 'web';
    TWiki::Func::writeDebug("$msg ($ip/$id/$web.$topic/$callerFile:$callerLine)") ;
  }
}


=pod

---++++ writeWarning($msg) 

Static Method to write a warning messages. 

=cut

sub writeWarning {
  my ($this, $msg, $callerFile, $callerLine) = @_;
  $callerFile = (defined $callerFile ? basename($callerFile) : 'callerFile');
  $callerLine ||= 'callerLine';

  my $ip = $ENV{'REMOTE_ADDR'} || 'ip';
  my $id = $ENV{'REMOTE_USER'} || 'ssoid';
  my $topic = $this->{session}->{topicName} || 'topic';
  my $web = $this->{session}->{webName} || 'web';

  TWiki::Func::writeWarning("LdapContrib - $msg ($ip/$id/$web.$topic/$callerFile:$callerLine)") ;
}


=pod

---++++ new($session) -> $ldap

Construct a new TWiki::Contrib::LdapContrib object

Options not passed to the constructor are taken from the global settings
in =lib/LocalSite.cfg=.

See =configure= for possible configurable options with description.

=cut

sub new {
  my $class = shift;
  my $session = shift;

  # setting the SESSION var rather early; some twiki engines do that a bit
  # later than calling for a login manager. that will cause all sorts of
  # problems as the TWiki::Func API can't be used then. see Item10084
  $TWiki::Plugins::SESSION = $session;

  my $this = {
    debug=>$TWiki::cfg{Ldap}{Debug} || 0,

    # Connection settings
    host => $TWiki::cfg{Ldap}{Host} || 'localhost',

    port => defined $TWiki::cfg{Ldap}{Port} && $TWiki::cfg{Ldap}{Port} =~ m/\A\d+\z/ 
      ? $TWiki::cfg{Ldap}{Port} : 389,

    version => defined $TWiki::cfg{Ldap}{Version} && $TWiki::cfg{Ldap}{Version} =~ m/\A\d+\z/
      ? $TWiki::cfg{Ldap}{Version} : 3,

    base    => $TWiki::cfg{Ldap}{Base}    || '',
    bindDN  => $TWiki::cfg{Ldap}{BindDN}  || '',

    # Connection settings continued, if not connecting anonymously
    bindPassword  => $TWiki::cfg{Ldap}{BindPassword}  || '',
    useSASL       => $TWiki::cfg{Ldap}{UseSASL}       ||  0,
    saslMechanism => $TWiki::cfg{Ldap}{SASLMechanism} || 'PLAIN CRAM-MD4 EXTERNAL ANONYMOUS',
    gssapiUser    => $TWiki::cfg{Ldap}{GSSAPIuser}    || '',
    useTLS        => $TWiki::cfg{Ldap}{UseTLS}        || 0,
    tlsSSLVersion => $TWiki::cfg{Ldap}{TLSSSLVersion} || 'tlsv1',
    tlsVerify     => $TWiki::cfg{Ldap}{TLSVerify}     || 'require',
    tlsCAPath     => $TWiki::cfg{Ldap}{TLSCAPath}     || '',
    tlsCAFile     => $TWiki::cfg{Ldap}{TLSCAFile}     || '',
    tlsClientCert => $TWiki::cfg{Ldap}{TLSClientCert} || '',
    tlsClientKey  => $TWiki::cfg{Ldap}{TLSClientKey}  || '',

    # User settings
    secondaryPasswordManager => $TWiki::cfg{Ldap}{SecondaryPasswordManager} || '',

    userBase => $TWiki::cfg{Ldap}{UserBase} 
      || $TWiki::cfg{Ldap}{BasePasswd} # DEPRECATED
      || $TWiki::cfg{Ldap}{Base} 
      || [],

    userScope       => $TWiki::cfg{Ldap}{UserScope}       || 'sub',
    loginFilter     => $TWiki::cfg{Ldap}{LoginFilter}     || 'objectClass=posixAccount',
    loginAttribute  => $TWiki::cfg{Ldap}{LoginAttribute}  || 'uid',
    loginPattern    => $TWiki::cfg{Ldap}{LoginPattern}    || '^.+$',
    mailAttribute   => $TWiki::cfg{Ldap}{MailAttribute}   || 'mail',

    wikiNameAttribute => $TWiki::cfg{Ldap}{WikiNameAttributes} 
      || $TWiki::cfg{Ldap}{WikiNameAttribute} 
      || 'cn',

    normalizeWikiNames        => undef, # To be set later
    normalizeLoginNames       => undef, # To be set later
    caseSensitiveLogin        => $TWiki::cfg{Ldap}{CaseSensitiveLogin}        || 0,
    wikiNameAliases           => $TWiki::cfg{Ldap}{WikiNameAliases}           || '', # DEPRECATED
    rewriteWikiNames          => $TWiki::cfg{Ldap}{RewriteWikiNames}          || {},
    allowChangePassword       => $TWiki::cfg{Ldap}{AllowChangePassword}       || 0,
    preserveTWikiUserMapping  => $TWiki::cfg{Ldap}{PreserveTWikiUserMapping}  || 0,
    preserveWikiNames         => $TWiki::cfg{Ldap}{PreserveWikiNames}         || 0,

    # Group settings
    mapGroups => $TWiki::cfg{Ldap}{MapGroups} || 0,
    groupBase => $TWiki::cfg{Ldap}{GroupBase} 
      || $TWiki::cfg{Ldap}{BaseGroup} # DEPRECATED
      || $TWiki::cfg{Ldap}{Base} 
      || [],

    groupScope => $TWiki::cfg{Ldap}{GroupScope}
      || 'sub',

    groupFilter           => $TWiki::cfg{Ldap}{GroupFilter}           || 'objectClass=posixGroup',
    groupAttribute        => $TWiki::cfg{Ldap}{GroupAttribute}        || 'cn',
    groupPattern          => $TWiki::cfg{Ldap}{GroupPattern}          || '^.+$',
    primaryGroupAttribute => $TWiki::cfg{Ldap}{PrimaryGroupAttribute} || 'gidNumber',
    memberAttribute       => $TWiki::cfg{Ldap}{MemberAttribute}       || 'memberUid',
    innerGroupAttribute   => $TWiki::cfg{Ldap}{InnerGroupAttribute}   || 'uniquegroup',
    memberIndirection     => $TWiki::cfg{Ldap}{MemberIndirection}     || 0,
    nativeGroupsBackoff   => $TWiki::cfg{Ldap}{WikiGroupsBackoff}     || 0,
    normalizeGroupNames   => undef, # To be set later
    caseSensitiveGroup    => $TWiki::cfg{Ldap}{CaseSensitiveGroup}    || 0,
    rewriteGroups         => $TWiki::cfg{Ldap}{RewriteGroups} || {},
    mergeGroups           => $TWiki::cfg{Ldap}{MergeGroups}   || 0,

    # Performance settings
    preCache => defined($TWiki::cfg{Ldap}{Precache})
      ? $TWiki::cfg{Ldap}{Precache} : 'all',

    maxCacheAge => defined $TWiki::cfg{Ldap}{MaxCacheAge} && $TWiki::cfg{Ldap}{MaxCacheAge} =~ m/\A\d+\z/
      ? $TWiki::cfg{Ldap}{MaxCacheAge}  : 86400,

    pageSize => defined $TWiki::cfg{Ldap}{PageSize} && $TWiki::cfg{Ldap}{PageSize} =~ m/\A\d+\z/
      ? $TWiki::cfg{Ldap}{PageSize}     : 500,

    # Security settings
    CLIOnlyRefresh    => $TWiki::cfg{Ldap}{CLIOnlyRefresh} || 0,
    backupCacheFile   => $TWiki::cfg{Ldap}{BackupCacheFile}   || 0,
    backupFileAge => defined $TWiki::cfg{Ldap}{BackupFileAge} && $TWiki::cfg{Ldap}{BackupFileAge} =~ m/\A\d+\z/
      ? $TWiki::cfg{Ldap}{BackupFileAge} : 86400,

    # Misc settings
    exclude => $TWiki::cfg{Ldap}{Exclude} || 'WIKIWORDS, admin, guest',

    # In-house attributes
    isConnected               => 0,
    ldap                      => undef, # connect later
    error                     => undef,

    # In-house cache refresh variables
    _refreshMode              => undef,
    _preserveTWikiUserMapping => undef,
    _wikiNameClashes          => undef,
    _allFetchedLogins         => undef,
    _primaryGroup             => undef,
    _groups                   => undef,
    @_
  };
  bless($this, $class);

  $this->{session} = $session;

  $this->{normalizeWikiName} = defined $TWiki::cfg{Ldap}{NormalizeWikiNames} ? $TWiki::cfg{Ldap}{NormalizeWikiNames}
    : defined $TWiki::cfg{Ldap}{NormalizeWikiName} ? $TWiki::cfg{Ldap}{NormalizeWikiName}
    : 1;

  $this->{normalizeLoginName} = defined $TWiki::cfg{Ldap}{NormalizeLoginNames} ? $TWiki::cfg{Ldap}{NormalizeLoginNames}
    : defined $TWiki::cfg{Ldap}{NormalizeLoginName} ? $TWiki::cfg{Ldap}{NormalizeLoginName}
    : 1;

  $this->{normalizeGroupName} = defined $TWiki::cfg{Ldap}{NormalizeGroupNames} ? $TWiki::cfg{Ldap}{NormalizeGroupNames}
    : defined $TWiki::cfg{Ldap}{NormalizeGroupName} ? $TWiki::cfg{Ldap}{NormalizeGroupName}
    : 1;

  my $workArea = $session->{store}->getWorkArea('LdapContrib');
  mkdir $workArea unless -d $workArea;
  $this->{cacheFile} = $workArea.'/cache.db';

  # Locking parameters 
  $this->{locking} = {
    'nonblocking'   => 0, # wait, not croak if lock is set
    'lockfile_name' => $this->{cacheFile} . '.lock',
    'lockfile_mode' => 0600,
    'mode'          => 'none'
  };

  if ($this->{useSASL}) {
    # $this->writeDebug("will use SASL authentication");
    require Authen::SASL;
  }
  
  # Item7509: don't force $this->{userBase} and $this->{groupBase} to be
  # array reference. At least with Net::LDAP 0.43, that causes errors

  @{$this->{wikiNameAttributes}} = split(/\s*,\s*/, $this->{wikiNameAttribute});

  # protect against actidental misconfiguration, that might lead
  # to an infinite loop during authorization etc.
  if ($this->{secondaryPasswordManager} eq 'TWiki::Users::LdapPasswdUser') {
    $this->writeWarning('Removing secondaryPasswordManager TWiki::Users::LdapPasswdUser to avoid infinite loops');
    $this->{secondaryPasswordManager} = '';
  } elsif ($this->{secondaryPasswordManager} eq 'none') {
    $this->{secondaryPasswordManager} = '';
  }

  # Legacy: Before, preCache was either true or false.
  # Now it's all, existing or off.
  # The old true equals all.
  # The old false equals off.
  if ($this->{preCache} eq '1') { # Item7509: it used to be "== 1", which causes warning
    $this->{preCache} = 'all';
  } elsif ($this->{preCache} !~ m/\A\s*(?:all|existing)\s*\z/) {
    $this->{preCache} = 0;
  }

  # create exclude map
  my %excludeMap;
  if ($this->{caseSensitiveLogin}) {
      %excludeMap = map { $_ => 1 } split(/\s*,\s*/, $this->{exclude});
  } else {
      # Since $this->{exclude} might contain groups, we must include the original value as well as the lowercase one
      %excludeMap = map { $_ => 1, lc($_) => 1 } split(/\s*,\s*/, $this->{exclude});
  }
  $this->{excludeMap} = \%excludeMap;

  # creating alias map (deprecated)
  my %aliasMap = ();
  foreach my $alias (split(/\s*,\s*/, $this->{wikiNameAliases})) {
    if ($alias =~ /^\s*(.+?)\s*=\s*(.+?)\s*$/) {
      $aliasMap{$1} = $2;
    }
  }
  $this->{wikiNameAliases} = \%aliasMap;

  #$this->writeDebug("constructed a new LdapContrib object");

  return $this;
}

=pod

---++++ getLdapContrib($session) -> $ldap

Returns a standard singleton TWiki::Contrib::LdapContrib object based on the site-wide
configuration. 

=cut

sub getLdapContrib {
  my $session = shift;

  # Item7509: don't assume LdapContrib is only for LdapUserMapping and
  # LdapPasswordUser. There are cases LdapContrib is used by LdapNgPlugin
  # for LDAP queries only
  my $obj = $sharedLdapContrib{$session};
  return $obj if $obj;

  $obj = new TWiki::Contrib::LdapContrib($session);
  $sharedLdapContrib{$session} = $obj;

  my $mode = $obj->getRefreshMode();
  $obj->refreshCache($mode) if $mode > 0;

  return $obj;
}

=pod

---++++ connect($login, $passwd) -> $boolean

Connect to LDAP server. If a $login name and a $passwd is given then a bind is done.
Otherwise the communication is anonymous. You don't have to connect() explicitely
by calling this method. The methods below will do that automatically when needed.

=cut

sub connect {
  my ($this, $dn, $passwd) = @_;

  $this->writeDebug("called connect");
  $this->writeDebug("dn=$dn", 2) if $dn;
  #$this->writeDebug("passwd=***", 2) if $passwd;

  require Net::LDAP;
  $this->{ldap} = Net::LDAP->new($this->{host},
    port=>$this->{port},
    version=>$this->{version},
  );
  unless ($this->{ldap}) {
    $this->{error} = "failed to connect to $this->{host}";
    $this->{error} .= ": $@" if $@;
    return 0;
  }

  # TLS bind
  if ($this->{useTLS}) {
    $this->writeDebug("using TLS");
    my %args = (
      verify => $this->{tlsVerify},
      cafile => $this->{tlsCAFile},
      capath => $this->{tlsCAPath},
    );
    $args{"clientcert"} = $this->{tlsClientCert} if $this->{tlsClientCert};
    $args{"clientkey"} = $this->{tlsClientKey} if $this->{tlsClientKey};
    $args{"sslversion"} = $this->{tlsSSLVersion} if $this->{tlsSSLVersion};
    $this->{ldap}->start_tls(%args);
  }

  $passwd = $this->toUtf8($passwd) if $passwd;

  # authenticated bind
  my $msg;
  if (defined($dn)) {
    die "illegal call to connect()" unless defined($passwd);
    $msg = $this->{ldap}->bind($dn, password=>$passwd);
    $this->writeDebug("bind for $dn");
  } 

  # proxy user 
  elsif ($this->{bindDN} && $this->{bindPassword}) {

    if ($this->{useSASL}) {
      # sasl bind
      my $sasl = Authen::SASL->new(
        mechanism => $this->{saslMechanism}, #'DIGEST-MD5 PLAIN CRAM-MD5 EXTERNAL ANONYMOUS',
        callback => {
          user => $this->{bindDN},
          pass => $this->{bindPassword},
        },
      );
      $this->writeDebug("sasl bind to $this->{bindDN}");
      $msg = $this->{ldap}->bind($this->{bindDN}, sasl=>$sasl, version=>$this->{version} );
    } else {
      # simple bind
      $this->writeDebug("proxy bind using ".$this->{bindDN});
      $msg = $this->{ldap}->bind($this->{bindDN},password=>$this->{bindPassword});
    }
  }

  elsif ($this->{useSASL} && $this->{saslMechanism} eq 'GSSAPI') {
    $this->writeDebug("proxy bind using GSSAPI");
    my $sasl = Authen::SASL->new(
      mechanism => 'GSSAPI',
      user      => $this->{gssapiUser},
    );
    $msg = $this->{ldap}->bind('', sasl => $sasl);
  }
  
  # anonymous bind
  else {
    $this->writeDebug("anonymous bind");
    $msg = $this->{ldap}->bind;
  }

  $this->{isConnected} = ($this->checkError($msg) == LDAP_SUCCESS)?1:0;
  $this->writeDebug("failed to bind, error: " . $this->checkError($msg)) unless $this->{isConnected};
  return $this->{isConnected};
}

=pod

---++++ disconnect()

Unbind the LDAP object from the server. This method can be used to force
a reconnect and possibly rebind as a different user.

=cut

sub disconnect {
  my $this = shift;

  return unless defined($this->{ldap}) && $this->{isConnected};

  $this->writeDebug("called disconnect()");
  $this->{ldap}->unbind();
  $this->{ldap} = undef;
  $this->{isConnected} = 0;
}

=pod

---++++ finish()

finalize this ldap object.

=cut

sub finish {
  my $this = shift;

  # In case a cache refresh process is being destroyed early, we have to remove the 
  # CACHEREFRESHPROCESS flag before it dies
  if (defined $this->{cacheRefreshProcess}) {
    $this->getCacheTie('write');
    delete $this->{data}->{'CACHEREFRESHPROCESS'};
    $this->untieCache();
  }

  #$this->writeDebug("finishing");
    
  $this->untieCache() unless $this->{locking}{mode} eq 'none';
  undef $this->{cacheDB};
  untie %{$this->{data}};

  $this->disconnect();
  delete $sharedLdapContrib{$this->{session}};
}

=pod

---++++ checkError($msg) -> $errorCode

Private method to check a Net::LDAP::Message object for an error, sets
$ldap->{error} and returns the ldap error code. This method is called
internally whenever a message object is returned by the server. Use
$ldap->getError() to return the actual error message.

=cut

sub checkError {
  my ($this, $msg) = @_;

  my $code = $msg->code();
  if ($code == LDAP_SUCCESS) {
    $this->{error} = undef;
  } else {
    $this->{error} = $code.': '.$msg->error();
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeWarning("LDAP ERROR: $this->{error}", $callerFile, $callerLine);
  } 
 
  return $code;
}

=pod

---++++ getError() -> $errorMsg

Returns the error message of the last LDAP action or undef it no
error occured.

=cut

sub getError {
  my $this = shift;
  return $this->{error};
}


=pod

---++++ getAccount($login) -> Net::LDAP::Entry object

Fetches an account entry from the database and returns a Net::LDAP::Entry
object on success and undef otherwise. Note, the login name is match against
the attribute defined in $ldap->{loginAttribute}. Account records are 
search using $ldap->{loginFilter} in the subtree defined by $ldap->{userBase}.

=cut

sub getAccount {
  my ($this, $login) = @_;

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called getAccount($login)", $callerFile, $callerLine);
  }
  $login = lc($login) unless $this->{caseSensitiveLogin};

  my $loginFilter = $this->{loginFilter};
  $loginFilter = "($loginFilter)" unless $loginFilter =~ /^\(.*\)$/;
  my $filter = '(&'.$loginFilter.'('.$this->{loginAttribute}.'='.$login.'))';

  my @entries; # There can only be one, but we need to make sure.
  foreach my $userBase (@{$this->{userBase}}) {
    my $msg = $this->search(
      filter => $filter, 
      base => $userBase,
      deref => 'always'
    );

    eval {
      $this->writeDebug("no such account") and next unless defined $msg;
      push @entries, $msg->entry(0) if defined $msg->entry(0);
    };
    if ($@) {
      $this->writeWarning("Error: $@ (LoginName $login)");
      next;
    }

  }

  if (@entries == 1) {
    return $entries[0];
  } else {
    $this->{error} = "Login $login is invalid - no hits in LDAP.";
    $this->writeDebug($this->{error});
    return undef;
  }
}


=pod

---++++ search($filter, %args) -> $msg

Returns an Net::LDAP::Search object for the given query on success and undef
otherwise. If $args{base} is not defined $ldap->{base} is used.  If $args{scope} is not
defined 'sub' is used (searching down the subtree under $args{base}. If no $args{limit} is
set all matching records are returned.  The $attrs is a reference to an array
of all those attributes that matching entries should contain.  If no $args{attrs} is
defined all attributes are returned.

If undef is returned as an error occured use $ldap->getError() to get the
cleartext message of this search() operation.

Typical usage:
<verbatim>
my $result = $ldap->search(filter=>'uid=TestUser');
</verbatim>

=cut

sub search {
  my ($this, %args) = @_;

  $args{base} = $this->{base} unless $args{base};
  $args{scope} = 'sub' unless $args{scope};
  $args{limit} = 0 unless $args{limit};
  $args{attrs} = ['*'] unless $args{attrs};
  $args{filter} = $this->toUtf8($args{filter}) if $args{filter};

  if ($this->{debug}) {
    my $attrString = join(',', @{$args{attrs}});
    $this->writeDebug("called search(filter=$args{filter}, base=$args{base}, scope=$args{scope}, limit=$args{limit}, attrs=$attrString)");
  }

  unless ($this->{ldap}) {
    unless ($this->connect()) {
      $this->writeDebug("error in search: ".$this->getError());
      return undef;
    }
  }

  my $msg = $this->{ldap}->search(%args);
  my $errorCode = $this->checkError($msg);

  # we set a limit so it is ok that it exceeds
  if ($args{limit} && $errorCode == LDAP_SIZELIMIT_EXCEEDED) {
    $this->writeDebug("limit exceeded");
    return $msg;
  }
  
  if ($errorCode != LDAP_SUCCESS) {
    $this->writeDebug("error in search: ".$this->getError());
    return undef;
  }
  $this->writeDebug("found ".$msg->count." entries");

  return $msg;
}

=pod

---++++ cacheBlob($entry, $attribute, $refresh) -> $pubUrlPath

Takes an Net::LDAP::Entry and an $attribute name, and stores its value into a
file. Returns the pubUrlPath to it. This can be used to store binary large
objects like images (jpegPhotos) into the filesystem accessible to the httpd
which can serve it in return to the client browser. 

Filenames containing the blobs are named using a hash value that is generated
using its DN and the actual attribute name whose value is extracted from the 
database. If the blob already exists in the cache it is _not_ extracted once
again except the $refresh parameter is defined.

Typical usage:
<verbatim>
my $blobUrlPath = $ldap->cacheBlob($entry, $attr);
</verbatim>

=cut

sub cacheBlob {
  my ($this, $entry, $attr, $refresh) = @_;

  $this->writeDebug("called cacheBlob()");
  require Digest::MD5;

  my $systemWeb = $TWiki::cfg{SystemWebName};
  my $dir = $TWiki::cfg{PubDir}.'/'.$systemWeb.'/LdapContrib';
  my $key = Digest::MD5::md5_hex($entry->dn().$attr);
  my $fileName = $dir.'/'.$key;

  if ($refresh || !-f $fileName) {
    #$this->writeDebug("caching blob");
    my $value = $entry->get_value($attr);
    return undef unless defined $value;
    mkdir($dir, 0775) unless -e $dir;

    open (FILE, ">$fileName");
    binmode(FILE);
    print FILE $value;
    close (FILE);
  } else {
    #$this->writeDebug("already got blob");
  }
  
  #$this->writeDebug("done cacheBlob()");
  return $TWiki::cfg{PubUrlPath}.'/'.$systemWeb.'/LdapContrib/'.$key;
}

=pod

---++ tieCache($mode)

tie the cache with $mode. 
$mode 'read' ties the cache successfully given that there is only other 'read' locks on it.
$mode 'write' ties the cache successfully given that there is no locks on it.

=cut

sub getCacheTie {
  my ($this, $mode) = @_;

  unless ($mode =~ m/^read|write$/) {
    $this->writeWarning("unknown tie mode in getCacheTie: '$mode'");
    return;
  }
    
  #if ($this->{debug}) {
  #  my ($callerFile, $callerLine) = (caller)[1,2];
  #  $this->writeDebug("called getCacheTie($mode)", $callerFile, $callerLine);
  #}

  my $existingTie = $this->{locking}{mode};
  if ($existingTie ne 'none' && $existingTie eq $mode || ($existingTie eq 'write' && $mode eq 'read')) {
    return;
  } elsif ($existingTie eq 'read' && $mode eq 'write') {
    $this->untieCache();
  }

  $this->{locking}{mode} = $mode;

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("tieing cache with mode $mode", $callerFile, $callerLine);
  }

  # Create the cache unless it exists
  unless (-e $this->{cacheFile}) {
    $this->{locking}{mode} = 'write';
    my %db_hash;
    my $x = tie %db_hash, 'DB_File::Lock', $this->{cacheFile}, O_CREAT, 0600, $DB_HASH , $this->{locking} or die "Error creating cache file $this->{cacheFile}: $!";
    undef($x);
    untie(%db_hash);
    $this->{locking}{mode} = $mode;
  }
  
  my $tie_flag = ($mode eq 'read' ? O_RDONLY : O_RDWR);
  $this->{cacheDB} = tie(%{$this->{data}}, 'DB_File::Lock', $this->{cacheFile}, $tie_flag, 0600, $DB_HASH, $this->{locking}) or die "Error tieing cache file $this->{cacheFile}: $!";
}

=pod

---++ untieCache()

unties the cache

=cut

sub untieCache {
  my ($this) = @_;

  if ($this->{debug}) {
    my $tie = $this->{locking}{mode};
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called untieCache ()", $callerFile, $callerLine);
  }

  undef $this->{cacheDB};
  untie %{$this->{data}};
  $this->{locking}{mode} = 'none';
}


=pod

---++++ getRefreshMode() -> $mode

   * ?refreshldap=on: normal refresh
   * ?refreshldap=force: nuke previsous decisions on wikiName clashes
   * no refresh if cache is younger than maxCacheAge
   * nuke refresh if no cache exists

=cut

sub getRefreshMode {
  return 0 unless ( $TWiki::cfg{UserMappingManager} =~ /LdapUserMapping/ ||
                    $TWiki::cfg{PasswordManager} =~ /LdapPasswdUser/ );
  my ($this) = @_;

  my $mode = 0;
  
  unless (-e $this->{cacheFile}) {
    $mode = 2;
  } elsif (CGI::param('refreshldap')) {
    my $context = TWiki::Func::getContext();
    if ( !$this->{CLIOnlyRefresh} ||($this->{CLIOnlyRefresh} && defined $context->{'command_line'}) ) {
      $mode = 1 if CGI::param('refreshldap') eq 'on';
      $mode = 2 if CGI::param('refreshldap') eq 'force';
    }
  }

  if ($this->{maxCacheAge} > 0) { # is cache expiration enabled
    # compute age of data
    my $cacheAge = 9999999999;
    my $now = time();
    my $lastUpdate = $this->{data}{LASTUPDATED} || 0;
    $cacheAge = $now - $lastUpdate if $lastUpdate;

    # don't refresh within 60 seconds
    if ($cacheAge < 10) {
      $mode = 0;
      $this->writeDebug("suppressing cache refresh within 10 seconds");
    } else {
      $mode = 1 if $cacheAge > $this->{maxCacheAge}
    }

    $this->writeDebug("cacheAge=$cacheAge, maxCacheAge=$this->{maxCacheAge}, LASTUPDATED=$lastUpdate, refresh=$mode");
  }

  return $mode;
}

=pod

---++++ refreshCache() -> $boolean

download all relevant records from the LDAP server and
store it into a database.

=cut

sub refreshCache {
  my ($this, $mode) = @_;

  return if !$this->{preCache} && !$this->{preserveTWikiUserMapping};

  $this->{_refreshMode} = $mode;
  $this->{_preserveTWikiUserMapping} = ($this->{preserveTWikiUserMapping} && $mode == 2 ? 1 : 0);
  
  $this->writeDebug(sprintf "called refreshCache with mode %s, preserveTWikiUserMapping: %s", $this->{_refreshMode}, $this->{_preserveTWikiUserMapping});

  # precache the LDAP directory if enabled in configuration file
  # $this->writeDebug("Config:" . $this->{preCache});
  my $data = $this->{data};

  my $isOk = 1;
  if ($this->{preCache} || $this->{_preserveTWikiUserMapping}) {

    # do we want to backup the cache file before it's overwritten?
    my $cacheBackupFile;
    $cacheBackupFile = $this->backupCacheFile() if $this->{backupCacheFile};

    my $tie = $this->{locking}{mode};

    # Set the CACHEREFRESHPROCESS flag, return early if it's already set.
    $this->getCacheTie('read');
    my $cacheRefreshProcess = $this->{data}->{'CACHEREFRESHPROCESS'};
    unless (defined $cacheRefreshProcess) {
      $this->getCacheTie('write');
      $cacheRefreshProcess = $this->{data}->{'CACHEREFRESHPROCESS'};
      unless (defined $cacheRefreshProcess) {
        $this->{data}->{'CACHEREFRESHPROCESS'} = $$;
        $this->{cacheRefreshProcess} = $$;
      }
      $this->untieCache() unless $tie eq 'write';
    }
      
    if (defined $cacheRefreshProcess) {
      $this->writeWarning("Can not refresh cache now. It is already being refreshed by process $cacheRefreshProcess!");
      $isOk = 0;
    }

    if ($isOk) {
      $this->getCacheTie('read');

      my $tempData = {};
      $isOk = $this->refreshUsersCache($tempData);
      if ($isOk) {
        $this->getCacheTie('write');
        $data = $this->{data};

        # if this is a force refresh, we wipe the database for email <-> login, wikiname <-> login and 
        # dn <-> login mappings, as well as the values for created and updated
        if ($mode == 2) {
          delete $data->{'WIKINAMES'};
          delete $data->{'LOGINNAMES'};
          while (my ($key, $val) = each %$data) {
            delete $data->{$key} if $key =~ m/\A(?:EMAIL2U|U2EMAIL|W2U|U2W|U2CREATED|U2UPDATED)/o;
          }
        }

        $data->{LASTUPDATED} = time();

        $this->untieCache() unless $tie eq 'write';

        # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
        $this->getCacheTie($tie) if $tie eq 'read';
      }

      if ($isOk && $this->{mapGroups}) {
        $this->getCacheTie('read');

        my %tempDataBackup = %$tempData;
        my $groupsOk = $this->refreshGroupsCache($tempData);

        if ($groupsOk) {
          $this->getCacheTie('write');
          $data = $this->{data};

          # wipe the database for group <-> member mappings and group <-> dn mappings
          if (defined $data->{'GROUPS'}) {
            for my $groupName (split /\s*,\s*/, $data->{'GROUPS'}) {
              delete $data->{"GROUPS::$groupName"}; # Destroy Group entry
            }
            delete $data->{'GROUPS'}; # Destroy Groups list
          }
    
          # wipe the database for orphan groups
          for my $key (keys %$data) {
            if ($key =~ m/\AGROUPS::(.+)/) {
              my $groupName = $1;
              delete $data->{"GROUPS::$1"}; # Destroy Group entry
            }
          }

          $data->{LASTUPDATED} = time();

          $this->untieCache() unless $tie eq 'write';
          $this->getCacheTie($tie) if $tie eq 'read';
        } else {
          # Rollback
          %$tempData = %tempDataBackup;
        }
      }

      if ($isOk) {

        $this->getCacheTie('write');

        # Make sure to remove the DN of all users and groups,
        # so that users/groups removed from LDAP get their DN removed from the cache.
        while (my ($key, $val) = each %$data) {
          delete $data->{$key} if $key =~ m/\A(?:DN2U|U2DN)/o;
        }

        # Copy (Override if exists) changes from $tempData into cache
        while ( my ($key, $val) = each %$tempData) { 
            $data->{$key} = $val;
        }

        $this->refreshIgnoredCache();

        $this->untieCache() unless $tie eq 'write';
        $this->getCacheTie($tie) if $tie eq 'read';
      }

    }
    
    unless ($isOk) {
      unlink $cacheBackupFile if defined $cacheBackupFile; 
    }

  }

  undef $this->{_refreshMode};

  return $isOk;
}

=pod

---++++ refreshUsersCache($tempData) -> $boolean

download all user records from the LDAP server and cache those who
do not exist in the local cache into the given hash reference

returns true if new records have been loaded

=cut

sub refreshUsersCache {
  my ($this, $tempData) = @_;

  $this->writeDebug("called refreshUsersCache()");
  
  unless ($tempData) {
    $this->writeWarning("error refreshing the user cache: hash ref not given.");
    return 0;
  }
  
  my $tie = $this->{locking}{mode};
  if ($tie eq 'none') {
    $this->writeWarning("error refreshing the user cache: existing cache not tied.");
    return 0;
  }
  my $data = $this->{data};

  my $TWikiUserMapping;
  if ($this->{_preserveTWikiUserMapping}) {
    # Are we migrating from TWiki::Users::TWikiUserMapping to TWiki::Users:LdapUserMapping?
    # If so, and the cache is either empty or refreshMode is set to 'force', we take the mappings from {UsersWebName}.{UsersTopicName} into consideration.

    # Getting the existing user mappings
    my $mapping = $this->getTWikiUserMapping();
    if (%$mapping) {
      $TWikiUserMapping->{'existing_users'} = $mapping;
      $TWikiUserMapping->{'deleted_from_ldap'} = dclone($mapping) if $this->{preserveWikiNames}; # To be shortened (if set)
      # To be filled: $TWikiUserMapping->{'present_in_ldap'} 
    } else {
      $this->{_preserveTWikiUserMapping} = 0;
    }
  }

  my $nrRecords = 0;
  my (%wikiNames, %loginNames);
  if (defined $data->{WIKINAMES} && defined $data->{LOGINNAMES}) {
    %wikiNames = map { $_ => 1 } split(/\s*,\s*/, $data->{WIKINAMES});
    %loginNames = map { $_ => 1 } split(/\s*,\s*/, $data->{LOGINNAMES});
  }

  # start reading pages    
  my @newUsersFromLdap; # if $preserveTWikiUserMappping
  for my $userBase (@{$this->{userBase}}) {

    # prepare search
    my @args = (
      filter => $this->{loginFilter}, 
      base   => $userBase,
      scope  => $this->{userScope},
      attrs  =>[
        $this->{loginAttribute},
        $this->{mailAttribute},
        $this->{primaryGroupAttribute},
        @{$this->{wikiNameAttributes}}
      ]
    );

    # use the control LDAP extension only if a valid pageSize value has been provided
    my $page;
    my $cookie;
    if ($this->{pageSize} > 0) {
      require Net::LDAP::Control::Paged;
      $page = Net::LDAP::Control::Paged->new(size => $this->{pageSize});
      push(@args, control => [$page]);
    } else {
      $this->writeDebug("reading users from cache in one chunk");
    }

    my $gotError = 0;

    while (1) {
      # perform search
      my $mesg = $this->search(@args);
      unless ($mesg) {
        $this->writeDebug("oops, no result querying for users");
        $this->writeWarning("error refreshing the user cache: ".$this->getError());
        $gotError = 1;
        last;
      }

      # process each entry on a page
      #
      # If we're migrating from TWiki::Users::TWikiUserMapping to TWiki::Users::LdapUserMapping,
      # first we have to split the users into three lists; deleted, present, and new users, which will
      # have to be cached in that order.
      # 
      # If not, we cache them directly here
      while (my $entry = $mesg->pop_entry()) {
        my $loginName = $entry->get_value($this->{loginAttribute});
        unless ($this->isValidCacheLoginName($loginName)) {
          $this->writeDebug("$loginName is not a valid LoginName, continuing to next LDAP entry");
          next;
        }

        # see refreshIgnoredCache
        $this->{_allFetchedLogins}{$loginName} = 1 if $this->{preCache} eq 'existing';

        my $prevWikiName = $this->getWikiNameOfLogin($loginName, 0);
        unless ($this->{_preserveTWikiUserMapping}) {
          next if !$prevWikiName && $this->{preCache} eq 'existing';
          $this->cacheUserFromEntry($entry, $tempData, \%wikiNames, \%loginNames, $prevWikiName) && $nrRecords++;
        } else {
          if (defined $TWikiUserMapping->{'existing_users'}{$loginName}) {
            push @{$TWikiUserMapping->{'present_in_ldap'}}, $entry;
            delete $TWikiUserMapping->{'deleted_from_ldap'}{$loginName};
          } else {
            push @newUsersFromLdap, $entry if $this->{preCache} eq 'all';
          }
        }
      } 

      # only use cookies and pages if we are using this extension
      if ($page) {
        # get cookie from paged control to remember the offset
        my ($resp) = $mesg->control(LDAP_CONTROL_PAGED) or last;

        $cookie = $resp->cookie or last;
        if ($cookie) {
          # set cookie in paged control
          $page->cookie($cookie);
        } else {
          # found all
          $this->writeDebug("ok, no more cookie");
          last;
        }
      }
    }

    # clean up
    if ($cookie) {
      $page->cookie($cookie);
      $page->size(0);
      $this->search(@args);
    }

    # check for error
    if ($gotError) {
      $this->writeWarning('Got an error in refreshUsersCache, aborting ....');
      return 0;
    }

  } # end reading pages
  $this->writeDebug("done reading pages");

  # Are we migrating from TWiki::Users::TWikiUserMapping to TWiki::Users::LdapUserMapping?
  # If so, this is the point where we actually put the users in cache.
  if ($this->{_preserveTWikiUserMapping}) {

    if ($this->{preserveWikiNames} && keys(%{$TWikiUserMapping->{'deleted_from_ldap'}}) > 0) {
      for my $loginName (keys %{$TWikiUserMapping->{'deleted_from_ldap'}}) {
        my $info = $TWikiUserMapping->{'deleted_from_ldap'}{$loginName};
          
        if (defined $info->{wikiName}) {
          my $wikiName = $info->{wikiName};
              
          $tempData->{"W2U::$wikiName"} = $loginName;
          $tempData->{"U2W::$loginName"} = $wikiName;
  
          if (defined $info->{date}) {
            $tempData->{"U2CREATED::$loginName"} = $info->{date};
          }
      
          $wikiNames{$wikiName} = $loginName;
          $loginNames{$loginName} = $wikiName;
          $nrRecords++;
        }
      }
    }

    if (defined $TWikiUserMapping->{'present_in_ldap'}) {
      for my $entry (@{$TWikiUserMapping->{'present_in_ldap'}}) {
        my $loginName = $entry->get_value($this->{loginAttribute});
        my $wikiName = $TWikiUserMapping->{'existing_users'}{$loginName}{'wikiName'}; 
        my $date;
        if (defined $TWikiUserMapping->{'existing_users'}{$loginName}{'date'}) {
          $date = $TWikiUserMapping->{'existing_users'}{$loginName}{'date'};
        }
        $this->cacheUserFromEntry($entry, $tempData, \%wikiNames, \%loginNames, $wikiName, $date) && $nrRecords++;
      }
    }

    if (@newUsersFromLdap) {
      for my $entry (@newUsersFromLdap) {
        my $wikiName = undef; # To be calculated by $entry
        $this->cacheUserFromEntry($entry, $tempData, \%wikiNames, \%loginNames, $wikiName) && $nrRecords++;
      }
    }
  }

  # resolving WikiName clashes
  $nrRecords += $this->resolveWikiNameClashes($tempData, \%wikiNames, \%loginNames);

  # remember list of all user names
  $tempData->{WIKINAMES} = join(',', sort keys %wikiNames);
  $tempData->{LOGINNAMES} = join(',', sort keys %loginNames);

  $this->writeDebug("$nrRecords users refreshed or added.");

  return 1;
}

=pod

---++++ resolveWikiNameClashes($tempData, %wikiNames, %loginNames) -> $integer

If there have been name clashes during cacheUserFromEntry() those entry records
have not yet been added to the cache. They are kept until all clashes have been
found and a deterministic renaming scheme can be applied. Clashed WikiNames will be
enumerated - !WikiName1, !WikiName2, !WikiName3 etc - and finally added to the database.

The renaming is kept stable by sorting the dn entry of all clashed entries.

returns the number of additional entries that have been cached

=cut

sub resolveWikiNameClashes {
  my ($this, $data, $wikiNames, $loginNames) = @_;

  return 0 unless $this->{_wikiNameClashes};
  unless ($data) {
    $this->writeWarning('hash ref not given in resolveWikiNameClashes!');
    return 0;
  }

  my $clashes = scalar(keys(%{$this->{_wikiNameClashes}}));
  $this->writeDebug("called resolveWikiNameClashes($clashes clashes)"); 

  my %suffixes = ();
  my $nrRecords = 0;
  my $refreshMode = $this->{_refreshMode} || 0;

  foreach my $item (sort { $a->{loginName} cmp $b->{loginName} } values %{ $this->{_wikiNameClashes} }) {

    my $wikiName = $item->{wikiName};
    my $loginName = $item->{loginName};

    my $prevWikiName = ($refreshMode < 2 ? $this->getWikiNameOfLogin($loginName, 0) : undef);
    my $newWikiName;
    my $prevLogin;

    if ($prevWikiName) {
      $this->writeDebug("found prevWikiName=$prevWikiName for $loginName");
      $newWikiName = $prevWikiName;
    } else {
      # search for a new wikiname 
      do {
        $suffixes{$wikiName}++;
        $newWikiName = $wikiName.$suffixes{$wikiName};

        $prevLogin = $this->getLoginOfWikiName($newWikiName);
      } until (!$prevLogin || $prevLogin eq $loginName);
    }

    $this->writeDebug("processing clash of loginName=$loginName on wikiName=$wikiName, dn=$item->{dn}, resolves to newWikiName=$newWikiName");

    $this->cacheUserFromEntry($item->{entry}, $data, $wikiNames, $loginNames, $newWikiName) && $nrRecords++;
  }

  delete $this->{_wikiNameClashes};

  return $nrRecords;
}

=pod

---++++ refreshGroupsCache($tempData) -> $boolean

download all group records from the LDAP server

returns true if new records have been loaded

=cut

sub refreshGroupsCache {
  my ($this, $tempData) = @_;
  
  $this->writeDebug("called refreshGroupsCache()");

  unless ($tempData) {
    $this->writeWarning("error refreshing the group cache: a hash ref is not given.");
    return 0;
  }

  my $tie = $this->{locking}{mode};
  if ($tie eq 'none') {
    $this->writeWarning("error refreshing the group cache: existing cache not tied.");
    return 0;
  }
  my $data = $this->{data};
  
  my $nrRecords = 0;
  my %groupNames;
  for my $groupBase (@{$this->{groupBase}}) {
    # prepare search
    my @args = (
      filter => $this->{groupFilter}, 
      base   => $groupBase, 
      scope  => $this->{groupScope},
      deref  => 'always',
      attrs  => [
        $this->{groupAttribute}, 
        $this->{memberAttribute}, 
        $this->{innerGroupAttribute}, 
        $this->{primaryGroupAttribute}
      ],
    );
    
    # use the control LDAP extension only if a valid pageSize value has been provided
    my $page;
    my $cookie;
    if ($this->{pageSize} > 0) {
      require Net::LDAP::Control::Paged;
      $page = Net::LDAP::Control::Paged->new(size => $this->{pageSize});
      push(@args, control => [$page]);
    } else {
      $this->writeDebug("reading group from cache in one chunk");
    }

    # read pages
    my $gotError = 0;
    while (1) {

      # perform search
      my $mesg = $this->search(@args);
      unless ($mesg) {
        # $this->writeDebug("oops, no result querying for groups");
        $this->writeWarning("error refeshing the groups cache: ".$this->getError());
        last;
      }

      # process each entry on a page
      while (my $entry = $mesg->pop_entry()) {
        $this->cacheGroupFromEntry($entry, $tempData, \%groupNames) && $nrRecords++;
      }

      # only use cookies and pages if we are using this extension
      if ($page) {
        # get cookie from paged control to remember the offset
        my ($resp) = $mesg->control(LDAP_CONTROL_PAGED) or last;
        
        $cookie = $resp->cookie or last;
        if ($cookie) {
          # set cookie in paged control
          $page->cookie($cookie);
        } else {
          # found all
          #$this->writeDebug("ok, no more cookie");
          last;
        }
      } else {
        last;
      }
    } # end reading pages

    # clean up
    if ($cookie) {
      $page->cookie($cookie);
      $page->size(0);
      $this->search(@args);
    }

    # check for error
    return 0 if $gotError;
  }
  
  # check for primary group membership
  if ($this->{_primaryGroup}) {
    foreach my $groupId (keys %{$this->{_primaryGroup}}) {
      my $groupName = $this->{_groupId}{$groupId};
      next unless $groupName;
      foreach my $member (keys %{$this->{_primaryGroup}{$groupId}}) {
        $this->writeDebug("adding $member to its primary group $groupName");
        $this->{_groups}{$groupName}{$member} = 1;
      }
    }
  }

  # assert group members to data store 
  foreach my $groupName (keys %{$this->{_groups}}) {
    my %members = ();
    if (defined $this->{_groups}{$groupName}) {
      foreach my $member (keys %{$this->{_groups}{$groupName}}) {

        # groups may store DNs to members instead of a memberUid, in this case we
        # have to lookup the corresponding loginAttribute
        if ($this->{memberIndirection}) {
          # $this->writeDebug("following indirection for $member");
          my $memberName = $tempData->{"DN2U::$member"};
          if ($memberName) {
            $members{$memberName} = 1;
          } else {
            $this->writeWarning("oops, $member not found, but member of $groupName") if $this->{preCache} eq 'all';
          } 
        } else {
          $members{$member} = 1;
        }
      }
    }
    $tempData->{"GROUPS::$groupName"} = join(',', sort keys %members);
    undef $this->{_groups}{$groupName};
  }
  undef $this->{_groups};

  # remember list of all groups
  $tempData->{GROUPS} = join(',', sort keys %groupNames);

  #$this->writeDebug("got $nrRecords keys in cache");

  return 1;
}

=pod

---++++ cacheUserFromEntry($entry, $data, $wikiNames, $loginNames, $wikiName, $date) -> $boolean

store a user LDAP::Entry to our internal cache 

If the $wikiName parameter is given explicitly then this will be the name under which this record
will be cached.

If the $date parameter is given explicitly then this will be the set date for when this user was first
cached.

returns true if new records have been created

=cut

sub cacheUserFromEntry {
  my ($this, $entry, $data, $wikiNames, $loginNames, $wikiName, $date) = @_;

  $data ||= $this->{data};
  unless ($data) {
    $this->writeWarning('$data is not defined in cacheUserFromEntry');
    return 0;
  }

  $wikiNames ||= {};
  $loginNames ||= {};
  my $refreshMode = $this->{_refreshMode} || 0;

  my $dn = $entry->dn();

  # 1. get it
  my $loginName = $entry->get_value($this->{loginAttribute});
  $loginName =~ s/^\s+//o;
  $loginName =~ s/\s+$//o;
  unless ($loginName) {
    $this->writeDebug("no loginName for $dn ... skipping");
    return 0;
  }
  $loginName = $this->fromUtf8($loginName);

  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid loginName");
    return 0;
  }

  # 2. normalize
  $loginName = $this->normalizeLoginName($loginName) if $this->{normalizeLoginName};
  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};

  my $isExplicitWikiName = (defined $wikiName) ? 1 : 0;
  my $prevWikiName = ($refreshMode < 2) ? $this->getWikiNameOfLogin($loginName, 0) : undef;

  if ($isExplicitWikiName) {
    # Are we feeded a wikiName?
    #$this->writeDebug("found explicit wikiName '$wikiName' for $dn");
  } elsif ($prevWikiName) {
    # keep a wikiName once it has been computed
    # $this->writeDebug("found previously stored wikiName '$prevWikiName' for loginName '$loginName'");
    $wikiName = $prevWikiName;
  } else {
    # 1. compute a new wikiName
    my @wikiName = ();
    foreach my $attr (@{$this->{wikiNameAttributes}}) {
      my $value = $entry->get_value($attr);
      next unless $value;
      $value =~ s/^\s+//o;
      $value =~ s/\s+$//o;
      $value = $this->fromUtf8($value);
      #$this->writeDebug("$attr=$value");
      push @wikiName, $value;
    }
    $wikiName = join(" ", @wikiName);

    unless ($wikiName) {
      # $wikiName = $loginName;
      # $this->writeWarning("no WikiNameAttributes found for $dn ... deriving WikiName from LoginName: '$wikiName'");
      $this->writeWarning("no WikiNameAttributes found for $dn ... ignoring");
      return 0;
    }

    # 2. rewrite
    my $oldWikiName = $wikiName;
    foreach my $pattern (keys %{$this->{rewriteWikiNames}}) {
      my $subst = $this->{rewriteWikiNames}{$pattern};
      if ($wikiName =~ /^(?:$pattern)$/) {
        my $arg1 = $1;
        my $arg2 = $2;
        my $arg3 = $3;
        my $arg4 = $4;
        my $arg5 = $5;
        $arg1 = '' unless defined $arg1;
        $arg2 = '' unless defined $arg2;
        $arg3 = '' unless defined $arg3;
        $arg4 = '' unless defined $arg4;
        $subst =~ s/\$1/$arg1/g;
        $subst =~ s/\$2/$arg2/g;
        $subst =~ s/\$3/$arg3/g;
        $subst =~ s/\$4/$arg4/g;
        $subst =~ s/\$5/$arg5/g;
        $wikiName = $subst;
        $this->writeDebug("rewriting '$oldWikiName' to '$wikiName' using rule $pattern"); 
        last;
      }
    }

    # 3. normalize
    if ($this->{normalizeWikiName}) {
      $wikiName = $this->normalizeWikiName($wikiName);
    }

    # 4. aliasing based on WikiName
    my $alias = $this->{wikiNameAliases}{$wikiName};
    if ($alias) {
      $this->writeDebug("using alias $alias for $wikiName");
      $wikiName = $alias;
    }

    # 5. Check if this WikiName is already in use by another login name
    my $prevLoginName = $this->getLoginOfWikiName($wikiName) || '';
    if ($prevLoginName && $prevLoginName ne $loginName) {
      $this->writeDebug("Clash on wikiName '$wikiName' from login $loginName - already in use by $prevLoginName. Renaming later");
      $this->{_wikiNameClashes}{$loginName} = {
        entry => $entry,
        dn => $dn,
        wikiName => $wikiName,
        loginName => $loginName,
      };
      return 0;
    }

    # 6. check for name clashes within this transaction
    my $clashLogin = $this->getLoginOfWikiName($wikiName, $data);
    if (defined $clashLogin) {
      if ($loginName ne $clashLogin) {
        $this->writeDebug("Clash on wikiName '$wikiName' from login $loginName - already in use by $clashLogin. Renaming later");
        $this->{_wikiNameClashes}{$loginName} = {
          entry=>$entry,
          dn=>$dn,
          wikiName=>$wikiName,
          loginName=>$loginName,
        };
      } else {
        # never reach: same dn found twice in same transaction
        $this->writeWarning("$dn found twice in ldap search... ignoring second one");
      }
      return 0;
    }
  }

  $wikiNames->{$wikiName} = $loginName;
  $loginNames->{$loginName} = $wikiName;
  

  # store this users' primary group
  if ($this->{primaryGroupAttribute}) {
    my $groupId = $entry->get_value($this->{primaryGroupAttribute});
    if ($groupId) {
      $groupId = lc($groupId) unless $this->{caseSensitiveGroup};
      $this->{_primaryGroup}{$groupId}{$loginName} = 1;
    }
  }

  my $emails;
  @{$emails} = $entry->get_value($this->{mailAttribute});

  my $isUpdated = 0;
  if ($refreshMode < 2 && defined $prevWikiName) {
    $isUpdated = 1 if defined $this->{data}{"U2W::$loginName"} && $this->{data}{"U2W::$loginName"} ne $wikiName;
    $isUpdated = 1 if defined $this->{data}{"U2EMAIL::$loginName"} && $this->{data}{"U2EMAIL::$loginName"} ne join(',',@$emails);
  }

  # store it
  $data->{"U2W::$loginName"} = $wikiName;
  $data->{"W2U::$wikiName"} = $loginName;
  $data->{"DN2U::$dn"} = $loginName if ($refreshMode == 2 || !defined $data->{"DN2U::$dn"});
  $data->{"U2DN::$loginName"} = $dn if ($refreshMode == 2 || !defined $data->{"U2DN::$loginName"});

  unless ($date) {
    $data->{"U2CREATED::$loginName"} = time() if ($refreshMode == 2 || !defined $this->{data}->{"U2CREATED::$loginName"});
  } else {
    $data->{"U2CREATED::$loginName"} = $date;
  }
  $data->{"U2UPDATED::$loginName"} = time() if $isUpdated;

  # get email addresses
  $data->{"U2EMAIL::$loginName"} = join(',',@$emails);

  # Make sure to preserve any other users belonging to the emails
  # when storing the EMAIL2U key
  if ($emails) {
    foreach my $email (@$emails) {
      $email =~ s/^\s+//o;
      $email =~ s/\s+$//o;

      my $prevMapping = $data->{"EMAIL2U::$email"};
      my %emails = ();

      if ($prevMapping) {
        %emails = map {$_ => 1} split(/\s*,\s*/, $prevMapping);
      }

      $emails{$loginName} = $email;
      $data->{"EMAIL2U::$email"} = join(',', sort keys %emails);
    }
  }

  return $loginName;
}

=pod

---++++ cacheGroupFromEntry($entry, $data, $groupNames) -> $boolean

store a group LDAP::Entry to our internal cache 

returns true if new records have been created

=cut

sub cacheGroupFromEntry {
  my ($this, $entry, $data, $groupNames) = @_;

  $data ||= $this->{data};
  unless ($data) {
    $this->writeWarning('$data is not defined in cacheGroupFromEntry');
    return 0;
  }
  $groupNames ||= {};

  my $dn = $entry->dn();
  $this->writeDebug("caching group for $dn");

  my $groupName = $entry->get_value($this->{groupAttribute});
  unless ($groupName) {
    $this->writeDebug("no groupName for $dn ... skipping");
    return 0;
  }
  unless ($this->isValidCacheGroupName($groupName)) {
    $this->writeDebug("$groupName is not a valid groupName");
    return 0;
  }

  $groupName =~ s/^\s+//o;
  $groupName =~ s/\s+$//o;
  $groupName = $this->fromUtf8($groupName);

  $groupName = $this->normalizeWikiName($groupName) if $this->{normalizeGroupName};
  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  # check for a rewrite rule
  my $foundRewriteRule = 0;
  my $oldGroupName = $groupName;
  foreach my $pattern (keys %{$this->{rewriteGroups}}) {
    my $subst = $this->{rewriteGroups}{$pattern};
    if ($groupName =~ /^(?:$pattern)$/) {
      my $arg1 = $1;
      my $arg2 = $2;
      my $arg3 = $3;
      my $arg4 = $4;
      my $arg5 = $5;
      $arg1 = '' unless defined $arg1;
      $arg2 = '' unless defined $arg2;
      $arg3 = '' unless defined $arg3;
      $arg4 = '' unless defined $arg4;
      $subst =~ s/\$1/$arg1/g;
      $subst =~ s/\$2/$arg2/g;
      $subst =~ s/\$3/$arg3/g;
      $subst =~ s/\$4/$arg4/g;
      $subst =~ s/\$5/$arg5/g;
      $groupName = $subst;
      $foundRewriteRule = 1;
      $this->writeDebug("rewriting '$oldGroupName' to '$groupName' using rule $pattern"); 
      last;
    }
  }

  if (!$this->{mergeGroups} && defined($groupNames->{$groupName})) {
    $this->writeWarning("$dn clashes with group $groupNames->{$groupName} on $groupName");
    return 0;
  }

  # Check if a LoginName or WikiName equals this group name, if so, we alter the group name a little.
  my $loginName = $this->{caseSensitiveLogin} ? $groupName : lc($groupName);
  if (defined $this->{data}{"U2W::$loginName"} || defined $this->{data}{"W2U::$groupName"}) {
    my $suffix;
    if ($this->{normalizeGroupName}) {
      $suffix = 'Group';
    } else {
      $suffix = '_group';
    }
    $this->writeWarning("group $groupName already in use as a LoginName or WikiName. Appending $suffix.");
    $groupName .= $suffix;
  }

  # remember this
  $data->{"DN2U::$dn"} = $groupName;
  $data->{"U2DN::$groupName"} = $dn;

  # cache groupIds
  my $groupId = $entry->get_value($this->{primaryGroupAttribute});
  if ($groupId) {
    $groupId = lc( $groupId ) unless $this->{caseSensitiveGroup};
    $this->{_groupId}{$groupId} = $groupName;
  }

  # fetch all members of this group
  $this->{_groups}{$groupName}= undef; # In case there are no LDAP members
  
  my $memberLists = $entry->get_value($this->{memberAttribute}, alloptions => 1);
  if (defined $memberLists) {
    for my $members ( values %$memberLists ) {
      for my $member ( @$members) {
        next unless $member;
        $member =~ s/^\s+//o;
        $member =~ s/\s+$//o;
        $this->{_groups}{$groupName}{$member} = 1; # delay til all groups have been fetched
      } 
    }
  }
  
  # fetch all inner groups of this group
  foreach my $innerGroup ($entry->get_value($this->{innerGroupAttribute})) {
    next unless $innerGroup;
    $innerGroup =~ s/^\s+//o;
    $innerGroup =~ s/\s+$//o;
    $this->{_groups}{$groupName}{$innerGroup} = 1; # delay til all groups have been fetched
  }
  
  # store it
  $this->writeDebug("adding groupName='$groupName', dn=$dn");

  $groupNames->{$groupName} = 1;

  return $groupName;
}

=pod

---++++ refreshIgnoredCache() -> true

Looks through the ignored/unknown users and groups,
and sees if any entries can be added or removed. 

Returns true if any changes has been done

=cut

sub refreshIgnoredCache {
  my ($this) = @_;

  $this->writeDebug("called refreshIgnoredCache()");

  my $listsAltered = 0;

  # Clean the unknown users list
  #
  # Remove all valid users from the unkown users list
  # If we have just performed a cache refresh with PreCache set to 'existing',
  # It means that there could be valid users stored in LDAP, but not in cache. 
  # These users are then stored in $this->{_allFetchedLogins}. If this isn't 
  # set, we use the users we already have in the cache
  my %ignoredUsers = map {$_ => 1} @{$this->getAllIgnoredUsers()};
  my %logins;
  if (defined $this->{_allFetchedLogins}) {
    %logins = %{$this->{_allFetchedLogins}};
  } else {
    %logins = map {$_ => 1} @{$this->getAllLoginNames()};
  }
  my @invalidIgnoredUsers;
  for my $ignoredUser (keys %ignoredUsers) {
    push @invalidIgnoredUsers, $ignoredUser if defined $logins{$ignoredUser};
  }
  if (@invalidIgnoredUsers) {
    $listsAltered = 1;
    if ($this->{debug}) {
      $this->writeDebug("Removing '$_' from unknown/ignored users list") for @invalidIgnoredUsers;
    }
    $this->removeIgnoredUsers(@invalidIgnoredUsers);
  }

  # Clean the unknown groups list
  #  - Remove entries which is valid users or WikiNames
  #  - Remove entries which is valid groups
  if ($this->{mapGroups}) {
    my %ignoredGroups = map {$_ => 1} @{$this->getAllIgnoredGroups()};          
    my %cachedLogins = map {$_ => 1} @{$this->getAllLoginNames()};

    my @deleteFromIgnoredGroups;

    # Remove entries which is valid users from the unknown groups list
    # A group lookup is aborted if the group exists as a user
    for my $cachedLogin (keys %cachedLogins) {
      push @deleteFromIgnoredGroups, $cachedLogin if defined $ignoredGroups{$cachedLogin};
    }

    # Remove entries which is valid WikiNames from the unkown groups list
    # A group lookup is aborted if the group exists as a WikiName
    my @cachedWikiNames = @{$this->getAllWikiNames()};
    for my $cachedWikiName (@cachedWikiNames) {
      push @deleteFromIgnoredGroups, $cachedWikiName if defined $ignoredGroups{$cachedWikiName};
    }
  
    # Remove entries which is valid groups from the unkown groups list
    my @groups = @{$this->getGroupNames()};
    for my $group (@groups) {
      push @deleteFromIgnoredGroups, $group if defined $ignoredGroups{$group};
    }
  
    #if (@deleteFromIgnoredGroups || @addToIgnoredGroups) {
    if (@deleteFromIgnoredGroups) {
      $listsAltered = 1;
    }

    for (@deleteFromIgnoredGroups) {
      delete $ignoredGroups{$_};
      $this->writeDebug("Removing '$_' from unknown/ignored groups list");
    }

    # Save the new unkown groups list
    my $tie = $this->{locking}{mode};
    $this->getCacheTie('write');
    my $data = $this->{data};
    $data->{UNKWNGROUPS} = join(',', sort keys %ignoredGroups); 
    $this->untieCache() unless $tie eq 'write';
    $this->getCacheTie($tie) if $tie eq 'read';
  }
  
  return 1;
}

=pod 

---++++ normalizeWikiName($name) -> $string

normalizes a string to form a proper <nop>WikiName

=cut

sub normalizeWikiName {
  my ($this, $name) = @_;

  $name = transliterate($name);

  my $wikiName = '';

  # first, try without forcing each part to be lowercase 
  foreach my $part (split(/[^$TWiki::regex{mixedAlphaNum}]/, $name)) {
    $wikiName .= ucfirst($part);
  }

  # if it isn't a valid WikiWord and there's no homepage of that name yet, then try more agressively to 
  # create a proper WikiName
  if (!TWiki::Func::isValidWikiWord($wikiName) && !TWiki::Func::topicExists($TWiki::cfg{UsersWebName}, $wikiName)) {
    $wikiName = '';
    foreach my $part (split(/[^$TWiki::regex{mixedAlphaNum}]/, $name)) {
      $wikiName .= ucfirst(lc($part));
    }
  }

  return $wikiName;
}

=pod 

---++++ normalizeLoginName($name) -> $string

normalizes a string to form a proper login

=cut

sub normalizeLoginName {
  my ($this, $name) = @_;

  $name =~ s/@.*$//o; # remove REALM

  $name = transliterate($name);
  $name =~ s/[^$TWiki::cfg{LoginNameFilterIn}]//;

  return $name;
}

=pod

---++++ transliterate($string) -> $string

transliterate some essential utf8 chars to a common replacement
in latin1 encoding. the list above is not exhaustive.

use http://www.ltg.ed.ac.uk/~richard/utf-8.html to add more recodings

=cut

sub transliterate {
  my $string = shift;

  if ($TWiki::cfg{Site}{CharSet} =~ /^utf-?8$/i) {
    $string =~ s/\xc3\xa0/a/go; # a grave
    $string =~ s/\xc3\xa1/a/go; # a acute
    $string =~ s/\xc3\xa2/a/go; # a circumflex
    $string =~ s/\xc3\xa3/a/go; # a tilde
    $string =~ s/\xc3\xa4/ae/go; # a uml
    $string =~ s/\xc3\xa5/a/go; # a ring above
    $string =~ s/\xc3\xa6/ae/go; # ae 
    $string =~ s/\xc4\x85/a/go; # a ogonek

    $string =~ s/\xc3\x80/A/go; # A grave
    $string =~ s/\xc3\x81/A/go; # A acute
    $string =~ s/\xc3\x82/A/go; # A circumflex
    $string =~ s/\xc3\x83/A/go; # A tilde
    $string =~ s/\xc3\x84/Ae/go; # A uml
    $string =~ s/\xc3\x85/A/go; # A ring above
    $string =~ s/\xc3\x86/AE/go; # AE
    $string =~ s/\xc4\x84/A/go; # A ogonek


    $string =~ s/\xc3\xa7/c/go; # c cedille 
    $string =~ s/\xc4\x87/c/go; # c acute
    $string =~ s/\xc3\x87/C/go; # C cedille 
    $string =~ s/\xc4\x86/C/go; # C acute

    $string =~ s/\xc3\xa8/e/go; # e grave
    $string =~ s/\xc3\xa9/e/go; # e acute
    $string =~ s/\xc3\xaa/e/go; # e circumflex
    $string =~ s/\xc3\xab/e/go; # e uml

    $string =~ s/\xc4\x99/e/go; # e ogonek
    $string =~ s/\xc4\x98/E/go; # E ogonek

    $string =~ s/\xc3\xb2/o/go; # o grave
    $string =~ s/\xc3\xb3/o/go; # o acute
    $string =~ s/\xc3\xb4/o/go; # o circumflex
    $string =~ s/\xc3\xb5/o/go; # o tilde
    $string =~ s/\xc3\xb6/oe/go; # o uml
    $string =~ s/\xc3\xb8/o/go; # o stroke

    $string =~ s/\xc3\xb3/o/go; # o acute
    $string =~ s/\xc3\x93/O/go; # O acute

    $string =~ s/\xc3\x92/O/go; # O grave
    $string =~ s/\xc3\x93/O/go; # O acute
    $string =~ s/\xc3\x94/O/go; # O circumflex
    $string =~ s/\xc3\x95/O/go; # O tilde
    $string =~ s/\xc3\x96/Oe/go; # O uml

    $string =~ s/\xc3\xb9/u/go; # u grave
    $string =~ s/\xc3\xba/u/go; # u acute
    $string =~ s/\xc3\xbb/u/go; # u circumflex
    $string =~ s/\xc3\xbc/ue/go; # u uml

    $string =~ s/\xc3\x99/U/go; # U grave
    $string =~ s/\xc3\x9a/U/go; # U acute
    $string =~ s/\xc3\x9b/U/go; # U circumflex
    $string =~ s/\xc3\x9c/Ue/go; # U uml

    $string =~ s/\xc3\x9f/ss/go; # sharp s
    $string =~ s/\xc5\x9b/s/go; # s acute
    $string =~ s/\xc5\x9a/S/go; # S acute

    $string =~ s/\xc3\xb1/n/go; # n tilde
    $string =~ s/\xc5\x84/n/go; # n acute
    $string =~ s/\xc5\x83/N/go; # N acute

    $string =~ s/\xc3\xbe/y/go; # y acute
    $string =~ s/\xc3\xbf/y/go; # y uml

    $string =~ s/\xc3\xac/i/go; # i grave
    $string =~ s/\xc3\xab/i/go; # i acute
    $string =~ s/\xc3\xac/i/go; # i circumflex
    $string =~ s/\xc3\xad/i/go; # i uml

    $string =~ s/\xc5\x82/l/go; # l stroke
    $string =~ s/\xc5\x81/L/go; # L stroke

    $string =~ s/\xc5\xba/z/go; # z acute
    $string =~ s/\xc5\xb9/Z/go; # Z acute
    $string =~ s/\xc5\xbc/z/go; # z dot
    $string =~ s/\xc5\xbb/Z/go; # Z dot

    #OptimIT addons - Croatian specific
    $string =~ s/\xc5\xa1/s/go; # s caron
    $string =~ s/\xc5\xa0/S/go; # S caron
    $string =~ s/\xc4\x91/d/go; # d stroke
    $string =~ s/\xc4\x90/D/go; # D stroke
    $string =~ s/\xc4\x8d/c/go; # c caron
    $string =~ s/\xc4\x8c/C/go; # C caron
    $string =~ s/\xc4\x87/c/go; # c acute
    $string =~ s/\xc4\x86/C/go; # C acute
    $string =~ s/\xc5\xbe/z/go; # z caron
    $string =~ s/\xc5\xbd/Z/go; # Z caron

  } else {
    $string =~ s/\xe0/a/go; # a grave
    $string =~ s/\xe1/a/go; # a acute
    $string =~ s/\xe2/a/go; # a circumflex
    $string =~ s/\xe3/a/go; # a tilde
    $string =~ s/\xe4/ae/go; # a uml
    $string =~ s/\xe5/a/go; # a ring above
    $string =~ s/\xe6/ae/go; # ae
    $string =~ s/\x01\x05/a/go; # a ogonek

    $string =~ s/\xc0/A/go; # A grave
    $string =~ s/\xc1/A/go; # A acute
    $string =~ s/\xc2/A/go; # A circumflex
    $string =~ s/\xc3/A/go; # A tilde
    $string =~ s/\xc4/Ae/go; # A uml
    $string =~ s/\xc5/A/go; # A ring above
    $string =~ s/\xc6/AE/go; # AE
    $string =~ s/\x01\x04/A/go; # A ogonek


    $string =~ s/\xe7/c/go; # c cedille
    $string =~ s/\x01\x07/C/go; # c acute
    $string =~ s/\xc7/C/go; # C cedille
    $string =~ s/\x01\x06/c/go; # C acute

    $string =~ s/\xe8/e/go; # e grave
    $string =~ s/\xe9/e/go; # e acute
    $string =~ s/\xea/e/go; # e circumflex
    $string =~ s/\xeb/e/go; # e uml
    $string =~ s/\x01\x19/e/go; # e ogonek
    $string =~ s/\xc4\x18/E/go; # E ogonek

    $string =~ s/\xf2/o/go; # o grave
    $string =~ s/\xf3/o/go; # o acute
    $string =~ s/\xf4/o/go; # o circumflex
    $string =~ s/\xf5/o/go; # o tilde
    $string =~ s/\xf6/oe/go; # o uml
    $string =~ s/\xf8/oe/go; # o stroke

    $string =~ s/\xd3/o/go; # o acute
    $string =~ s/\xf3/O/go; # O acute

    $string =~ s/\xd2/O/go; # O grave
    $string =~ s/\xd3/O/go; # O acute
    $string =~ s/\xd4/O/go; # O circumflex
    $string =~ s/\xd5/O/go; # O tilde
    $string =~ s/\xd6/Oe/go; # O uml

    $string =~ s/\xf9/u/go; # u grave
    $string =~ s/\xfa/u/go; # u acute
    $string =~ s/\xfb/u/go; # u circumflex
    $string =~ s/\xfc/ue/go; # u uml

    $string =~ s/\xd9/U/go; # U grave
    $string =~ s/\xda/U/go; # U acute
    $string =~ s/\xdb/U/go; # U circumflex
    $string =~ s/\xdc/Ue/go; # U uml

    $string =~ s/\xdf/ss/go; # sharp s
    $string =~ s/\x01\x5b/s/go; # s acute
    $string =~ s/\x01\x5a/S/go; # S acute

    $string =~ s/\xf1/n/go; # n tilde
    $string =~ s/\x01\x44/n/go; # n acute
    $string =~ s/\x01\x43/N/go; # N acute

    $string =~ s/\xfe/y/go; # y acute
    $string =~ s/\xff/y/go; # y uml

    $string =~ s/\xec/i/go; # i grave
    $string =~ s/\xed/i/go; # i acute
    $string =~ s/\xee/i/go; # i circumflex
    $string =~ s/\xef/i/go; # i uml

    $string =~ s/\x01\x42/l/go; # l stroke
    $string =~ s/\x01\x41/L/go; # L stroke

    $string =~ s/\x01\x7a/z/go; # z acute
    $string =~ s/\x01\x79/Z/go; # Z acute
    $string =~ s/\x01\x7c/z/go; # z dot
    $string =~ s/\x01\x7b/Z/go; # Z dot


    #OptimIT addons - Croatian specific
    $string =~ s/\x01\x61/s/go; # s caron
    $string =~ s/\x01\x60/S/go; # S caron
    $string =~ s/\x01\x11/d/go; # d stroke
    $string =~ s/\x01\x10/D/go; # D stroke
    $string =~ s/\x01\x0d/c/go; # c caron
    $string =~ s/\x01\x0c/C/go; # C caron
    $string =~ s/\x01\x07/c/go; # c acute
    $string =~ s/\x01\x06/C/go; # C acute
    $string =~ s/\x01\x7e/z/go; # z caron
    $string =~ s/\x01\x7d/Z/go; # Z caron
  }

  return $string;
}


=pod

---++++ getGroupNames() -> @array

Returns a list of known group names.

=cut

sub getGroupNames {
  my ($this) = @_;

  $this->writeDebug("called getGroupNames()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $groupNames = TWiki::Sandbox::untaintUnchecked($data->{GROUPS}) || '';

  $this->untieCache() if $tie eq 'none';

  my @groupNames = sort(split /\s*,\s*/, $groupNames);
  return \@groupNames;
}

=pod

---++++ isGroup($group) -> $boolean

check if a given name is a ldap group

=cut

sub isGroup {
  my ($this, $group) = @_;
 
  $this->writeDebug("called isGroup($group)");
  unless ($this->isValidCacheGroupName($group)) {
    $this->writeDebug("$group is not a valid groupName");
    return undef;
  }
 
  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $status;

  my $isWikiName = defined $data->{"W2U::$group"};

  my $loginName = ($this->{caseSensitiveLogin} ? $group : lc($group));
  my $isUser = defined $data->{"U2W::$loginName"};

  $group = lc( $group ) unless $this->{caseSensitiveGroup};
  my $isGroup = defined $data->{"GROUPS::$group"};

  if ($isGroup && ($isUser || $isWikiName)) {
    $this->writeWarning("$group is both a Group and a LoginName and/or WikiName!");
  }

  if ($isGroup) {
    $status = 1;
  } elsif ($isUser || $isWikiName) {
    $status = 0;
  } else {
    $this->checkCacheForGroupName($group);
    $status = 1 if defined $data->{"GROUPS::$group"};
  }

  $this->untieCache() if $tie eq 'none';
  
  return $status;
}

=pod

---++++ getEmails($login, $checkCache) -> @emails

fetch emails from LDAP

=cut

sub getEmails {
  my ($this, $login, $checkCache) = @_;

  $this->writeDebug("called getEmails($login)");

  unless ($this->isValidCacheLoginName($login)) {
    $this->writeDebug("$login is not a valid loginName");
    return [];
  }

  $checkCache = 1 unless defined $checkCache;

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  $login = lc($login) unless $this->{caseSensitiveLogin};

  my $emails = TWiki::Sandbox::untaintUnchecked($data->{ "U2EMAIL::" . $login }) || '';

  $this->untieCache() if $tie eq 'none';

  my @emails = split(/\s*,\s*/, $emails);
  return \@emails;
}

=pod

---++++ getLoginOfEmail($email) \@users

get all users matching a given email address

=cut

sub getLoginOfEmail {
  my ($this, $email) = @_;

  $this->writeDebug("called getLoginOfEmail($email)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};
  
  my $loginNames = TWiki::Sandbox::untaintUnchecked($data->{"EMAIL2U::".$email}) || '';

  $this->untieCache() if $tie eq 'none';

  my @loginNames = split(/\s*,\s*/,$loginNames);
  return \@loginNames;
  
}

=pod

---++++ getGroupMembers($groupName) -> \@array

=cut

sub getGroupMembers {
  my ($this, $groupName) = @_;

  return undef unless $groupName;
  
  # $this->writeDebug("called getGroupMembers($groupName)");
  unless ($this->isValidCacheGroupName($groupName)) {
    $this->writeDebug("$groupName is not a valid groupName");
    return undef;
  }
    
  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  my $tie = $this->{locking}{mode};
  
  $this->getCacheTie('read');
  my $data = $this->{data};

  unless ($this->{preCache}) {
    # Make sure that the group is in the cache. This will cause the addition of the group to the cache if it exists in LDAP
    unless ($this->isGroup($groupName)) {
      $this->untieCache() if $tie eq 'none';
      return undef;
    }
  }

  my $members = TWiki::Sandbox::untaintUnchecked($data->{"GROUPS::$groupName"}) || '';
 
  $this->untieCache() if $tie eq 'none';

  my @members = split(/\s*,\s*/, $members);

  return \@members;
}

=pod

---++++ isGroupMember($loginName, $groupName) -> $boolean

check if a given user is member of an ldap group

=cut

sub isGroupMember {
  my ($this, $loginName, $groupName) = @_;

  $this->writeDebug("called isGroupMember($loginName,$groupName)");

  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid loginName");
    return 0;
  }

  unless ($this->isValidCacheGroupName($groupName)) {
    $this->writeDebug("$groupName is not a valid groupName");
    return 0;
  }
  
  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  unless ($this->{preCache}) {
    my $status;

    # We need to make sure both user and group are in the cache. 
    # These calls will trigger LDAP lookups if appropriate.
    $status = 0 unless ($this->checkCacheForLoginName($loginName));
    $status = 0 unless ($this->isGroup($groupName));

    if (defined $status) {
      $this->untieCache() if $tie eq 'none';
      return 0;
    }
  }

  my $members = $data->{"GROUPS::$groupName"} || '';

  $this->untieCache() if $tie eq 'none';

  return ($members =~ /\b$loginName\b/)?1:0;
}

=pod 

---++++ getWikiNameOfLogin($loginName, $checkCache) -> $wikiName

returns the wikiName of a loginName or undef if it does not exist

$checkCache regulates if we should check the cache for login name or not,
which we should not do when we are in the middle of caching, which would
cause a deep recursion loop.

=cut

sub getWikiNameOfLogin {
  my ($this, $loginName, $checkCache) = @_;

  unless ($loginName) {
    $this->writeWarning("loginName was not given in getWikiNameOfLogin()");
    return 0;
  }

  #if ($checkCache && $this->{debug}) {
  #  my ($callerFile, $callerLine) = (caller)[1,2];
  #  $this->writeDebug("called getWikiNameOfLogin($loginName)", $callerFile, $callerLine);
  #}

  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid loginName");
    return 0;
  }
  
  $checkCache = 1 unless defined $checkCache;

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  # Make sure the user has been retreived from LDAP
  $this->checkCacheForLoginName($loginName) if $this->{preCache} ne 'all' && $checkCache;
  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};
  my $wikiName = TWiki::Sandbox::untaintUnchecked($data->{"U2W::$loginName"});

  $this->untieCache() if $tie eq 'none';

  return $wikiName;
}

=pod 

---++++ getLoginOfWikiName($wikiName, $data) -> $loginName

returns the loginNAme of a wikiName or undef if it does not exist

=cut

sub getLoginOfWikiName {
  my ($this, $wikiName, $data) = @_;
  
  unless ($wikiName) {
    $this->writeWarning("wikiName was not given in getLoginOfWikiName()");
    return 0;
  }

  #if ($this->{debug}) { 
  #  my ($callerFile, $callerLine) = (caller)[1,2];
  #  $this->writeDebug("called getLoginOfWikiName($wikiName)", $callerFile, $callerLine);
  #}

  my $tie;
  unless (defined $data) {
    $tie = $this->{locking}{mode};
    $this->getCacheTie('read');
    $data = $this->{data};
  }
  
  my $loginName = TWiki::Sandbox::untaintUnchecked($data->{"W2U::$wikiName"});
  
  unless ($loginName) {
    my $alias = $this->{wikiNameAliases}{$wikiName};
    $loginName = TWiki::Sandbox::untaintUnchecked($data->{"W2U::$alias"}) if defined($alias);
  }

  unless (defined $data) {
    $this->untieCache() if $tie eq 'none';
  }

  return $loginName;
}

=pod 

---++++ getAllWikiNames() -> \@array

returns a list of all known wikiNames

=cut

sub getAllWikiNames {
  my ($this) = shift;

  $this->writeDebug("called getAllWikiNames()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my @wikiNames = grep { length($_) > 0 } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{WIKINAMES}) || '');

  $this->untieCache() if $tie eq 'none';

  return \@wikiNames;
}

=pod 

---++++ getAllLoginNames() -> \@array

returns a list of all known loginNames

=cut

sub getAllLoginNames {
  my ($this) = @_;

  $this->writeDebug("called getAllLoginNames()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my @loginNames = grep { length($_) > 0 } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{LOGINNAMES}) || '');

  $this->untieCache() if $tie eq 'none';

  return \@loginNames;
}

=pod 

---++++ getDnOfLogin($loginName) -> $dn

returns the Distinguished Name of the LDAP record of the given name

=cut

sub getDnOfLogin {
  my ($this, $loginName) = @_;

  return unless $loginName;
  $this->writeDebug("called getDnOfLogin($loginName)");
  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid loginName");
    return undef;
  }

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};
  
  my $dn = TWiki::Sandbox::untaintUnchecked($data->{"U2DN::$loginName"});

  $this->untieCache() if $tie eq 'none';

  return $dn;
}

=pod 

---++++ getDnOfGroup($groupName) -> $dn

returns the Distinguished Name of the LDAP record of the given name

=cut

sub getDnOfGroup {
  my ($this, $groupName) = @_;

  return unless $groupName;
  $this->writeDebug("called getDnOfGroup($groupName)");
  unless ($this->isValidCacheGroupName($groupName)) {
    $this->writeDebug("$groupName is not a valid groupName");
    return undef;
  }

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};
  
  my $dn = TWiki::Sandbox::untaintUnchecked($data->{"U2DN::$groupName"});

  $this->untieCache() if $tie eq 'none';

  return $dn;
}

=pod 

---++++ getDnOfWikiName($wikiName) -> $dn

returns the Distinguished Name of the LDAP record of the given name

=cut

sub getDnOfWikiName {
  my ($this, $wikiName) = @_;

  return unless $wikiName;

  $this->writeDebug("called getDnOfWikiName($wikiName)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $loginName = TWiki::Sandbox::untaintUnchecked($data->{"W2U::$wikiName"});

  my $dn = undef;
  $dn = TWiki::Sandbox::untaintUnchecked($data->{"U2DN::$loginName"}) if $loginName; 

  $this->untieCache() if $tie eq 'none';

  return $dn;
}


=pod 

---++++ getWikiNameOfDn($dn) -> $wikiName

returns the wikiName used by a given Distinguished Name; reverse of getDnOfWikiName()

=cut

sub getWikiNameOfDn {
  my ($this, $dn) = @_;

  return unless $dn;

  $this->writeDebug("called getWikiNameOfDn($dn)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $loginName = TWiki::Sandbox::untaintUnchecked($data->{"DN2U::$dn"});

  my $wikiName = undef;
  $wikiName = TWiki::Sandbox::untaintUnchecked($data->{"U2W::$loginName"}) if $loginName;

  $this->untieCache() if $tie eq 'none';

  return $wikiName;
}


=pod 

---++++ changePassword($loginName, $newPassword, $oldPassword) -> $boolean

=cut

sub changePassword {
  my ($this, $loginName, $newPassword, $oldPassword) = @_;

  return undef unless 
    $this->{allowChangePassword} && defined($oldPassword) && $oldPassword ne '1';

  $this->writeDebug("called changePassword(...)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $dn = $this->getDnOfLogin($loginName);

  $this->untieCache() if $tie eq 'none';

  return undef unless $dn;
  return undef unless $this->connect($dn, $oldPassword);

  my $msg = $this->{ldap}->set_password(
    oldpasswd => $oldPassword, 
    newpasswd => $newPassword
  );

  my $errorCode = $this->checkError($msg);

  if ($errorCode != LDAP_SUCCESS) {
    $this->writeWarning("error in changePassword: ".$this->getError());
    return undef;
  }

  return 1;
}

=pod

---++++ isValidCacheLoginName($loginName) -> $boolean

Determineds if a $loginName is valid for the cache.

Used to determine if the cache should be bothered or not.
Especially useful when locking is used. 

=cut

sub isValidCacheLoginName {
  my ($this, $loginName) = @_;

  return 0 unless $loginName;

  # Scenario A: excludeMap 'WIKIWORDS', and the $loginName is a WikiWord
  if (defined $this->{excludeMap}{'WIKIWORDS'}) {
    my $copy = $loginName;
    $copy =~ s/\AMain\.//;
    return 0 if TWiki::Func::isValidWikiWord($copy);
  }

  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};

  $loginName =~ s/_(?:unknown|detached)\d*\z//;
  
  # Scenario B: $loginName does not match loginPattern
  return 0 unless $loginName =~ m/$this->{loginPattern}/;

  # Scenario C: excludeMap contains the $loginName 
  return 0 if defined $this->{excludeMap}{$loginName};

  # Scenario D: excludeMap contains 'EMAILS', and the $loginName is a email
  if (defined $this->{excludeMap}{'EMAILS'}) {
    my $copy = $loginName;

    use bytes;
    # Reverse the encoding used to generate cUIDs in login2cUID
    # use bytes to ignore character encoding
    $copy =~ s/_([0-9a-f][0-9a-f])/chr(hex($1))/gei;
    no bytes;

    return 0 if $copy =~ m/\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\z/i;
  }  

  return 1;
}
=pod

---++++ checkCacheForLoginName($loginName) -> $boolean

grant that the current loginName is cached. If not, it will download the LDAP
record for this specific user and update the LDAP cache with this single record.

This happens when the user is authenticated externally, e.g. using apache's
mod_authz_ldap or some other SSO, and the internal cache 
is not yet updated. It is completely updated regularly on a specific time
interval (default every 24h). See the LdapContrib settings.

=cut


sub checkCacheForLoginName {
  my ($this, $loginName) = @_;

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called checkCacheForLoginName($loginName)", $callerFile, $callerLine);
  }
  
  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid LoginName. Returning early...\n");
    return 0;
  }

  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};
 
  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my $status;
  if ($data->{"U2W::$loginName"}) {
    $status = 1;
  } elsif ($this->isIgnoredUser($loginName)) {
    $status = 0;
  }

  if (defined $status) {
    $this->untieCache() if $tie eq 'none';
    return $status;
  }

  my $result = 0;
  # update cache selectively
  $this->writeDebug("$loginName is unknown, need to refresh part of the ldap cache");

  my $entry = $this->getAccount($loginName);

  # We tie a 'write' tie to the cache, unless a 'write' tie already exists.
  $this->getCacheTie('write');

  unless (defined $entry) {
    $this->writeDebug("no result looking for user $loginName in LDAP (loginAttribute $this->{loginAttribute}). Adding login to ignore list.");
    $this->addIgnoredUser($loginName);
  } else {
    # merge this user record

    my %wikiNames = map { $_ => 1 } @{$this->getAllWikiNames()};
    my %loginNames = map { $_ => 1 } @{$this->getAllLoginNames()};
    
    # The loginName might change in cacheUserFromEntry..
    $loginName = $this->cacheUserFromEntry($entry, $data, \%wikiNames, \%loginNames);
    if ($loginName) {
      # resolving WikiName clashes
      $this->resolveWikiNameClashes($data, \%wikiNames, \%loginNames);

      # get the dn of the freshly cached user
      my $dn = $this->getDnOfLogin($loginName);

      # If we don't we dont precache at all, or if we precache without groups...
      if (!$this->{preCache} || !$this->{mapGroups}) {
        # If cache key GROUP2UNCACHEDMEMBERSDN contains dn's, then this means that a group contained 
        # unknown users to the LDAP cache on a earlier group refresh. If this $dn is in that list,
        # we delete the group and re-cache it.
        my %groupNames = map {$_ => 1} @{$this->getGroupNames()};
        foreach my $groupName (keys %groupNames) {
          if (defined $data->{"GROUP2UNCACHEDMEMBERSDN::$groupName"}) {
            my $dnList = TWiki::Sandbox::untaintUnchecked($data->{"GROUP2UNCACHEDMEMBERSDN::$groupName"}) || '';
            my @membersDn = split(/\s*;\s*/, $dnList);
            LOOP: {
              foreach my $memberDn (@membersDn) {
                if ($memberDn eq $dn) {
                  $this->writeDebug("refreshing group $groupName to catch new members");
                  $this->removeGroupFromCache($groupName);
                  $this->checkCacheForGroupName($groupName);
                  last LOOP;
                }
              }
            }
          }
        }
      }

      $data->{WIKINAMES} = join(',', sort keys %wikiNames);
      $data->{LOGINNAMES} = join(',', sort keys %loginNames);

      $result = 1;
    }
  }

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return $result;
}

=pod

---++++ removeGroupFromCache($groupName) -> $boolean

Remove a group from the cache

=cut

sub removeGroupFromCache {
  my ($this, $groupName) = @_;

  return 0 unless defined $groupName;
  
  $this->writeDebug("called removeGroupFromCache($groupName)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my %groupNames = map { $_ => 1 } @{ $this->getGroupNames() };
  my $dn = $this->getDnOfGroup($groupName);

  delete $groupNames{$groupName};
  delete $data->{"GROUPS::$groupName"};
  delete $data->{"GROUP2UNCACHEDMEMBERSDN::$groupName"} if defined $data->{"GROUP2UNCACHEDMEMBERSDN::$groupName"};
  delete $data->{"U2DN::$groupName"};
  delete $data->{"DN2U::$dn"} if defined $dn;

  $data->{GROUPS} = join(',', sort keys %groupNames);

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return 1;
}

=pod

---++++ removeUserFromCache($wikiName,$loginName) -> $boolean

removes a user from the cache.

A $wikiName or $loginName, or both, has to be defined,
which each option yielding the same results. However, in case
of a corrupt cache where login to wikiName is defined, but not
wikiName to login (or vice versa), it is safest to alway define both
in order to ensure a complete removal.

=cut

sub removeUserFromCache {
  my ($this, $wikiName, $loginName) = @_;

  return 0 if !defined $wikiName && !defined $loginName;

  $this->writeDebug("called removeUserFromCache($wikiName)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my %wikiNames = map { $_ => 1 } @{ $this->getAllWikiNames() };
  my %loginNames = map { $_ => 1 } @{ $this->getAllLoginNames() };

  my $loginNameDn;
  $loginNameDn = $this->getDnOfLogin($loginName) if defined $loginName;

  my ($cacheLoginName, $cacheLoginNameDn);
  if (defined $wikiName) {
    $cacheLoginName = $this->getLoginOfWikiName($wikiName);
    if (defined $cacheLoginName) {
      $cacheLoginNameDn = $this->getDnOfLogin($cacheLoginName);
    }
  }

  my ($cacheWikiName);
  if (defined $loginName) {
    $cacheWikiName = $this->getWikiNameOfLogin($loginName) if defined $loginName;
  }

  # Errors
  if (defined $loginName && !defined $cacheWikiName) {
    $this->writeWarning("No WikiName was found for given LoginName '$loginName' in removeUserFromCache");
  }

  if (defined $wikiName && !defined $cacheLoginName) {
    $this->writeWarning("No LoginName was found for given WikiName '$wikiName' in removeUserFromCache");
  }

  # Fatal errors
  if (defined $cacheLoginName && defined $loginName && $loginName ne $cacheLoginName) {
    $this->writeWarning("Given LoginName '$loginName' does not match looked-up LoginName from WikiName '$wikiName' ($cacheLoginName)");
    $this->untieCache() unless $tie eq 'write';
    $this->getCacheTie($tie) if $tie eq 'read';
    return 0;
  }

  if (defined $cacheWikiName && defined $wikiName && $wikiName ne $cacheWikiName) {
    $this->writeWarning("Given WikiName '$wikiName' does not match looked-up WikiName from LoginName '$loginName' ($cacheWikiName)");
    $this->untieCache() unless $tie eq 'write';
    $this->getCacheTie($tie) if $tie eq 'read';
    return 0;
  }

  for ($loginName, $cacheLoginName) {
    delete $loginNames{$_};
    delete $data->{"U2W::$_"};
    delete $data->{"U2DN:$_"};
    delete $data->{"U2CREATED::$_"};
    delete $data->{"U2UPDATED::$_"};
    delete $data->{"U2EMAIL::$_"};
  }

  for ($loginNameDn, $cacheLoginNameDn) {
    delete $data->{"DN2U::$_"} if defined $_;
  }

  for ($wikiName, $cacheWikiName) {
    delete $wikiNames{$_};
    delete $data->{"W2U::$_"};
  }

  my $v = sub {
    my ($loginName) = @_;

    foreach my $email (@{$this->getEmails($loginName)}) {
      my %emails = map { $_ => 1 } split(/\s*,\s*/, $data->{"EMAIL2U::$email"});
      delete $emails{$loginName};
      $data->{"EMAIL2U::$email"} = join(',', sort keys %emails);
    }
  };
  $v->($loginName);
  $v->($cacheLoginName);

  $data->{LOGINNAMES} = join(',', keys %loginNames);
  $data->{WIKINAMES} = join(',', keys %wikiNames);

  return 1;

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';
}


=pod

---++++ renameWikiName($loginName, $oldWikiName, $newWikiName)

assigns the new !WikiName to the given login

=cut

sub renameWikiName {
  my ($this, $loginName, $oldWikiName, $newWikiName) = @_;

  $this->writeDebug("renameWikiName($loginName, $oldWikiName, $newWikiName)");

  return 0 if length($newWikiName) == 0;
  return 0 if $oldWikiName eq $newWikiName;
  return 0 unless TWiki::Func::isValidWikiWord($newWikiName);

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my $status = 0;
  if (defined $data->{"W2U::$oldWikiName"} && not defined $data->{"W2U::$newWikiName"}) {
    my %allWikiNames = map {$_ => 1} @{$this->getAllWikiNames()};
    if (defined $allWikiNames{$oldWikiName} && not defined $allWikiNames{$newWikiName}) {
      my %wikiNames = map {$_ => 1} @{$this->getAllWikiNames()};
      my %loginNames = map {$_ => 1} @{$this->getAllLoginNames()};

      # If preserveWikiNames is true, we will preserve this WikiName. 
      # Since LoginName <-> TWikiName mapping is a one to one relationship, we are going to
      # Alter the LoginName slightly
      if ($this->{preserveWikiNames}) {
        my $count = 0;

        my $newLoginForOldWikiName = sprintf "%s_%d", $loginName, time();

        $loginNames{$newLoginForOldWikiName} = 1;
        $data->{"W2U::$oldWikiName"} = $newLoginForOldWikiName;
        $data->{"U2W::$newLoginForOldWikiName"} = $oldWikiName;
        $data->{"U2CREATED::$newLoginForOldWikiName"} = time();
      } else {
        delete $wikiNames{$oldWikiName};
        delete $data->{"W2U::$oldWikiName"};
      }

      $wikiNames{$newWikiName} = 1;
      $data->{"U2W::$loginName"} = $newWikiName;
      $data->{"W2U::$newWikiName"} = $loginName;
      $data->{"U2UPDATED::$loginName"} = time;

      $data->{WIKINAMES} = join(',', sort keys %wikiNames);
      $data->{LOGINNAMES} = join(',', sort keys %loginNames);

      $status = 1;  
    } else {
      #$this->writeWarning("oldWikiName=$oldWikiName not found in cache, or newWikiName=$newWikiName found.");
    }
  } else {
    #$this->writeWarning("oldWikiName=$oldWikiName not found in cache, or newWikiName=$newWikiName found.");
  }

  # Release write lock
  $this->untieCache() unless $tie eq 'write';

  # Re-tie the cache if it was tied prior to this subroutine being called
  $this->getCacheTie($tie) if $tie eq 'read';

  return $status;
}

=pod 

---++++ getAllIgnoredUsers() -> \@array

returns a list of all ignored users that should not be relookedup in LDAP

=cut

sub getAllIgnoredUsers {
  my ($this) = @_;

  # $this->writeDebug("called getAllIgnoredUsers()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my @ignoredUsers = grep { length($_) > 0 } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{UNKWNUSERS}) || '');

  $this->untieCache() if $tie eq 'none';

  return \@ignoredUsers;
}

=pod 

---++++ addIgnoredUser($login) -> $boolean

Insert a new user in the list of ignored users that should not be lookedup in LDAP

=cut

sub addIgnoredUser {
  my ($this, $login) = @_;

  $this->writeDebug("called addIgnoredUser($login)");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  $login = lc($login) unless $this->{caseSensitiveLogin};

  my $status = 0;
  my %ignoredUsers = map { $_ => 1 } @{$this->getAllIgnoredUsers()};
  unless (defined $ignoredUsers{$login}) {
    $ignoredUsers{$login} = 1;
    $data->{UNKWNUSERS} = join(',', sort keys %ignoredUsers); 
    $status = 1;
  }

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return $status;
}

=pod 

---++++ isIgnoredUser($login) -> $boolean

returns 1 if $login is an ignored user that should not be relookedup in LDAP

=cut

sub isIgnoredUser {
  my ($this, $login) = @_;

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called isIgnoredUser($login)", $callerFile, $callerLine);
  }

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  $login = lc($login) unless $this->{caseSensitiveLogin};
  my %ignoredUsers = map { $_ => 1 } @{$this->getAllIgnoredUsers()};

  $this->untieCache() if $tie eq 'none';

  return defined $ignoredUsers{$login};
}


=pod 

---++++ removeIgnoredUsers(@logins) -> true

Takes a list of @logins and removes them (if they exist) from the UNKWNUSERS list.

=cut

sub removeIgnoredUsers {
  my ($this, @logins) = @_;

  $this->writeDebug("called removeIgnoredUsers(\@logins)");

  unless ($this->{caseSensitiveLogin}) {
    $_ = lc($_) for @logins;
  }

  my %removedUsers;
  $removedUsers{$_} = 1 for @logins;

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my @ignoredUsers = grep { not exists $removedUsers{$_} } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{UNKWNUSERS}) || '');
  $data->{UNKWNUSERS} = join(',', @ignoredUsers);

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return 1;
}

=pod 

---++++ emptyIgnoredUsers() -> true

Set UNKWNUSERS to empty string.

=cut

sub emptyIgnoredUsers {
  my ($this) = @_;

  $this->writeDebug("called emptyIgnoredUsers()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  $data->{UNKWNUSERS} = '';

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return 1;
}

=pod 

---++++ getAllIgnoredGroups() -> \@array

returns a list of all ignored groups that should not be relookedup in LDAP

=cut

sub getAllIgnoredGroups {
  my ($this) = @_;

  $this->writeDebug('called getAllIgnoredGroups()');

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  my @ignoredGroups = grep { length($_) > 0 } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{UNKWNGROUPS}) || '');

  # Release lock if no lock was acquired prior to this subroutine being called.
  $this->untieCache() if $tie eq 'none';

  return \@ignoredGroups;
}

=pod 

---++++ addIgnoredGroup($groupName) -> $boolean

Insert a new group in the list of ignored groups that should not be lookedup in LDAP

=cut

sub addIgnoredGroup {
  my ($this, $groupName) = @_;

  $this->writeDebug("called addIgnoredGroup($groupName)");

  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my $status = 0;
  my %ignoredGroups = map { $_ => 1 } @{$this->getAllIgnoredGroups()};
  unless (defined $ignoredGroups{$groupName}) {
    $ignoredGroups{$groupName} = 1;
    $data->{UNKWNGROUPS} = join(',', sort keys %ignoredGroups); 
    $status = 1;
  }
    
  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return $status;
}

=pod 

---++++ isIgnoredGroup($groupName) -> $boolean

returns 1 if $groupName is an ignored group that should not be relookedup in LDAP

=cut

sub isIgnoredGroup {
  my ($this, $groupName) = @_;
  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called isIgnoredGroup($groupName)", $callerFile, $callerLine);
  }

  my %ignoredGroups = map { $_ => 1 } @{$this->getAllIgnoredGroups()};

  return defined $ignoredGroups{$groupName};
}


=pod 

---++++ removeIgnoredGroups(@groups) -> true

Takes a list of @groups and removes them (if they exist) from the UNKWNGROUPS list.

=cut

sub removeIgnoredGroups {
  my ($this, @groups) = @_;

  $this->writeDebug("called removeIgnoredGroups(\@groups)");

  unless ($this->{caseSensitiveLogin}) {
    $_ = lc($_) for @groups;
  }

  my %removedGroups;
  $removedGroups{$_} = 1 for @groups;

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  my @ignoredGroups = grep { not exists $removedGroups{$_} } split(/\s*,\s*/, TWiki::Sandbox::untaintUnchecked($data->{UNKWNGROUPS}) || '');
  $data->{UNKWNGROUPS} = join(',', @ignoredGroups);

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return 1;
}

=pod 

---++++ emptyIgnoredGroups() -> true

Set UNKWNGROUPS to empty string.

=cut

sub emptyIgnoredGroups {
  my ($this) = @_;

  $this->writeDebug("called emptyIgnoredGroups()");

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('write');
  my $data = $this->{data};

  $data->{UNKWNGROUPS} = '';

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return 1;
}


=pod

---++++ isValidCacheGroupName($groupName) -> $boolean

Determineds if a $groupName is valid for the cache.

Used to determine if the cache should be bothered or not.
Especially useful when locking is used. 

=cut

sub isValidCacheGroupName {
  my ($this, $groupName) = @_;
  
  return 0 unless $groupName;

  # Scenario A: excludeMap 'WIKIWORDS', and the $groupName is a WikiWord
  if (defined $this->{excludeMap}{'WIKIWORDS'}) {
    my $copy = $groupName;
    $copy =~ s/\AMain\.//;
    return 0 if TWiki::Func::isValidWikiWord($copy);
  }

  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  # Scenario B: $groupName does not match groupPattern
  return 0 unless $groupName =~ m/$this->{groupPattern}/;

  # Scenario C: excludeMap contains the $groupName 
  return 0 if defined $this->{excludeMap}{$groupName};

  # Scenario D: excludeMap contains 'EMAILS', and the $groupName is a email
  if (defined $this->{excludeMap}{'EMAILS'}) {
    my $copy = $groupName;

    use bytes;
    # Reverse the encoding used to generate cUIDs in login2cUID
    # use bytes to ignore character encoding
    $copy =~ s/_([0-9a-f][0-9a-f])/chr(hex($1))/gei;
    no bytes;

    return 0 if $copy =~ m/\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\z/i;
  }  

  return 1;
}

=pod

---++++ checkCacheForGroupName($groupName) -> $boolean

grant that the current groupName is cached. If not, it will download the LDAP
record for this specific group and its subgroups and update the LDAP cache with the retreived records.

This happens when the precache mode is off. See the LdapContrib settings.

=cut

sub checkCacheForGroupName {
  my ($this, $groupName) = @_;

  if ($this->{debug}) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeDebug("called checkCacheForGroupName($groupName)", $callerFile, $callerLine);
  }

  unless ($this->isValidCacheGroupName($groupName)) {
    $this->writeDebug("$groupName is not a valid groupName");
    return 0;
  }

  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};
    
  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  # Skip lookup if group was already not found in LDAP since last cache expiration
  my $status;
  my %groupNames = map { $_ => 1 } @{ $this->getGroupNames() };
  if (defined $groupNames{$groupName}) {
    $status = 1;
  } elsif ($this->isIgnoredGroup($groupName)) {
    $status = 0;
  }

  if (defined $status) {
    # Release lock if no lock was acquired prior to this subroutine being called.
    $this->untieCache() if $tie eq 'none';
    return $status;
  }

  my $result = 0;

  # update cache selectively
  $this->writeDebug("group $groupName is unknown, need to refresh part of the ldap cache");

  my $entry = $this->getGroup($groupName);

  # We tie a 'write' tie to the cache, unless a 'write' tie already exists.
  $this->getCacheTie('write');

  unless (defined $entry) {
    my ($callerFile, $callerLine) = (caller)[1,2];
    $this->writeWarning("no result looking for group $groupName in LDAP (groupAttribute $this->{groupAttribute}). Adding group to ignore list.", $callerFile, $callerLine);
    $this->addIgnoredGroup($groupName);
  } else {

    # merge this group record
    my %groupNames = map { $_ => 1 } @{ $this->getGroupNames() };

    # $groupName might change in cacheGroupFromEntry ...
    $groupName = $this->cacheGroupFromEntry($entry, $data, \%groupNames);
    if ($groupName) {

      # remember list of all groups
      $data->{GROUPS} = join(',', sort keys %groupNames);

      # check for primary group membership
      if ($this->{_primaryGroup}) {
        foreach my $groupId (keys %{ $this->{_primaryGroup} }) {
          my $currentGroupName = $this->{_groupId}{$groupId};

          if (defined $currentGroupName && $groupName eq $currentGroupName) {
            foreach my $member (keys %{ $this->{_primaryGroup}{$groupId} }) {

              #$this->writeDebug("adding $member to its primary group $currentGroupName");
              $this->{_groups}{$currentGroupName}{$member} = 1;
            }
          }
        }
      }

      # assert group members to data store
      my %members = ();
      my $storeUncachedMembersDn = !$this->{preCache} || !$this->{mapGroups};
      my %uncachedMembersDn = ();

      foreach my $member (keys %{ $this->{_groups}{$groupName} }) {

        # groups may store DNs to members instead of a memberUid, in this case we
        # have to lookup the corresponding loginAttribute
        if ($this->{memberIndirection}) {

          $this->writeDebug("following indirection for $member");

          my $memberName = $data->{"DN2U::$member"};
          if ($memberName) {
            $members{$memberName} = 1;
          } else {
            # Recursive check for groups when not in precache mode
            if (!$this->{preCache} && $member =~ /$this->{groupBase}/i) {
              my $innerGroupName = $member;
              $innerGroupName =~ s/$this->{groupBase}//o;
              $innerGroupName =~ s/$this->{groupAttribute}=//o;
              $innerGroupName =~ s/^,+//o;
              $innerGroupName =~ s/,+$//o;

              # Smell: this may not be reliable and may work only with membersindirection. TO CHECK
              if ($innerGroupName ne "" && $this->isGroup($innerGroupName)) {
                $members{$innerGroupName} = 1;
                next;
              }
            }

            if ($storeUncachedMembersDn) {
              $this->writeWarning("oops, $member not found, but member of $groupName");
              $uncachedMembersDn{$member} = 1;
            }
          }
        } else {
          $members{$member} = 1;
        }
      }

      $data->{"GROUPS::$groupName"} = join(',', sort keys %members);

      if ($this->{memberIndirection} && $storeUncachedMembersDn) {
        $data->{"GROUP2UNCACHEDMEMBERSDN::$groupName"} = join(';', sort keys %uncachedMembersDn);
      }

      $result = 1;
      undef $this->{_groups}{$groupName};
    }
    undef $this->{_groups};
  }

  # The write lock can now be released.
  $this->untieCache() unless $tie eq 'write';

  # Re-tie cache if it was tied with 'read' prior to this subroutine being called.
  $this->getCacheTie($tie) if $tie eq 'read';

  return $result;
}

=pod

---++++ getGroup($groupName) -> Net::LDAP::Entry object

Fetches a group entry from the database and returns a Net::LDAP::Entry
object on success and undef otherwise. Note, the group name is match against
the attribute defined in $ldap->{groupAttribute}. Account records are 
search using $ldap->{groupFilter} in the subtree defined by $ldap->{groupBase}.

=cut

sub getGroup {
  my ($this, $groupName) = @_;

  $this->writeDebug("called getGroup($groupName)");
  $groupName = lc( $groupName ) unless $this->{caseSensitiveGroup};

  my $filter = '(&(' . $this->{groupFilter} . ')(' . $this->{groupAttribute} . '=' . $groupName . '))';

  my @entries; # There can only be one, but we need to make sure.
  foreach my $groupBase (@{$this->{groupBase}}) {
    my $msg = $this->search(
      filter => $filter, 
      base => $groupBase,
      deref => 'always'
    );

    eval {
      $this->writeDebug("no such group") and next unless defined $msg;
      push @entries, $msg->entry(0) if defined $msg->entry(0);
    };
    if ($@) {
      $this->writeWarning("Error: $@ (GroupName $groupName)");
      next;
    }
  }

  if (@entries == 1) {
    return $entries[0];
  } else {
    $this->{error} = "Group $groupName is invalid - no hits in LDAP.";
    $this->writeDebug($this->{error});
    return undef;
  }
}

=pod 

---++++ getDateOfLogin($loginName) -> $timestamp

returns the stored creation, or update date of a login, if any

=cut

sub getDateOfLogin {
  my ($this, $loginName, $type) = @_;

  return unless $loginName;
  return unless $type =~ m/\A(?:created|updated)\z/;
  # $this->writeDebug("called getDateOfLogin($loginName, $type)");

  unless ($this->isValidCacheLoginName($loginName)) {
    $this->writeDebug("$loginName is not a valid loginName");
    return undef;
  }

  my $tie = $this->{locking}{mode};
  $this->getCacheTie('read');
  my $data = $this->{data};

  $loginName = lc( $loginName ) unless $this->{caseSensitiveLogin};

  my $timestamp = TWiki::Sandbox::untaintUnchecked($data->{"U2" . uc($type) . "::$loginName"});

  $this->untieCache() if $tie eq 'none';

  return $timestamp;
}

=pod

---++++ fromUtf8($string) -> $string

Wrapper to use Unicode::MapUTF8 for Perl < 5.008
and Encode for later versions.
[adopted from <nop>I18N.pm]

=cut

sub fromUtf8 {
  my ($this, $utf8string) = @_;

  my $charset = $TWiki::cfg{Site}{CharSet};
  return $utf8string if $charset =~ /^utf-?8$/i;

  if ($] < 5.008) {

    # use Unicode::MapUTF8 for Perl older than 5.8
    require Unicode::MapUTF8;
    if (Unicode::MapUTF8::utf8_supported_charset($charset)) {
      return Unicode::MapUTF8::from_utf8({ -string => $utf8string, -charset => $charset });
    } else {
      $this->$this->writeWarning('Conversion from $encoding no supported, ' . 'or name not recognised - check perldoc Unicode::MapUTF8');
      return $utf8string;
    }
  } else {

    # good Perl version, just use Encode
    require Encode;
    import Encode;
    my $encoding = Encode::resolve_alias($charset);
    if (not $encoding) {
      $this->$this->writeWarning('Conversion to "' . $charset . '" not supported, or name not recognised - check ' . '"perldoc Encode::Supported"');
      return $utf8string;
    } else {

      # converts to $charset, generating HTML NCR's when needed
      my $octets = Encode::decode('utf-8', $utf8string);
      return Encode::encode($encoding, $octets, &Encode::FB_HTMLCREF());
    }
  }
}

=begin text

---++++ toUtf8($string) -> $utf8string

Wrapper to use Unicode::MapUTF8 for Perl < 5.008
and Encode for later versions.
[adopted from <nop>I18N.pm]

=cut

sub toUtf8 {
  my ($this, $string) = @_;

  my $charset = $TWiki::cfg{Site}{CharSet};
  return $string if $charset =~ /^utf-?8$/i;

  if ($] < 5.008) {

    # use Unicode::MapUTF8 for Perl older than 5.8
    require Unicode::MapUTF8;
    if (Unicode::MapUTF8::utf8_supported_charset($charset)) {
      return Unicode::MapUTF8::to_utf8({ -string => $string, -charset => $charset });
    } else {
      $this->$this->writeWarning('Conversion from $encoding no supported, ' . 'or name not recognised - check perldoc Unicode::MapUTF8');
      return $string;
    }
  } else {

    # good Perl version, just use Encode
    require Encode;
    import Encode;
    my $encoding = Encode::resolve_alias($charset);
    if (not $encoding) {
      $this->$this->writeWarning('Conversion to "' . $charset . '" not supported, or name not recognised - check ' . '"perldoc Encode::Supported"');
      return undef;
    } else {
      my $octets = Encode::decode($encoding, $string, &Encode::FB_PERLQQ());
      return Encode::encode('utf-8', $octets);
    }
  }
}

=pod

---++++ getTWikiUserMapping() -> \%hash

Reads the {UsersWebName}.{UsersTopicName} topic and then returning a reference to a 
login => {wikiName => $wikiName, $date => $date } hash.
<verbatim>
   * Rules containing invalid login names, WikiNames or dates will be discarded.
   * In case of duplicate login names and WikiNames, the first found will be kept.
</verbatim>

=cut

sub getTWikiUserMapping {
  my ($this) = @_;

  my $userWeb = $TWiki::cfg{UsersWebName};
  my $userTopic = $TWiki::cfg{UsersTopicName};

  my %TWikiUserMapping;

  if (TWiki::Func::topicExists($userWeb,$userTopic)) {
    $this->writeDebug("TWikiUserMapping mappings from $userWeb.$userTopic will be loaded into the LdapContrib database.");

    # Loading the TWikiMapping topic  
    my $TWikiUserMappingTopic = TWiki::Func::readTopicText($userWeb, $userTopic);
  
    my %wikiNamesSeen;
    my %loginNamesSeen;

    for ((split("\n", $TWikiUserMappingTopic))) {
      if ($_ =~ m/\A\s+\*\s+([^\s\n \-]+)\s+-\s+([^\s\n \-]+)\s+-\s+(.+)/o) {
        my ($wikiName, $loginName, $date) = ($1, $2, $3);
        my $epoch = &getEpoch($date);

        # Validating identity
        my $originalLoginName = $loginName;
        $loginName = $this->normalizeLoginName($loginName) if $this->{normalizeLoginName};

        $this->writeWarning("$userWeb.$userTopic: login name $originalLoginName not valid. Check {Exclude},{NormalizeLoginNames},{LoginPattern} in config. Skipping ...") and next
          if !$loginName || !$this->isValidCacheLoginName($loginName);

        # Have we already treated this login name?
        $this->writeWarning("$userWeb.$userTopic: login name $originalLoginName already stored. Skipping ...") and next
          if defined $loginNamesSeen{$loginName};

        # Validating date
        $this->writeWarning("$userWeb.$userTopic: date $date could not be recognized. Skipping ...") and next
          unless $epoch;

        # Validating WikiName
        $this->writeWarning("$userWeb.$userTopic: WikiName $wikiName not valid. Skipping ...") and next
          unless TWiki::Func::isValidWikiWord($wikiName);

        # Have we already treated this WikiName?
        $this->writeWarning("$userWeb.$userTopic: login name $originalLoginName already stored. Skipping ...") and next
          if defined $wikiNamesSeen{$wikiName};

        # Storing WikiName and date
        $TWikiUserMapping{$loginName}{wikiName} = $wikiName;
        $TWikiUserMapping{$loginName}{date} = $epoch;

        $wikiNamesSeen{$wikiName}++;
        $loginNamesSeen{$loginName}++;
      }
    }
  
  } else {
    $this->writeWarning("TWikiUserMapping topic $userWeb.$userTopic could not be found. Aborting migration from TWikiUserMapping.");
  }
    
  return \%TWikiUserMapping;
}

=pod

---++++ getEpoch() -> $epoch

The user topic containing mappings between WikiNames and loginnames used by TWikiUserMapping
contains a myriad of different date formats, for example:
<verbatim>
   * 01-02-2010
   * 2010-02-01
   * 01 02 2010
   * Feb 2010
   * 01 Feb 2010
</verbatim>

This method tries to parse these formats and give the epoch time back, for storing in the LdapContrib database.

=cut

sub getEpoch {
  my ($dateString) = @_;

  $dateString =~ s/\A\s+//;
  $dateString =~ s/\s+\z//;
  
  my %regex = (
    textMonths => qr/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec/o,
    numMonths => qr/01|02|03|04|05|06|07|08|09|10|11|12/o,
    bothMonths => qr/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|01|02|03|04|05|06|07|08|09|10|11|12/o,
  );
  
  my %monthConv = (
    Jan => '01', Feb => '02', Mar => '03', 
    Apr => '04', May => '05', Jun => '06', 
    Jul => '07', Aug => '08', Sep => '09', 
    Oct => '10', Nov => '11', Dec => '12'
  );

  # Get Date
  my ($day, $month, $year);
  if ($dateString =~ m/\A([0-3][0-9])-($regex{bothMonths})-(\d{4})\z/) {
    # Format: 01-02-2010
    $day = $1;
    $month = (defined $monthConv{$2} ? $monthConv{$2} : $2);
    $year = $3;
  } elsif ($dateString =~ m/\A(\d{4})-($regex{numMonths})-([0-3][0-9])\z/) {
    # Format: 2010-02-01
    $day = $3;
    $month = (defined $monthConv{$2} ? $monthConv{$2} : $2);
    $year = $1;
  } elsif ($dateString =~ m/\A([0-3][0-9]) ($regex{numMonths}) (\d{4})\z/) {
    # Format: 01 02 2010
    $day = $1;
    $month = (defined $monthConv{$2} ? $monthConv{$2} : $2);
    $year = $3;
  } elsif ($dateString =~ m/\A($regex{textMonths}) (\d{4})\z/) {
    # Format: Jan 2010
    $month = $monthConv{$1};
    $year = $2;
  } elsif ($dateString =~ m/\A([0-3]?[0-9]) ($regex{textMonths}) (\d{4})\z/) {
    # Format: 10 Oct 2008
    $day = $1;
    $month = $monthConv{$2};
    $year = $3;
  }

  my $epoch;
  eval { $epoch = timelocal(0,0,0, $day || '01', $month-1, $year) } if defined $month && defined $year;

  return $epoch;
}

=pod

---++++ backupCacheFile() -> void 

Before replacing the freshly created temporary cache with the production cache file, backup the file.

This subroutine requires the File::Copy module.

TODO: Check if the path used in this routine is compatible in a windows environment.

=cut

sub backupCacheFile {
  my ($this) = @_;

  $this->writeDebug('Called backupCacheFile()');

  my $workArea = $this->{session}->{store}->getWorkArea('LdapContrib');
  my @dbFiles = glob( "$workArea/cache.bak.*");
  
  # Return early if the most recent backup is too fresh
  if ($this->{backupFileAge} =~ m/\A\s*(\d+)\s*\z/) {
    my $minAge = $1;
    if (@dbFiles) {
      my $modTime = (stat($dbFiles[-1]))[10];
      if (time - $modTime < $minAge) {
        return undef;
      }
    }
  }

  # Return early if the File::Copy module is not present on the system
  eval {
    require File::Copy;
    File::Copy->import();
    1;
  };

  $this->writeWarning("Could not backup cache file. File::Copy could not be loaded: $@") and return undef if $@;

  unlink TWiki::Sandbox::untaintUnchecked($dbFiles[0]) if @dbFiles >= 7;
  
  my ($sec,$min,$hour,$mday,$mon,$year) = (localtime(time))[0 .. 5];
  $year += 1900;
  $mon++;

  my $cacheBackupFile = sprintf "%s.%04d-%02d-%02d-%02d-%02d-%02d", "$workArea/cache.bak", $year, $mon, $mday, $hour, $min, $sec;

  copy ("$workArea/cache.db", $cacheBackupFile ); 
  $cacheBackupFile = undef and $this->writeWarning("Could not create backup: $!") if defined $!;

  return $cacheBackupFile;
}

1;
