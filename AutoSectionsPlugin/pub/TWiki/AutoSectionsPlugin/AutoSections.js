if (window.jQuery) {
  // Backward compatiblity for .attr() vs .prop():
  if ($.fn.jquery.match(/^0|^1\.[0-5](\.|$)/)) {
    // jQuery 1.5-
    $.fn.prop = $.fn.attr;
  }

  $(function () {
    var allSections = [];

    function makeSection(node, level) {
      var $a = $(node).find('span.twikiAutoSectionMenu');

      if ($a.length > 0) {
        var id = $a.prop('id');

        if (id && id.match(/^twikiAutoSection\/(\S+?)\/([^\/]+)\/(\d+)/)) {
          return {
            web:   RegExp.$1,
            topic: RegExp.$2,
            id:    RegExp.$3,
            level: level,
            headingNode: node,
            wrapperNode: null,
            contentNode: null
          };
        }
      }
    }

    function traverseElement(node) {
      var origChildNodes = []; // Save child nodes, as node.childNodes will be altered

      for (var i = 0; i < node.childNodes.length; i++) {
        origChildNodes.push(node.childNodes[i]);
      }

      var stack = [];
      var topLevel = 0;

      function foundLevel(level) {
        while (stack.length > 0 && stack[stack.length - 1].level >= level) {
          stack.pop();
        }

        if (topLevel == 0 || topLevel > level) {
          topLevel = level;
        }
      }

      function setupSection(section) {
        section.wrapperNode = document.createElement('DIV');
        section.wrapperNode.className = 'twikiAutoSectionWrapper';
        section.wrapperNode.id = 'twikiAutoSectionWrapper/' + section.web + '/' + section.topic + '/' + section.id;

        if (stack.length == 0) {
          // Insert the new <div> before <hX>
          section.headingNode.parentElement.insertBefore(section.wrapperNode, section.headingNode);
        } else {
          // Append the new <div> into the stack top's <div>
          stack[stack.length - 1].contentNode.appendChild(section.wrapperNode);
        }

        section.contentNode = document.createElement('DIV');
        section.contentNode.className = 'twikiAutoSectionContent';
        section.wrapperNode.appendChild(section.contentNode);
        section.contentNode.appendChild(section.headingNode);

        stack.push(section);
      }

      for (var i = 0; i < origChildNodes.length; i++) {
        var child = origChildNodes[i];

        if (child.nodeType == 1) {
          if (child.nodeName.match(/^H([1-6])$/i)) {
            var level = new Number(RegExp.$1);

            if (level == 1) {
              level = 2;
            }

            var section = makeSection(child, level);

            if (section) {
              foundLevel(level);
              setupSection(section);
              allSections.push(section);
              continue;
            }
          }

          var childLevel = traverseElement(child);

          if (childLevel > 0) {
            foundLevel(childLevel);
          }
        }

        if (stack.length > 0) {
          stack[stack.length - 1].contentNode.appendChild(child);
        }
      }

      return topLevel;
    }

    traverseElement(document.body);

    var allEditLinks = [];
    var $allEditLinks;
    var editing = false;

    $.each(allSections, function (idx, section) {
      var $wrapper = $(section.wrapperNode);
      var $content = $(section.contentNode);
      var $heading = $(section.headingNode);

      var editUrl     = twikiAutoSectionsPlugin.editUrl + "/" + section.web + "/" + section.topic;
      var viewUrl     = twikiAutoSectionsPlugin.viewUrl + "/" + section.web + "/" + section.topic;
      var stealthMode = twikiAutoSectionsPlugin.stealthMode;
      var editLabel   = twikiAutoSectionsPlugin.editLabel;
      var rawLabel    = twikiAutoSectionsPlugin.rawLabel;
      var cancelLabel = twikiAutoSectionsPlugin.cancelLabel;
      var topicDate   = twikiAutoSectionsPlugin.topicDate;

      var tag = $heading.prop("tagName").toLowerCase(); // h1, h2, etc.

      $wrapper.prepend(
        "<div class='twikiAutoSectionEditor' style='display: none'>" +
          "<" + tag + " class='twikiAutoSectionEditorHeading'>" +
            "<div style='float: right'>" +
              "<span class='twikiAutoSectionMenu'>" +
                "<a href='javascript:void(0)' class='twikiAutoSectionLink'>" +
                  cancelLabel +
                "</a>" +
              "</span>" +
            "</div>" +
            "<span class='twikiAutoSectionStatusLabel'>(editing)</span>" +
          "</" + tag + ">" +
          "<div class='twikiAutoSectionFormHolder'>" +
            "<div>" +
              "<textarea disabled='disabled'>Loading...</textarea>" +
            "</div>" +
            "<div class='twikiAutoSectionButtons' style='margin-top: 5px'>" +
              "<input type='submit' name='action_save' value='Save' class='twikiSubmit' disabled='disabled' /> " +
              "<input type='submit' name='action_quiet' value='Quiet save' class='twikiButton' disabled='disabled' /> " +
              "<input type='button' name='action_checkpoint' value='Save and Continue' class='twikiButton' disabled='disabled' /> " +
              "<input type='button' name='action_cancel' value='Cancel' class='twikiButton twikiButtonCancel' /> " +
            "</div>" +
          "</div>" +
        "</div>"
      );

      var $editor = $wrapper.find(".twikiAutoSectionEditor").eq(0);
      var $statusLabel = $editor.find(".twikiAutoSectionStatusLabel");
      var $formHolder = $editor.find(".twikiAutoSectionFormHolder");
      var $textarea = $editor.find("textarea");
      var $formButtons = $();
      var $formButtonHolder = $(".twikiAutoSectionButtons");
      var accessKeys = [];

      var width = "100%"; //$content.outerWidth();
      var height = $content.outerHeight() + 40;
      if (height > 400) height = 400;
      $textarea.css({width: width, height: height});

      var $editorHeading = $editor.find(".twikiAutoSectionEditorHeading");
      var $menu = $heading.find("span.twikiAutoSectionMenu");

      var $rawLink = $("<a href='javascript:void(0)' class='twikiAutoSectionLink'></a>");
      $menu.append($rawLink);
      $rawLink.html(rawLabel);

      var $editLink = $("<a href='javascript:void(0)' class='twikiAutoSectionLink'></a>");
      $menu.append($editLink);
      $editLink.html(editLabel);

      allEditLinks.push($menu.get(0));

      if (stealthMode) {
        $menu.css({opacity: '0'});
        $menu.mouseover(function () {$menu.css({opacity: '1'})});
        $menu.mouseout(function () {$menu.css({opacity: '0'})});
      }

      $rawLink.css({opacity: '0'});
      $editLink.css({opacity: '1'});
      $menu.mouseover(function () {$rawLink.css({opacity: '1'})});
      $menu.mouseout(function () {$rawLink.css({opacity: '0'})});

      var ajaxLoad = null;
      var ajaxSave = null;
      var ajaxCancel = null;
      var ajaxResume = null;
      var formLoaded = false;
      var checkpointInvoked = false;

      $editLink.click(function () {editing = true; loadForm()});
      $rawLink.click(function () {editing = false; loadForm()});

      var loadForm = function () {
        $content.hide();
        $editor.show();
        $formHolder.show();
        $allEditLinks.hide();

        if (editing) {
          $statusLabel.html("(editing)");
        } else {
          $statusLabel.html("(raw view)");
        }

        if (formLoaded) {
          if (editing) {
            invokeAjaxResume();
          }
          updateFormStyle();
        } else if (!ajaxLoad) {
          if (!editing) {
            $formButtonHolder.hide();
          }

          var params = {
            skin: "autosection",
            AutoSection: section.id,
            AutoSectionDate: topicDate,
            t: new Date().getTime() / 1000
          };

          ajaxLoad = $.get(editUrl, params, function (data) {
            if (!data.match(/^<form /)) {
              if (data.match(/\[AutoSectionsPlugin\](.*?)\[\/AutoSectionsPlugin\]/)) {
                alert(RegExp.$1);
              } else {
                alert("Edit conflict: Please edit the entire topic instead of a section.");
              }
              cancelEditing();
              ajaxLoad = null;
              return;
            }

            $formHolder.html(data);
            setupForm();

            formLoaded = true;
            ajaxLoad = null;

            if (!editing) {
              invokeAjaxCancel();
            }

            updateFormStyle();
          });
        }
      };

      var updateFormStyle = function () {
        if (editing) {
          $textarea.prop({readonly: false});
          $textarea.focus();
          $formButtonHolder.show();
        } else {
          $textarea.prop({readonly: true});
          $textarea.blur();
          $formButtonHolder.hide();
        }

        $formButtons.each(function (i) {
          $(this).prop({accesskey: accessKeys[i]});
        });
      };

      $menu.mouseover(function() {
        $content.addClass("twikiAutoSectionHighlighted");
      });

      $menu.mouseout(function() {
        $content.removeClass("twikiAutoSectionHighlighted");
      });

      $editorHeading.find("a").eq(0).click(cancelEditing);
      $formHolder.find("input[name=action_cancel]").click(cancelEditing);

      var setupForm = function () {
        var $form = $formHolder.find("form");
        $form.find("input[name=redirectto]").val(location.href.replace(/#.*/, ''));

        $textarea = $form.find("textarea");
        $textarea.css({width: width, height: height});

        $formButtonHolder = $form.find(".twikiAutoSectionButtons");

        $formButtons = $form.find("input:submit, input:button");

        // Save access key info to retrieve later
        $formButtons.each(function (i) {
          accessKeys[i] = $(this).prop("accesskey");
        });

        var $cancelButton = $form.find("input[name=action_cancel]");
        $cancelButton.click(cancelEditing);

        var $checkpointButton = $form.find("input[name=action_checkpoint]");

        $checkpointButton.click(function () {
          var formdata = $form.serialize() + "&action_checkpoint=on";
          checkpointInvoked = true;

          $statusLabel.html("(saving...)");
          $textarea.prop({disabled: true});
          $formButtons.prop({disabled: true});

          ajaxSave = $.post($form.prop("action"), formdata, function () {
            $textarea.prop({disabled: false});
            $textarea.focus();
            $formButtons.prop({disabled: false});
            $statusLabel.html("(editing)");
            ajaxSave = null;
          });
        });

        $form.submit(function () {
          var scroll = $(document).scrollTop();
          document.cookie = "AutoSectionsScroll=" + scroll;
        });
      };

      function reloadPage() {
        var scroll = $(document).scrollTop();
        document.cookie = "AutoSectionsScroll=" + scroll;
        location.reload();
      }

      function invokeAjaxCancel() {
        var $form = $formHolder.find("form");
        var formdata = $form.serialize() + "&action_cancel=on";

        if (ajaxCancel) {
          ajaxCancel.abort();
          ajaxCancel = null;
        }

        if (ajaxResume) {
          ajaxResume.abort();
          ajaxResume = null;
        }

        ajaxCancel = $.post($form.prop("action"), formdata, function () {
          if (checkpointInvoked) {
            reloadPage();
          }

          ajaxCancel = null;
        });
      }

      function invokeAjaxResume() {
        if (ajaxCancel) {
          ajaxCancel.abort();
          ajaxCancel = null;
        }

        if (ajaxResume) {
          ajaxResume.abort();
          ajaxResume = null;
        }

        ajaxResume = $.get(editUrl, {
          skin: "autosection",
          AutoSection: section.id,
          breaklock: "on",
          t: new Date().getTime() / 1000
        }, function () {
          ajaxResume = null;
        });
      }

      function cancelEditing() {
        if (formLoaded) {
          if (editing) {
            invokeAjaxCancel();
          }

          if (checkpointInvoked) {
            return;
          }
        } else if (checkpointInvoked) {
          reloadPage();
          return;
        }

        editing = false;

        $formButtons.removeAttr("accesskey");
        $editor.hide();
        $content.show();
        $allEditLinks.show();

        if (ajaxSave) {
          ajaxSave.abort();
          ajaxSave = null;
        }
      }
    });

    $allEditLinks = $(allEditLinks);
  });
}
