# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013-2014 Mahiro Ando & Hideyo Imazu
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org and
# TWiki Contributors.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

=pod

---+ package TWiki::Plugins::AutoSectionsPlugin

=cut

# Always use strict to enforce variable scoping
use strict;

package TWiki::Plugins::AutoSectionsPlugin;

# Name of this Plugin, only used in this module
our $pluginName = 'AutoSectionsPlugin';

require TWiki;
require TWiki::Func;    # The plugins API
require TWiki::Plugins; # For the API version
require TWiki::OopsException;

our $VERSION = '$Rev$';
our $RELEASE = '2013-10-30';
our $SHORTDESCRIPTION = 'Automatically make editable sections based on headings';
our $NO_PREFS_IN_TOPIC = 1;
our $debug = 0;

use TWiki::Plugins::AutoSectionsPlugin::Core qw(
    markHeadings
    modifyHeadings
    unmarkHeadings
    selectSection
    replaceSection
);

my $autoSectionsEnabled;
my $urlParamEnabled;
my $stealthModeEnabled;
my $editLabel;
my $rawLabel;
my $cancelLabel;
my $anySections;
my $disableEditing;

sub initPlugin {
    my( $topic, $web, $user, $installWeb ) = @_;

    if( $TWiki::Plugins::VERSION < 1.024 ) {
        TWiki::Func::writeWarning( "Version mismatch between $pluginName and Plugins.pm" );
        return 0;
    }

    my $cgi = TWiki::Func::getCgiQuery();
    $debug = TWiki::Func::isTrue($TWiki::cfg{Plugins}{$pluginName}{Debug});

    $urlParamEnabled = TWiki::Func::isTrue($cgi->param('AutoSections'));

    my $configValue = $cgi->param('AutoSections') ||
            TWiki::Func::getPreferencesValue('AUTOSECTIONS') ||
            $TWiki::cfg{Plugins}{$pluginName}{Default};

    $autoSectionsEnabled = TWiki::Func::isTrue(
        $configValue, 1 # Enabled by default
    );

    $stealthModeEnabled = $configValue && lc $configValue eq 'hover';

    # Edit label
    $editLabel = TWiki::Func::getPreferencesValue('AUTOSECTIONS_EDITLABEL') ||
        $TWiki::cfg{Plugins}{$pluginName}{EditLabel} ||
        'edit';
    $editLabel = '' if $editLabel eq 'off';

    $editLabel = TWiki::Func::expandCommonVariables($editLabel) if $editLabel =~ /%/;

    # Raw label
    $rawLabel = TWiki::Func::getPreferencesValue('AUTOSECTIONS_RAWLABEL') ||
        $TWiki::cfg{Plugins}{$pluginName}{RawLabel} ||
        'raw';
    $rawLabel = '' if $rawLabel eq 'off';

    $rawLabel = TWiki::Func::expandCommonVariables($rawLabel) if $rawLabel =~ /%/;

    # Cancel label
    $cancelLabel = TWiki::Func::getPreferencesValue('AUTOSECTIONS_CANCELLABEL') ||
        $TWiki::cfg{Plugins}{$pluginName}{CancelLabel} ||
        'cancel';

    $cancelLabel = TWiki::Func::expandCommonVariables($cancelLabel) if $cancelLabel =~ /%/;

    $anySections = 0;
    $disableEditing = 0;

    TWiki::Func::registerTagHandler('EDITAUTOSECTIONS', \&_EDITAUTOSECTIONS);

    return 1;
}

sub _EDITAUTOSECTIONS {
    my ($session, $params) = @_;
    my $label;

    if ($urlParamEnabled) {
        $label = $params->{endlabel} || 'Finish Editing';
    } else {
        $label = $params->{_DEFAULT} ||
            $params->{label} || 'Edit Sections';
    }

    my $value = $params->{value} || 'on';

    if (!$urlParamEnabled && $autoSectionsEnabled) {
        $disableEditing = 1;
    }

    my $class = $urlParamEnabled ? 'twikiButton twikiButtonCancel' : 'twikiSubmit';

    return qq(<form>).
        ($urlParamEnabled ? '' :
            qq(<input type="hidden" name="AutoSections" value="$value"/>)).
        qq(<input type="submit" value="$label" class="$class"/>).
        qq(</form>);
}

sub beforeCommonTagsHandler {
    # do not uncomment, use $_[0], $_[1]... instead
    ### my ( $text, $topic, $web, $meta ) = @_;

    return unless TWiki::Func::getContext()->{body_text};

    my $topic = $_[1];
    my $web = $_[2];
    my $wikiName = TWiki::Func::getWikiName();

    return unless $autoSectionsEnabled &&
            !TWiki::Func::getContext()->{inactive} &&
            !TWiki::Func::getContext()->{content_slave} &&
            TWiki::Func::checkAccessPermission(
                    'CHANGE', $wikiName, undef, $topic, $web, undef);

    my $cgi = TWiki::Func::getCgiQuery();

    if ($TWiki::cfg{Plugins}{MultiEditPlugin}{Enabled}
            && $_[0] =~ /<section([ >]|$)/m) {
        # If AUTOSECTIONS variable is explicitly set, blow off <section> tags.
        # Otherwise, disable this plugin and respect any existing <section> tags.
        if (TWiki::Func::isTrue(
            $cgi->param("AutoSections") ||
            TWiki::Func::getPreferencesValue("AUTOSECTIONS")
        )) {
            $_[0] =~ s/<\/?section[^>]*?>//sg;
        } else {
            $disableEditing = 1;
            return;
        }
    }

    if (markHeadings($_[0], $topic, $web)) {
        $anySections = 1;
    }
}

sub preRenderingHandler {
    if ($anySections) {
        unless ($disableEditing) {
            if (modifyHeadings($_[0])) {
                _addToHEAD();
            }
        }

        unmarkHeadings($_[0]);
    }

    my $cgi = TWiki::Func::getCgiQuery();

    if (defined(my $scroll = $cgi->cookie('AutoSectionsScroll'))) {
        TWiki::Func::addToHEAD("$pluginName/AutoSectionsScroll", <<END);
<script type="text/javascript">
document.cookie = "AutoSectionsScroll=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
if (window.jQuery) {
    \$(function () {
        var scroll = $scroll;
        var \$document = \$(document);
        var retry = 20; // times
        var interval = 100; // msec
        var attemptScroll = function () {
            \$document.scrollTop(scroll);
            if (\$document.scrollTop() < scroll) {
                if (--retry) {
                    setTimeout(attemptScroll, interval);
                }
            }
        };
        attemptScroll();
    });
}
</script>
END
    }
}

sub _addToHEAD {
    my $jsPath = qq(%PUBURL%/%SYSTEMWEB%/$pluginName/AutoSections.js);
    my $t = $debug ? time() : $RELEASE;
    my $stealthMode = $stealthModeEnabled ? 'true' : 'false';
    (my $editLabelEsc = $editLabel) =~ s/"/\\"/g;
    (my $rawLabelEsc = $rawLabel) =~ s/"/\\"/g;
    (my $cancelLabelEsc = $cancelLabel) =~ s/"/\\"/g;

    TWiki::Func::addToHEAD($pluginName, <<END);
<script type="text/javascript">
var twikiAutoSectionsPlugin = {
    editUrl: "%SCRIPTURL{edit}%",
    viewUrl: "%SCRIPTURL{view}%",
    stealthMode: $stealthMode,
    editLabel: "$editLabelEsc",
    rawLabel: "$rawLabelEsc",
    cancelLabel: "$cancelLabelEsc",
    topicDate: %DATETIME{date="%REVINFO{"\$iso"}%" format="\$epoch"}%
};
</script>
<script type="text/javascript" src="$jsPath?t=$t"></script>
<style>
.twikiAutoSectionHighlighted {
    background-color: #EEE;
}
a:link.twikiAutoSectionLink {
    font-weight: normal;
    font-size: 11px;
    padding: 2px;
    border-style: none;
}
#patternMainContents h1 a:hover.twikiAutoSectionLink,
#patternMainContents h2 a:hover.twikiAutoSectionLink,
#patternMainContents h3 a:hover.twikiAutoSectionLink,
#patternMainContents h4 a:hover.twikiAutoSectionLink,
#patternMainContents h5 a:hover.twikiAutoSectionLink,
#patternMainContents h6 a:hover.twikiAutoSectionLink,
a:hover.twikiAutoSectionLink {
    color: #00F;
}
</style>
END
}

sub beforeEditHandler {
    # do not uncomment, use $_[0], $_[1]... instead
    ### my ( $text, $topic, $web ) = @_;

    my $cgi = TWiki::Func::getCgiQuery();

    if (defined(my $selectedID = $cgi->param('AutoSection'))) {
        if (my $baseDate = $cgi->param('AutoSectionDate')) {
            my $topic = $_[1];
            my $web = $_[2];
            my ($topicDate, $user) = TWiki::Func::getRevisionInfo($web, $topic);

            if ($baseDate < $topicDate) {
                throw TWiki::OopsException(
                    'autosection',
                    web    => $web,
                    topic  => $topic,
                    params => [
                        "$user has modified this topic. " .
                        "Please refresh the page and try again. " .
                        "Or edit the entire topic."
                    ],
                );
            }
        }

        selectSection($_[0], $selectedID);
    }
}

sub beforeSaveHandler {
    # do not uncomment, use $_[0], $_[1]... instead
    ### my ( $text, $topic, $web ) = @_;

    my $cgi = TWiki::Func::getCgiQuery();

    if (defined(my $selectedID = $cgi->param('AutoSection'))) {
        my $topic = $_[1];
        my $web = $_[2];

        require TWiki::Meta;
        my $newMeta = TWiki::Meta->new($TWiki::Plugins::SESSION, $web, $topic, $_[0]);
        my $newText = $newMeta->text;
        $newText =~ s/^\s+|\s+$//g;

        my ($topicMeta, $topicText) = TWiki::Func::readTopic($web, $topic);
        replaceSection($topicText, $selectedID, $newText);

        $newMeta->text($topicText);
        $_[0] = $newMeta->getEmbeddedStoreForm();
    }
}

1;
