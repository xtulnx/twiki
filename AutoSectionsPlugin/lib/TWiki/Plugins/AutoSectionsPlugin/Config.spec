# ---+ Extensions
# ---++ AutoSectionsPlugin
# Automatically make editable sections based on headings
# **BOOLEAN**
# Show all the edit links by default?
$TWiki::cfg{Plugins}{AutoSectionsPlugin}{Default} = 1;
# **BOOLEAN**
# Enable debug
$TWiki::cfg{Plugins}{AutoSectionsPlugin}{Debug} = 0;
1;
