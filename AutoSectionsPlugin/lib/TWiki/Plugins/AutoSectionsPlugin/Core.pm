# Module for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013-2014 Mahiro Ando & Hideyo Imazu
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org and
# TWiki Contributors.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

use strict;
package TWiki::Plugins::AutoSectionsPlugin::Core;
use base 'Exporter';

our @EXPORT_OK = qw(
    markHeadings
    modifyHeadings
    unmarkHeadings
    selectSection
    replaceSection
);

require TWiki;
require TWiki::Func;
require TWiki::Plugins;

my $headingRegex = qr/$TWiki::regex{headerPatternDa}|$TWiki::regex{headerPatternHt}/;
my $notocRegex = $TWiki::regex{headerPatternNoTOC};

sub markHeadings {
    ### my ( $text, $topic, $web ) = @_;

    my $removed = {};
    _takeOutBlocks($_[0], $removed);

    my $context = {
        nextID => 0,
        web    => $_[2],
        topic  => $_[1],
    };

    my $changed = $_[0] =~ s/($headingRegex)/_markHeading($1, $context)/mego;
    _putBackBlocks($_[0], $removed);
    return $changed;
}

sub _markHeading {
    my ($line, $context) = @_;
    my $web = $context->{web};
    my $topic = $context->{topic};
    my $id = $context->{nextID}++;
    my ($level, $prefix, $notoc, $body, $suffix) = _parseHeading($line);
    my $mark = "<!--AutoSection/$web/$topic/$id/$level-->";
    return $mark."\n".$prefix.$notoc.$body.$suffix;
}

sub modifyHeadings {
    ### my ( $text ) = @_;
    return $_[0] =~ s{<!--AutoSection/(\S+?)/([^/]+)/(\d+)/(\d+)-->\n(.*)}{_modifyHeading($1, $2, $3, $4, $5)}ge;
}

sub _modifyHeading {
    my ($web, $topic, $id, $level, $line) = @_;
    my (undef, $prefix, $notoc, $body, $suffix) = _parseHeading($line);

    my $edit =
        "<div style='float: right'>".
            "<span id='twikiAutoSection/$web/$topic/$id/$level' class='twikiAutoSectionMenu'>".
            "</span>".
        "</div>";

    return $prefix.$notoc.$edit.$body.$suffix;
}

sub unmarkHeadings {
    ### my ( $text ) = @_;
    # Clean possibly stale markers
    $_[0] =~ s{^<!--AutoSection/(\S+?)/([^/]+)/(\d+)-->}{};
}

sub selectSection {
    ### my ( $text, $selectedID ) = @_;
    my $selectedID = $_[1];

    my $removed = {};
    _takeOutBlocks($_[0], $removed);

    my $context = {
        selectedLevel => 0,
        isSelected    => 0,
        nextID        => 0,
        changed       => 0,
    };

    $_[0] =~ s/(.*(?:\n|$))/_selectSection($1, $selectedID, $context)/ge;
    $context->{changed} = 1 if $_[0] =~ s/\s+$//;
    _putBackBlocks($_[0], $removed);
    return $context->{changed};
}

sub _selectSection {
    my ($line, $selectedID, $context) = @_;

    if ($line =~ /$headingRegex/o) {
        my ($level, $prefix, $notoc, $body, $suffix) = _parseHeading($line);
        my $currentID = $context->{nextID}++;

        if ($currentID < $selectedID) {
            $context->{isSelected} = 0;
        } elsif ($currentID == $selectedID) {
            $context->{isSelected} = 1;
            $context->{selectedLevel} = $level;
        } elsif ($context->{isSelected}) {
            if ($context->{selectedLevel} >= $level) {
                $context->{isSelected} = 0;
            }
        }
    }

    if ($context->{isSelected}) {
        return $line;
    } else {
        $context->{changed} = 1;
        return '';
    }
}

sub replaceSection {
    ### my ( $text, $selectedID, $newSectionText ) = @_;
    my $selectedID = $_[1];

    my $removed = {};
    _takeOutBlocks($_[0], $removed);

    my $context = {
        selectedLevel => 0,
        isSelected    => 0,
        nextID        => 0,
        newTextRef    => \$_[2],
        changed       => 0,
    };

    $_[0] =~ s/(.*(?:\n|$))/_replaceSection($1, $selectedID, $context)/ge;
    $_[0] =~ s/^\s+|\s+$//g;
    _putBackBlocks($_[0], $removed);
    return $context->{changed};
}

sub _replaceSection {
    my ($line, $selectedID, $context) = @_;

    if ($line =~ /$headingRegex/o) {
        my ($level, $prefix, $notoc, $body, $suffix) = _parseHeading($line);
        my $currentID = $context->{nextID}++;

        if ($currentID < $selectedID) {
            $context->{isSelected} = 0;
        } elsif ($currentID == $selectedID) {
            $context->{isSelected} = 1;
            $context->{selectedLevel} = $level;
            my $newTextRef = $context->{newTextRef};
            return $$newTextRef ? $$newTextRef."\n\n" : '';
        } elsif ($context->{isSelected}) {
            if ($context->{selectedLevel} >= $level) {
                $context->{isSelected} = 0;
            }
        }
    }

    if ($context->{isSelected}) {
        $context->{changed} = 1;
        return '';
    } else {
        return $line;
    }
}

sub _parseHeading {
    my ($line) = @_;
    my $level = 0;
    my $prefix = '';
    my $notoc = '';
    my $body = '';
    my $suffix = '';

    if ($line =~ /^(---+(\++|\#+))(.*)$/) {
        $prefix = $1;
        $level = length($2);
        $body = $3;
    } elsif ($line =~ /^(<h([1-6])>)(.+?)(<\/h\2>.*)$/i) {
        $prefix = $1;
        $level = $2;
        $body = $3;
        $suffix = $4;
    }

    $level = 2 if $level == 1;

    if ($body =~ /(\s*$notocRegex)(.*)/o) {
        $notoc = $1;
        $body = $3;
    }

    return ($level, $prefix, $notoc, $body, $suffix);
}

sub _takeOutBlocks {
    my $renderer = $TWiki::Plugins::SESSION->renderer;
    my $removed = $_[1];
    $_[0] = $renderer->takeOutBlocks( $_[0], 'verbatim', $removed );
    $_[0] = $renderer->takeOutBlocks( $_[0], 'literal', $removed );
    $_[0] = $renderer->takeOutBlocks( $_[0], 'pre', $removed );
}

sub _putBackBlocks {
    my $renderer = $TWiki::Plugins::SESSION->renderer;
    my $removed = $_[1];
    $renderer->putBackBlocks( \$_[0], $removed, 'pre' );
    $renderer->putBackBlocks( \$_[0], $removed, 'literal' );
    $renderer->putBackBlocks( \$_[0], $removed, 'verbatim' );
}

1;
