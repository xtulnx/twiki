use strict;

package AutoSectionsPlugin::TestSelectSection;
use base qw(TWikiFnTestCase);

use AutoSectionsPluginUtil qw(removeIndent);
use TWiki::Plugins::AutoSectionsPlugin::Core qw(selectSection);

my $web = 'TestCases';
my $topic = 'AutoSectionsTestTopic';

sub test_no_headings {
    my $this = shift;
    my $text = "Test";
    selectSection($text, 0);
    $this->assert_str_equals("", $text);
}

sub test_simple {
    my $this = shift;
    my $text = "---++ Test";
    selectSection($text, 0);
    $this->assert_str_equals("---++ Test", $text);
}

sub test_multiple_headings {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0
        ---+++ h3-1
           * section 1
        ---+++ h3-2
           * section 2
        ---++ h2-2
           * section 3
        ---+++ h3-3
           * section 4
        ---+++ h3-4
           * section 5
    ));

    selectSection($text, 2);

    $this->assert_str_equals(removeIndent(qq(
        ---+++ h3-2
           * section 2
    )), $text);
}

sub test_nested_headings {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0
        ---+++ h3-1
           * section 1
        ---+++ h3-2
           * section 2
        ---++ h2-2
           * section 3
        ---+++ h3-3
           * section 4
        ---+++ h3-4
           * section 5
        ---++ h2-2
           * section 6
    ));

    selectSection($text, 3);

    $this->assert_str_equals(removeIndent(qq(
        ---++ h2-2
           * section 3
        ---+++ h3-3
           * section 4
        ---+++ h3-4
           * section 5
    )), $text);
}

sub test_verbatim {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0
        <verbatim>
        ---+++ h3-1
           * in verbatim
        ---+++ h3-2
           * in verbatim
        </verbatim>
        ---++ h2-2
           * section 1
        <verbatim>
        ---++ h2-3
           * in verbatim
        </verbatim>
        ---+++ h3-3
           * section 2
        ---++ h2-4
           * section 3
    ));

    selectSection($text, 1);

    $this->assert_str_equals(removeIndent(qq(
        ---++ h2-2
           * section 1
        <verbatim>
        ---++ h2-3
           * in verbatim
        </verbatim>
        ---+++ h3-3
           * section 2
    )), $text);
}

1;
