use strict;

package AutoSectionsPlugin::TestMarkHeadings;
use base qw(TWikiFnTestCase);

use AutoSectionsPluginUtil qw(removeIndent);
use TWiki::Plugins::AutoSectionsPlugin::Core qw(markHeadings);

my $web = 'TestCases';
my $topic = 'AutoSectionsTestTopic';
my $mark = "AutoSection/$web/$topic";

sub test_no_headings {
    my $this = shift;
    my $text = "Test";
    markHeadings($text, $topic, $web);
    $this->assert_str_equals("Test", $text);
}

sub test_simple {
    my $this = shift;
    my $text = "---++ Test";
    markHeadings($text, $topic, $web);
    $this->assert_str_equals("<!--$mark/0/2-->\n---++ Test", $text);
}

sub test_multiple_headings {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
        ---+++ h3-1
        ---+++ h3-2
        ---++ h2-2
        ---+++ h3-3
        ---+++ h3-4
    ));

    markHeadings($text, $topic, $web);

    $this->assert_str_equals(removeIndent(qq(
        <!--$mark/0/2-->\n---++ h2-1
        <!--$mark/1/3-->\n---+++ h3-1
        <!--$mark/2/3-->\n---+++ h3-2
        <!--$mark/3/2-->\n---++ h2-2
        <!--$mark/4/3-->\n---+++ h3-3
        <!--$mark/5/3-->\n---+++ h3-4
    )), $text);
}

sub test_verbatim {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
        <verbatim>
        ---+++ h3-1
        ---+++ h3-2
        </verbatim>
        ---++ h2-2
        <verbatim>
        ---+++ h3-3
        </verbatim>
        ---+++ h3-4
    ));

    markHeadings($text, $topic, $web);

    $this->assert_str_equals(removeIndent(qq(
        <!--$mark/0/2-->\n---++ h2-1
        <verbatim>
        ---+++ h3-1
        ---+++ h3-2
        </verbatim>
        <!--$mark/1/2-->\n---++ h2-2
        <verbatim>
        ---+++ h3-3
        </verbatim>
        <!--$mark/2/3-->\n---+++ h3-4
    )), $text);
}

sub test_notoc {
    my $this = shift;

    my $text = removeIndent(qq(
        ---+!! h1
        ---++ h2-1
        ---+++ %NOTOC% h3-1
        ---+++ h3-2
        ---++ !!! h2-2
        ---+++ h3-3
        ---+++ h3-4
    ));

    markHeadings($text, $topic, $web);

    $this->assert_str_equals(removeIndent(qq(
        <!--$mark/0/2-->\n---+!! h1
        <!--$mark/1/2-->\n---++ h2-1
        <!--$mark/2/3-->\n---+++ %NOTOC% h3-1
        <!--$mark/3/3-->\n---+++ h3-2
        <!--$mark/4/2-->\n---++ !!! h2-2
        <!--$mark/5/3-->\n---+++ h3-3
        <!--$mark/6/3-->\n---+++ h3-4
    )), $text);
}

1;
