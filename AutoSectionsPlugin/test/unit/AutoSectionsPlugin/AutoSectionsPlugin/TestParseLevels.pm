use strict;

package AutoSectionsPlugin::TestParseLevels;
use base qw(TWikiFnTestCase);

use AutoSectionsPluginUtil qw(removeIndent);
use TWiki::Plugins::AutoSectionsPlugin::Core qw(parseLevels);

my $web = 'TestCases';
my $topic = 'AutoSectionsTestTopic';
my $mark = "AutoSection/$web/$topic";

sub test_empty {
    my $this = shift;
    $this->assert_deep_equals(parseLevels(''), []);
}

sub test_simple {
    my $this = shift;
    $this->assert_deep_equals(parseLevels('2, 3, 4'), [undef, undef, 0, 1, 2, undef, undef, undef, undef]);
}

sub test_reverse {
    my $this = shift;
    $this->assert_deep_equals(parseLevels('4-6, 1-3'), [undef, 1, 1, 1, 0, 0, 0]);
}

sub test_default {
    my $this = shift;
    $this->assert_deep_equals(parseLevels('1-2, 3, 4, 5, 6'), [undef, 0, 0, 1, 2, 3, 4, 5]);
}

1;
