use strict;

package AutoSectionsPlugin::TestReplaceSection;
use base qw(TWikiFnTestCase);

use AutoSectionsPluginUtil qw(removeIndent);
use TWiki::Plugins::AutoSectionsPlugin::Core qw(replaceSection);

my $web = 'TestCases';
my $topic = 'AutoSectionsTestTopic';

sub test_no_headings {
    my $this = shift;
    my $text = "Test";
    replaceSection($text, 0, "Modified");
    $this->assert_str_equals("Test", $text);
}

sub test_simple {
    my $this = shift;
    my $text = "---++ Test";
    replaceSection($text, 0, "---++ Modified\n   * Test\n");
    $this->assert_str_equals("---++ Modified\n   * Test", $text);
}

sub test_multiple_headings {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0

        ---+++ h3-1
           * section 1

        ---+++ h3-2
           * section 2

        ---++ h2-2
           * section 3

        ---+++ h3-3
           * section 4

        ---+++ h3-4
           * section 5
    ));

    replaceSection($text, 2, removeIndent(qq(
        ---+++ h3-2 modified
           * modified section 2
    )));

    $this->assert_str_equals(removeIndent(qq(
        ---++ h2-1
           * section 0

        ---+++ h3-1
           * section 1

        ---+++ h3-2 modified
           * modified section 2

        ---++ h2-2
           * section 3

        ---+++ h3-3
           * section 4

        ---+++ h3-4
           * section 5
    )), $text);
}

sub test_nested_headings {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0

        ---+++ h3-1
           * section 1

        ---+++ h3-2
           * section 2

        ---++ h2-2
           * section 3

        ---+++ h3-3
           * section 4

        ---+++ h3-4
           * section 5

        ---++ h2-3
           * section 6
    ));

    replaceSection($text, 3, removeIndent(qq(
        ---++ h2-2 modified
           * modified section 3

        ---+++ h3-3 modified
           * modified section 4

        ---+++ h3-4 modified
           * modified section 5
    )));

    $this->assert_str_equals(removeIndent(qq(
        ---++ h2-1
           * section 0

        ---+++ h3-1
           * section 1

        ---+++ h3-2
           * section 2

        ---++ h2-2 modified
           * modified section 3

        ---+++ h3-3 modified
           * modified section 4

        ---+++ h3-4 modified
           * modified section 5

        ---++ h2-3
           * section 6
    )), $text);
}

sub test_verbatim {
    my $this = shift;

    my $text = removeIndent(qq(
        ---++ h2-1
           * section 0

        <verbatim>
        ---+++ h3-1
           * in verbatim

        ---+++ h3-2
           * in verbatim
        </verbatim>

        ---++ h2-2
           * section 1

        <verbatim>
        ---++ h2-3
           * in verbatim
        </verbatim>

        ---+++ h3-3
           * section 2

        ---++ h2-4
           * section 3
    ));

    replaceSection($text, 1, removeIndent(qq(
        ---++ h2-2 modified
           * modified section 1

        ---++ h2-3 added
           * added

        ---+++ h3-3 modified
           * modified section 2
    )));

    $this->assert_str_equals(removeIndent(qq(
        ---++ h2-1
           * section 0

        <verbatim>
        ---+++ h3-1
           * in verbatim

        ---+++ h3-2
           * in verbatim
        </verbatim>

        ---++ h2-2 modified
           * modified section 1

        ---++ h2-3 added
           * added

        ---+++ h3-3 modified
           * modified section 2

        ---++ h2-4
           * section 3
    )), $text);
}

1;
