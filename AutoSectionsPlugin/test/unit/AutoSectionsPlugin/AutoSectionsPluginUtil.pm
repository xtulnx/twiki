use strict;
package AutoSectionsPluginUtil;
use base qw(Exporter);

our @EXPORT_OK = (qw(
    removeIndent
));

sub removeIndent {
    my $text = shift;
    $text =~ s/^\n//;
    $text =~ /^([^\n\S]*)/;
    my $indent = $1;
    $text =~ s/^$indent//mg;
    $text =~ s/\s*$//;
    return $text;
}

1;
