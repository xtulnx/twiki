package AutoSectionsPluginSuite;
use base 'Unit::TestSuite';

sub include_tests {
    return qw(
        AutoSectionsPlugin::TestMarkHeadings
        AutoSectionsPlugin::TestSelectSection
        AutoSectionsPlugin::TestReplaceSection
    );
};

1;
