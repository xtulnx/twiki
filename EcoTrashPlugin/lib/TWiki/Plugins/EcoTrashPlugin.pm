# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 20114 Timothe Litt, litt [at] acm.org
# Plugin interface:
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::EcoTrashPlugin;

use warnings;
use strict;

use Carp;
use TWiki::Func;

# =========================
our $VERSION           = '$Rev$';
our $RELEASE           = '2014-01-31';
our $SHORTDESCRIPTION  = 'Trash manager to restore or permanently delete topics and attachments (manually and automated)';
our $NO_PREFS_IN_TOPIC = 1;

# =========================
my $debug = $TWiki::cfg{Plugins}{EcoTrashPlugin}{Debug} || 0;
my $core;
my $baseWeb;
my $baseTopic;

# =========================
sub initPlugin {
    ( $baseTopic, $baseWeb ) = @_;

    # check for Plugins.pm versions
    if ( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning(
            "Version mismatch between EcoTrashPlugin and Plugins.pm");
        return 0;
    }

    $core = undef;
    TWiki::Func::registerTagHandler( 'TRASH', \&VarTRASH );
    TWiki::Func::registerRESTHandler( 'Recycle', \&RECYCLE );

    # Plugin initialized correctly
    TWiki::Func::writeDebug(
        "- EcoTrashPlugin: initPlugin( " . "$baseWeb.$baseTopic ) is OK" )
      if $debug;

    return 1;
}

# All functions are dynamically loaded and invoked as object methods.

# =========================
sub AUTOLOAD {
    our $AUTOLOAD;

    unless ($core) {
        require TWiki::Plugins::EcoTrashPlugin::Core;
        $core =
          TWiki::Plugins::EcoTrashPlugin::Core->new( $baseWeb, $baseTopic );
    }

    my $method = $AUTOLOAD;
    $method =~ s/^.*:://;
    $method = $core->can($method);

    confess("EcoTrashPlugin: $AUTOLOAD is not implemented") unless ($method);

    unshift @_, $core;
    goto &$method;
}

1;

# EOF
