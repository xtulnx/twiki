# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2000-2003 Andrea Sterbini, a.sterbini@flashnet.it
# Copyright (C) 2001-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2001-2014 TWiki Contributors.
# All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# For licensing info read LICENSE file in the TWiki root.

=pod

---+ package SsoForwardCookiePlugin

This plugin forwards SSO cookies to external resources.

=cut

# Always use strict to enforce variable scoping
use strict;

package TWiki::Plugins::SsoForwardCookiePlugin;

# Name of this Plugin, only used in this module
our $pluginName = 'SsoForwardCookiePlugin';

require TWiki::Func;    # The plugins API
require TWiki::Plugins; # For the API version

# $VERSION is referred to by TWiki, and is the only global variable that
# *must* exist in this package. It should always be Rev enclosed in dollar
# signs so that TWiki can determine the checked-in status of the plugin.
# It is used by the build automation tools, so you should leave it alone.
our $VERSION = '$Rev$';

# This is a free-form string you can use to "name" your own plugin version.
# It is *not* used by the build automation tools, but is reported as part
# of the version number in PLUGINDESCRIPTIONS. Add a release date in ISO
# format (preferred) or a release number such as '1.3'.
our $RELEASE = '2013-06-06';

# One line description, is shown in the %SYSTEMWEB%.TextFormattingRules topic:
our $SHORTDESCRIPTION = 'This plugin forwards SSO cookies to external resources.';

# You must set $NO_PREFS_IN_TOPIC to 0 if you want your plugin to use preferences
# stored in the plugin topic. This default is required for compatibility with
# older plugins, but imposes a significant performance penalty, and
# is not recommended. Instead, use $TWiki::cfg entries set in LocalSite.cfg, or
# if you want the users to be able to change settings, then use standard TWiki
# preferences that can be defined in your Main.TWikiPreferences and overridden
# at the web and topic level.
our $NO_PREFS_IN_TOPIC = 1;

# Define other global package variables
our $debug;

my $handler;

sub initPlugin {
    my( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.5 ) {
        TWiki::Func::writeWarning( "Version mismatch between $pluginName and Plugins.pm" );
        return 0;
    }

    TWiki::Func::registerExternalHTTPHandler( \&handleExternalHTTPRequest );
    undef $handler;

    # Plugin correctly initialized
    return 1;
}

sub handleExternalHTTPRequest {
    my ($session, $url) = @_;
    my $this = ($handler ||= __PACKAGE__->new());

    my $headers = [];
    my $params = {};
    my $cookieForwarded = 0;

    $this->writeDebug("Begin hook: $url") if $this->debug;

    if (defined(my $matchedDomain = $this->matchDomain($url))) {
        if (@{$this->{cookieNames}}) {
            if ($this->debug) {
                my $names = join(', ', @{$this->{cookieNames}});
                $this->writeDebug("Forwardable cookie names = $names");
            }

            my $cookieJar = ($params->{cookie_jar} ||= do {
                require HTTP::Cookies;
                HTTP::Cookies->new();
            });

            my $domain = $this->{cookieDomain} || $matchedDomain;
            my $path = $this->{cookiePath} || '/';
            $this->addForwardedCookies($cookieJar, $this->{cookieNames}, $domain, $path);
            $cookieForwarded = 1;

        } else {
            $this->writeDebug("No cookie names are configured") if $this->debug;
        }
    }

    $this->writeDebug("End hook") if $this->debug;

    if (($this->{logging} == 1 && $cookieForwarded) || $this->{logging} == 2) {
        $this->writeAuditLog($session, $url, $cookieForwarded);
    }

    return ($headers, $params);
}

sub new {
    my ($class) = @_;
    my $cfg = $TWiki::cfg{Plugins}{SsoForwardCookiePlugin} || {};

    my @domains;
    $class->parseDomains($cfg->{Domains} || '', \@domains);

    my @cookieNames;

    for my $name (split /\s*,\s*/, ($cfg->{CookieNames} || '')) {
        $name =~ s/^\s+|\s+$//g;
        push @cookieNames, $name if $name ne '';
    }

    # RFC2965: Domain comparisons SHALL be case-insensitive
    # RFC2109: x.y.com domain-matches .y.com but not y.com
    my $domainPattern = join('|', map {
        (/^\./ ? '' : '^').quotemeta($_).'$';
    } @domains);

    my $localPattern;

    if (defined $cfg->{LocalDomain}) {
        my @parts;
        for my $part (split /\s*,\s*/, $cfg->{LocalDomain}) {
            $part =~ s/^\s+|\s+$//g;
            next if $part eq '';
            $part = ".$part" if $part !~ /^\./;
            push @parts, $part;
        }
        if (@parts) {
            $localPattern = join('|', map {quotemeta($_)} @parts);
        }
    }

    my $this = bless {
        debug         => TWiki::isTrue($cfg->{Debug}),
        domainPattern => (@domains ? qr{($domainPattern)}i : undef),
        cookieNames   => \@cookieNames,
        cookieDomain  => $cfg->{CookieDomain},
        cookiePath    => $cfg->{CookiePath},
        localPattern  => (defined $localPattern ? qr{$localPattern} : undef),
        mdrepoTable   => $cfg->{MdrepoTable},
        logging       => $cfg->{Logging} || 0,
    }, $class;

    if ($this->debug) {
        for my $key (keys %$cfg) {
            my $lhs = "\$cfg{Plugins}{SsoForwardCookiePlugin}{$key}";
            my $rhs = $cfg->{$key};
            $this->writeDebug("$lhs = $rhs");
        }
    }

    return $this;
}

sub parseDomains {
    my ($this, $input, $result) = @_;
    $result ||= [];
    my $systemWeb;

    for my $domain (split /\s*,\s*/, $input) {
        $domain =~ s/^\s+|\s*#.*|\s+$//g;
        next if $domain eq '';

        if ($domain =~ m{^topic:(.*)}i) {
            my $webTopic = $1;
            my $systemWeb = $TWiki::cfg{SystemWebName};

            if ($webTopic =~ /%/) {
                $webTopic = TWiki::Func::expandCommonVariables($webTopic, $systemWeb);
            }

            my ($web, $topic) = TWiki::Func::normalizeWebTopicName($systemWeb, $webTopic);
            my $wikiName = TWiki::Func::getWikiName();
            my ($meta, $text) = TWiki::Func::readTopic($web, $topic);

            if (TWiki::Func::checkAccessPermission('VIEW', $wikiName, $text, $topic, $web, $meta)) {
                if (defined $text && $text ne '') {
                    while ($text =~ /^\s*\|(.*?)\|(?:.*\|)?\s*$/mg) {
                        (my $name = $1) =~ s/^\s+|\s+$//g;
                        next if $name =~ /^\*.*\*$/; # exclude "| *Text* |"
                        push @$result, $name;
                    }
                } else {
                    $this->writeWarning("$domain cannot be read");
                }
            } else {
                $this->writeWarning("$domain is not accessible for user $wikiName");
            }
        } else {
            push @$result, $domain;
        }
    }

    return $result;
}

sub debug {
    my ($this) = @_;
    return $this->{debug};
}

sub writeDebug {
    my $this = shift;

    if ($this->{debug}) {
        TWiki::Func::writeDebug("SsoForwardCookiePlugin: $_") foreach @_;
    }
}

sub writeAuditLog {
    my ($this, $session, $url, $cookieForwarded) = @_;

    # Item7261: $user and/or $wikiName might be undefined
    my $wikiName = TWiki::Func::getWikiName();
    my $user = TWiki::Func::wikiToUserName($wikiName) || $wikiName ||
        'unknown';

    my $web = $session->{SESSION_TAGS}{WEB};
    my $topic = $session->{SESSION_TAGS}{TOPIC};
    my $baseWeb = $session->{SESSION_TAGS}{BASEWEB};
    my $baseTopic = $session->{SESSION_TAGS}{BASETOPIC};
    my $webTopic = defined($web) && defined($topic) ? "$web.$topic" :
        'noweb.notopic';

    if ($webTopic ne "$baseWeb.$baseTopic") {
        $webTopic .= " (base: $baseWeb.$baseTopic)";
    }

    my $note = $cookieForwarded ? 'FORWARDED' : 'NOT FORWARDED';
    TWiki::Func::writeDebug("SsoForwardCookiePlugin | $user | $webTopic | $url | $note |");
}

sub writeWarning {
    my $this = shift;
    TWiki::Func::writeWarning("SsoForwardCookiePlugin: $_") foreach @_;
}

sub matchDomain {
    my ($this, $url) = @_;
    my $pattern = $this->{domainPattern};
    my $mdrepoTable = $this->{mdrepoTable};

    $this->writeDebug("Analyzing URL: $url") if $this->debug;

    if (!$pattern && !$mdrepoTable) {
        $this->writeDebug("No SSO domains are configured") if $this->debug;
        return undef;
    }

    if ($this->debug) {
        $this->writeDebug("Pattern = $pattern") if $pattern;
        $this->writeDebug("MdrepoTable = $mdrepoTable") if $mdrepoTable;
    }

    my $host = $this->normalizeHost($url);
    return undef unless defined $host;

    $this->writeDebug("Target host: $host") if $this->debug;

    if ($pattern) {
        if ($host =~ $pattern) {
            my $matched = $1;
            $this->writeDebug("Matched domain: $matched") if $this->debug;
            return $matched;
        } else {
            $this->writeDebug("Domain not matched: $host") if $this->debug;
        }
    }

    if ($mdrepoTable) {
        if (my $session = $TWiki::Plugins::SESSION) {
            if (my $mdrepo = $session->{mdrepo}) {
                if (my $rec = $mdrepo->getRec($mdrepoTable, $host)) {
                    $this->writeDebug("Found domain in mdrepo: $host") if $this->debug;
                    return $host;
                } else {
                    $this->writeDebug("Domain not found in mdrepo: $host") if $this->debug;
                }
            } else {
                $this->writeDebug("Mdrepo is unavailable") if $this->debug;
            }
        } else {
            $this->writeDebug("TWiki session is unavailable") if $this->debug;
        }
    }

    $this->writeDebug("Did not match any domains") if $this->debug;
    return undef;
}

sub normalizeHost {
    my ($this, $url) = @_;

    # Retrieve scheme and host
    my $scheme = 'http';
    my $host;
   
    if (ref $url) {
        $host = $url->host;
        $host .= ':'.$url->port if $url->port;
        $scheme = $url->scheme || 'http';
    } else {
        if ($url =~ m{^(https?)://([^\/\?\#]+)}) {
            $scheme = $1;
            $host = $2;
            $host =~ s/.*@//;
        }
    };

    return undef if !defined $host;

    # Remove default port
    my $defaultPort = $scheme eq 'https' ? 443 : 80;
    $host =~ s/:$defaultPort$//;

    # Remove local domain
    if (my $pattern = $this->{localPattern}) {
        if ($host =~ s/($pattern)(:|$)/$2/) {
            my $local = $1;
            $this->writeDebug("Local domain detected: $local") if $this->debug;
        }
    }

    return $host;
}

sub addForwardedCookies {
    my ($this, $cookieJar, $cookieNames, $domain, $path) = @_;
    $path ||= '/';

    my $forwardAll = (@$cookieNames == 1 && $cookieNames->[0] eq '*');
    my $shouldForward = {map {$_ => 1} @$cookieNames};

    if ($this->debug) {
        $this->writeDebug("All cookies are forwarded") if $forwardAll;

        my $len = length($ENV{HTTP_COOKIE} || '');
        $this->writeDebug("HTTP_COOKIE env var has length $len");
    }

    my $cnt = 0;

    if ( $ENV{HTTP_COOKIE} ) {
        # Item7261: checks if $ENV{HTTP_COOKIE} is defined before working on it
        for my $c (split(/\s*;\s*/, $ENV{HTTP_COOKIE})) {
            my ($name, $value) = split(/=/, $c, 2);

            if ($forwardAll || $shouldForward->{$name}) {
                $this->writeDebug("- Adding cookie: $name") if $this->debug;
                $cookieJar->set_cookie(undef, $name, $value, $path, $domain);
                $cnt++;
            } else {
                $this->writeDebug("- Ignoring cookie: $name") if $this->debug;
            }
        }
    }

    $this->writeDebug("$cnt cookie(s) have been added") if $this->debug;
    return $cookieJar;
}

1;
