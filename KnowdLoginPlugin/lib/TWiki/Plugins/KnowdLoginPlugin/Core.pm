# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2014 Wave Systems Corp.
# Copyright (C) 2014 Peter Thoeny, peter[at]thoeny.org 
# Copyright (C) 2014 TWiki Contributors. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::KnowdLoginPlugin::Core;

# =========================
sub new {
    my ( $class, $debug ) = @_;

    my $this = {
          Debug           => $debug,
          ScoreThreshold  => $TWiki::cfg{Plugins}{KnowdLoginPlugin}{ScoreThreshold}
                          || 400,
          ClaimIdUrl      => $TWiki::cfg{Plugins}{KnowdLoginPlugin}{ClaimIdUrl}
                          || 'https://id.wave.com/upi/willcall/claim?ticket=',
          WorkAreaDir     => TWiki::Func::getWorkArea( 'KnowdLoginPlugin' ),
        };
    bless( $this, $class );
    $this->_writeDebug( "new: constructor" );

    return $this;
}

# =========================
sub VarKNOWD
{
    my ( $this, $params ) = @_;
    my $action  = $params->{_DEFAULT} || $params->{action} || '';
    $this->_writeDebug( "VarKNOWD($action)" );
    my $text = '';
    if( $action eq 'checkid' ) {
        $text = $this->_checkID( $params->{id} || 'unknown' );
    } elsif( $action eq 'getdevices' ) {
        $text = $this->_getDevices( $params->{user} );
    } elsif( $action eq 'registerdevice' ) {
        $text = $this->_registerDevice( $params->{id}, $params->{name}, $params->{user} );
    } elsif( $action eq 'deregisterdevice' ) {
        $text = $this->_deregisterDevice( $params->{id}, $params->{user} );
    } elsif( $action ) {
        $text = "KNOWD ERROR: Unrecogized action parameter $action";
    } else {
        $text = '';
    }
    $this->_writeDebug( "VarKNOWD($action) return: $text" );
    return $text;
}

# =========================
sub checkMagic
{
    my ( $this, $magic ) = @_;

    $this->_writeDebug( "checkMagic($magic)" );
    if( $magic ) {
        my @devices = $this->_searchDeviceData( 'magic', $magic );
        if( @devices ) {
            my $login = $devices[0]->{login};
            $this->_writeDebug( " - found magic for user $login" );
            return $login;
        }
    }
    $this->_writeDebug( " - magic not found" );
    return undef;
}

# =========================
sub setDeviceUser
{
    my ( $this, $id, $loginName ) = @_;

    $this->_writeDebug( "_setDeviceUser($id, $loginName)" );
    unless( $id && $loginName ) {
        return 'KNOWD ERROR: Setting a device user requires id and user parameter';
    }
    unless( TWiki::Func::getContext()->{authenticated} ) {
        return 'KNOWD ERROR: Setting a device user can only be done once authenticated';
    }
    $id =~ /^(.*)$/; $id = $1;
    my $wikiName = TWiki::Func::getWikiName( $loginName );
    my $deviceData = $this->_readDeviceData( $id );
    if( $deviceData ) {
        if(  $deviceData->{wikiname} && $wikiName ne $deviceData->{wikiname} ) {
            return "KNOWD ERROR: Device $id is already registered to "
                 . '%USERSWEB%.' . $deviceData->{wikiname};
        }
        $deviceData->{login}    = $loginName;
        $deviceData->{wikiname} = $wikiName;
        $this->_saveDeviceData( $id, $deviceData );

    } else {
        $deviceData = {
          login    => $loginName,
          wikiname => $wikiName,
          name     => '',
          magic    => '',
          score    => 0,
          setupurl => '',
          rtime    => 0
        };
        $this->_saveDeviceData( $id, $deviceData );
    }
    return '';
}

# =========================
sub _registerDevice
{
    my ( $this, $id, $name, $user ) = @_;

    $this->_writeDebug( "_registerDevice($id, $name)" );
    unless( $id && $name ) {
        return 'KNOWD ERROR: Registering a device requires id and name parameters';
    }
    unless( TWiki::Func::getContext()->{authenticated} ) {
        return 'KNOWD ERROR: Registering a device can only be done once authenticated';
    }
    my $authWikiName = TWiki::Func::getWikiName();
    my $isAdmin = TWiki::Func::isAnAdmin( $authWikiName );
    my $wikiName = TWiki::Func::getWikiName( $user );
    my $loginName = TWiki::Func::wikiToUserName( $wikiName );
    if( $authWikiName ne $wikiName && !$isAdmin ) {
        return 'KNOWD ERROR: Only the device owner or an administrator can register a device';
    }
    my $deviceData = $this->_readDeviceData( $id );
    unless( $deviceData ) {
        return "KNOWD ERROR: Device $id does not exist";
    }
    if( $deviceData->{wikiname} && $wikiName ne $deviceData->{wikiname} ) {
        return "KNOWD ERROR: Device $id is already registered to "
            . '%USERSWEB%.' . $deviceData->{wikiname};
    }
    my $msg = "KNOWD NOTE: Device $id has been updated";
    $msg = "KNOWD NOTE: Device $id has been registered" unless( $deviceData->{name} );
    $deviceData->{login}    = $loginName;
    $deviceData->{wikiname} = $wikiName;
    $deviceData->{name}     = $name; # set or change name of registered device
    $deviceData->{rtime}    = time();
    $this->_saveDeviceData( $id, $deviceData );
    return $msg;
}

sub _deregisterDevice
{
    my ( $this, $id, $user ) = @_;

    $this->_writeDebug( "_deregisterDevice($id)" );
    unless( $id ) {
        return 'KNOWD ERROR: Deregistering a device requires an id parameter';
    }
    unless( TWiki::Func::getContext()->{authenticated} ) {
        return 'KNOWD ERROR: Deregistering a device can only be done once authenticated';
    }
    my $authWikiName = TWiki::Func::getWikiName();
    my $isAdmin = TWiki::Func::isAnAdmin( $authWikiName );
    my $wikiName = TWiki::Func::getWikiName( $user );
    my $loginName = TWiki::Func::wikiToUserName( $wikiName );
    if( $authWikiName ne $wikiName && !$isAdmin ) {
        return 'KNOWD ERROR: Only the device owner or an administrator can deregister a device';
    }
    my $deviceData = $this->_readDeviceData( $id );
    if( !$deviceData || $deviceData->{name} eq '' ) {
        return "KNOWD ERROR: Device $id does not exist";
    }
    if( $wikiName ne $deviceData->{wikiname} ) {
        return 'KNOWD ERROR: Device owner ' . $deviceData->{wikiname}
             . ' and specified user must be the same';
    }
    $deviceData->{login}    = '';
    $deviceData->{wikiname} = '';
    $deviceData->{name}     = '';
    $deviceData->{magic}    = '';
    $deviceData->{score}    = 0 unless( $deviceData->{score} );
    $deviceData->{setupurl} = '';
    $deviceData->{rtime}    = 0;
    $this->_saveDeviceData( $id, $deviceData );
    return "KNOWD NOTE: Device $id has been removed";
}

# =========================
sub _getDevices
{
    my ( $this, $user ) = @_;

    my $authWikiName = TWiki::Func::getWikiName();
    my $isAdmin = TWiki::Func::isAnAdmin( $authWikiName );
    my $wikiName = TWiki::Func::getWikiName( $user );
    my $loginName = TWiki::Func::wikiToUserName( $wikiName );
    $this->_writeDebug( "_getDevices($wikiName)" );
    if( $authWikiName ne $wikiName && !$isAdmin ) {
        return 'KNOWD ERROR: Only the device owner or an administrator can see devices';
    }
    my @devices = $this->_searchDeviceData( 'wikiname', $wikiName );
    return '' unless( scalar @devices );
    my $tml = '%CALCULATE{';
    my @ids = ();
    foreach my $deviceData ( @devices ) {
        my $id = $deviceData->{id};
        push( @ids, $id );
        $tml .= '$SETHASH(knowd-login, '    . $id . ', ' . $deviceData->{login}    . ')';
        $tml .= '$SETHASH(knowd-wikiname, ' . $id . ', ' . $deviceData->{wikiname} . ')';
        $tml .= '$SETHASH(knowd-name, '  . $id . ", '''" . $deviceData->{name}  . "''')";
        $tml .= '$SETHASH(knowd-score, '    . $id . ', ' . $deviceData->{score}    . ')';
        $tml .= '$SETHASH(knowd-setupurl, ' . $id . ', ' . $deviceData->{setupurl} . ')';
        $tml .= '$SETHASH(knowd-atime, '    . $id . ', ' . $deviceData->{atime}    . ')';
        $tml .= '$SETHASH(knowd-rtime, '    . $id . ', ' . $deviceData->{rtime}    . ')';
    }
    $tml .= '}%';
    # Execute the CALCULATE to set the hash variables, after that the TML is no longer needed:
    TWiki::Func::expandCommonVariables( $tml );
    return join( ', ', sort @ids );
}

# =========================
sub _checkID
{
    my ( $this, $id ) = @_;

    $this->_writeDebug( "_checkID($id)" );
    my $url = $this->{ClaimIdUrl} . $id;
    my $response = TWiki::Func::getExternalResource( $url, {timeout => 10} );
    if( !$response->is_error()
      && $response->isa('HTTP::Response')
      && $response->content() =~ /"result": *"OK"/i
      && $response->content() =~ /\\?"id\\?": *\\?"(.*?)\\?"/i ) {
        # Sample response:
        # {"message":"", "result":"OK", "info":"{\"id\":\"a27d00fc-d4f4-4ac1-9a20-
        # dd748d0c2954\", \"score\":244, \"needsSetup\":false, \"setupURL\":null}"}
        my $id = $1;
        my $deviceData = $this->_readDeviceData( $id );
        if( $response->content() =~ /\\?"score\\?": *([0-9]+)/i ) {
            $deviceData->{score} = $1;
        }
        $deviceData->{setupurl} = '';
        if( $response->content() =~ /\\?"setupURL\\?":[ \"]+([^\"]+)/i ) {
            $deviceData->{setupurl} = $1;
        }
        $deviceData->{magic} = '';
        $text = '{"result":"OK","message":""'
              . ',"id":"'.$id.'"'
              . ',"score":'.$deviceData->{score}
              . ',"setupurl":"'.$deviceData->{setupurl}.'"'
              . ',"name":"'.$deviceData->{name}.'"';
        if( $deviceData->{name} && $deviceData->{score} >= $this->{ScoreThreshold} ) {
            # Device is enrolled & has good score: Tell client to login automatically.
            # Save a magic number so that automatic login cannot be spoofed.
            my $random = rand();
            $random =~ s/^0\./m/;
            $deviceData->{magic} = $random;
            $text .= ',"magic":"'.$random.'"'
        }
        $this->_saveDeviceData( $id, $deviceData );
        $text .= '}';

    } else {
        $text = '{"result":"error","message":"Redemption of claim ticket failed, '
              . $response->code() . ', '
              . $response->message() . '"}';
    }
    $this->_writeDebug( " action=checkid, return: $text" );
    return $text;
}

# =========================
sub _searchDeviceData {
    my ( $this, $key, $value ) = @_;

    my @devices = ();
    if( opendir( DIR, $this->{WorkAreaDir} ) ) {
        my @files = sort
                    grep { /^id-.*\.txt$/ }
                    readdir(DIR);
        closedir DIR;
        for my $id ( @files ) {
            $id =~ s/^id-(.*)\.txt$/$1/;
            my $deviceData = $this->_readDeviceData( $id );
            my $v = $deviceData->{$key};
            if( $v && $v eq $value ) {
                push( @devices, $deviceData );
            }
        }
    }
    return @devices;
}

# =========================
sub _readDeviceData
{
    my ( $this, $id ) = @_;
    my $deviceData;
    return $deviceData unless( $id );
    $this->_writeDebug( "_readDeviceData($id)" );
    my $fileName = $this->{WorkAreaDir} . "/id-$id.txt";
    my $found = 0;
    foreach my $line ( split( /[\n\r]/, TWiki::Func::readFile( $fileName ) ) ) {
        if( $line =~ /^([\w]+): ([^\n\r]*)/ ) {
            $deviceData->{$1} = $2;
            $found = +1;
        }
    }
    $deviceData->{id} = $id if( $found );
    # use Data::Dumper;
    # $this->_writeDebug( Dumper($deviceData) );
    return $deviceData;
}

# =========================
sub _saveDeviceData
{
    my ( $this, $id, $deviceData ) = @_;
    return unless( $id );
    $this->_writeDebug( "_saveDeviceData($id)" );
    $deviceData->{atime} = time();
    my $fileName = $this->{WorkAreaDir} . "/id-$id.txt";
    my $text = "# This file is generated, do not modify\n";
    foreach my $key ( sort grep { !/^id$/ } keys %{$deviceData} ) {
        $text .= "$key: " . $deviceData->{$key} . "\n";
    }
    # $this->_writeDebug( " \n=====data=====\n$text=====end=====" );
    TWiki::Func::saveFile( $fileName, $text );
}

# =========================
sub _writeDebug
{
    my ( $this, $msg ) = @_;
    return unless( $this->{Debug} );
    TWiki::Func::writeDebug( "- KnowdLoginPlugin::Core::$msg" );
}

# =========================
1;
