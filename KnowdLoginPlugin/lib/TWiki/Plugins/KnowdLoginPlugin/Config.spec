# ---+ Extensions
# ---++ Knowd Login Plugin
# This is the configuration of the <b>KnowdLoginPlugin</b>.
# <p />
# To use Knowd for authentication you need to:
# <br />- set the {LoginManager} to <b>TWiki::LoginManager::KnowdLogin</b>,
# <br />- set {Register}{AllowLoginName} to <b>1</b>
# <br />These settings can be found in the <b>Security setup</b> section.

# **STRING 10**
# Trust score threshold to allow user to login without a password.
$TWiki::cfg{Plugins}{KnowdLoginPlugin}{ScoreThreshold} = '400';

# **STRING 80**
# URL to claim an ID and trust score based on the claim ticket.
$TWiki::cfg{Plugins}{KnowdLoginPlugin}{ClaimIdUrl} = 'https://id.wave.com/upi/willcall/claim?ticket=';

# **BOOLEAN**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{KnowdLoginPlugin}{Debug} = 0;

# **PERL**
# This setting is required to enable executing the knowd script from the bin directory
$TWiki::cfg{SwitchBoard}{knowd} = [
          'TWiki::Plugins::KnowdLoginPlugin',
          'knowd',
          {
            'knowd' => 1
          }
        ];

1;
