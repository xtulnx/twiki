# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2014 Wave Systems Corp.
# Copyright (C) 2014 Peter Thoeny, peter[at]thoeny.org 
# Copyright (C) 2014 TWiki Contributors. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::KnowdLoginPlugin;

# =========================
our $VERSION = '$Rev$';
our $RELEASE = '2014-04-21';
our $SHORTDESCRIPTION = 'Knowd - login without password on devices that have a high enough trust score';
our $NO_PREFS_IN_TOPIC = 1;

# =========================
our $debug = $TWiki::cfg{Plugins}{KnowdLoginPlugin}{Debug} || 0;
our $web;
our $topic;
our $core;

# =========================
sub initPlugin
{
    ( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning( "Version mismatch between KnowdLoginPlugin and Plugins.pm" );
        return 0;
    }

    TWiki::Func::registerTagHandler( 'KNOWD', \&_KNOWD );

    # Plugin correctly initialized
    TWiki::Func::writeDebug( "- TWiki::Plugins::KnowdLoginPlugin::initPlugin( $web.$topic ) is OK" ) if $debug;

    return 1;
}

# =========================
sub _KNOWD
{
    my ( $session, $params ) = @_;

    # Lazy loading, e.g. compile core module only when required
    unless( $core ) {
        require TWiki::Plugins::KnowdLoginPlugin::Core;
        $core = new TWiki::Plugins::KnowdLoginPlugin::Core( $debug );
    }
    return $core->VarKNOWD( $params );
}

# =========================
#   Called by TWiki::LoginManager::KnowdLogin
sub checkMagic
{
    my ( $magic ) = @_;

    # Lazy loading, e.g. compile core module only when required
    unless( $core ) {
        require TWiki::Plugins::KnowdLoginPlugin::Core;
        $core = new TWiki::Plugins::KnowdLoginPlugin::Core( $debug );
    }
    return $core->checkMagic( $magic );
}

# =========================
#   Called by TWiki::LoginManager::KnowdLogin
sub setDeviceUser
{
    my ( $id, $user ) = @_;

    # Lazy loading, e.g. compile core module only when required
    unless( $core ) {
        require TWiki::Plugins::KnowdLoginPlugin::Core;
        $core = new TWiki::Plugins::KnowdLoginPlugin::Core( $debug );
    }
    return $core->setDeviceUser( $id, $user );
}

# =========================
#   Invoked by bin/knowd
sub knowd {
    my $session = shift;

    unless( $core ) {
        require TWiki::Plugins::KnowdLoginPlugin::Core;
        $core = new TWiki::Plugins::KnowdLoginPlugin::Core( $debug );
    }
    my $query = TWiki::Func::getCgiQuery();
    my $params;
    $params->{action} = $query->param('action') || '';
    $params->{id}     = $query->param('id') || '';
    my $text = $core->VarKNOWD( $params );
    TWiki::Func::writeDebug( "- knowd script return: $text" ) if $debug;
    # FIXME: Func API does not work
    ## TWiki::Func::writeHeader();
    ## print $text;
    $TWiki::Plugins::SESSION->writeCompletePage( $text );
}

# =========================
1;
