# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org 
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::TWikiAppsPlugin;

# =========================
our $VERSION = '$Rev$';
our $RELEASE = '2013-03-13';
our $SHORTDESCRIPTION = 'Manage TWiki application packaging and installation';
our $NO_PREFS_IN_TOPIC = 1;

# =========================
my $debug = $TWiki::cfg{Plugins}{TWikiAppsPlugin}{Debug} || 0;
my $core;
my $baseWeb;
my $baseTopic;

# =========================
sub initPlugin {
    ( $baseTopic, $baseWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning( "Version mismatch between NotifyAuthorsPlugin and Plugins.pm" );
        return 0;
    }

    $core = undef;
    TWiki::Func::registerTagHandler( 'TWIKIAPPS', \&_TWIKIAPPS );

    # Plugin correctly initialized
    TWiki::Func::writeDebug( "- TWikiAppsPlugin: initPlugin( "
      . "$baseWeb.$baseTopic ) is OK" ) if $debug;

    return 1;
}

# =========================
sub _TWIKIAPPS {
#   my ( $session, $params, $theTopic, $theWeb ) = @_;

    # Lazy loading, e.g. compile core module only when needed
    unless( $core ) {
        require TWiki::Plugins::TWikiAppsPlugin::Core;
        $core = new TWiki::Plugins::TWikiAppsPlugin::Core( $baseWeb, $baseTopic );
    }
    return $core->VarTWIKIAPPS( @_ );
}

# =========================
1;
