# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::TWikiAppsPlugin::Core;

my $debug = $TWiki::cfg{Plugins}{TWikiAppsPlugin}{Debug} || 0;

# =========================
sub new {
    my ( $class, $baseWeb, $baseTopic ) = @_;

    my $this = {
        baseWeb          => $baseWeb,
        baseTopic        => $baseTopic,
      };
    bless( $this, $class );
    TWiki::Func::writeDebug( "- TWikiAppsPlugin Core: Constructor" ) if $debug;

    my $query = TWiki::Func::getCgiQuery();
    if( $query && $query->param( 'twikiapps_action' ) ) {
        my $params;
        $params->{action} = $query->param( 'twikiapps_action' );
        $this->VarTWIKIAPPS( undef, $params, $topic, $web );
    }

    return $this;
}

# =========================
sub VarTWIKIAPPS {
    my ( $this, $session, $params, $topic, $web ) = @_;

    my $query = TWiki::Func::getCgiQuery();
    my $action = $params->{_DEFAULT} || $params->{action} || '';
    TWiki::Func::writeDebug( "- TWikiAppsPlugin TWIKIAPPS: action=$action" ) if $debug;
    $this->{text}     = '';
    $this->{note}     = '';
    $this->{error}    = '';
    $this->{redirect} = '';

    if( $action eq 'x' ) {
        $this->_actionX( $params );
    } elsif( $action eq 'y' ) {
        $this->_actionY( $web, $topic, $params );
    }

    if( $this->{redirect} ) {
        my ( $rWeb, $rTopic, $rParams ) = split( /, */, $this->{redirect} );
        my $url = TWiki::Func::getScriptUrl( $rWeb, $rTopic, 'view');
        $url .= '?' . $rParams if( $rParams );
        TWiki::Func::redirectCgiQuery( undef, $url, 0 );
    }

    my $text = $this->{text};
    my $style = 'margin: 0 0.5em 0 0; padding: 0.3em 0.5em;';
    if( $this->{note} ) {
        $text .= "<span style='background-color: #feffc2; $style'> "
               . $this->{note}
               . " [[$web.$topic][OK]] </span>";
    }
    if( $this->{error} ) {
        $text .= "<span style='background-color: #ff9594; $style'> *Error:* "
               . $this->{error}
               . " [[$web.$topic][OK]] </span>"
    }
    return $text;
}

# =========================
sub _actionX {
    my ( $this, $params ) = @_;
    $this->{text} .= 'X';
}

# =========================
sub _actionY {
    my ( $this, $web, $topic, $params ) = @_;
    $this->{text} .= 'Y';
}

# =========================
1;
