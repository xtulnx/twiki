# ---+ Extensions
# ---++ TWikiAppsPlugin

# **STRING 80**
# 
$TWiki::cfg{Plugins}{TWikiAppsPlugin}{Demo} = '';

# **BOOLEAN**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{TWikiAppsPlugin}{Debug} = 0;

1;
