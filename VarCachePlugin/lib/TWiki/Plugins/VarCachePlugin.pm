# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2004-2014 Peter Thoeny, peter[at]thoeny.org, TWiki.org
# Copyright (C) 2008-2014 TWiki Contributors
#
# For licensing info read LICENSE file in the TWiki root.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

# =========================
package TWiki::Plugins::VarCachePlugin;
use strict;

# =========================
our $VERSION = '$Rev: 27026 $';
our $RELEASE = '2014-02-22';

# =========================
our $SHORTDESCRIPTION = 'Cache TWiki variables in selected topics for faster page rendering';
our $NO_PREFS_IN_TOPIC = 1;
our $pluginName = 'VarCachePlugin';  # Name of this Plugin
our $web;
our $topic;
our $user;
our $installWeb;
our $debug;
our $unescapeInInclude;
our $existingTopics;
# =========================
our $escVarCacheToken = '-ESC-VARCACHE-';
our $noVarCacheToken  = 'VARCACHE_EXCLUDE_';

# =========================
sub initPlugin
{
    ( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.024 ) {
        TWiki::Func::writeWarning( "Version mismatch between $pluginName and Plugins.pm" );
        return 0;
    }

    # Get plugin debug flag
    $debug = TWiki::Func::getPreferencesFlag( "\U$pluginName\E_DEBUG" );

    # Plugin correctly initialized
    TWiki::Func::writeDebug( "- TWiki::Plugins::${pluginName}::initPlugin( $web.$topic ) is OK" ) if $debug;
    return 1;
}

# =========================
sub beforeCommonTagsHandler
{
### my ( $text, $topic, $web ) = @_;   # do not uncomment, use $_[0], $_[1]... instead

    TWiki::Func::writeDebug( "- ${pluginName}::beforeCommonTagsHandler( $_[2].$_[1] )" ) if $debug;

    return unless( $_[0] =~ /%VARCACHE/ );

    $_[0] =~ s/%VARCACHE(?:{(.*?)})?%/_handleVarCache( $_[2], $_[1], $1 )/ge;

    # if "save cache", escape %VARIABLES{}% enclosed in <varcache_exclude> tag
    $unescapeInInclude = 0;
    if( $_[0] =~ /%--VARCACHE\:save\001/ ) {
        $_[0] =~ s/<varcache_exclude>(.*?)<\/varcache_exclude>/_escapeExcludes( $1 )/geos;
    } else {
        $_[0] =~ s/\%$noVarCacheToken/\%/gos;
        $unescapeInInclude = 1;
    }

    # if "read cache", replace all text with marker, to be filled in afterCommonTagsHandler
    $_[0] =~ s/^.*(%--VARCACHE\:read\:.*?--%).*$/$1/os;
}

# =========================
sub commonTagsHandler
{
### my ( $text, $topic, $web, $inInclude ) = @_;   # do not uncomment, use $_[0], $_[1]... instead
    return unless( $_[3] && $unescapeInInclude );
    $_[0] =~ s/\%$noVarCacheToken/\%/gos;
}

# =========================
sub afterCommonTagsHandler
{
### my ( $text, $topic, $web ) = @_;   # do not uncomment, use $_[0], $_[1]... instead

    TWiki::Func::writeDebug( "- ${pluginName}::afterCommonTagsHandler( $_[2].$_[1] )" ) if $debug;

    $_[0] =~ s/<\/?varcache_exclude>//gos;

    return unless( $_[0] =~ /%--VARCACHE\:/ );

    if( $_[0] =~ /%--VARCACHE\:([a-z]+)\:?(.*?)(?:\001(.*?)\001)?--%/ ) {
        my $save = ( $1 eq "save" );
        my $age = $2 || 0;
        my $tag = $3;
        my $cacheFilename = _cacheFileName( $_[2], $_[1], $save );

        if( $save ) {
            # update cache
            TWiki::Func::saveFile( $cacheFilename, $_[0] );
            my $msg = _formatMsg( $_[2], $_[1], $tag );
            $_[0] =~ s/%--VARCACHE\:.*?--%/$msg/go;
            if( $_[0] =~ /($escVarCacheToken|$noVarCacheToken)/ ) {
                $_[0] =~ s/\%$noVarCacheToken/\%/gos;
                $_[0] = TWiki::Func::expandCommonVariables( _unescapeExcludes( $_[0] ), $_[1], $_[2] );
            }

            # cache addToHEAD info
            # FIXME: Enhance the TWiki::Func API to get the head info cleanly
            $cacheFilename = _cacheFileName( $_[2], $_[1], 0, 1 );
            my $htmlHeader = '';
            TWiki::Func::saveFile( $cacheFilename, '' ); # clear
            my $session = $TWiki::Plugins::SESSION;
            if( $session && $session->{_HTMLHEADERS} ) {
                $htmlHeader = join( "\n",
                  map { '<!--VARCACHE_HEAD_'.$_.'-->'.$session->{_HTMLHEADERS}{$_} }
                  keys %{$session->{_HTMLHEADERS}} );
            }
            TWiki::Func::saveFile( $cacheFilename, $htmlHeader );

        } else {
            # read cache
            $_[0] = TWiki::Func::readFile( $cacheFilename );
            my $msg = _formatMsg( $_[2], $_[1], $tag );
            $msg =~ s/\$age/_formatAge($age)/geo;
            $_[0] =~ s/%--VARCACHE.*?--%/$msg/go;
            if( $_[0] =~ /($escVarCacheToken|$noVarCacheToken)/ ) {
                $_[0] =~ s/\%$noVarCacheToken/\%/gos;
                $_[0] = TWiki::Func::expandCommonVariables( _unescapeExcludes( $_[0] ), $_[1], $_[2] );
            }

            # restore addToHEAD info from cache
            $cacheFilename = _cacheFileName( $_[2], $_[1], 0, 1 );
            my $htmlHeader = TWiki::Func::readFile( $cacheFilename );
            foreach ( split /<!--VARCACHE_HEAD_/, $htmlHeader ) {
                if( /^(.*?)-->(.+)$/s ) {
                    TWiki::Func::addToHEAD( $1, $2 );
                }
            }
        }
    }
}

# =========================
sub beforeSaveHandler
{
### my ( $text, $topic, $web, $error, $meta ) = @_;   # do not uncomment, use $_[0], $_[1]... instead

    my $webTopic = "$_[2].$_[1]";
    TWiki::Func::writeDebug( "- ${pluginName}::beforeSaveHandler( $webTopic )" ) if $debug;

    # check if topic already exists, needed in afterSaveHandler's invalidate action
    if( TWiki::Func::topicExists( $_[2], $_[1] ) ) {
        $existingTopics->{ $webTopic } = 1;
    }
}

# =========================
sub afterSaveHandler
{
### my ( $text, $topic, $web, $error, $meta ) = @_;   # do not uncomment, use $_[0], $_[1]... instead

    my $theTopic = $_[1];
    my $theWeb   = $_[2];
    my $theMeta  = $_[4];
    my $webTopic = "$theWeb.$theTopic";
    TWiki::Func::writeDebug( "- ${pluginName}::afterSaveHandler( $webTopic )" ) if $debug;

    return if(  $_[3] ); # no action if save error

    my $invalidateOnCreate = TWiki::Func::getPreferencesValue( "\U$pluginName\E_INVALIDATEONCREATE" ) || 'parent';
    my $invalidateOnUpdate = TWiki::Func::getPreferencesValue( "\U$pluginName\E_INVALIDATEONUPDATE" ) || 'include';

    if( ( $invalidateOnCreate =~ /parent/ && !$existingTopics->{ $webTopic } )
     || ( $invalidateOnUpdate =~ /parent/ && $existingTopics->{ $webTopic } ) ) {
        # invalidate parent's cache
        my $parent = $theMeta->getParent();
        if( $parent ) {
            my ( $aWeb, $aTopic ) = TWiki::Func::normalizeWebTopicName( $theWeb, $parent );
            _invalidateVarCache( $aWeb, $aTopic );
        }
    }

    my @topics = ();
    if( ( $invalidateOnCreate =~ /include/ && !$existingTopics->{ $webTopic } )
     || ( $invalidateOnUpdate =~ /include/ && $existingTopics->{ $webTopic } ) ) {
        # invalidate including topics' cache
        @topics = grep { !/^$theTopic$/ } TWiki::Func::getTopicList( $theWeb );
        my $regex = "\\\%INCLUDE{\\s*[\\\"\\']*\\b($theTopic|$webTopic)\\b";
        my $result = TWiki::Func::searchInWebContent( $regex, $theWeb, \@topics,
                                        { type => 'regex', files_without_match => 1 } );
        foreach my $aTopic ( keys %$result ) {
            _invalidateVarCache( $theWeb, $aTopic );
        }
    }

    if( ( $invalidateOnCreate =~ /backlinks?/ && !$existingTopics->{ $webTopic } )
     || ( $invalidateOnUpdate =~ /backlinks?/ && $existingTopics->{ $webTopic } ) ) {
        # invalidate backlink topics' cache
        @topics = grep { !/^$theTopic$/ } TWiki::Func::getTopicList( $theWeb ) unless( @topics );
        my $regex = "(^|[ \\s\\(]|\\[\\[)($theTopic|$webTopic)\\b";
        my $result = TWiki::Func::searchInWebContent( $regex, $theWeb, \@topics,
                                        { type => 'regex', files_without_match => 1 } );
        foreach my $aTopic ( keys %$result ) {
            _invalidateVarCache( $theWeb, $aTopic );
        }
    }
}

# =========================
sub _formatMsg
{
    my ( $theWeb, $theTopic, $msg ) = @_;

    $msg =~ s|<nop>||go;
    $msg =~ s|\$link|%SCRIPTURL{view}%/%WEB%/%TOPIC%?varcache=refresh|go;
    $msg =~ s|%ATTACHURL%|%PUBURL%/$installWeb/$pluginName|go;
    $msg =~ s|%ATTACHURLPATH%|%PUBURLPATH%/$installWeb/$pluginName|go;
    $msg = TWiki::Func::expandCommonVariables( $msg, $theTopic, $theWeb );
    return $msg;
}

# =========================
sub _formatAge
{
    my ( $age ) = @_;

    my $unit = "hours";
    if( $age > 24 ) {
        $age /= 24;
        $unit = "days";
    } elsif( $age < 1 ) {
        $age *= 60;
        $unit = "min";
        if( $age < 1 ) {
            $age *= 60;
            $unit = "sec";
        }
    }
    if( $age >= 3 || $unit eq "sec" ) {
        $age = int( $age );
        return "$age $unit";
    }
    return sprintf( "%1.1f $unit", $age );
}

# =========================
sub _escapeExcludes
{
    my ( $theText ) = @_;
    $theText =~ s/\%([A-Za-z])/\%$escVarCacheToken$1/go;
    $theText =~ s/}\%/}$escVarCacheToken\%/go;
    return $theText;
}

# =========================
sub _unescapeExcludes
{
    my ( $theText ) = @_;
    $theText =~ s/}$escVarCacheToken\%/}\%/go;
    $theText =~ s/\%$escVarCacheToken([A-Za-z])/\%$1/go;
    return $theText;
}

# =========================
sub _handleVarCache
{
    my ( $theWeb, $theTopic, $theArgs ) = @_;

    my $query = TWiki::Func::getCgiQuery();
    my $action = "check";
    if( $query ) {
        my $tmp = $query->param( 'varcache' ) || "";
        if( $tmp eq "refresh" ) {
            $action = "refresh";
        } else {
            $action = "" if( grep{ !/^(varcache|topic)$/ } $query->param );
        }
    }

    if( $action eq "check" ) {
        my $filename = _cacheFileName( $theWeb, $theTopic, 0 );
        if( -e $filename ) {
            my $now = time();
            my $cacheTime = (stat $filename)[9] || 10000000000;
            # CODE_SMELL: Assume file system for topics
            $filename = TWiki::Func::getDataDir() . "/$theWeb/$theTopic.txt";
            my $topicTime = (stat $filename)[9] || 10000000000;
            my $refresh = TWiki::Func::extractNameValuePair( $theArgs )
                       || TWiki::Func::extractNameValuePair( $theArgs, "refresh" );
            $refresh = TWiki::Func::getPreferencesValue( "\U$pluginName\E_REFRESH" ) unless( $refresh || $refresh eq '0' );
            $refresh = 24 unless( $refresh || defined $refresh && $refresh eq '0' );
            $refresh *= 3600;
            if( ( ( $refresh == 0 ) || ( $cacheTime >= $now - $refresh ) )
             && ( $cacheTime >= $topicTime ) ) {
                # add marker for afterCommonTagsHandler to read cached file
                my $paramMsg = TWiki::Func::extractNameValuePair( $theArgs, "cachemsg" )
                            || TWiki::Func::getPreferencesValue( "\U$pluginName\E_CACHEMSG" )
                            || 'This topic was cached $age ago ([[$link][refresh]])';
                $cacheTime = sprintf( "%1.6f", ( $now - $cacheTime ) / 3600 );
                return "%--VARCACHE\:read:$cacheTime\001$paramMsg\001--%";
            }
        }
        $action = "refresh";
    }

    if( $action eq "refresh" ) {
        # add marker for afterCommonTagsHandler to refresh cache file
        my $paramMsg = TWiki::Func::extractNameValuePair( $theArgs, "updatemsg" )
                    || TWiki::Func::getPreferencesValue( "\U$pluginName\E_UPDATEMSG" )
                    || 'This topic is now cached ([[$link][refresh]])';
        return "%--VARCACHE\:save\001$paramMsg\001--%";
    }

    # else normal uncached processing
    return "";
}

# =========================
sub _invalidateVarCache
{
    my ( $theWeb, $theTopic ) = @_;

    my $filename = _cacheFileName( $theWeb, $theTopic, 0 );
    unlink( $filename ) if( -e $filename );
    $filename = _cacheFileName( $theWeb, $theTopic, 0, 1 );
    unlink( $filename ) if( -e $filename );
}

# =========================
sub _cacheFileName
{
    my ( $web, $topic, $mkDir, $isHead ) = @_;

    my $dir = TWiki::Func::getWorkArea($pluginName) . "/$web";
    if( ( $mkDir ) && ( ! -e "$dir" ) ) {
        my $sumsk = umask( 002 );
        mkdir( $dir, $TWiki::cfg{RCS}{dirPermission} );
        umask( $sumsk );
    }

    return "$dir/${topic}_cache." . ($isHead? 'head' : 'txt');
}

# =========================
1;
