use strict;

# tests for the correct expansion of VAR

package Fn_VAR;

use base qw( TWikiFnTestCase );

use TWiki;
use Error qw( :try );
use Unit::Request;

sub new {
    my $self = shift()->SUPER::new('VAR', @_);
    return $self;
}

sub single_var {
    my ($this, $var, $expected) = @_;
    my $result = $this->{twiki}->handleCommonTags(
        $var, $this->{test_web}, $this->{test_topic});
    $this->assert_equals($expected, $result);
}

sub test_VAR {
    my $this = shift;

    my $result;

    $this->{twiki}->{store}->saveTopic(
        $this->{twiki}->{user}, $this->{test_web},
        'WebPreferences', <<SPLOT);
   * Set BLEEGLE = gibbut
SPLOT
    $this->{twiki}->{store}->saveTopic(
        $this->{twiki}->{user}, $this->{users_web},
        'WebPreferences', <<SPLOT);
   * Set BLEEGLE = frabbeque
   * Set GOPPM = 
SPLOT
    $this->{twiki}->{store}->saveTopic(
        $this->{twiki}->{user}, $this->{users_web},
        'TestGroup', <<SPLOT);
   * Set GROUP = TWikiAdminUser
SPLOT
    $this->{twiki}->{store}->saveTopic(
        $this->{twiki}->{user}, $this->{test_web},
        $this->{test_topic}, <<SPLOT);
   * Set BLEEGLE = onizuka
SPLOT

    $this->{twiki}->finish();
    my $query = new Unit::Request('');
     $query->method('POST');
    $query->path_info("/$this->{test_web}/$this->{test_topic}");
    $this->{twiki} = new TWiki(undef, $query);

    $this->single_var(qq(%VAR{"VAR"}%),
                      '');
    $this->single_var(qq(%VAR{"VAR" default="bar"}%),
                      'bar');
    $this->single_var(qq(%VAR{"BLEEGLE" web="$this->{users_web}"}%),
                      'frabbeque');
    $this->single_var(qq(%VAR{"GOPPM" web="$this->{users_web}" default="bar"}%),
                      '');
    $this->single_var(
      qq(%VAR{"GOPPM" web="$this->{users_web}" default="bar" ignorenull="on"}%),
                      'bar');
    $this->single_var(qq(%VAR{"BLEEGLE" web="$this->{test_web}"}%),
                      'gibbut');
    $this->single_var(qq(%VAR{"BLEEGLE"}%),
                      'onizuka');
    $this->single_var(qq(%VAR{"WEB"}%),
                      $this->{test_web});
    $this->single_var(qq(%VAR{"GROUP" topic="$this->{users_web}.TestGroup"}%),
                      'TWikiAdminUser');
    $this->single_var(
        qq(%VAR{"GROUP" web="$this->{users_web}" topic="TestGroup"}%),
                      'TWikiAdminUser');
    $this->single_var(qq(%VAR{"GROUP" topic="TestGroup"}%),
                      '');
    $this->single_var(qq(%VAR%),
                      '');
}

1;
