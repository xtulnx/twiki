# ---+ Extensions
# ---++ DoxygenImportPlugin

# **STRING 80**
# Name of template topic to use to generate topics. Can be Web or Web.TopicName.
# Default: WebTopicEditTemplate. The following variables are handled in template
# topic text: %DOXYGENTEXT% expands to generated text, %DOXYGENTITLE% to generated
# title. Template topic text is ignored if %DOXYGENTEXT% is missing.
$TWiki::cfg{Plugins}{DoxygenImportPlugin}{TemplateTopic} = '';

# **BOOLEAN**
# Log plugin actions. See output in data/logYYYYMM.txt
$TWiki::cfg{Plugins}{DoxygenImportPlugin}{LogAction} = 1;

# **BOOLEAN**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{DoxygenImportPlugin}{Debug} = 0;

1;
