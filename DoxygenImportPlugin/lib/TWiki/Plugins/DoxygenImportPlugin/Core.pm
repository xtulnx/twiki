# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013 Wave Systems Corp.
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

use Archive::Zip qw(:ERROR_CODES :CONSTANTS :PKZIP_CONSTANTS);

package TWiki::Plugins::DoxygenImportPlugin::Core;

my $webTopicRegex   = '[^' . $TWiki::regex{mixedAlphaNum} . '_\-\.\/]';
my $pluginName = 'DoxygenImportPlugin';
my $debug = $TWiki::cfg{Plugins}{$pluginName}{Debug} || 0;
my $AZ_OK = 0;

# Avoid recursion on upload if another plugin such as PublishWebPlugin uses
# expandCommonVariables in afterSaveHandler.
my $ignoreActionUrlParam = 0;

# =========================
sub new {
    my ( $class, $baseWeb, $baseTopic ) = @_;

    my $this = {
        TemplateTopic    => $TWiki::cfg{Plugins}{$pluginName}{TemplateTopic} || 'WebTopicEditTemplate',
        LogAction        => $TWiki::cfg{Plugins}{$pluginName}{LogAction},
        baseWeb          => $baseWeb,
        baseTopic        => $baseTopic,
      };
    bless( $this, $class );
    TWiki::Func::writeDebug( "- ${pluginName}::Core::new( $baseWeb, $baseTopic )" ) if $debug;

    unless( $ignoreActionUrlParam ) {
        my $query = TWiki::Func::getCgiQuery();
        if( $query && $query->param( 'doxygen_action' ) ) {
            my $params;
            $params->{action} = $query->param( 'doxygen_action' );
            $this->VarDOXYGENIMPORT( undef, $params, $baseTopic, $baseWeb );
        }
    }

    return $this;
}

# =========================
sub VarDOXYGENIMPORT {
    my ( $this, $session, $params, $topic, $web ) = @_;

    my $query = TWiki::Func::getCgiQuery();
    my $action = $params->{_DEFAULT} || $params->{action} || 'navigation';
    TWiki::Func::writeDebug( "- ${pluginName}::Core::VarDOXYGENIMPORT( action=$action )" ) if $debug;
    $this->{text}     = '';
    $this->{note}     = '';
    $this->{error}    = '';
    $this->{redirect} = '';

    if( $action eq 'navigation' ) {
        $this->_actionNavigation( $web, $topic, $params );
    } elsif( $action eq 'import' ) {
        $this->_actionImportForm( $web, $topic, $params, $query );
    } elsif( $action eq 'upload' ) {
        $this->_actionUpload( $query );
    }

    if( $this->{redirect} ) {
        my ( $rWeb, $rTopic, $rParams ) = split( /, */, $this->{redirect} );
        my $url = TWiki::Func::getScriptUrl( $rWeb, $rTopic, 'view');
        $rParams = '' unless( $rParams );
        if( $this->{error} ) {
            $rParams .= ';' if( $rParams );
            $rParams .= 'doxygen_error=' . _urlEncode( $this->{error} );
        }
        if( $this->{note} ) {
            $rParams .= ';' if( $rParams );
            $rParams .= 'doxygen_note=' . _urlEncode( $this->{note} );
        }
        $url .= '?' . $rParams if( $rParams );
        TWiki::Func::redirectCgiQuery( undef, $url, 0 ); # redirect and don't return
    }

    my $text = $this->{text};
    my $style = 'margin: 0 0.5em 0 0; padding: 0.3em 0.5em;';
    if( $this->{note} ) {
        $text .= "<span style='background-color: #feffc2; $style'> "
               . $this->{note}
               . " [[$web.$topic][OK]] </span>";
    }
    if( $this->{error} ) {
        $text .= "<span style='background-color: #ff9594; $style'> *Error:* "
               . $this->{error}
               . " [[$web.$topic][OK]] </span>";
    }
    return $text;
}

# =========================
sub _actionNavigation {
    my ( $this, $web, $topic, $params ) = @_;

    return unless TWiki::Func::attachmentExists( $web, $topic, 'index.html' );

    my $text = TWiki::Func::readAttachment( $web, $topic, 'index.html' );
    $this->{text} .= $text;
}

# =========================
sub _actionImportForm {
    my ( $this, $web, $topic, $params, $query ) = @_;
    my $text = '<div style="padding: 8px 10px; background-color: #f0f0f8; border: #a0a0a8;">' . "\n"
          . 'To import, upload a ZIP file containing all files of a library generated '
          . 'by Doxygen: <br />' . "\n"
          . '<form action="%SCRIPTURL{viewauth}%/%WEB%/%TOPIC%" method="post"'
          . ' enctype="multipart/form-data" onsubmit="'
          . "document.getElementById('upload').value='Please wait...'; "
          . "document.getElementById('upload').disabled=true; "
          . "document.getElementById('wait').style.visibility='visible';\">\n"
          . '<input type="file" name="zip" id="zip" size="60" class="twikiInputField"'
          . ' onchange="checkZip(1);" />' . "\n"
          . '<input type="hidden" name="doxygen_action" value="upload" />' . "\n"
          . '<input type="hidden" name="doxygen_topic" value="' . "$web.$topic" . '" />' . "\n";
    if( $params->{template} ) {
        $text .= '<input type="hidden" name="doxygen_template" value="'
              . $params->{template} . '" />' . "\n";
    }
    $text .= '<input type="submit" value="Upload &amp; Import" id="upload" class="twikiSubmit" />' . "\n"
          . '<span id="wait" style="visibility: hidden;"> %ICON{processing-bg}% </span>' . "\n"
          . '</form>' . "\n"
          . '</div>' . "\n"
          . '<script type="text/javascript">' . "\n"
          . 'function checkZip(showAlert) {' . "\n"
          . '  var fileElement   = document.getElementById("zip");' . "\n"
          . '  var submitElement = document.getElementById("upload");' . "\n"
          . '  var re = /\.zip$/i;' . "\n"
          . '  var result = fileElement.value.match(re);' . "\n"
          . '  if( result ) {' . "\n"
          . '    submitElement.disabled = false;' . "\n"
          . '    return true;' . "\n"
          . '  } else {' . "\n"
          . '    if(showAlert) { alert("Invalid file type, please select a .zip file"); };' . "\n"
          . '    submitElement.disabled = true;' . "\n"
          . '    return false;' . "\n"
          . '  }' . "\n"
          . '}' . "\n"
          . 'checkZip(0);' . "\n"
          . '</script>';
    $this->{text} .= $text;
    if( $query->param( 'doxygen_note' ) ) {
        $this->{note} .= $query->param( 'doxygen_note' );
    }
    if( $query->param( 'doxygen_error' ) ) {
        $this->{error} .= $query->param( 'doxygen_error' );
    }
}

# =========================
sub _actionUpload {
    my ( $this, $query ) = @_;

    my ( $baseWeb, $baseTopic ) = $this->_sanitizeWebTopic( $query->param( 'doxygen_topic' ) );
    $this->{redirect} = "$baseWeb, $baseTopic";
    return if( $this->{error} );

    if( $query->request_method() !~ /^POST$/i ) {
        $this->{error} = 'Invalid form submission: POST method is required.';
        return;
    }

    # Check if zip file
    my $zipFile = $query->param( 'zip' ) || 'not-specified';
    $zipFile =~ s/.*\///; # Remove any file path in case present 
    my $zipHandle = $query->upload( 'zip' );
    unless( $zipFile =~ /\.zip$/i && $zipFile ) {
        $this->{error} = "Invalid file '$zipFile' - please upload a .zip file of Doxygen "
                        . "generated files. ";
        return;
    }
    my $zipObj = Archive::Zip->new();
    unless( $zipObj->read( $zipHandle ) eq $AZ_OK ) {
        $this->{error} = "Zip read error or '$zipFile' is not a zip file. ";
        return;
    }

    # Create temp directory to unzip files into
    my $workArea = TWiki::Func::getWorkArea( $pluginName );
    $tempDir = $workArea . '/tmp-' . int( rand( 1000000000 ) ); # Unzip files in workarea
    mkdir( $tempDir );
    chdir( $tempDir ); # Change to the temp directory - some Archive::Zip ignore the path
    my $zipContent = $this->_unzipFiles( $zipFile, $zipObj, $tempDir );
    if( $this->{error} ) {
        $this->_rmTempDir( $tempDir, $zipContent );
        return;
    }

    # Set template topic if specified
    if( $query->param( 'doxygen_template' ) ) {
        $this->{TemplateTopic} = $query->param( 'doxygen_template' );
    }

    # Import content of zip file
    $this->_processDoxygenImport( $zipFile, $tempDir, $zipContent, $baseWeb, $baseTopic );
    $this->_rmTempDir( $tempDir, $zipContent ); # Remove temp dir recursively

    $this->{note} = "Import of '$zipFile' succeeded." unless( $this->{error} );
}

# =========================
sub _rmTempDir {
    my ( $this, $tempDir, $zipContent ) = @_;

    if( $debug ) {
        TWiki::Func::writeDebug( "- ${pluginName}::Core::_rmTempDir( $tempDir ) "
                               . "- temp dir and unzipped content is NOT removed when debug is on" );
        return;
    }
    foreach my $file ( keys %{ $zipContent } ) {
        unlink( "$tempDir/$file" );
    }
    rmdir( $tempDir );
}

# =========================
sub _unzipFiles {
    my ( $this, $zipFile, $zipObj, $tempDir ) = @_;

    TWiki::Func::writeDebug( "- ${pluginName}::Core::_unzipFiles( $zipFile, $tempDir )" ) if $debug;
    my $zipContent;
    my $foundIndex = 0;
    my @memberNames = $zipObj->memberNames();
    foreach my $fileName ( sort @memberNames ) {
        $member = $zipObj->memberNamed( $fileName );
        next if( $member->isDirectory() );
        next if( $fileName =~ /__MACOSX/ ); # Ignore OS-X files
        $fileName =~ s/.*\///; # Remove path, if any
        next if( $fileName =~ /^\./ ); # Ignore hidden files, including OS-X ._* HSF info files
        $fileName =~ s/$TWiki::cfg{NameFilter}//goi; # Remove problematic chars
        $fileName =~ /^(.*)$/;
        $fileName = $1; # untaint
        $foundIndex = 1 if( $fileName eq 'index.html' );
        my $result = $zipObj->extractMemberWithoutPaths( $member, "$tempDir/$fileName" );
        if( $result eq $AZ_OK ) {
            $zipContent->{$fileName} = 1;
        } else {
            $this->{error} .= "Unzip error on '$zipFile', can't unzip $fileName: $result. ";
        }
    }
    unless( $foundIndex ) {
        $this->{error} .= "Missing index.html in '$zipFile' - please upload a .zip file of Doxygen "
                        . "generated files. ";
    }
    return $zipContent;
}

# =========================
sub _processDoxygenImport {
    my ( $this, $zipFile, $tempDir, $zipContent, $baseWeb, $baseTopic ) = @_;

    TWiki::Func::writeDebug( "- ${pluginName}::Core::_processDoxygenImport( $baseWeb.$baseTopic )" )
      if $debug;

    # Create DoxygenFiles topic if needed - this topics holds all imported files
    unless( TWiki::Func::topicExists( $baseWeb, 'DoxygenFiles' ) ) {
        $this->_saveTopic( $baseWeb, 'DoxygenFiles',
          "---+ Doxygen Files\n\nThis topic contains common Doxygen files.\n" );
    }

    # Get list of previously generated Doxygen topics
    my @oldDoxygenTopics = grep { /^${baseTopic}_/ }
                           TWiki::Func::getTopicList( $baseWeb );
    my $newDoxygenTopics;

    # Process all files
    foreach my $file ( keys %{ $zipContent } ) {
        if( $file =~ /^(.*)\.html$/ ) {
            TWiki::Func::writeDebug( "--- processing html file '$file'" ) if $debug;
            my $name = $1;
            $name =~ s/$TWiki::cfg{NameFilter}//goi; # Remove problematic chars
            my $topic = $baseTopic . '_' . $name;
            my $html = TWiki::Func::readFile( "$tempDir/$file" );
            my ( $title, $text ) = $this->_convertHtmlToTWiki( $baseWeb, $baseTopic, $html );
            if( $name eq 'index' ) {
                # Do NOT update base topic content, assume it contains %DOXYGENIMPORT{navigation}%.
                # Instead, attach the content as a file, to be used by %DOXYGENIMPORT{navigation}%.
                TWiki::Func::saveFile( "$tempDir/$file", $text );
                $this->_attachFile( $baseWeb, $baseTopic, $tempDir, $file );
            } else {
                $this->_saveTopic( $baseWeb, $topic, $text, $baseTopic, $title );
                $newDoxygenTopics->{$topic} = 1;
            }
        } else {
            TWiki::Func::writeDebug( "--- processing attachment '$file'" ) if $debug;
            ## next if( TWiki::Func::attachmentExists( $baseWeb, 'DoxygenFiles', $file ) );
            $this->_attachFile( $baseWeb, 'DoxygenFiles', $tempDir, $file );
        }
    }

    # Delete previously generated topics that are obsolete
    for my $topic ( @oldDoxygenTopics ) {
        next if( exists $newDoxygenTopics->{$topic} );
        # SMELL: No API to clean Trash to avoid move error, so delete file in Trash directly
        my $file = $TWiki::cfg{DataDir} . "/Trash/$topic.txt";
        unlink( $file ) if( -e $file );
        $file .= ',v';
        unlink( $file ) if( -e $file );
        # Move obsolete topic to Trash
        TWiki::Func::moveTopic( $baseWeb, $topic, 'Trash' , $topic );
    }

    # Write log
    my $nFiles = scalar keys %$newDoxygenTopics;
    $this->_writeLog( "Import $zipFile, generated $nFiles topics", $baseWeb, $baseTopic );
}

# =========================
sub _convertHtmlToTWiki {
    my ( $this, $baseWeb, $baseTopic, $html ) = @_;

    my $text = '';
    my $title = '';
    my $doxygenPub = '%PUBURLPATH%/' . $baseWeb . '/DoxygenFiles';
    if( $html =~ /<head[^>]*>(.*?)<\/head>/s ) {
        my $head = $1;
        foreach my $line ( split( /[\n\r]/, $head ) ) {
            # Remember title
            if( $line =~ /<title>(.*?)<\/title>/ ) {
                $title = $1;
            }
            # Add link and script tags to body text, but ignore jquery.js
            next unless( $line =~ /^<(link|script)/ );
            next if( $line =~ /src="jquery.js"/ );
            $line =~ s/( (href|src)=")(?=[a-z])/$1$doxygenPub\//;
            $text .= $line . "\n";
        }
    }
    $text =~ s/[\n\r]+$//s;

    $html =~ s/.*<body[^>]*>//s;             # Remove header and body tag
    $html =~ s/<\/body>.*//s;                # Remove end body tag
    $html =~ s/<div id="titlearea">.*<!-- end header part -->//s; # Remove title area
    $html =~ s/<!-- start footer.*?-->.*//s; # Remove useless footer
    $html =~ s/\n[\n\r]+/\n/s;               # Remove empty lines
    $html =~ s/( src=")(?=[a-z])/$1$doxygenPub\//gs; # Point image refs to DoxygenFiles topic
    # Convert HTML page links to topic URLs
    $html =~ s/( href=")([a-z_].*?)\.html/$1%SCRIPTURL{view}%\/$baseWeb\/${baseTopic}_$2/gs;
    $html =~ s/($baseWeb\/${baseTopic})_index\b/$1/gs; # BASENAME_index becomes BASENAME 

    return ( $title, $text . $html ); 
}

# =========================
sub _saveTopic {
    my ( $this, $web, $topic, $text, $baseTopic, $title ) = @_;

    TWiki::Func::writeDebug( "- ${pluginName}::Core::_saveTopic( $web.$topic )" ) if $debug;
    my ( $topicMeta, $topicText );
    my ( $tWeb, $tTopic ) = TWiki::Func::normalizeWebTopicName( $web, $this->{TemplateTopic} );
    my $isTemplate = 1;
    if( $baseTopic && TWiki::Func::topicExists( $tWeb, $tTopic ) ) {
        # Template topic exists - use it
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $tWeb, $tTopic );
    } elsif( TWiki::Func::topicExists( $web, $topic ) ) {
        # Topic exists - read existing topic
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $web, $topic );
        $isTemplate = 0;
    } elsif( TWiki::Func::topicExists( $web, $baseTopic ) ) {
        # Base topic exists - read it to inherit meta data
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $web, $baseTopic );
    } elsif( TWiki::Func::topicExists( $web, $tTopic ) ) {
        # WebTopicEditTemplate exists - use template topic in current web
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $web, $tTopic );
    } elsif( TWiki::Func::topicExists( $TWiki::cfg{UsersWebName}, $tTopic ) ) {
        # WebTopicEditTemplate exists in Main web - use it
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $TWiki::cfg{UsersWebName}, $tTopic );
    } else {
        # WebTopicEditTemplate exists in TWiki web - use it
        ( $topicMeta, $topicText ) = TWiki::Func::readTopic( $TWiki::cfg{SystemWebName}, $tTopic );
    }

    # Handle text
    unless( $topicText =~ s/%DOXYGENTEXT%/$text/g ) {
        $topicText = $text;
    }
    $topicText =~ s/%DOXYGENTITLE%/$title/g;

    if( $isTemplate ) {
        # Expand set of TWiki variables on topic creation
        $topicText = TWiki::Func::expandVariablesOnTopicCreation( $topicText );
        # Remove attachment meta data
        $topicMeta->remove( 'FILEATTACHMENT' );
    }

    # Set "Title" form field if exists
    my $titleField = $topicMeta->get( 'FIELD', 'Title' );
    if( $title && $titleField ) {
        $titleField->{value} = $title;
        $topicMeta->putKeyed( 'FIELD', $titleField );
    }

    # set parent
    $topicMeta->put( 'TOPICPARENT', { name => $baseTopic } ) if( $baseTopic );

    # save updated topic:
    TWiki::Func::saveTopic( $web, $topic, $topicMeta, $topicText, { forcenewrevision => 1 } );
}

# =========================
sub _attachFile {
    my ( $this, $web, $topic, $filePath, $fileName ) = @_;

    TWiki::Func::writeDebug( "- ${pluginName}::Core::_attachFile( $fileName to $web.$topic )" )
      if $debug;

    $ignoreActionUrlParam = 1; # Prevent recursion
    TWiki::Func::saveAttachment( $web, $topic, $fileName,
        {
            file       => "$filePath/$fileName",
            filepath   => '',
            filedate   => time(),
            filesize   => -s "$filePath/$fileName",
            comment    => 'Attached by Doxygen Import Plugin',
        }
      );
    $ignoreActionUrlParam = 0;
}

# =========================
sub _sanitizeWebTopic {
    my ( $this, $text ) = @_;
    $text ||= '';
    $text =~ s/$webTopicRegex//go;
    unless( $text =~ /^(.+)\.(.+)$/ ) {
        $this->{error} = "Can't import Doxygen files, inproper use of parameters"
                       . " (doxygen_topic is missing).";
        return( $this->{baseWeb}, $this->{baseTopic} );
    }
    return( $1, $2 );
}

# =========================
sub _writeLog {
    my( $this, $extra, $web, $topic, $wikiUser ) = @_;
    if( $this->{LogAction} && $TWiki::Plugins::VERSION >= 1.4 ) {
        TWiki::Func::writeLog( 'doxygenimport', $extra, $web, $topic, $wikiUser );
    }
}

# =========================
sub _urlEncode {
    my $text = shift;
    $text =~ s/([^0-9a-zA-Z-_.:~!*'\/])/'%'.sprintf('%02x',ord($1))/ge;
    return $text;
}

# =========================
1;
