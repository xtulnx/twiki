# ---+ Extensions
# ---++ ActionTrackerPlugin
# **BOOLEAN**
# %ACTIONSEARCH% searches single web or multiple webs.
$TWiki::cfg{Plugins}{ActionTrackerPlugin}{SearchCurrentWebOnly} = 0;
# **BOOLEAN**
# Gather up all web notifiers from WebNotify topics.
$TWiki::cfg{Plugins}{ActionTrackerPlugin}{GatherWebNotify} = 1;

1;

