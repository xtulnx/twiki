%META:TOPICINFO{author="TWikiContributor" date="1361604272" format="1.1" version="$Rev$"}%
---+!! Notify Authors Plugin
<!--
   Contributions to this plugin are appreciated. Please update the plugin page at
   http://twiki.org/cgi-bin/view/Plugins/NotifyAuthorsPlugin or provide feedback at
   http://twiki.org/cgi-bin/view/Plugins/NotifyAuthorsPluginDev.
   If you are a TWiki contributor please update the plugin in the SVN repository.
-->
<sticky><div style="float: right; background-color: #EBEEF0; margin: 0 0 20px 20px; padding: 0 10px 0 10px;">
%TOC{title="Page contents"}%
</div><div style="float:right; background-color: white; margin: 0 0 15px 15px;">
<img src="%PUBURLPATH%/%WEB%/%TOPIC%/notify-authors-discussion-300.png" alt="notify-authors-discussion-300.png" width="300" height="316" />
</div></sticky>

%SHORTDESCRIPTION%

---++ Introduction

Ever wanted to notify everybody involved in a conversation on a TWiki topic? Use this plugin to add a "notify authors" button to topics. When pressed, all authors on the topic will be notified by e-mail. The e-mail contains a link to the topic so that everybody is kept in the loop. Authors are defined by user signatures in the topic at hand.

---++ Syntax Rules

=%<nop>NOTIFYAUTHORS{...}%=

%INCLUDE{ "VarNOTIFYAUTHORS" section="parameters" }%

The showbutton action adds a hidden input field named ="notifyauthors_action"= with value ="notify"= to automate the interaction between =action="showbutton"= and =action="notify"=.

If needed, the following preferences settings defined on a web level of topic level may override the configure settings:

   * Set <nop>NOTIFYAUTHORSPLUGIN_BUTTONTEXT = Notify button label
   * Set <nop>NOTIFYAUTHORSPLUGIN_TOOLTIPTEXT = Notify button tooltip
   * Set <nop>NOTIFYAUTHORSPLUGIN_BUTTONTEXT = Notify button text
   * Set <nop>NOTIFYAUTHORSPLUGIN_HELPTEXT = Help text shown next to the notify button, =%<nop>NOTIFYAUTHORLIST%= expands to the list of authors
   * Set <nop>NOTIFYAUTHORSPLUGIN_NOTIFIEDTEXT = Text shown after sending notification e-mail, =%<nop>NOTIFYAUTHORLIST%= expands to the list of authors
   * Set <nop>NOTIFYAUTHORSPLUGIN_SUMMARYFORMAT = Format of topic summary included in the e-mail

---++ Example

=%<nop>NOTIFYAUTHORS%= is typically used together with =%<nop>COMMENT%= of the CommentPlugin.

<sticky><div style="float:right; margin: 5px 0 15px 15px; background-color: white;">
__Screenshot of a discussion:__ %BR%
<img src="%PUBURLPATH%/%WEB%/%TOPIC%/notify-authors-discussion.png" alt="Notify authors discussion" width="462" height="487" />
</div></sticky>
__Write this:__
<verbatim>
%COMMENT%
%NOTIFYAUTHORS%
</verbatim>

__To get this:__

%COMMENT{ nopost="on" }%
%NOTIFYAUTHORS%

__Note:__ Use the TWiki:Plugins.ScrollBoxAddOn to get a nice box around the "notify authors" button, such as:

<verbatim>
%COMMENT%
%INCLUDE{ "%SYSTEMWEB%.ScrollBoxAddOn" section="scroll_box_engine" }%
%INCLUDE{ "%SYSTEMWEB%.ScrollBoxAddOn" section="static_box_start"
 boxstyle="padding: 7px; width: auto;"
}%
%NOTIFYAUTHORS%
%INCLUDE{ "%SYSTEMWEB%.ScrollBoxAddOn" section="static_box_end" }%
</verbatim>

---++ Default Comment Box With Notify Button

If you want to add the notify authors button to the default COMMENT box do this:

1. Define a custom comment template in %SYSTEMWEB%.UserCommentsTemplate called =threadmode_and_notify= that defines a comment box and a box with a notify authors button. This requires CommentPlugin version 2013-02-10 or later. Content:

<verbatim>
<verbatim>
%TMPL:DEF{PROMPT:threadmode_and_notify}%<a name="threadmode_and_notify">
</a>%COMMENTFORMSTART%
<div class="commentPlugin commentPluginPromptBox" style="margin: 5px 0;">
%IF{ "istopic '%SYSTEMWEB%.ScrollBoxAddOn'" then="$percntINCLUDE{ \"%SYSTEMWEB%.ScrollBoxAddOn\" section=\"scroll_box_engine\" }$percnt $percntINCLUDE{ \"%SYSTEMWEB%.ScrollBoxAddOn\" section=\"static_box_start\" boxstyle=\"padding: 7px; width: auto; background-color: #f6f6fb;\" }$percnt" else="<nop>" }%
<div><textarea %DISABLED% rows="%rows|5%" cols="%cols|80%" name="comment" class="twikiTextarea" wrap="soft" style="width: 100%" onfocus="if(this.value=='%MESSAGE%')this.value=''" onblur="if(this.value=='')this.value='%MESSAGE%'">%MESSAGE%</textarea></div>
<div style="padding: 5px 0 0 0;"><input %DISABLED% type="submit" value="%button|Add comment%" class="twikiButton" /></div>
%IF{ "istopic '%SYSTEMWEB%.ScrollBoxAddOn'" then="$percntINCLUDE{ \"%SYSTEMWEB%.ScrollBoxAddOn\" section=\"static_box_end\" }$percnt" else="<nop>" }%
</div><!--/commentPlugin-->
%COMMENTFORMEND%
%IF{ "istopic '%SYSTEMWEB%.ScrollBoxAddOn'" then="$percntINCLUDE{ \"%SYSTEMWEB%.ScrollBoxAddOn\" section=\"static_box_start\" boxstyle=\"padding: 7px; width: auto; background-color: #f6f6fb;\" }$percnt" else="<nop>" }%
%NOTIFYAUTHORS{ anchor="threadmode_and_notify" }%
%IF{ "istopic '%SYSTEMWEB%.ScrollBoxAddOn'" then="$percntINCLUDE{ \"%SYSTEMWEB%.ScrollBoxAddOn\" section=\"static_box_end\" }$percnt" else="<nop>" }%%TMPL:END%
</verbatim>
<verbatim>
%TMPL:DEF{OUTPUT:threadmode_and_notify}%%POS:BEFORE%

%URLPARAM{"comment"}%

%STARTSECTION{ type="expandvariables" }%%IF{ "defined 'BUBBLESIGNATUREFORMAT'" then="$percntBUBBLESIGNATUREFORMAT$percnt" else="-- $percntWIKIUSERNAME$percnt - $percntDATE$percnt" }%%ENDSECTION{ type="expandvariables" }%
%TMPL:END%
</verbatim>
</verbatim>

2. Define this site-level preferences setting in [[%LOCALSITEPREFS%]], or on a web-level in WebPreferences:

   * Set <nop>COMMENTPLUGIN_DEFAULT_TYPE = threadmode_and_notify

With this, the default =%<nop>COMMENT%= has now also a notify authors button as shown in above screenshot.

---++ E-mail Template

The plugin has a =notifyauthors.tmpl= templates file in the =twiki/templates= directory. The plugin handles the TWiki variables, and in addition these special variables:

| *Variable* | *Expands to* |
| =%<nop>NOTIFYTITLE%= | E-mail subject, containing "TWiki Notification: Web.TopicName Has Been Updated" |
| =%<nop>NOTIFYADDRESSES%= | E-mail "To" list, comma-space separated |
| =%<nop>NOTIFYSUMMARY%= | Summary built from topic content. The format of the summary is defined by the ={Plugins}{NotifyAuthorsPlugin}{SummaryFormat}= setting |
| =%<nop>NOTIFYWEB%= | Web where notification is sent from |
| =%<nop>NOTIFYTOPIC%= | Topic where notification is sent from |
| =%<nop>NOTIFYFROM%= | !WikiName of person who pressed the notify button |

---++ Plugin Installation & Configuration

You do not need to install anything on the browser to use this plugin. These instructions are for the administrator who installs the plugin on the TWiki server. 
%TWISTY{
 mode="div"
 showlink="Show details %ICONURL{toggleopen}% "
 hidelink="Hide details %ICONURL{toggleclose}% "
}%

   * For an __automated installation__, run the [[%SCRIPTURL{configure}%][configure]] script and follow "Find More Extensions" in the in the __Extensions__ section. 
      * See the [[http://twiki.org/cgi-bin/view/Plugins/BuildContribInstallationSupplement][installation supplement]] on TWiki.org for more information.

   * Or, follow these __manual installation__ steps: 
      * Download the ZIP file from the Plugins home (see below).
      * Unzip ==NotifyAuthorsPlugin.zip== in your twiki installation directory. Content:
        | *File:* | *Description:* |
        | ==data/TWiki/NotifyAuthorsPlugin.txt== | Plugin topic |
        | ==data/TWiki/VarNOTIFYAUTHORS.txt== | Variable documentation topic |
        | ==pub/TWiki/NotifyAuthorsPlugin/*png== | Screenshots |
        | ==templates/notifyauthors.tmpl== | E-mail template |
        | ==lib/TWiki/Plugins/NotifyAuthorsPlugin.pm== | Plugin Perl module |
        | ==lib/TWiki/Plugins/NotifyAuthorsPlugin/Core.pm== | Plugin core module |
        | ==lib/TWiki/Plugins/NotifyAuthorsPlugin/Config.spec== | Configure spec file |
      * Set the ownership of the extracted directories and files to the webserver user.
      * Install the dependencies (if any).

   * Plugin __configuration and testing__: 
      * Run the [[%SCRIPTURL{configure}%][configure]] script and enable the plugin in the __Plugins__ section.
      * Configure additional plugin settings in the __Extensions__ section if needed. Settings:
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{AuthorPatterns}= # Patterns of author signatures, separated by comma-space. Use regular expressions; capture the WikiName of the user in parenthesis.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{ButtonText}= # Notify button label.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{TooltipText}= # Notify button tooltip text.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{HelpText}= # Help text shown next to the notify button.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{NotifiedText}= # Text shown after sending notification e-mail.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{AnchorName}= # Anchor name to jump to after submit. Leave empty for no anchor jump.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{UseEmailField}= # Use the "Email" form field of user profile topics instead of the e-mail stored in the password system. This is useful if LDAP authentication is used.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{LogAction}= # Log notify action.
        %BR% =$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{Debug}= # Debug plugin. See output in data/debug.txt
      * Test if the installation was successful by creating a sandbox topic using above example.
%ENDTWISTY%

---++ Plugin Info

   * One line description, is shown in the %SYSTEMWEB%.TextFormattingRules topic: 
      * Set SHORTDESCRIPTION = Notify authors of a discussion thread via e-mail

%TABLE{ tablewidth="100%" columnwidths="170," }%
|  Plugin Author: | TWiki:Main.PeterThoeny |
|  Copyright: | &copy; 2013 Wave Systems Corp. %BR% &copy; 2013 TWiki:Main.PeterThoeny &amp; TWiki:TWiki.TWikiContributor |
|  License: | GPL ([[http://www.gnu.org/copyleft/gpl.html][GNU General Public License]]) |
|  Sponsor: | [[http://www.wave.com/][Wave Systems Corp.]] |
|  Plugin Version: | 2013-02-22 |
%TWISTY{
 mode="div"
 showlink="Show Change History %ICONURL{toggleopen}%"
 hidelink="Hide Change History %ICONURL{toggleclose}%"
}%
%TABLE{ tablewidth="100%" columnwidths="170," }%
|  2013-02-22: | TWikibug:Item7154: Log notify action; fix internal server error if specified user does not exist |
|  2013-02-21: | TWikibug:Item7154: New NOTIFYSUMMARY variable handled in notify template |
|  2013-02-13: | TWikibug:Item7126: Preferences settings override configure settings, such as NOTIFYAUTHORSPLUGIN_BUTTONTEXT overrides ...{ButtonText} |
|  2013-02-11: | TWikibug:Item7126: Option to get e-mails from user profile form field "Email" instead of the password system |
|  2013-02-10: | TWikibug:Item7126: Initial version |
%ENDTWISTY%
%TABLE{ tablewidth="100%" columnwidths="170," }%
|  TWiki Dependency: | $TWiki::Plugins::VERSION 1.1 |
|  CPAN Dependencies: | none |
|  Other Dependencies: | none |
|  Perl Version: | 5.005 |
|  TWiki:Plugins.Benchmark: | %SYSTEMWEB%.GoodStyle nn%, %SYSTEMWEB%.FormattedSearch nn%, %TOPIC% nn% |
|  Plugin Home: | http://TWiki.org/cgi-bin/view/Plugins/NotifyAuthorsPlugin |
|  Feedback: | http://TWiki.org/cgi-bin/view/Plugins/NotifyAuthorsPluginDev |
|  Appraisal: | http://TWiki.org/cgi-bin/view/Plugins/NotifyAuthorsPluginAppraisal |

__Related Topics:__ VarNOTIFYAUTHORS, %SYSTEMWEB%.TWikiPlugins, %SYSTEMWEB%.DeveloperDocumentationCategory, %SYSTEMWEB%.AdminDocumentationCategory, %SYSTEMWEB%.TWikiPreferences, CommentPlugin

%META:FILEATTACHMENT{name="notify-authors-discussion.png" attachment="notify-authors-discussion.png" attr="h" comment="" date="1360626552" path="notify-authors-discussion.png" size="52300" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="notify-authors-discussion-300.png" attachment="notify-authors-discussion-300.png" attr="h" comment="" date="1360463963" path="notify-authors-discussion-300.png" size="43343" user="TWikiContributor" version="1"}%
