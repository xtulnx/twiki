# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013 Wave Systems Corp.
# Copyright (C) 2013-2014 Peter Thoeny & TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::NotifyAuthorsPlugin::Core;

# =========================
sub new {
    my ( $class, $debug ) = @_;

    my $this = {
          Debug          => $debug,
          ButtonText     => _getPref( 'ButtonText', 'Notify authors' ),
          TooltipText    => _getPref( 'TooltipText',
            'Notify via e-mail all authors who contributed to this page' ),
          HelpText       => _getPref( 'HelpText',
            'Notify %NOTIFYAUTHORLIST% by e-mail to solicit feedback on this topic.' ),
          NotifiedText   => _getPref( 'NotifiedText',
            '%NOTIFYAUTHORLIST% have been notified by e-mail.' ),
          AnchorName     => _getPref( 'AnchorName', '' ),
          AuthorPatterns => _getPref( 'AuthorPatterns',
            '\\-\\- +%USERSWEB%\\.(\\w+), \\%USERSIG{[ "]*(\\w+), ' .
            '\\%BUBBLESIG{[ "]*(\\w+),' ),
          SummaryFormat  => _getPref( 'SummaryFormat',
            '$summary(100)' ),
          UseEmailField  => _getPref( 'UseEmailField', 0 ),
          LogAction      => _getPref( 'LogAction', 0 ),
        };
    bless( $this, $class );

    #$this->{Debug} = 1; # Uncomment to enable debugging just in this file

    TWiki::Func::writeDebug( "- NotifyAuthorsPlugin Core constructor" ) if $this->{Debug};

    return $this;
}

# =========================
sub VarNOTIFYAUTHORS
{
    my ( $this, $session, $params, $topic, $web ) = @_;

    my $query = TWiki::Func::getCgiQuery();
    my $urlParamAction = '';
    if( $query ) {
        $urlParamAction = $query->param('notifyauthors_action') || '';
    }
    my $action = $params->{action} || $urlParamAction || 'showbutton';
    TWiki::Func::writeDebug( "- NotifyAuthorsPlugin NOTIFYAUTHORS, action=$action" )
      if $this->{Debug};
    $this->{anchor} = $params->{anchor} || $this->{AnchorName};
    $this->{anchor} = '#' . $this->{anchor} if( $this->{anchor} );
    $this->{text}   = '<a name="NotifyAuthorsButton"></a>';
    $this->{help}   = '';
    $this->{note}   = '';
    $this->{error}  = '';

    if( $action eq 'showbutton' ) {
        $this->_actionShowButton( $web, $topic );
    } elsif( $action eq 'notify' ) {
        $this->_actionNotify( $web, $topic );
    }

    my $text = $this->{text};
    my $style = 'margin: 0 0.5em 0 0; padding: 0.3em 0.5em;';
    if( $this->{help} ) {
        $text .= "<span style='color: #666666; $style'> "
               . $this->{help} . " </span>"
    }
    if( $this->{note} ) {
        $text .= "<span style='background-color: #feffc2; $style'> "
               . $this->{note}
               . " [[$web.$topic" . $this->{anchor} . "][OK]] </span>";
    }
    if( $this->{error} ) {
        $text .= "<span style='background-color: #ff9594; $style'> *Error:* "
               . $this->{error}
               . " [[$web.$topic" . $this->{anchor} . "][OK]] </span>"
    }
    return $text;
}

# =========================
sub _actionShowButton
{
    my ( $this, $web, $topic ) = @_;
    $this->{text} .= '<form action="%SCRIPTURL{viewauth}%/%WEB%/%TOPIC%'
        . $this->{anchor} . '">'
        . '<input type="hidden" name="notifyauthors_action" value="notify" />'
        . '<input type="submit" value="' . $this->{ButtonText}
        . '" title="' . $this->{TooltipText} . '" class="twikiButton" />'
        . '</form>';
    my @authors = $this->_getAuthorsFromTopic( $web, $topic );
    my $notifyAuthors = '<nop>' . join( ', <nop>', @authors );
    $this->{help} = $this->{HelpText};
    $this->{help} =~ s/%NOTIFYAUTHORLIST%/$notifyAuthors/go;
    return;
}

# =========================
sub _actionNotify
{
    my ( $this, $web, $topic ) = @_;

    # Identify authors and gather e-mail addresses
    my @authors = $this->_getAuthorsFromTopic( $web, $topic );
    my $toEmails = '';
    if( $this->{UseEmailField} ) {
        # Get e-mails from Email form field of user profile topics
        my $result = TWiki::Func::searchInWebContent(
          '%META:FIELD{name="Email"',
          $TWiki::cfg{UsersWebName},
          \@authors, {} );
        my @emails = ();
        foreach my $user (keys %$result ) {
            foreach my $line ( @{$result->{$user}} ) {
                if( $line =~ s/^.*value="([^"]*)".*$/$1/o ) {
                    push( @emails, "$user <$line>" );
                }
            }
        }
        $toEmails = join( ', ', @emails );

    } else {
        # Get e-mails from password system
        my @emails = ();
        foreach my $user ( @authors ) {
           foreach my $mail ( TWiki::Func::wikinameToEmails( $user ) ) {
               push( @emails, "$user <$mail>" );
           }           
        }
        $toEmails = join( ', ', @emails );
    }

    unless( scalar @authors ) {
        $this->{note} = "No authors could be found in !$topic topic.";
        return;
    }

    $this->_sendEmail( $web, $topic, $toEmails );
    unless( $this->{error} ) {
        my $notifyAuthors = '<nop>' . join( ', <nop>', @authors );
        $this->{note} .= $this->{NotifiedText};
        $this->{note} =~ s/%NOTIFYAUTHORLIST%/$notifyAuthors/go;
        if( $this->{LogAction} && $TWiki::Plugins::VERSION >= 1.4 ) {
            my $extra = 'notified: ' . join( ', ', @authors );
            TWiki::Func::writeLog( 'notifyauthors', $extra, $web, $topic );
        }
    }

    return;
}

# =========================
sub _getAuthorsFromTopic {
    my( $this, $web, $topic ) = @_;

    my $regex = $this->{AuthorPatterns};
    $regex =~ s/\%(USERSWEB|MAINWEB)\%/$TWiki::cfg{UsersWebName}/g;
    my @regexes = split( /, */, $regex );
    $regex =~ s/, */|/g;

    my %seen;
    my @authors =
      grep { ! $this->{UseEmailField}
            || TWiki::Func::topicExists( $TWiki::cfg{UsersWebName}, $_ ) }
      grep { !$seen{$_}++ }
      sort
      map { foreach my $r ( @regexes ) { s/^.*?$r.*$/$1/; } $_ }
      grep { /$regex/ }
      split( /\r?\n/, TWiki::Func::readTopicText( $web, $topic, undef, 1 ) );

    return @authors;
}

# =========================
sub _sendEmail {
    my( $this, $web, $topic, $toEmails ) = @_;

    # For title, use TOPICTITLE if available, else space out topic name
    my $rawTitle = "%TOPICTITLE{ topic=\"$web.$topic\" }%";
    my $title = TWiki::Func::expandCommonVariables( $rawTitle, $topic, $web );
    if( $title eq $rawTitle || $title eq $topic ) {
        $title = "%SPACEOUT{$topic}%";
    }
    my $wikiName = TWiki::Func::getWikiName();
    my $template = 'notifyauthors'; # FIXME: Config value

    my $text = TWiki::Func::readTemplate( $template );
    $text =~ s/%NOTIFYSUMMARY%/$this->_getTopicSummary( $web, $topic )/geo;
    $text =~ s/%NOTIFYFROM%/$wikiName/go;
    $text =~ s/%NOTIFYADDRESSES%/$toEmails/go;
    $text =~ s/%NOTIFYWEB%/$web/go;
    $text =~ s/%NOTIFYTOPIC%/$topic/go;
    $text =~ s/%NOTIFYTITLE%/$title/go;
    $text = TWiki::Func::expandCommonVariables( $text, $topic, $web );

    $this->{error} = TWiki::Func::sendEmail( $text );
    $this->{note} .= "<verbatim>$text</verbatim>" if( $this->{Debug} );
    return;
}

# =========================
sub _getTopicSummary
{
    my ( $this, $web, $topic ) = @_;
    my $var = "\%SEARCH{ \"$topic\" scope=\"topic\" web=\"$web\" topic=\"$topic\""
            . ' nonoise="on" format="' . $this->{SummaryFormat} . '" }%';
    my $text = TWiki::Func::expandCommonVariables( $var, $topic, $web );
    $text =~ s/<nop>//go; # Remove <nop> tags
    return $text;
}

# =========================
sub _getPref
{
    my ( $name, $default ) = @_;
    my $value = TWiki::Func::getPreferencesValue( uc( "NotifyAuthorsPlugin_$name" ) )
             || $TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{$name}
             || $default;
    return $value;
}

# =========================
1;
