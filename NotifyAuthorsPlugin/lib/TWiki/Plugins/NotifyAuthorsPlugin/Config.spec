# ---+ Extensions
# ---++ NotifyAuthorsPlugin

# **STRING 50**
# Patterns of author signatures, separated by comma-space. Use regular expressions; capture the WikiName of the user in parenthesis.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{AuthorPatterns} = '\\-\\- +%USERSWEB%\\.(\\w+), \\%USERSIG{[ "]*(\\w+), \\%BUBBLESIG{[ "]*(\\w+)';

# **STRING 50**
# Notify button label.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{ButtonText} = 'Notify authors';

# **STRING 50**
# Notify button tooltip text.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{TooltipText} = 'Notify all authors who contributed to this page via e-mail';

# **STRING 50**
# Help text shown next to the notify button.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{HelpText} = 'Notify %NOTIFYAUTHORLIST% by e-mail to solicit feedback on this topic.';

# **STRING 50**
# Text shown after sending notification e-mail.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{NotifiedText} = '%NOTIFYAUTHORLIST% have been notified by e-mail.';

# **STRING 50**
# Anchor name to jump to after submit. Leave empty for no anchor jump.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{AnchorName} = 'NotifyAuthorsButton';

# **STRING 50**
# Format of the NOTIFYSUMMARY variable in the e-mail template.
# Use the same syntax as in the format parameter of the SEARCH{} variable.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{SummaryFormat} = '$summary(120)';

# **BOOLEAN**
# Use the "Email" form field of user profile topics instead of the e-mail
stored in the password system. This is useful if LDAP authentication is used.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{UseEmailField} = 0;

# **BOOLEAN**
# Log notify action.
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{LogAction} = 1;

# **BOOLEAN**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{Debug} = 0;

1;
