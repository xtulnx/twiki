# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2013 Wave Systems Corp.
# Copyright (C) 2013-2014 Peter Thoeny & TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::NotifyAuthorsPlugin;

# =========================
our $VERSION = '$Rev$';
our $RELEASE = '2013-02-22';
our $SHORTDESCRIPTION = 'Notify authors of a discussion thread via e-mail';
our $NO_PREFS_IN_TOPIC = 1;

# =========================
my $debug = $TWiki::cfg{Plugins}{NotifyAuthorsPlugin}{Debug} || 0;
my $core;

# =========================
sub initPlugin
{
    my ( $topic, $web ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning( "Version mismatch between NotifyAuthorsPlugin and Plugins.pm" );
        return 0;
    }

    TWiki::Func::registerTagHandler( 'NOTIFYAUTHORS', \&_NOTIFYAUTHORS );

    # Plugin correctly initialized
    TWiki::Func::writeDebug( "- TWiki::Plugins::NotifyAuthorsPlugin::initPlugin( $web.$topic ) is OK" ) if $debug;

    return 1;
}

# =========================
sub _NOTIFYAUTHORS
{
#   my ( $session, $params, $theTopic, $theWeb ) = @_;

    # Lazy loading, e.g. compile core module only when needed
    unless( $core ) {
        require TWiki::Plugins::NotifyAuthorsPlugin::Core;
        $core = new TWiki::Plugins::NotifyAuthorsPlugin::Core( $debug );
    }
    return $core->VarNOTIFYAUTHORS( @_ );
}

# =========================
1;
