# ---+ Extensions
# ---++ RequireRegistrationPlugin
# **STRING 80**
# Comma-separated list of actions that this plugin should work on. Default:
# 'attach, edit, viewauth'.
$TWiki::cfg{Plugins}{RequireRegistrationPlugin}{Actions} = 'attach, edit, viewauth';
# **BOOLEAN**
# Check if the user profile topic exists.
$TWiki::cfg{Plugins}{RequireRegistrationPlugin}{RequireProfileTopic} = 0;
# **NUMBER**
# Refresh time in seconds to do a meta refresh redirect to the registration
# page. Specify -1 to use an immediate redirect CGI query.
$TWiki::cfg{Plugins}{RequireRegistrationPlugin}{Refresh} = 0;
# **BOOLEAN**
# Debug flag - see output in <code>twiki/data/debug.txt</code>.
$TWiki::cfg{Plugins}{RequireRegistrationPlugin}{Debug} = 0;
1;
