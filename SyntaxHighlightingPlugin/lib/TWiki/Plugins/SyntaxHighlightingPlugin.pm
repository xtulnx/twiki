# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2008 Andrew Jones, andrewjones86@gmail.com
# Copyright (C) 2000-2001 Andrea Sterbini, a.sterbini@flashnet.it
# Copyright (C) 2001-2014 Peter Thoeny, Peter[at]Thoeny.org
# Copyright (C) 2008-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at 
# http://www.gnu.org/copyleft/gpl.html
#
# =========================
#
# This is the Syntax Highlighting TWiki plugin.
# originally written by
# Nicolas Tisserand (tisser_n@epita.fr), Nicolas Burrus (burrus_n@epita.fr)
# and Perceval Anichini (anichi_p@epita.fr)
# Current version by Andrew Jones (andrewjones86@gmail.com)
# 
# It uses enscript as syntax highlighter.
# 
# Use it in your twiki text by writing %CODE{"language"}% ... %ENDCODE%
# with language = ada asm awk bash c changelog c++ csh delphi diff diffs
# diffu elisp fortran fortran_pp haskell html idl inf java javascript
# ksh m4 mail makefile maple matlab modula_2 nested nroff objc outline
# pascal perl postscript python rfc scheme sh skill sql states synopsys
# tcl tcsh tex vba verilog vhdl vrml wmlscript zsh 

package TWiki::Plugins::SyntaxHighlightingPlugin;
use strict;

use IPC::Run qw( run ) ;

use vars qw(    $VERSION
                $RELEASE
                $NO_PREFS_IN_TOPIC
                $SHORTDESCRIPTION
                $pluginName
                %languages
                %extraMapping
                $globalError
           );

$VERSION = '$Rev';
$RELEASE = '2013-12-11';
$SHORTDESCRIPTION = 'Highlight source code fragments for many languages';
$NO_PREFS_IN_TOPIC = 1;
%extraMapping = (
      "c++" => "cpp",
    );

$pluginName = 'SyntaxHighlightingPlugin';

# =========================
sub initPlugin {
    my( $topic, $web, $user, $installWeb ) = @_;
    
    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.1 ) {
        TWiki::Func::writeWarning( "Version mismatch between $pluginName and Plugins.pm" );
        return 0;
    }

    TWiki::Func::registerTagHandler( 'SYNTAXHIGHLIGHTING', \&_handleSyntaxHighlighting );

    # Plugin correctly initialized
    _Debug("initPlugin( $web.$topic ) is OK");

    return 1;
}

# =========================
sub _handleSyntaxHighlighting {
    my( $session, $params ) = @_;

    if( $params->{_DEFAULT} && $params->{_DEFAULT} =~ /^supported/i ) {
        _initLanguages();
        return _returnError( $globalError ) if( $globalError );
        return join( ', ', sort keys( %languages ) );
    }
    return _returnError( "SYNTAXHIGHLIGHTING Error: Unknown action" );
}


# =========================
sub commonTagsHandler {
    $_[0] =~ s/%(CODE|SYNTAX){(.*?)}%\s*(.*?)%END(CODE|SYNTAX)%/&_handleCode($2, $3)/egs;
}

# =========================
sub _handleCode {
    my( $paramStr, $code ) = @_;

    _initLanguages();
    return _returnError( $globalError ) if( $globalError );

    my %params = TWiki::Func::extractParameters( $paramStr );
    my $lang = $params{lang} || $params{syntax} || $params{_DEFAULT} 
      || $TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{DefaultLang} || '';
    my $num = lc $params{num} || lc $params{number} || lc $params{numbered}
      || $TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Numbering} || 'off';
    my $step = lc $params{step} || lc $params{numstep} || lc $params{numberstep}
      || lc $params{numberedstep}
      || $TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Step} || '1';
    my $style = $params{style}
      || $TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Style}
      || 'border: 1px solid #ccc; padding: 0 0.6em; background-color: #f2f5f8;';
    my $numstyle = $params{numstyle}
      || $TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{NumStyle}
      || 'display: inline-block; border-right: 3px solid #e0a95c; color: #333;'
       . 'margin: 0 0.6em 0 -0.6em; padding: 0 0.3em; background-color: #eed6b4;';
    $lang =~ s/[^A-Za-z0-9_\+\-]//g; # sanitize
    if( $lang =~ /^(.*)$/ ) {
        $lang = lc( $1 );
    }

    my $text = '';
    my $error = '';
    if( $languages{$lang} ) {
        $text = _highlightLang( $code, $lang );
    } else {
        $error = _returnError( "Language $lang is either undefined or unsupported. "
               . "Check %SYSTEMWEB%.$pluginName for more information." );
        $code =~ s/&/&amp;/go; # escape html and entities
        $code =~ s/</&lt;/go;
        $code =~ s/>/&gt;/go;
        $text  = "\n<pre>\n$code\n</pre>\n";
    }

    # check if we got a result
    if( $text =~ s/.*\<pre\>\n(.*?)\n?\<\/pre\>.*/$1/osi ) {
        # append our own line counter
        $num = 0 if( $num eq 'off' );
        $num = 1 if( $num eq 'on' );
        $num =~ s/[^0-9]//g;
        $num = 0 unless( $num );
        $step =~ s/[^-0-9]//g;
        $step = 0 unless( $step );
        if( $num > 0 ) {
            my $line = $num - $step;
            my $format = "<b style='$numstyle'>%5d<\/b>";
            $text =~ s/^/sprintf( $format, $line += $step )/mgeo
        }
        my $out = "<!-- !$pluginName -->\n"
                . "<pre class='syntaxHighlightingPlugin' style='$style'>"
                . "$text</pre>\n"
                . "<!-- end !$pluginName -->";
        return "$error$out";
    } else {
        _Warn( "Error with enscript while highlighting. Check it's installed "
          . "correctly and the path is correct" );
        _returnError('Internal error. Notify you administrator at %WIKIWEBMASTER%.');
    }
}

# =========================
# runs enscript to highlight the code
sub _highlightLang {
    my( $code, $lang ) = @_;
    
    my $enscript = $TWiki::cfg{Plugins}{$pluginName}{EnscriptPath} || 'enscript';

    my @cmd;
    push @cmd, $enscript;
    push @cmd, qw( --color --language=html --output - --silent) ;
    push @cmd, "--highlight=$languages{$lang}" ;
    my $out = '' ;
    run \@cmd, \$code, \$out ;

    return $out;
}

# =========================
# query enscript to get the list of languages & initialize %languages
sub _initLanguages {
    return if( $globalError || scalar %languages );

    my $enscript = $TWiki::cfg{Plugins}{$pluginName}{EnscriptPath} || 'enscript';
    my @cmd;
    push @cmd, $enscript;
    push @cmd, "--help-highlight";
    my $in  = '';
    my $out = '';
    my $err = '';
    my $ret;
    eval {
        $ret = run( \@cmd, \$in, \$out, \$err );
    };
    if( $ret ) {
        %languages =
          map { /^Name[ \t]*: *(.*)/; (lc($1), lc($1)); }
          grep { /^Name[ \t]*: / }
          split( /[\n\r]+/, $out );
        @languages{keys %extraMapping} = values %extraMapping;

    } elsif( $err ) {
        $globalError = "Error: $err";
    } else {
        $globalError = "Error: Can't find enscript";
    }
}

# =========================
# output an error to screen
sub _returnError {
    my( $text ) = @_;

    my $out = '<span class="twikiAlert">';
    $out .= "$pluginName: $text";
    $out .= '</span>';

    return $out;
}

# =========================
sub _Debug {
    my $text = shift;
    my $debug = $TWiki::cfg{Plugins}{$pluginName}{Debug} || 0;
    TWiki::Func::writeDebug( "- TWiki::Plugins::${pluginName}: $text" ) if $debug;
}

# =========================
sub _Warn {
    my $text = shift;
    TWiki::Func::writeWarning( "- TWiki::Plugins::${pluginName}: $text" );
}

# =========================
1;
