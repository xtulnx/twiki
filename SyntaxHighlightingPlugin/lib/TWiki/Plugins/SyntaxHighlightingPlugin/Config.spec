# ---+ Extensions
# ---++ SyntaxHighlightingPlugin

# **STRING** 
# Configure the enscript path.
$TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{EnscriptPath} = 'enscript';

# **STRING**
# Default language.
$TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{DefaultLang} = '';

# **STRING**
# Default for line numbering, 'off' or 'on'.
$TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Numbering} = 'off';

# **STRING**
# Style of pre tag containing the source code.
$TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Style} = 'border: 1px solid #ccc; padding: 0 0.6em; background-color: #f2f5f8';

# **BOOLEAN**
# Debug setting
$TWiki::cfg{Plugins}{SyntaxHighlightingPlugin}{Debug} = 0;

1;
