# Plugin for TWiki Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2006 Michael Daum http://wikiring.com
# Portions Copyright (C) 2006 Spanlink Communications
# Copyright (C) 2007-2011 TWiki:TWiki.TWikiContributor
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

package TWiki::Plugins::LdapNgPlugin;

use strict;
use vars qw(
    $VERSION $RELEASE $isInitialized $NO_PREFS_IN_TOPIC $SHORTDESCRIPTION %wikiUsers $headAdded
  );
use Data::Dumper;

$VERSION = '$Rev: 24846 (2013-02-04) $';
$RELEASE = '2013-02-01';
$NO_PREFS_IN_TOPIC = 1;
$SHORTDESCRIPTION = 'Query and display data from an LDAP directory';

###############################################################################
sub initPlugin { 
  my $user = $_[2];

  $isInitialized = 0;

  TWiki::Func::registerTagHandler('LDAP', \&handleLdap);
  TWiki::Func::registerTagHandler('LDAPUSERS', \&handleLdapUsers)
    unless ( $TWiki::cfg{Plugins}{LdapNgPlugin}{DisableLDAPUSERS} );

  &initRest()
    if $TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Enabled} && (
      !$TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{RequireLoggedIn} ||
      $TWiki::cfg{DefaultUserLogin} ne $user
    );
   
  return 1; 
}

###############################################################################
sub initCore {
  return if $isInitialized;
  eval 'use TWiki::Plugins::LdapNgPlugin::Core;';
  die $@ if $@;
  $isInitialized = 1;
}

###############################################################################
sub handleLdap {
  initCore();
  return TWiki::Plugins::LdapNgPlugin::Core::handleLdap(@_);
}

###############################################################################
sub handleLdapUsers {
  initCore();
  return TWiki::Plugins::LdapNgPlugin::Core::handleLdapUsers(@_);
}

###############################################################################
sub initRest {
   # Add REST Handler
   TWiki::Func::registerRESTHandler('get', \&_restHandleLdapUser);

   # If we are using view or viewauth, we load all wikiUsers for 
   # use in &renderWikiWordHandler
   my %context = %{TWiki::Func::getContext()};
   if (defined $context{'view'}) {
      eval 'use TWiki::Contrib::LdapContrib;';
      die $@ if $@;
      my $session = $TWiki::Plugins::SESSION;
      my $ldap = TWiki::Contrib::LdapContrib::getLdapContrib($session);
      %wikiUsers = map { $_ => 1 } @{$ldap->getAllWikiNames()};
   }
}

###############################################################################
sub renderWikiWordHandler {
   # Add a <span class='wikiUser'> around all WikiUsers in the topic, and add
   # CSS and JS if at least one wikiUser is found. The JS will intercepts 
   # mouse hover's on these <span>'s, and then ask REST for some user info
   my( $linkText, $hasExplicitLinkLabel, $web, $topic ) = @_;
   my $addCSSJS = 0;
   if ($TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Enabled} && 
      $web eq $TWiki::cfg{UsersWebName} && 
      defined $wikiUsers{$topic}) {

      unless ($headAdded) {
         my $base = '%PUBURLPATH%/%SYSTEMWEB%/LdapNgPlugin';
         my $head = "<script src='$base/DynamicWikiNames.js'></script>";
         $head .= "<link rel='stylesheet' href='$base/DynamicWikiNames.css' type='text/css' media='all' />";
         TWiki::Func::addToHEAD('LDAPNGPLUGIN_DYNAMICWIKINAMES', $head);
         $headAdded = 1;
      }

      return "<span class='wikiUser $topic'>$linkText</span>";

   }
}

###############################################################################
sub _restHandleLdapUser {
   my ($session) = @_;

   # By asking /rest/LdapNgPlugin/get?wikiUser=<ValidWikiUser>,
   # the asker will receive a LDAP summary of the LoginName behind that WikiUser,
   # based on the parameters given in 
   # $TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Parameters}

   # Which wikiUser is being asked for?
   return '' unless CGI::param('wikiUser'); 
   my $wikiUser = CGI::param('wikiUser');

   # Find the corresponding LoginName
   my $ldap = TWiki::Contrib::LdapContrib::getLdapContrib($session);
   my $loginName = $ldap->getLoginOfWikiName($wikiUser);
   return '' unless $loginName;

   my $filter = $TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Filter};
   my $format = $TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Format};

   return "Error: LDAP filter missing in configure." unless $filter;
   return "Error: Format missing in configure." unless $format;
   return "Error: \$TWiki::cfg{Ldap}{LoginAttribute} is not defined in configure." unless defined $TWiki::cfg{Ldap}{LoginAttribute};
    
   $filter =~ s/\A\(//;
   $filter =~ s/\)\z//;
   my $params = {
     '_DEFAULT' => "(&($TWiki::cfg{Ldap}{LoginAttribute}=$loginName)($filter))",
     'format'   => $format
   };

   my @args = (
      $session,
      $params,
      $session->{topicName}, 
      $session->{webName}, 
   );

   my $text = TWiki::Func::renderText(&handleLdap(@args));

   $text =~ s/\t+/ /g;
   if (defined $TWiki::cfg{AntiSpam}{EmailPadding}) {
      $text =~ s/\Q$TWiki::cfg{AntiSpam}{EmailPadding}\E//g;
   }
   return $text;
}

1;
