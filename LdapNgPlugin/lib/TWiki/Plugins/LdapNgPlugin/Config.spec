# ---+ Extensions
# ---++ LdapNgPlugin
# **BOOLEAN**
$TWiki::cfg{Plugins}{LdapNgPlugin}{UseDefaultServer} = 1;
# **BOOLEAN**
$TWiki::cfg{Plugins}{LdapNgPlugin}{DisableLDAPUSERS} = 1;
# **STRING**
$TWiki::cfg{Plugins}{LdapNgPlugin}{Helper} = '';
# **BOOLEAN**
$TWiki::cfg{Plugins}{LdapNgPlugin}{CacheBlob} = 0;
# **BOOLEAN**
# Clear encoded variables
$TWiki::cfg{Plugins}{LdapNgPlugin}{AutoClear} = 0;
# **BOOLEAN**
# Inert separator after header and before footer
$TWiki::cfg{Plugins}{LdapNgPlugin}{SeparatorAfterHeaderBeforeFooter} = 0;
# **BOOLEAN**
# Enable the dynamic WikiNames feature, for a LDAP-generated summary when hovering over a WikiName
$TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Enabled} = 1;
# **BOOLEAN**
# Require that the user is authenticated to use the dynamic WikiName feature
$TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{RequireLoggedIn} = 1;
# **STRING**
# What classifies a user?
$TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Filter} = 'objectClass=posixAccount';
# **STRING**
# How should the info box for Dynamic WikiNames be formatted? 
$TWiki::cfg{Plugins}{LdapNgPlugin}{DynamicWikiNames}{Format} = '|Name|$givenName $sn|$n|Mail|$mail|';
1;
