var twikiPath;
var pubPath
{
    var scripts = document.getElementsByTagName('script');
    var thisScript = scripts[scripts.length-1];
    pubPath = thisScript.src.replace(/[^\/]+$/, '');
    twikiPath = pubPath.replace(/pub.+/,'');
}

var userInfo = {};
var userBox;

$(function() {

    // Go trough every .wikiUser element in DOM, 
    // wrap every element and their master <a>
    $('#patternMainContents .wikiUser').each(function() {
      
        // Find the WikiUser
        var search = $(this).attr('class').match(/^wikiUser\s*(.*)$/);
        if (!(search === null)) {
            var wikiUser = search[1];

            // Initialize the wikiUser in the userInfo array
            if (!(wikiUser in userInfo)) {
                userInfo[wikiUser] = false;
            }
            
            // Create the wrapper
            $(this).parent().wrap('<div class="' + $(this).attr('class') + '" style="position: relative; display: inline-block;"></div>'); 
            $(this).parent().after('<div class="wikiuser-infobox ' + wikiUser + '"></div>');
            $(this).parent().text($(this).text());

        }
    });

    // A users hovers over a .wikiUser element
    var timer;
    $('#patternMainContents .wikiUser').hover(
        function(e) {
            var that = $(this); 
                
            clearTimer(timer);

            // We only go further if we can find a WikiUser
            var search = that.attr('class').match(/^wikiUser\s*(.*)$/);
            if (!(search === null)) {
                var wikiUser = search[1];

                // If the user hovers longer than 100ms, we are going to show the infobox
                timer = setTimeout(function() {

                    // Get the infobox 
                    var infoBox = that.children('.wikiuser-infobox');
        
                    // If the information about this WikiUser is not already fetched via rest, we fetch it
                    if (!(wikiUser in userInfo) || userInfo[wikiUser] === false) {

                        // Show a spinner until we fetch the infobox
                        var html = '<img src="' + pubPath + 'spinner.gif" class="spinner" />';
                        infoBox.html(html);
                        infoBox.show();

                        // Initialize the userInfo entry if unitialized (happens when preCache is false)
                        if (!(wikiUser in userInfo)) {
                          userInfo[search[wikiUser]] = false;
                        }

                        // Fetch the user, store it,  show it
                        // 1. Fetch it
                        $.ajax({
                            dataType : 'html',
                            url : twikiPath + 'bin/rest/LdapNgPlugin/get',
                            data : {
                                'wikiUser' : wikiUser
                            },
                            success : function(html, textStatus, jqXHR) {
                                // 2. Store it
                                userInfo[wikiUser] = html;

                                // 3. Show it
                                infoBox.html(html);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // abort, abort, abort
                                $('.wikiUser > span.questionmark').remove();
                                $('.wikiUser').removeClass('wikiUser');
                                $('.wikiuser-infobox').remove();
                            }
                        }); 
                    } else {
                        // We already have the infobox via an previous hover.
                        // Show the infoBox. 
                        infoBox.html(userInfo[wikiUser]);
                        infoBox.show();
                    }
                }, 150);
            }
        }, function (e) {
            // mouse out
            clearTimer(timer);

            // Hide all infoboxes
            $(this).children('.wikiuser-infobox').hide();
        }
    );

    $('.wikiUser').click(function(e) {
        // If the user clicks on a WikiUser, we abort the attempt 
        // to show the infobox by clearing by clearing the timer
        clearTimer(timer);
    });

});


// Clear a timer
function clearTimer (timer) {
    if (timer) {
        clearTimeout(timer);
        timer = null;
    }
}
