# ---+ Extensions
# ---++ ChartPlugin
# **BOOLEAN EXPERT**
# Allow generating a png file. In case generating png file causes fatal error,
# you can switch this off as workaround.
$TWiki::cfg{Plugins}{ChartPlugin}{AllowPng} = 1;
1;

