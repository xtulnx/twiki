# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2001-2014 Peter Thoeny, Peter[at]Thoeny.org
# Copyright (C) 2008-2014 TWiki Contributors
# Copyright (C) 2002-2006 Crawford Currie, cc@c-dot.co.uk
# Copyright (C) 2000-2001 Andrea Sterbini, a.sterbini@flashnet.it
#
# For licensing info read LICENSE file in the TWiki root.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::TWikiDrawPlugin;

use vars qw(
        $web $topic $user $installWeb $VERSION $RELEASE $editButton
    );

$VERSION = '$Rev: 26846 $';
$RELEASE = '2013-05-17';

my $debug;
my $editmess;
my $noeditmess;

sub initPlugin {
  ( $topic, $web, $user, $installWeb ) = @_;

  # check for Plugins.pm versions
  if( $TWiki::Plugins::VERSION < 1 ) {
    TWiki::Func::writeWarning( "Version mismatch between TWikiDrawPlugin and Plugins.pm" );
    return 0;
  }

  # Get plugin preferences
  $debug = TWiki::Func::isTrue( $TWiki::cfg{Plugins}{TWikiDrawPlugin}{Debug} );
  $editButton = TWiki::Func::getPreferencesValue( "TWIKIDRAWPLUGIN_EDIT_BUTTON" );
  $editmess = TWiki::Func::getPreferencesValue( "TWIKIDRAWPLUGIN_EDIT_TEXT" ) ||
    "Edit drawing using TWiki Draw applet (requires a Java 1.1 enabled browser)";
  $editmess =~ s/['"]/`/g;
  $noeditmess = TWiki::Func::getPreferencesValue( "TWIKIDRAWPLUGIN_NOEDIT_TEXT" ) ||
    "Drawing is not editable here (insufficient permission or read-only site)";
  $noeditmess =~ s/['"]/`/g;

  # Register tag handler
  TWiki::Func::registerTagHandler( "DRAWING", \&_DRAWING );

  return 1;
}

sub _DRAWING {
  my( $session, $params, $topic, $web ) = @_;
  my $nameVal = $params->{_DEFAULT} || 'untitled';
  $nameVal =~ s/[^A-Za-z0-9_\.\-]//go; # delete special characters

  # FIXME: should really use TWiki server-side include mechanism....
  my $pubDir = TWiki::Func::getPubDir($web);
  my $mapFile = "$pubDir/$web/$topic/$nameVal.map";
  my $drawRev = _getRev($web, $topic, "$nameVal.draw");
  my $gifRev = _getRev($web, $topic, "$nameVal.gif");
  my $img = "src=\"%ATTACHURLPATH%/$nameVal.gif?r=$gifRev\"";
  unless( -e "$pubDir/$web/$topic/$nameVal.gif" ) {
    $img = "src=\"%PUBURLPATH%/%SYSTEMWEB%/TWikiDrawPlugin/newdrawing.gif\"";
  }
  my $editable = $editButton && _checkEditable( $web, $topic );
  my $param1 = $nameVal;
  my $param2 = $drawRev;
  my $param3 = $debug ? time : $RELEASE;
  my $editUrl = $editable ?
    TWiki::Func::getOopsUrl($web, $topic, "twikidraw", $param1, $param2, $param3)
    : 'javascript:void(0)';
  my $imgText = "";
  my $edittext = $editable ? $editmess : $noeditmess;
  $edittext =~ s/%F%/$nameVal/g;
  my $hover =
    "onmouseover=\"window.status='$edittext';return true;\" ".
      "onmouseout=\"window.status='';return true;\"";

  my $map = TWiki::Func::readFile( $mapFile ) if -e $mapFile;
  $map =~ s/^\s+|\s+$//g if $map;

  if ( $map ) {
    my $mapname = $nameVal;
    $mapname =~ s/^.*\/([^\/]+)$/$1/;
    $img .= " usemap=\"#$mapname\"";
    # Unashamed hack to handle Web.TopicName links
    $map =~ s/href=\"((\w+)\.)?(\w+)(#\w+)?\"/&_processHref($2,$3,$4,$web)/ge;
    $map = TWiki::Func::expandCommonVariables( $map, $topic );
    $map =~ s/%MAPNAME%/$mapname/g;
    $map =~ s/%TWIKIDRAW%/$editUrl/g;
    $map =~ s/%EDITTEXT%/$edittext/g;
    $map =~ s/%HOVER%/$hover/g;
    $map =~ s/[\r\n]+//g;

    # Add an edit link just above the image if required
    $imgText = "<br /><a href=\"$editUrl\" title=\"$edittext\" $hover>".
      "Edit</a><br />" if ( $editable );
#	$imgText = "<br /><button onclick=\"window.location='$editUrl'\" $hover>".
#          "Edit</button><br />" if ( $editButton == 1 );
    $imgText .= "<img $img />$map";
  } else {
    $imgText = "<img $img $hover alt=\"$edittext\" title=\"$edittext\" />";
    # insensitive drawing; the whole image gets a rather more
    # decorative version of the edit URL
    $imgText = "<a href=\"$editUrl\" $hover>$imgText</a>" if ( $editable == 1 );
#	$imgText = "<br /><button onclick=\"window.location='$editUrl'\" $hover>".
#          "$edittext</button><br />" if ( $editButton == 1 );
  }
  return $imgText;
}

sub _getRev {
    my ( $web, $topic, $attach ) = @_;

    my ( $date, $user, $rev, $comment ) =
        TWiki::Func::getRevisionInfo( $web, $topic, undef, $attach );

    return $rev || 0;
}

sub _processHref {
    my ( $web, $topic, $anchor, $defweb ) = @_;

    $web = $defweb unless ( $web );
    $anchor = "" unless $anchor;

    return "href=\"%SCRIPTURLPATH{\"view\"}%/$web/$topic$anchor\"";
}

sub _checkEditable {
    my ( $web, $topic ) = @_;

    my $context = TWiki::Func::getContext();
    return 0 if $context->{inactive} || $context->{content_slave};

    my $wikiName = TWiki::Func::getWikiName();
    return TWiki::Func::checkAccessPermission(
        'CHANGE', $wikiName, undef, $topic, $web, undef);
}

1;
