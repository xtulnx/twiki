# AddOn for TWiki Collaboration Platform, http://TWiki.org/
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

=pod

---+ package RunTopicsAddOn

Invoked via the commandline, RunTopicsAddOn runs 'view' on given 
topics in given webs, creating a topic if a template is given. 
The invoking user is 'admin', so access controls are bypassed.
The 'all' keyword works on webs, which selects all webs.

Example while standing in the tools/ folder:

Run 'view' on the WebHome topic on CMS and Atlas webs
./runtopics webs=CMS,Atlas topics=WebHome

Run 'view' on the FooBar topic in all webs, creating it with 
by cloning Web.TemplateTopic if it does not exist.
./runtopics webs=all topics=FooBar Web.TemplateTopic 

=cut

# Always use strict to enforce variable scoping
use strict;

package TWiki::Contrib::RunTopicsAddOn;

use strict;

use vars qw( $VERSION $RELEASE $SHORTDESCRIPTION );

$VERSION = '$Rev: 0 (2014-05-20) $';
$RELEASE = '1.0';
$SHORTDESCRIPTION = 'TWiki tool to run view on a given set of topics in a given set of webs.';

use Time::HiRes qw(gettimeofday tv_interval);
require TWiki::Func;    # The plugins API
require TWiki::Plugins; # For the API version

sub runTopics {
  my ($webs, $topics, $template) = @_;

  # 1. Create a TWiki session
  my $session = new TWiki('admin', undef, {command_line => 1});
  $TWiki::Plugins::SESSION = $session;

  $template ||= '';
  my ($templateWeb, $templateTopic) = ($template =~ m/^([^.]+)\.(.+)$/);

  my $error = 0;
  if (!defined $webs || !defined $topics) {
    $error = 1;
  } elsif ($webs !~ m/^webs=.+$/ || $topics !~ m/^topics=.+$/) {
    $error = 1;
  } elsif ( defined $templateWeb && !defined $templateTopic ) {
    $error = 1;
  } elsif ( defined $templateWeb && !TWiki::Func::topicExists($templateWeb, $templateTopic) ) {
    $error = 1;
  } 

  die "Invalid argument list. Example: './runTopics webs=all topics=FooBar Web.TemplateTopic" if $error;

  $webs =~ s/^webs=//;
  $topics =~ s/^topics=//;
  my ($templateTopicMeta, $templateTopicText);
  if (defined $templateWeb && defined $templateTopic) {
    ($templateTopicMeta, $templateTopicText) = TWiki::Func::readTopic($templateWeb, $templateTopic);
  }

  my (@webList, @topicList);
  unless ($webs eq 'all') {
    @webList = split /\s*,\s*/, $webs;
  } else {
    @webList = TWiki::Func::getListOfWebs();
  }
  
  @topicList = split /\s*,\s*/, $topics;


  # 3. Iterate over the webs and topics, creating topics if templatetopic is set
  for my $web (@webList) {
    for my $topic (@topicList) {
      if (!TWiki::Func::topicExists($web, $topic) && defined $templateWeb && defined $templateTopic) {
        print "Creating $web.$topic from template $templateWeb.$templateTopic ...";
        TWiki::Func::saveTopic($web, $topic, undef, $templateTopicText);
        print "DONE !\n";
      }
      
      if (TWiki::Func::topicExists($web, $topic)) {
        my $t0 = [gettimeofday()];
        print STDERR "Running 'perl -T view -topic $web.$topic -user admin ...' ";
        `perl -T view -topic $web.$topic -user admin`;
        printf STDERR "DONE! (%s)\n", tv_interval($t0);
      } else {
        print STDERR "Topic $web.$topic does not exist, and no template is given. Skipping ...\n";
      }
    }  
  }
}

1;
