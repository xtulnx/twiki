# ---+ Extensions
# ---++ MailInContrib

# **PERL**
# The configuration is in the form of an (perl) array of mailbox
# specifications. Each specification defines a number of fields:
# <ul>
#  <li> onError - what you want TWiki to do if an error occurs while processing
#   a mail (comma-separated list of options). Available options:
#   <ul>
#    <li> reply - reply to the sender</li>
#    <li> delete - delete the mail from the inbox</li>
#    <li> log - log the error to the TWiki warning log</li>
#   </ul>
#   Note: if you don't specify delete, TWiki will continue to try to process the
#   mail each time the cron job runs.
#  </li>
#  <li> topicPath - where you want TWiki to look for the name of the target
#   topic (comma-separated list of options). Available options:
#   <ul>
#    <li> to - look in the To:, CC: and BCC: for <code>Web.TopicName@example.com</code>
#     or <code>"Web TopicName" &lt;web.topicname@example.com&gt;</code> </li>
#    <li> to+tag - look in the To:, CC: and BCC: for <code>Web+TopicName@example.com</code>
#     or <code>mailbox+Web.TopicName@example.com</code>. The to+tag option is useful
#     if you want to use a single mailbox to send mail to multiple topics. The tag
#     separator can be any single non-alphanumeric character, usually a '+'. </li>
#    <li> subject - look in the Subject: e.g "Web.TopicName: mail for TWiki".
#     If "to" and "subject" are both enabled, but a valid topic name is not found
#     in the To:/CC:/BCC:, the Subject: will still be parsed to try and get the
#     topic.</li>
#   </ul>
#  <li> removeTopicFromSubject - if "true", remove the topic name from the subject
#      (only useful if the <code>topicPath</code> contains "subject").</li>
#  <li> folder - name of the mail folder<br />
#      Note: support for POP3 requires that the Email::Folder::POP3
#      module is installed. Support for IMAP requires
#      Email::Folder::IMAP etc.
#      Folder names are as described in the documentation for the
#      relevant Email::Folder type e.g. for POP3, the folder name might be:
#      <code>pop://me:123@mail.isp.com:110/</code></li>
#  <li> user - name of the default user.<br />
#      The From: in the mail is parsed to extract the senders email
#      address. This is then be looked up in the TWiki users database
#      to find the wikiname. If the user is not found, then this default
#      user will be used. If the default user is left blank, then the
#      user *must* be found from the mail.
#      The identity of the sending user is important for access controls.
#      This must be a user *login* name.e.g. 'guest'
#  </li>
#  <li> onSuccess - what  you want TWiki to do with messages that have been successfully added to a TWiki topic
#     (comma-separated list of options)
#     Available options:
#   <ul>
#    <li> reply - reply to the sender</li>
#    <li> delete - delete the mail from the inbox</li>
#   </ul>
#  <li> defaultWeb - name of the web to save mails in if the web name isn't
#   specified in the mail. If this is undefined or left blank, then mails must
#   contain a full web.topicname or the mail will be rejected.</li>
#  <li> onNoTopic - what do you want TWiki to do if it can't find a valid
#   topic name in the mail (one option). Available options:
#   <ul>
#    <li> error - treat this as an error (overrides all other options)</li>
#    <li> spam - save the mail in the spambox topic.
#    Note: if you clear this, then TWiki will simply ignore the mail..</li>
#   </ul>
#  </li>
#  <li> spambox - optional required of onNoTopic = spam. Name of the topic
#   where you want to save mails that don't have a valid web.topic. You must
#   specify a full web.topicname
#  </li>
#  <li> ignoreMessageTime - optional. If "false" (which is the default),
#   then the MailInContrib ignores previously-processed mail, as determined
#   by the mail "Date". If "true", then MailInContrib does not filter mail
#   based on the "Date" - which may be important if the interval between
#   <code>mailincron</code> runs is less than the message propagation time
#   or less than the error in the sending PC's clock. 
#   <em>It is <strong>only</strong> useful to set <code>ignoreMessageTime
#   </code> to 1 if both <code>onError</code> and <code>onSuccess</code>
#   contain "delete".</em> Otherwise, <em>every</em> message will processed 
#   <em>every time</em> that <code>mailincron</code> runs.
#  </li>
#  <li> post - optional, defines how to post a mail. It contains a hash with:
#   <ul>
#    <li> template - optional, default is 'default'. MailInContrib template to 
#     use to format the incoming e-mail. Consult the MailInContrib documentation
#     for detail.
#    </li>
#    <li> templateTopic - optional, default is 'WebTopicEditTemplate'. Topic
#     template to use in case the mail topic does not exist.
#    </li>
#    <li> web - optional, default is '$web', available tokens '$web' and '$topic'.
#     Use this to redefine the web. For example, specify '$web/Emails' to post to
#     an Eng/Emails subweb if the user specified the Eng web.
#    </li>
#    <li> topic - optional, default is '$topic', available tokens '$web', '$topic'
#     and '$autoinc(0001)'. Use this to redefine the topic. For example, specify
#     '$topicFeedback' to post to a ProjectID1234Feedback topic when the user
#     specified the ProjectID1234 topic. Use '$autoinc(0001)' for auto-numbered
#     topics, similar to the AUTOINC0001 feature of the core TWiki. Specify
#     '$topic-Mail$autoinc(001)' to get topics such as Project1234-Mail001,
#     Project1234-Mail002, etc.
#    </li>
#    <li> where - optional, default is 'bottom'. Where in the topic to add the
#     incoming e-mail. Available options:
#     <ul>
#      <li> top - at the top of the topic </li>
#      <li> above - above a marker in the topic </li>
#      <li> below - below a marker the topic </li>
#      <li> bottom - at the bottom of the topic </li>
#     </ul>
#     Consult the MailInContrib documentation for detail.
#    </li>
#    <li> hideQuotes - optional, default is 0. If set to 1, quoted text is hidden
#     in a twisty.
#    </li>
#    <li> attachments - optional, default is 'attach, link'. Available options:
#     <ul>
#      <li> attach - attach files - this is the default </li>
#      <li> ignore - discard attachments </li>
#      <li> nosave - link only to attachments, but do not attach files - useful
#       to refer to attachments in another topic when using the post2 option </li>
#      <li> link - link attachments in mail body </li>
#      <li> hide - hide attachments, do not show in attachment table </li>
#     </ul>
#    </li>
#   </ul>
#  </li>
#  <li> post2 - optional, defines how to post the same mail a second time. It
#   takes the same options like post. Use this for example to post the full 
#   mail to an archive web, then in addition a summary of the e-mail to a 
#   project page. Create a template for the summary post, in it specify 
#   <tt>%<nop>MAILSUMMARY{500}%</tt> instead of <tt>%<nop>MAILBODY%</tt> for
#   the mail body, and
#   <tt>[<nop>[%<nop>WEB%/Archive.%<nop>MAILPREVTOPIC%][Details...]]</tt>
#   to link to the mail archive, assuming the archive is in a sub-web called
#   Archive. Consult the MailInContrib documentation for detail.
#  </li>
#  <li> content - optional, defaults to "extract plain text". 
#   Specifies what part of the mail to extract and how to process it.
#   It takes a number of fields:
#   <ul>
#    <li> type - specifies type of content to extract. 
#       Available options:
#     <ul>
#      <li> text - extract the plain-text portion </li>
#      <li> html - extract the HTML portion, by preference 
#        - reverts to the plain-text if the mail does not contain HTML 
#      </li>
#      <li> debug - extract the whole MIME message, verbatim </li>
#     </ul>
#    </li>
#    <li> processors - specifies a list of processors, to be applied in the order listed.
#       Each processor is described by a perl Hash. The <em>pkg</em> key identifies the
#       Perl package that provides the processor. Any other keys are passed as options
#       to the processor. </li>
#   </ul>
#  </li>
# </ul>
$TWiki::cfg{MailInContrib} = [
 {
   folder => 'pop://example_user:password@example.com/Inbox',
   onError => 'log',
   onNoTopic => 'error',
   onSuccess => 'log delete',
   topicPath => 'to subject',
   removeTopicFromSubject => 1,
   ignoreMessageTime => 0,
   post => {
       template => 'default',
       templateTopic => 'WebTopicEditTemplate',
       web => '$web',
       topic => '$topic',
       where => 'bottom',
       hideQuotes => 0,
       attachments => 'attach, link',
   },
   processors => [
        { pkg => 'TWiki::Contrib::MailInContrib::NoScript' },
        { pkg => 'TWiki::Contrib::MailInContrib::FilterExternalResources' },
   ],
 },
];

1;
