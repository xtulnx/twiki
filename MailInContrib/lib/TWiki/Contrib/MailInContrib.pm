# Extension for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2005-2014 TWiki Contributors. All Rights Reserved.
# Copyright (C) 2009 TWiki Contributors. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Contrib::MailInContrib;

use strict;
use TWiki;
use Assert;

use Email::Folder;
use Email::FolderType::Net;
use Email::MIME;
use Email::Delete;
use Time::ParseDate;
use Error qw( :try );
use Carp;

our $VERSION          = '$Rev: 27555 (2014-05-27) $';
our $RELEASE          = '2014-06-03';
our $SHORTDESCRIPTION = 'E-mail content into TWiki topics';

BEGIN {
    $SIG{__DIE__} = sub { Carp::confess $_[0] };
}

{
    package MIMEFolder;

    use base qw/Email::Folder/;

#    our @ISA = qw(      );

    sub bless_message {
        my $self = shift;
        my $message = shift || die "You must pass a message\n";

        return Email::MIME->new($message);
    }
}

=pod

---++ ClassMethod new( $session )
   * =$session= - ref to a TWiki object
Construct a new inbox processor.

=cut

sub new {
    my ( $class, $session, $debug ) = @_;
    my $this = bless( {}, $class );
    $this->{session}      = $session;
    $this->{debug}        = $debug;
    $this->{startTime}    = time();

    $this->{webNameRegex} = TWiki::Func::getRegularExpression( 'webNameRegex' )
      || qr/[[:upper:]]+[[:alpha:][:digit:]_]*/o;
    my $wikiWordRegex   = TWiki::Func::getRegularExpression( 'wikiWordRegex' ) 
      || qr/[[:upper:]]+[[:lower:][:digit:]]+[[:upper:]]+[[:alpha:][:digit:]]*/o;
    my $abbrevRegex     = TWiki::Func::getRegularExpression( 'abbrevRegex' )
      || qr/[[:upper:]]{3,}s?\b/o;
    # Allow also topic names like Bug-1234 and PID-0016_Summary
    my $dashTopicRegex  = qr/[[:upper:]]+[[:alpha:][:digit:]_\-]+/o;
    $this->{topicNameRegex} = qr/(?:(?:$wikiWordRegex)|(?:$abbrevRegex)|(?:$dashTopicRegex))/o;

    # Find out when we last processed mail
    my $workdir = TWiki::Func::getWorkArea('MailInContrib');
    if ( -e "$workdir/timestamp" ) {
        open( F, "<$workdir/timestamp" ) || die $!;
        $this->{lastMailIn} = <F>;
        chomp( $this->{lastMailIn} );
        close(F);
    }
    else {
        $this->{lastMailIn} = 0;
    }

    return $this;
}

=pod

---++ ObjectMethod wrapUp( $box )
Clean up after processing inboxes, setting the time-stamp
indicating when the processor was last run.

=cut

#SMELL: could this be done in a DESTROY?
sub wrapUp {
    my $this = shift;

    # re-stamp
    my $workdir = TWiki::Func::getWorkArea('MailInContrib');
    open( F, ">$workdir/timestamp" ) || die $!;
    print F $this->{startTime}, "\n";
    close(F);
}

sub _getUser {
    my $u = shift;

    if ( $TWiki::Plugins::SESSION->{users}->can('getCanonicalUserID') ) {
        return $TWiki::Plugins::SESSION->{users}->getCanonicalUserID($u);
    }
    else {
        return $TWiki::Plugins::SESSION->{users}->findUser($u);
    }
}

=pod

---++ ObjectMethod processInbox( $box )
   * =$box= - hash describing the box
Scan messages in the box that have been received since the last run,
and process them for inclusion in TWiki topics.

=cut

sub processInbox {
    my ( $this, $box ) = @_;

    $TWiki::Plugins::SESSION = $this->{session};

    die "No folder specification" unless $box->{folder};

    my $startTime = time();
    my $count = 0;
    my $ftype = Email::FolderType::folder_type( $box->{folder} );
    print STDERR "Process $ftype folder $box->{folder}\n" if $this->{debug};

    my $folder = new MIMEFolder( $box->{folder} );

    my $user;
    my %kill;

    # Set defaults if necessary
    $box->{topicPath}             ||= 'subject';
    $box->{defaultWeb}            ||= '';
    $box->{onNoTopic}             ||= 'error';
    $box->{onError}               ||= 'log';
    $box->{onSuccess}             ||= 'log';
    $box->{post}->{template}      ||= 'default';
    $box->{post}->{where}         ||= 'bottom';
    $box->{content}->{type}       ||= 'text';
    $box->{content}->{processors} ||= [
        { pkg => 'TWiki::Contrib::MailInContrib::NoScript' },
        { pkg => 'TWiki::Contrib::MailInContrib::FilterExternalResources' },
    ];

    # Load the mail templates
    TWiki::Func::loadTemplate('MailInContrib');

    # Load second so that user templates override
    TWiki::Func::loadTemplate('MailInContribUser');

    print STDERR "Scanning $box->{folder}\n" if $this->{debug};
    my $mail;    # an Email::Simple object
    my $num = -1;    # message number
    while ( ( $mail = $folder->next_message() ) ) {
        $num++;

        my $received = 0;
        foreach my $receipt ( $mail->header('Received') ) {
            if ( $receipt =~ /; (.*?)$/ ) {
                $receipt = Time::ParseDate::parsedate($1);
                $received = $receipt if $receipt > $received;
            }
        }
        if ( !$received && $mail->header('Date') ) {

            # Use the send date
            $received = Time::ParseDate::parsedate( $mail->header('Date') );
        }
        $received ||= time();

     # Try to get the target topic by
     #    1. examining the "To" and "cc" addresses to see if either has
     #       a valid web.wikiname (if enabled in config)
     #    2. if the subject line starts with a valid TWiki Web.WikiName
     #       (if optionally followed by a colon, the rest of the subject
     #       line will be ignored)
     #    3. Routing the comment to the spambox if it is enabled
     #    4. Otherwise replying to the user to say "no thanks" if replyonnotopic
        my ( $web, $topic, $user );

        my $subject         = $mail->header('Subject');
        my $originalSubject = $subject;

        my $from = $mail->header('From');

        print STDERR "Message from $from: ", $mail->header('Subject'), "\n"
          if $this->{debug};

        $from =~ s/^.*<(.*)>.*$/$1/;
        my $targets = $this->{session}->{users}->findUserByEmail($from);
        if ( $targets && scalar(@$targets) ) {
            $user = $targets->[0];
        }

        my @to = split( /,\s*/, $mail->header('To') || '' );
        if ( defined $mail->header('CC') ) {
            push( @to, split( /,\s*/, $mail->header('CC') ) );
        }
        # Try BCC via 'Delivered-To' or 'Envelope-To'
        if ( defined $mail->header('Delivered-To') ) {
            push( @to, split( /,\s*/, $mail->header('Delivered-To') ) );
        } elsif ( defined $mail->header('Envelope-To') ) {
            push( @to, split( /,\s*/, $mail->header('Envelope-To') ) );
        }

        # Use the address in the <> if there is one
        @to = map { /^.*<(.*)>.*$/ ? $1 : $_; } @to;
        print STDERR "Targets: ", join( ' ', @to ), "\n" if $this->{debug};
        print STDERR "Subject: $subject\n" if $this->{debug};

        unless ($user) {
            unless ( $box->{user} && ( $user = _getUser( $box->{user} ) ) ) {
                $this->_onError(
                    $box,
                    $mail,
                    'Could not determine submitters WikiName from'
                      . "\nFrom: $from\nand there is no valid default username",
                    \%kill,
                    $num
                );
                next;
            }
        }

        print STDERR "User is '", ( $user || 'undefined' ), "'\n"
          if ( $this->{debug} );

        # See if we can get a valid web.topic out of to: or cc:
        my $webNameRegex   = $this->{webNameRegex};
        my $topicNameRegex = $this->{topicNameRegex};
        if ( $box->{topicPath} =~ /\bto\b([^ ])\btag\b/ ) {
            my $tagSeparator = $1; # usually a '+'
            # Text for <Web+TopicName@example.com> and <user+Web.TopicName@example.com>
            foreach my $target (@to) {
                my ( $guessweb, $guesstopic );
                if( $target =~ /^($webNameRegex)\Q$tagSeparator\E($topicNameRegex)\@/i ) {
                    ( $guessweb, $guesstopic ) = ( $1, $2 );
                } elsif( $target =~ /^[\w\.\-]+\Q$tagSeparator\E(?:($webNameRegex)\.)($topicNameRegex)\@/i ) {
                    ( $guessweb, $guesstopic ) = ( $1, $2 );
                } else {
                    next;
                }
                ( $guessweb, $guesstopic ) = TWiki::Func::normalizeWebTopicName(
                  ( $guessweb || $box->{defaultWeb} ), $guesstopic );
                if ( TWiki::Func::topicExists( $guessweb, $guesstopic ) ) {
                    # Found an existing topic
                    ( $web, $topic ) = ( $guessweb, $guesstopic );
                    last;
                }
            }

        } elsif ( $box->{topicPath} =~ /\bto\b/ ) {
            # Test for <Web.TopicName@example.com> and <TopicName@example.com>
            foreach my $target (@to) {
                next
                  unless $target =~ /^(?:($webNameRegex)\.)($topicNameRegex)\@/i;
                my ( $guessweb, $guesstopic ) = TWiki::Func::normalizeWebTopicName(
                  ( $1 || $box->{defaultWeb} ), $2 );
                if ( TWiki::Func::topicExists( $guessweb, $guesstopic ) ) {

                    # Found an existing topic
                    ( $web, $topic ) = ( $guessweb, $guesstopic );
                    last;
                }
            }
        }

        # If we didn't get the name of an existing topic from the
        # To: or CC:, use the Subject:
        if (  !$topic
            && $box->{topicPath} =~ /\bsubject\b/
            && $subject =~
s/^(\s*(?:($webNameRegex)\.)?($topicNameRegex)(:\s*|\s*$))/$box->{removeTopicFromSubject} ? '' : $1/e
          )
        {
            ( $web, $topic ) = TWiki::Func::normalizeWebTopicName(
                ( $2 || $box->{defaultWeb} ), $3 );

            # This time the topic doesn't have to exist
        }

        $web ||= $box->{defaultWeb};

        print STDERR "Topic $web.", $topic || '', "\n" if $this->{debug};

        unless ( TWiki::Func::webExists($web) ) {
            $topic = '';
            # restore original subject in case the subject line specified a web that does
            # not exist
            $subject = $originalSubject;
        }

        if ( !$topic ) {
            if ( $box->{onNoTopic} =~ /\berror\b/ ) {
                $this->_onError(
                    $box,
                    $mail,
                    'Could not add your submission; no valid web.topic found in'
                      . "\nTo: "
                      . $mail->header('To')
                      . "\nSubject: "
                      . $subject,
                    \%kill,
                    $num
                );
            }
            if ( $box->{onNoTopic} =~ /\bspam\b/ ) {
                if ( $box->{spambox} && $box->{spambox} =~ /^(.*)\.(.*)$/ ) {
                    ( $web, $topic ) = ( $1, $2 );
                }
            }
            print STDERR "Skipping; no topic\n" if ( $this->{debug} );
            next unless $topic;
        }

        if ( $box->{ignoreMessageTime} or $received > $this->{lastMailIn} ) {
            my $err = '';
            my $sender = $mail->header('From') || 'unknown';
            unless ( TWiki::Func::webExists($web) ) {
                $err = "Web $web does not exist";
            }
            else {
                my $to     = $mail->header('To') || '';
                my $cc     = $mail->header('CC') || '';
                my $date   = $mail->header('Date') || '%DATE%';
                my @attachments = ();
                my $body        = '';

                $this->_extract( $mail, \$body, \@attachments, $box );

                # First post
                $err .= $this->_saveTopic( $box->{post}, $user, $web, $topic, $body, $subject,
                                           $sender, $to, $cc, $date, \@attachments );
                # Second post
                if( !$err && $box->{post2} && ref( $box->{post2} ) eq 'HASH' ) {
                    $box->{post2}->{prevWeb}   = $box->{post}->{prevWeb};
                    $box->{post2}->{prevTopic} = $box->{post}->{prevTopic};
                    my $err2 = $this->_saveTopic( $box->{post2}, $user, $web, $topic, $body,
                                           $subject, $sender, $to, $cc, $date, \@attachments );
                    $err .= "; $err2" if( $err2 );
                }
            }

            print "Mail-In start at: " . _formatTime( $startTime ) . "\n" unless( $count++ );
            if ($err) {
                print "- Processed mail from $sender for $web.$topic, error: $err\n";
                $this->_onError(
                    $box,
                    $mail,
                    "TWiki encountered an error while adding your mail to $web.$topic: $err",
                    \%kill,
                    $num
                );
            }
            else {
                print "- Processed mail from $sender for $web.$topic, OK\n";
                if ( $box->{onSuccess} =~ /\breply\b/ ) {
                    $this->_reply( $box, $mail,
                      "Thank you for your successful submission to $web.$topic"
                    );
                }
                if ( $box->{onSuccess} =~ /\bdelete\b/ ) {
                    $kill{ $mail->header('Message-ID') } = $num;
                }
            }
        }
        elsif ( $this->{debug} ) {
            print STDERR "Skipping; late: $received <= $this->{lastMailIn}\n";
        }
    }

    my $returnCode = 0;
    eval 'use Email::Delete';
    if ($@) {
        TWiki::writeWarning("Cannot delete from inbox: $@\n");
    }
    else {
        Email::Delete::delete_message(
            from     => $box->{folder},
            matching => sub {
                my $test       = shift;
                my $message_id = $test->header('Message-ID');
                if ( defined $message_id and defined $kill{$message_id} ) {
                    print STDERR "Delete $message_id\n"
                      if $this->{debug};
                    $returnCode = 1;
                }
            }
        );
    }
    print "- Mail-In end at: " . _formatTime( time() ) . "\n" if( $count );
    return $returnCode;
}

sub _onError {
    my ( $this, $box, $mail, $mess, $kill, $num ) = @_;

    $this->{error} = $mess;    # used by the tests

    print STDERR "ERROR: $mess\n" if ( $this->{debug} );

    if ( $box->{onError} =~ /\blog\b/ ) {
        TWiki::Func::writeWarning($mess);
    }
    if ( $box->{onError} =~ /\breply\b/ ) {
        $this->_reply( $box, $mail,
            "TWiki found an error in your e-mail submission\n\n$mess\n\n"
              . $mail->as_string() );
    }
    if ( $box->{onError} =~ /\bdelete\b/ ) {
        $kill->{ $mail->header('Message-ID') } = $num;
    }
}

sub _extract {
    my ( $this, $mime, $text, $attach, $box ) = @_;

    $this->{currentBox}  = $box;
    $this->{currentMime} = $mime;

    if ( $box->{content}->{type} =~ /debug/i ) {
        $$text .= "<verbatim>" . $mime->as_string . "</verbatim>";
    }
    elsif ( $box->{content}->{type} =~ /html/i ) {
        $this->_extractHtmlAndAttachments( $mime, $text, $attach );
    }
    else {
        $this->_extractPlainTextAndAttachments( $mime, $text, $attach );
    }
}

sub _currentBox {
    my $this = shift;
    return $this->{currentBox};
}

sub _currentMime {
    my $this = shift;
    return $this->{currentMime};
}

sub _extractHtmlAndAttachments {
    my ( $this, $mime, $text, $attach ) = @_;
    my $ct = $mime->content_type || 'text/plain';
    my $dp = $mime->header('Content-Disposition') || 'inline';
    print STDERR "\nContent-type: $ct\n" if $this->{debug};
    if ( $ct =~ m[multipart/mixed] ) {
        $this->_extractMultipartMixed( $mime, $text, $attach );
    }
    elsif ( $ct =~ m[multipart/alternative] ) {
        $this->_extractMultipartAlternative( $mime, $text, $attach );
    }
    elsif ( $ct =~ m[multipart/related] ) {
        my $found;
        $found = _extractMultipartHtml( $mime, $text, $attach );
        print STDERR "Found multipart/related HTML\n"
          if $found and $this->{debug};
        if ( not $found ) {
            print STDERR "Cannot find HTML. Extracting plain text\n"
              if $this->{debug};
            $this->_extractPlainTextAndAttachments( $mime, $text, $attach );
        }
    }
    elsif ( $ct =~ m[text/html] and $dp =~ /inline/ ) {
        print STDERR "Extracting text/html\n" if $this->{debug};
        $this->_extractPlainHtml( $mime, $text );
    }
    else {
        print STDERR "Extracting plain text and attachments\n"
          if $this->{debug};
        $this->_extractPlainTextAndAttachments( $mime, $text, $attach );
    }
}

sub _extractMultipartMixed {
    my ( $this, $mime, $text, $attach ) = @_;
    foreach my $part ( grep { $_ != $mime } $mime->parts() ) {
        print STDERR "Multipart/mixed: Recursing\n" if $this->{debug};
        $this->_extractHtmlAndAttachments( $part, $text, $attach );
    }
}

sub _extractMultipartAlternative {
    my ( $this, $mime, $text, $attach ) = @_;

    print STDERR "Multipart/alternative\n" if $this->{debug};

    # See what alternatives are available
    my @alternates = map +{
        mime => $_,
        ct   => $_->content_type || 'text/plain',
      },
      grep { $_ != $mime } $mime->parts();

    my ($multipartRelatedAlternate) =
      grep { $_->{ct} =~ m[multipart/related] } @alternates;
    my ($htmlAlternate) = grep { $_->{ct} =~ m[text/html] } @alternates;

    # Pick one
    my $found;
    if ($multipartRelatedAlternate) {
        $found =
          $this->_extractMultipartHtml( $multipartRelatedAlternate->{mime},
            $text, $attach );
        print STDERR "Found multipart/related HTML\n"
          if $found and $this->{debug};
    }
    if ( $htmlAlternate and not $found ) {
        $found = $this->_extractPlainHtml( $htmlAlternate->{mime}, $text );
        print STDERR "Found text/html\n" if $found and $this->{debug};
    }
    if ( not $found ) {
        print STDERR "Cannot find HTML - Extracting plain text\n"
          if $this->{debug};
        $this->_extractPlainTextAndAttachments( $mime, $text, $attach );
    }
}

sub _extractMultipartHtml {
    my ( $this, $mime, $text, $attach ) = @_;
    my @bits = map +{
        mime => $_,
        ct   => $_->content_type || 'text/plain',
        dp   => $_->header('Content-Disposition') || 'inline'
      },
      grep { $_ != $mime } $mime->parts();
    my ($htmlBit) =
      grep { $_->{ct} =~ m[text/html] and $_->{dp} =~ /inline/ } @bits;
    return unless $htmlBit;    # Not found

    my $html = $this->_extractAndTrimHtml( $htmlBit->{mime} );
    return unless $html;
    for my $bit ( grep { $_ != $htmlBit } @bits ) {
        my $filename = $bit->{mime}->filename();
        $filename = _sanitizeAttachmentName( $bit->{mime}->filename() )
          if defined $filename;
        my $cid = $bit->{mime}->header('Content-ID') || '';
        my $cid_used = '';
        print STDERR "cid:[$cid]\n" if $cid and $this->{debug};
        if ( $cid =~ /^\s*<?((.*?)\@.*?)>?\s*$/ ) {
            $cid = $1;
            $filename = _sanitizeAttachmentName($2);
            $cid_used =
              ( $html =~ s{"cid:\Q$cid\E"}{"%ATTACHURLPATH%/$filename"} );
        }
        if ( $filename
            and ( $bit->{dp} !~ /inline/ or ( $cid and $cid_used ) ) )
        {
            push(
                @$attach,
                {
                    payload  => $bit->{mime}->body(),
                    filename => $filename
                }
            );
        }
    }
    $$text .=
"<literal><div class=\"twikiMailInContribHtml\">$html</div></literal>\n";
    return 1;
}

sub _extractPlainHtml {
    my ( $this, $mime, $text, $box, $topMime ) = @_;
    my $html = $this->_extractAndTrimHtml($mime);
    return unless $html;
    $$text .=
"<literal><div class=\"twikiMailInContribHtml\">$html</div></literal>\n";
    return 1;
}

sub _extractAndTrimHtml {
    my ( $this, $mime, $box, $topMime ) = @_;
    return unless $mime;
    my $html = $mime->body();
    return unless $html;

# Remove anything outside the body tag, and change the body tag into a div tag
# It is better to keep the body tag as a tag (and not just discard it altogether)
# because that tag sometimes has attributes that should be retained.
    $html =~ s{.*<body([^>]*>.*)</body>.*}{<div$1</div>}is;

    $html = $this->_applyProcessors( $mime, $html );

    return unless $html =~ /\S/;
    return $html;
}

sub _applyProcessors {
    my ( $this, $mimeForContent, $content ) = @_;
    return unless $mimeForContent;

    my $box = $this->_currentBox();
    return $content
      unless $box
      and $box->{content}->{processors}
      and ref( $box->{content}->{processors} ) eq 'ARRAY';

    my $topMime = $this->_currentMime();

    for my $processorCfg ( @{ $box->{content}->{processors} } ) {
        my $pkg = $processorCfg->{pkg};
        eval "use $pkg;";
        die $@ if $@;

        my $processor =
          $pkg->new( $box, $topMime, $mimeForContent, $processorCfg );
        $processor->process($content);
    }

    return $content;
}

# Extract plain text and attachments from the MIME
sub _extractPlainTextAndAttachments {
    my ( $this, $mime, $text, $attach ) = @_;

    foreach my $part ( $mime->parts() ) {
        my $ct = $part->content_type || 'text/plain';
        my $dp = $part->header('Content-Disposition') || 'inline';
        if ( $ct =~ m[text/plain] && $dp =~ /inline/ ) {
            $$text .= $this->_applyProcessors( $part, $part->body() );
        }
        elsif ( $part->filename() ) {
            push(
                @$attach,
                {
                    payload  => $part->body(),
                    filename => $part->filename()
                }
            );
        }
        elsif ( $part != $mime ) {
            $this->_extractPlainTextAndAttachments( $part, $text, $attach );
        }
    }
}

sub _saveTopic {
    my ( $this, $post, $user, $web, $topic, $body, $subject, $from, $to, $cc, $date, $attachments ) = @_;
    my $err = '';

    my $session = $TWiki::Plugins::SESSION;
    my $curUser = $session->{user};
    $TWiki::Plugins::SESSION->{user} = $user;

    try {
        my $template = $post->{template}    || 'default';
        my $aWeb     = $post->{web}         || $web;
        my $pWeb     = $post->{prevWeb}     || $web;
        my $aTopic   = $post->{topic}       || $topic;
        my $pTopic   = $post->{prevTopic}   || $topic;
        my $where    = $post->{where}       || 'bottom';
        my $attFlags = $post->{attachments} || 'attach, link';
        $aWeb   =~ s/\$web/$web/g;
        $aWeb   =~ s/\$topic/$topic/g;
        $aTopic =~ s/\$web/$web/g;
        $aTopic =~ s/\$topic/$topic/g;
        $aTopic =~ s/\$autoinc\(([0-9]+)\)/_autoInc( $aWeb, $aTopic, $1 )/ge;
        $post->{prevWeb}   = $aWeb;   # save web name in case post2 wants to use it
        $post->{prevTopic} = $aTopic; #  "  topic  "

        my ( $meta, $text );
        if ( TWiki::Func::topicExists( $aWeb, $aTopic ) ) {
            ( $meta, $text ) = TWiki::Func::readTopic( $aWeb, $aTopic );
        } else {
            my ( $tmplWeb, $tmplTopic );
            if( $post->{templateTopic} ) {
                ( $tmplWeb, $tmplTopic ) =
                  TWiki::Func::normalizeWebTopicName( $aWeb, $post->{templateTopic} );
                $tmplTopic =~ s/$TWiki::cfg{NameFilter}//go;
                ( $meta, $text ) = TWiki::Func::readTopic( $tmplWeb, $tmplTopic );
            }
            $tmplTopic = 'WebTopicEditTemplate';
            unless( $text ) {
                ( $meta, $text ) = TWiki::Func::readTopic( $aWeb, $tmplTopic );
            }
            unless( $text ) {
                $tmplWeb = TWiki::Func::getMainWebname();
                ( $meta, $text ) = TWiki::Func::readTopic( $tmplWeb, $tmplTopic );
            }
            unless( $text ) {
                $tmplWeb = TWiki::Func::getTwikiWebname();
                ( $meta, $text ) = TWiki::Func::readTopic( $tmplWeb, $tmplTopic );
            }
            $text = TWiki::Func::expandVariablesOnTopicCreation( $text, $aWeb, $aTopic );
        }

        if ( $text =~ /<!--MAIL(?:{(.*?)})?-->/ ) {
            my $opts = new TWiki::Attrs($1);
            $template = $opts->{template} if( $opts->{template} );
            $where    = $opts->{where}    if( $opts->{where} );
        }
        elsif ( $text =~ /%COMMENT(?:{.*?})?%/ ) {
            #my $opts = new TWiki::Attrs($1);
            # FIXME: Make $where aware of CommentPlugin comment's above/below based on type
        }

        # the $insert variable is initialized from
        # %SYSTEMWEB%/MailInContribTemplate and the recommended way to change
        # the look and feel of the output pages is to copy
        # MailInContribTemplate as MailInContribUserTemplate and edit to
        # taste. - VickiBrown - 07 Sep 2007
        my $insert = TWiki::Func::expandTemplate( "MAILIN:$template" )
            || "\n   * Subject: %SUBJECT%\n%MAILBODY%\n-- %WIKIUSERNAME% - %SERVERTIME%\n";
        $insert =~ s/%MAILWEB%/$aWeb/g;
        $insert =~ s/%MAILPREVWEB%/$pWeb/g;
        $insert =~ s/%MAILTOPIC%/$aTopic/g;
        $insert =~ s/%MAILPREVTOPIC%/$pTopic/g;
        $insert =~ s/%(?:MAIL)?SUBJECT%/$subject/g;
        $insert =~ s/%MAILFROM%/$from/g;
        $insert =~ s/%MAILTO%/$to/g;
        if( $cc ) {
            $insert =~ s/%MAILCC%/$cc/g;
        } else {
            # Remove CC bullet line, assume format:   * CC: %MAILCC%
            $insert =~ s/(.*\n).*?CC(:)? *%MAILCC%\n/$1/gs;
        }
        $insert =~ s/%MAILDATE%/$date/g;
        $insert =~ s/%MAILISODATE%/_isoDate($date)/ge;
        $body   =~ s/\r//g;
        my $size = 500;
        if( $insert =~ /%(?:MAIL)?SUMMARY{['" ]*([0-9]+)/ ) {
            $size = $1;
        }
        my $summary = $body;
        $summary =~ s/^(.{$size}).*/$1.../s;
        if( $post->{hideQuotes} ) {
            # Hide quoted text in a twisty
            $body =~ s/(\n>.*)$/%TWISTY{mode="div"}%$1%ENDTWISTY%/s;
        }

        my $attached = 0;
        my $atts     = '';
        my $attTmpl  = TWiki::Func::expandTemplate( "MAILIN:$template:ATTACHMENT" )
                    || '   * [[%ATTACHURL%/%A_FILE%][%A_FILE%]]';
        my $imgTmpl  = TWiki::Func::expandTemplate( "MAILIN:$template:IMAGE" ) || $attTmpl;

        unless( $attFlags =~ /\bignore\b/ ) {
            foreach my $att (@$attachments) {
                unless( $attFlags =~ /\bnosave\b/ ) {
                    unless ( $attached ) {
                        $attached = 1;
                        TWiki::Func::saveTopic(
                            $aWeb, $aTopic, $meta, $text,
                            {
                                comment          => "Submitted by e-mail",
                                forcenewrevision => 1,
                            }
                        );
                    }
                    $err .= $this->_saveAttachment( $aWeb, $aTopic, $att, $attFlags );
                }
                my $fileName = _sanitizeAttachmentName( $att->{filename} );
                my $tmpl = $attTmpl;
                if( $fileName =~ /\.(jpg|jpeg|gif|png)$/i ) {
                    $tmpl = $imgTmpl;
                }
                $tmpl =~ s/%MAILWEB%/$aWeb/g;
                $tmpl =~ s/%MAILPREVWEB%/$pWeb/g;
                $tmpl =~ s/%MAILTOPIC%/$aTopic/g;
                $tmpl =~ s/%MAILPREVTOPIC%/$pTopic/g;
                $tmpl =~ s/%A_FILE%/$fileName/g;
                $atts .= $tmpl if( $attFlags =~ /\blink\b/ );
            }
        }
        unless( $atts ) {
            # Hack to remove gratuitous attachments header, assuming specific format
            $insert =~ s/[\-\+_ ]*Attachments?[:_ \n]*%(?:MAIL)?ATTACHMENTS%//si;
        }
        $insert =~ s/%(?:MAIL)?ATTACHMENTS%/$atts/;
        $insert =~ s/%TEXT%/$body/g;  # Legacy, undocumented
        $insert =~ s/%MAILBODY%/$body/g;
        $insert =~ s/%(?:MAIL)?SUMMARY(?:{.*?})?%/$summary/g;
        $insert = TWiki::Func::expandVariablesOnTopicCreation( $insert, $aWeb, $aTopic );

        # Reload the topic if we added attachments.
        if ( $attached ) {
            ( $meta, $text ) = TWiki::Func::readTopic( $aWeb, $aTopic );
        }

        if ( $where eq 'top' ) {
            $text = $insert . $text;
        }
        elsif ( $where eq 'above' ) {
            $text =~ s/(<!--MAIL(?:{.*?})?-->|%COMMENT(?:{.*?})?%)/$insert$1/;
        }
        elsif ( $where eq 'below' ) {
            $text =~ s/(<!--MAIL(?:{.*?})?-->|%COMMENT(?:{.*?})?%)/$1$insert/;
        }
        else { # if ( $where eq 'bottom' ) or anything else
            $text .= $insert;
        }

        if( defined &TWiki::Func::writeLog ) {
            $subject =~ s/^(.{50}).*/$1.../s;
            my $extra = "From: $from; To: $to; Subject: $subject";
            TWiki::Func::writeLog( 'mailin', $extra, $aWeb, $aTopic );
        }

        print STDERR "Save topic $aWeb.$aTopic:\n$text\n" if ( $this->{debug} );
        ASSERT( !$meta || $meta->isa('TWiki::Meta') ) if DEBUG;
        TWiki::Func::saveTopic(
            $aWeb, $aTopic, $meta, $text,
            {
                comment          => "Submitted by e-mail",
                forcenewrevision => ( !$attached ),
            }
        );

    }
    catch TWiki::AccessControlException with {
        my $e = shift;
        $err .= $e->stringify();
    }
    catch Error::Simple with {
        my $e = shift;
        $err .= $e->stringify();
    }
    finally {
        $TWiki::Plugins::SESSION->{user} = $curUser;
    };
    return $err;
}

sub _saveAttachment {
    my ( $this, $web, $topic, $attachment, $attFlags ) = @_;
    my $filename = _sanitizeAttachmentName( $attachment->{filename} );
    my $payload  = $attachment->{payload};
    my $hide     = ( $attFlags  =~ /\bhide\b/ );

    print STDERR "Save attachment $filename\n" if ( $this->{debug} );

    my $tmpfile = $web . '_' . $topic . '_' . $filename;
    $tmpfile = $TWiki::cfg{PubDir} . '/' . $tmpfile;

    $tmpfile .= 'X' while -e $tmpfile;
    open( TF, ">$tmpfile" ) || return 'Could not write ' . $tmpfile;
    print TF $attachment->{payload};
    close(TF);

    my $err = '';
    TWiki::Func::saveAttachment( $web, $topic, $filename,
      {
        comment => "Submitted by e-mail",
        file => $tmpfile,
        filesize => -s $tmpfile,
        filedate => time(),
        hide => $hide,
      } );
    unlink($tmpfile);
    return $err;
}

# Reply to a mail
sub _reply {
    my ( $this, $box, $mail, $body ) = @_;
    my $addressee =
         $mail->header('Reply-To')
      || $mail->header('From')
      || $mail->header('Return-Path');
    die "No addressee" unless $addressee;
    my $message =
        "To: $addressee"
      . "\nFrom: "
      . $mail->header('To')
      . "\nSubject: RE: your TWiki submission to "
      . $mail->header('Subject')
      . "\n\n$body\n";
    my $errors = TWiki::Func::sendEmail( $message, 5 );
    if ($errors) {
        print "Failed trying to send mail: $errors\n";
    }
}

sub _autoInc {
    my ( $web, $topic, $start ) = @_;
    # Shamelessly stolen from TWiki::UI::Save's AUTOINC
    my $next = $start;
    my $baseTopic = $topic;
    my $nameFilter = $topic;
    $nameFilter =~ s/\$autoinc\(([0-9]+)\).*/([0-9]+)/;
    my @list =
      sort{ $a <=> $b }
      map{ s/^$nameFilter.*/$1/; s/^0*([0-9])/$1/; $_ }
      grep{ /^$nameFilter/ }
      TWiki::Func::getTopicList( $web );
    if( scalar @list ) {
        # find last one, and increment by one
        $next = $list[$#list] + 1;
        my $len = length( $start );
        $start =~ s/^0*([0-9])/$1/; # cut leading zeros
        $next = $start if( $start > $next );
        my $pad =  $len - length($next);
        if( $pad > 0 ) {
            $next = '0' x $pad . $next; # zero-pad
        }
    } else {
        # use start
    }
    return $next;
}

sub _isoDate {
    my ( $date ) = @_;
    my %MON2NUM =  ( 'jan' => '01', 'feb' => '02', 'mar' => '03', 'apr' => '04', 'may' => '05',
      'jun' => '06', 'jul' => '07', 'aug' => '08', 'sep' => '09', 'oct' => '10', 'nov' => '11',
      'dec' => '12'
    );
    if( $date =~ /([0-9]{1,2}) ([\w]{3})[\w]* ([0-9]{4})/ ) {
        # identified official e-mail date format
        $date = "$3-" . $MON2NUM{lc($2)} . "-$1";
        $date =~ s/\-([0-9])$/-0$1/;
    } else {
        # could not detect date format, use local system time instead
        my ( $sec, $min, $hour, $day, $mon, $year ) = localtime( time() );
        $date =   sprintf( '%.4u', $year + 1900 )
          . '-' . sprintf( '%.2u', $mon + 1 )
          . '-' . sprintf( '%.2u', $day );
    }
    return $date;
}

sub _formatTime {
    my ( $time ) = @_;
    my ( $sec, $min, $hour, $day, $mon, $year ) = localtime( $time );
    my $text =   sprintf( '%.4u', $year + 1900 )
         . '-' . sprintf( '%.2u', $mon + 1 )
         . '-' . sprintf( '%.2u', $day )
         . ' ' . sprintf( '%.2u', $hour )
         . ':' . sprintf( '%.2u', $min )
         . ':' . sprintf( '%.2u', $sec );
    return $text;
}

sub _sanitizeAttachmentName {
    my ( $filename ) = @_;
    # SMELL: Should only use TWiki::Func
    ( $filename ) = TWiki::Sandbox::sanitizeAttachmentName( $filename );
    $filename =~ s/[\(\)]//go;
    return $filename;
}

1;
