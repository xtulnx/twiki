---+!! !LdapContribAdminPlugin
<!--
One line description, required for extensions repository catalog.
   * Set SHORTDESCRIPTION = An admin panel for viewing and changing the LdapContrib database.
-->
%SHORTDESCRIPTION%

%TOC%

---++ Introduction
!LdapContribGuiPlugin provides a GUI to administer the LDAP database created by TWiki:Plugins/LdapContrib.

The admin navigates the admin panel by using JQuery making AJAX calls over the REST architecture. TWiki:Plugins/JQueryPlugin needs to be installed.

---++ Usage
Place the !%LDAPCONTRIBADMIN% variable in a topic. This will expand the admin panel.

---++ Features
   * View all users, displaying stored LDAP DN, emails, !WikiName, created date and update date for every user. 
      * Using a built-in search box you can narrow the search by searching for both login names and !WikiNames using a regular expression.
   * Change !WikiName for users, allowing the !WikiName you are changing to be preserved from being taken by other users.
   * Delete users.

   * View and delete ignored users and groups. These are groups that have been tried looked up in LDAP and which could not be found. The ignore lists prevents these entries to be looked up again to reduce the stress on the LDAP server.

   * View groups with it's users. 
      * Only users which is present in the !LdapContrib cache are shown, not showing members only present on the LDAP server.
   * Possibility to delete groups and it's user memberships (the users themselves are not touched by doing this).

   * An overview, showing the number of stored user accounts, groups, ignored users and groups, as well as a dump of the !LdapContrib settings.
   * The overview also show's a graph displaying the number of user registrations per month through time. The graph is generated using TWiki:Plugins/JqPlotPlugin, if its available.

---++ Examples
<div style="float:left; margin:0 25px 25px 0;>
  <a href="%ATTACHURLPATH%/LdapContribAdminGroupSearch.jpg"><img alt="LdapContribAdminGroupSearch.jpg" src="%ATTACHURLPATH%/LdapContribAdminGroupSearch.jpg" width="450" /></a>
  <br />
  <em>The groups section.</em>
</div>
<div style="float:left; margin:0 25px 25px 0;>
  <a href="%ATTACHURLPATH%/LdapContribAdminUserSearch.jpg"><img alt="LdapContribAdminUserSearch.jpg" src="%ATTACHURLPATH%/LdapContribAdminUserSearch.jpg" width="450" /></a>
  <br />
  <em>The users section.</em>
</div>
<div style="float:left; margin:0 25px 25px 0;>
  <a href="%ATTACHURLPATH%/LdapContribAdminStats.jpg"><img alt="LdapContribAdminStats.jpg" src="%ATTACHURLPATH%/LdapContribAdminStats.jpg" width="450" /></a>
  <br />
  <em>The statistics section.</em>
</div>

<div style="clear: both"></div>

---++ Installation Instructions

%$INSTALL_INSTRUCTIONS%

---++ Manifest
| *File* | *Description* |  
%$MANIFEST% 

---++ Plugin Info

Many thanks to the following sponsors for supporting this work:
   * [[http://www.cern.ch][CERN]]

|  Plugin Author(s): | TWiki:Main.TerjeAndersen |
|  Copyright: | &copy; CERN 2014 |
|  License: | [[http://www.gnu.org/licenses/gpl.html][GPL (Gnu General Public License)]] |
|  Plugin Version: | %$VERSION% |
|  Change History: | <!-- versions below in reverse order -->&nbsp; |
|  2014-05-21      | TWikibug:Item7499: Creating the plug-in -- TWiki:Main.TerjeAndersen |
|  Dependencies: | %$DEPENDENCIES% |
|  Plugin Home: | http://twiki.org/cgi-bin/view/Plugins/LdapContribAdminPlugin |
|  Feedback: | http://twiki.org/cgi-bin/view/Plugins/LdapContribAdminPluginDev |
|  Appraisal: | http://twiki.org/cgi-bin/view/Plugins/LdapContribAdminPluginAppraisal |

__Related Topics:__ %TWIKIWEB%.TWikiPlugins, %TWIKIWEB%.DeveloperDocumentationCategory, %TWIKIWEB%.AdminDocumentationCategory, %TWIKIWEB%.TWikiPreferences

<!-- Do _not_ attempt to edit this topic; it is auto-generated. Please add comments/questions/remarks to the feedback topic on twiki.org instead. -->

%META:FILEATTACHMENT{name="LdapContribAdminGroupSearch.jpg" attachment="LdapContribAdminGroupSearch.jpg" path="LdapContribAdminGroupSearch.jpg" attr="h"}%
%META:FILEATTACHMENT{name="LdapContribAdminUserSearch.jpg" attachment="LdapContribAdminUserSearch.jpg" path="LdapContribAdminUserSearch.jpg" attr="h"}%
%META:FILEATTACHMENT{name="LdapContribAdminStats.jpg" attachment="LdapContribAdminStats.jpg" path="LdapContribAdminStats.jpg" attr="h"}%
