//
// This JS file serves the LdapContribAdminPlugin, the GUI for the admin panel.
//
//

// 
// Administrator Panel: (SEE FURTER DOWN FOR THE ADMIN-ONLY PART)
//

var twikiPath;
var pubPath
{
    var scripts = document.getElementsByTagName('script');
    var thisScript = scripts[scripts.length-1];
    pubPath = thisScript.src.replace(/[^\/]+$/, '');
    twikiPath = pubPath.replace(/pub.+/,'');
}
console.log(twikiPath + ' ' + pubPath);

var requestParams = {
  viewerRole : 'user', // Which user do we want to be? Ensures that admins see's the same as users when using panels intended for users 
  view : 'users', // The navigation panel (all users, all groups, statistics ...)
  userType : 'wikiNames', // When searching in 'all users', do we search in loginNames, or wikiNames?
  searchString : '.', // Search (Perl regex) searchString
  regexSearch : '1',
  caseSensitiveSearch : true, // Case sensitivity option in search
};

// Mapping for replacing content title when user navigates:
var titles = {
  'users' : 'Users list:',
  'ignoredUsers' : 'Ignored User Values:',
  'groups' : 'Groups list:',
  'ignoredGroups' : 'Ignored Group Values:',
  'overview' : 'Overview:'
};

$(function() {
  requestParams['viewerRole'] = document.getElementById('LdapContribAdmin').className;

  // Perform search when user hits the enter button ..
  $('#LdapContribAdmin #SearchBar').keydown(function(e) {
    if (e.keyCode == 13) {
      doSearch();
    }
  });

  // .. or manually clicking on the search button
  $('#LdapContribAdmin button#doSearch').click(function() {
    doSearch();
  });

  // panel navigation handler
  $('#LdapContribAdmin.admin a.navigate').click(function(e) {
    e.preventDefault();

    // What do the user wants to see?
    var id = $(this).attr('id');

    // Is the action valid?
    if (id in titles) {
      requestParams['view'] = id;

      // Remove the dynamic content at navigate 
      $('#LdapContribAdmin .dynamicContainer').html('');

      // Reset char container
      $('#LdapContribAdmin #chart').remove();
      $('#LdapContribAdmin .dynamicContainer').after('<div id="chart"></div>');

      // Show the title- and content container
      $('#LdapContribAdmin .actionTitle').text(titles[requestParams['view']]);
      $('#LdapContribAdmin .actionTitle, #LdapContribAdmin .dynamicContainer').show();

      // Cosmetic
      $('#LdapContribAdmin.admin a.navigate.selected').removeClass('selected');
      $(this).addClass('selected');

      $('#LdapContribAdmin #SearchBar').val('');
      requestParams['searchString'] = '';

      if (requestParams['view'] === 'users') {
        // 'All users list': Radio buttons for searching in wikiNames or loginNames
        $('.userType').show();
        if (requestParams['userType'] === 'loginNames') {
          $("input[value='loginNames']").attr('checked','checked');
        } else {
          $("input[value='wikiNames']").attr('checked','checked');
        }
      } else {
        $('.userType').hide();
      }

      if (requestParams['view'] !== 'overview') {
        $('#LdapContribAdmin .searchLine').show();
      } else {
        $('#LdapContribAdmin .searchLine').hide();

        // Get content from server-side
        doAjax(function(response) {
          displayResponseContent(response);

          // Overview: create chart using JqPlotPlugin if the data for it is returned from server-side.
          if (response['createdSummary']) {
            if ($.jqplot !== undefined) {
              drawCreatedSummaryChart('New users hitting the TWiki', response['createdSummary']); 
            } else {
              $('#LdapContribAdmin .dynamicContainer').append('<p class="red">Could not create chart - JqPlotPlugin could not be found.</p>');
            }
          }
        });
      }
    }
  });
  
  // Select all checkboxes in the parent table.
  $(document).delegate('#LdapContribAdmin.admin a#selectAll', 'click', function(e) {
    e.preventDefault();
    $(this).parents('table').find('input[type=checkbox]').attr('checked', 'checked');
  });

  // De-select all checkboxes in the parent table
  $(document).delegate('#LdapContribAdmin.admin a#selectNone', 'click', function(e) {
    e.preventDefault();
    $(this).parents('table').find('input[type=checkbox]').attr('checked', '');
  });

  // Handler to delete ignored users
  $(document).delegate('#LdapContribAdmin.admin button#deleteIgnoredUsers', 'click', function(e) {

    // Get the IDs for the users marked for deletion
    var searchIDs = $(this).parents('table').find('input:checkbox:checked').map(function(){
      return $(this).val();
    }).get();

    if (searchIDs.length > 0) {
      var confirmed = confirm("Are you sure you want to delete: " + searchIDs.join(', '));
      if (confirmed === true) {
      
        var priorView = requestParams['view'];
        requestParams['view'] = 'ignoredUsers';
        requestParams['remove'] = searchIDs.join(',');
  
        doAjax(function(response) {

          displayResponseContent(response);
          
          // The ignored users are now deleted, we don't want to signal to the backend
          // that they should be deleted once more
          delete requestParams['remove'];

          // Reset view
          requestParams['view'] = priorView;
        });
      }
    }
  
  });

  // Handler to delete all ignored users
  $(document).delegate('#LdapContribAdmin.admin button#deleteAllIgnoredUsers', 'click', function(e) {
    var confirmed = confirm("Are you sure you want to delete -all- ignored users?");
    if (confirmed === true) {
      var priorView = requestParams['view'];
      requestParams['view'] = 'ignoredUsers';
      requestParams['remove'] = 'all';

      doAjax(function(response) {

        displayResponseContent(response);
        
        // The ignored users are now deleted, we don't want to signal to the backend
        // that they should be deleted once more
        delete requestParams['remove'];

        // Reset view
        requestParams['view'] = priorView;
      });
    }
  });

  // Handler to delete ignored groups
  $(document).delegate('#LdapContribAdmin.admin button#deleteIgnoredGroups', 'click', function(e) {

    // Get the IDs for the groups marked for deletion
    var searchIDs = $(this).parents('table').find('input:checkbox:checked').map(function(){
      return $(this).val();
    }).get();

    if (searchIDs.length > 0) {
      var confirmed = confirm("Are you sure you want to delete: " + searchIDs.join(', '));
      if (confirmed === true) {
      
        var priorView = requestParams['view'];
        requestParams['view'] = 'ignoredGroups';
        requestParams['remove'] = searchIDs.join(',');
  
        doAjax(function(response) {

          displayResponseContent(response);
          
          // The ignored groups are now deleted, we don't want to signal to the backend
          // that they should be deleted once more
          delete requestParams['remove'];

          // Reset view
          requestParams['view'] = priorView;
        });
      }
    }
  
  });

  // Handler to delete all ignored groups
  $(document).delegate('#LdapContribAdmin.admin button#deleteAllIgnoredGroups', 'click', function(e) {
    var confirmed = confirm("Are you sure you want to delete -all- ignored groups?");
    if (confirmed === true) {
      var priorView = requestParams['view'];
      requestParams['view'] = 'ignoredGroups';
      requestParams['remove'] = 'all';

      doAjax(function(response) {

        displayResponseContent(response);
        
        // The ignored groups are now deleted, we don't want to signal to the backend
        // that they should be deleted once more
        delete requestParams['remove'];

        // Reset view
        requestParams['view'] = priorView;
      });
    }
  });

  // The user has clicked the 'edit' button.
  $(document).delegate('#LdapContribAdmin.admin .edit', 'click', function() {
    switch (requestParams['view']) {
      case 'users':
        var wikiNameCell = $(this).parent().siblings('.wikiName');
        var wikiName = wikiNameCell.text();
  
        var html = '<input type="text" name="newWikiName" value="' + wikiName + '" /> ';
        html += '<a href="#" class="save">' + String.fromCharCode(10004) + '</a>';
        html += '<a href="#" class="cancel">' + String.fromCharCode(10006) + '</a>';

        wikiNameCell.html(html);

        wikiNameCell.attr('id', wikiName);

        break;
      default:
        break;
    }
  });

  // The user has clicked the 'cancel' button, to cancel an edit.
  $(document).delegate('#LdapContribAdmin.admin .cancel', 'click', function(e) {
    e.preventDefault();    
    var row = $(this).parents('tr');
    var inputs = row.find("input[type='text']");
    $.each(inputs, function() {
      $(this).parent().text($(this).val());
    });
  });

  // The user has clicked the 'save' button, to save an edit.
  $(document).delegate('#LdapContribAdmin.admin .save', 'click', function(e) {
    e.preventDefault();    
    switch (requestParams['view']) {
      case 'users':

        var oldWikiName = $(this).parent().attr('id');
        var newWikiName = $(this).parent().find('input').val();
        var loginName   = $(this).parent().siblings('.loginName').text();

        var priorView = requestParams['view'];
        requestParams['view'] = 'changeWikiName';
        requestParams['loginName'] = loginName;
        requestParams['oldWikiName'] = oldWikiName;
        requestParams['newWikiName'] = newWikiName;

        requestParams['userType'] = 'loginNames';
        requestParams['searchString'] = '^' + loginName + '(_\\d+)?$';
        requestParams['regexSearch'] = '1';

        doAjax(function(response) {

          displayResponseContent(response);

          // The wikiName is now changed. Delete unnecessary parameters.
          delete requestParams['preserveOldWikiName'];
          delete requestParams['oldWikiName'];
          delete requestParams['newWikiName'];
          delete requestParams['loginName'];

          // Reset view
          requestParams['view'] = priorView;
      
          // Set the search cosmetics 
          $('#LdapContribAdmin #SearchBar').val(requestParams['searchString']);
          $('#LdapContribAdmin #regexSearch').attr('checked','checked');
          $("input[value='loginNames']").attr('checked','checked');
        });

      default:
        break;
    }
  });

  // Delete single user/group
  $(document).delegate('#LdapContribAdmin.admin .del', 'click', function() {
    switch (requestParams['view']) {
      case 'users':

        var wikiNameCell = $(this).parent().siblings('.wikiName');
        var wikiName = wikiNameCell.text();
        var loginName = $(this).parent().siblings('.loginName').text();

        var confirmed = confirm("are you sure you want to delete user with LoginName '" + loginName + "' and WikiName '" + wikiName + "'?");
        if (confirmed === true) {

          var priorView = requestParams['view'];
          requestParams['view'] = 'deleteUser';
          requestParams['wikiName'] = wikiName;
          requestParams['loginName'] = loginName;
          
          doAjax(function(response) {
            
            displayResponseContent(response);
          
            // The user is now deleted. Delete unnecessary parameters.
            delete requestParams['wikiName'];
            delete requestParams['loginName'];
      
            // Reset view
            requestParams['view'] = priorView;
          });
        }

        break;
      case 'groups':

        var group = $(this).parent().siblings('.groupName').text();
        var confirmed = confirm("are you sure you want to delete the group '" + group + "'?");
        if (confirmed === true) {
          var priorView = requestParams['view'];
          requestParams['view'] = 'deleteGroup';
          requestParams['group'] = group;
          
          doAjax(function(response) {

            displayResponseContent(response);

            // The group is now deleted. Delete unnecessary parameters.
            delete requestParams['group'];

            // Reset view
            requestParams['view'] = priorView;
          });
        }
      
        break;

      default:
        break;
    }
  });

});

// Perform a search 
function doSearch () {

  // Retrieve and validate searchString
  searchString = $('#LdapContribAdmin #SearchBar').val();
  if (searchString.length === 0) {
    alert('No search phrase given, try again!');
    return;
  }
  requestParams['searchString'] = searchString;
  
  // Case sensitivity? 
  requestParams['caseSensitiveSearch'] = ($('#LdapContribAdmin #caseSensitiveSearch').attr('checked') === true ? '1' : '0');

  // Use regular expressions? 
  requestParams['regexSearch'] = ($('#LdapContribAdmin #regexSearch').attr('checked') === true ? '1' : '0');
  
  if (requestParams['view'] === 'users') {
    requestParams['userType'] = $('#LdapContribAdmin .searchLine input[name=userType]:checked').val();
  }

  doAjax(function(response) {
    displayResponseContent(response);
  });
}


// Display content from the content delivered from the 
// AJAX call in doAjax()
function displayResponseContent (response) {

  console.log('called displayResponseContent');

  // Empty and re-fill content container with new data
  $('#LdapContribAdmin .dynamicContainer').html('');
  var html = response.html.replace(/\n/g, '<br />');
  $('#LdapContribAdmin .dynamicContainer').html(html);

  // Display error if set by backend
  if (response.message) {
    alert(response.message);
  }
}

// Do an AJAX call
function doAjax (callback) {
  
  // Setting backend url and logging the AJAX call.
  var url = twikiPath + 'bin/rest/LdapContribAdminPlugin/do';
  console.log('Calling ' + url + ', view ' + requestParams['view']);

  $.ajax({
    dataType : 'json',
    url : url,
    data : requestParams,
    beforeSend : function() {
      // Remove the dynamic content
      $('#LdapContribAdmin .dynamicContainer').html('');

      // Show a spinner to let the user know that something is happening
      $('#LdapContribAdmin .loader').show();
    },
    success : callback,
    error: function(jqXHR, textStatus, errorThrown) {
      // Dump any error to the content container 
      $('#LdapContribAdmin .dynamicContainer').html('An error occured: jqXHR: ' + jqXHR + ', textStatus: ' + textStatus + ', errorThrown: ' + errorThrown);
    },
    complete : function() {
      // Hide spinner
      $('#LdapContribAdmin .loader').hide();
    },
    type: 'POST'
  }) 
}

// Draw a chart, using JqPlotPlugin, in the div with id 'chart'
function drawCreatedSummaryChart (title, jsonData) {
  var data = [[]];
  var count = 0;
  for (var key in jsonData) {
    data[0].push([key, jsonData[key]]);
  }
  var plot1 = $.jqplot('chart', data, {
      title: {
        text : title,
        textAlign : 'left'
      },
      axes:{
        xaxis:{
          renderer:$.jqplot.DateAxisRenderer,
          tickOptions:{
            formatString:'%b&nbsp;%Y'
          } 
        },
        yaxis:{
          min:0
        }
      },
      highlighter: {
        show: true,
        sizeAdjust: 7.5
      },
      cursor: {
        show: false
      }
  });
}
