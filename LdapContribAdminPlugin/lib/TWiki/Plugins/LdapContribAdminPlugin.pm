# Plugin for TWiki Collaboration Platform, http://TWiki.org/
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

=pod 

---+ package TWiki::Plugins::LdapContribAdminPlugin

=cut

# Always use strict to enforce variable scoping
use strict;

package TWiki::Plugins::LdapContribAdminPlugin;
our $pluginName = 'LdapContribAdminPlugin';

require TWiki::Func;    # The plugins API
require TWiki::Plugins; # For the API version
use POSIX;
use Data::Dumper;
use HTML::Entities;

require TWiki::Contrib::LdapContrib;

our $VERSION = '$Rev: 25093 (2013-02-16) $';
our $RELEASE = '2013-02-16';
our $SHORTDESCRIPTION = 'An admin panel for viewing and changing the LdapContrib database.';

our $NO_PREFS_IN_TOPIC = 1;

# Define other global package variables
our $debug;
our $search; # search phrase, regex or not
our $caseSensitiveSearch; # true or false
our $regexSearch; # true or false
our $userType; # loginNames, wikiNames

our ($JqPlotPluginInstalled, $JQueryPluginInstalled);

our $isAdmin = 0;

=pod

---++ initPlugin($topic, $web, $user, $installWeb) -> $boolean
   * =$topic= - the name of the topic in the current CGI query
   * =$web= - the name of the web in the current CGI query
   * =$user= - the login name of the user
   * =$installWeb= - the name of the web the plugin is installed in

=cut

sub initPlugin {
    my( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.1 ) {
        TWiki::Func::writeWarning( "Version mismatch between $pluginName and Plugins.pm" );
        return 0;
    }

    $isAdmin = TWiki::Func::isAnAdmin($TWiki::Plugins::SESSION->{remoteUser}) ? 1 : 0;
  
    eval { require TWiki::Plugins::JqPlotPlugin; };
    $JqPlotPluginInstalled = length($@) == 0;

    eval { require TWiki::Plugins::JQueryPlugin; };
    $JQueryPluginInstalled = length($@) == 0;

    TWiki::Func::registerRESTHandler('do', \&_restDo);

    TWiki::Func::registerTagHandler( 'LDAPCONTRIBADMIN', \&VAR_LDAPCONTRIBADMIN );

    # Plugin correctly initialized
    return 1;
}

sub VAR_LDAPCONTRIBADMIN {
  my($session, $params, $theTopic, $theWeb, $meta, $textRef) = @_;

  my $html = '';
  if (!$JQueryPluginInstalled) {
    $html = 'JQueryPlugin must be installed to use this variable.';
  } elsif ($isAdmin) {
    &addHead();
  
$html = "
<div id='LdapContribAdmin' class='admin'>

<h2>LdapContrib Admin</h2>

<p class='description'>LdapContrib Admin is an admin panel which allows you to admister the local cache keeping track on the mapping between TWikiUsers and their corresponding LDAP identities.</p>

<h3>Available Actions:</h3>
   * <a href='#' id='users' class='navigate'>View users list</a>
   * <a href='#' id='ignoredUsers' class='navigate'>View users values ignored for further LDAP lookup</a>
   * <a href='#' id='groups' class='navigate'>View groups list</a>
   * <a href='#' id='ignoredGroups' class='navigate'>View group values ignored for further LDAP lookup</a>
   * <a href='#' id='overview' class='navigate'>Overview</a>

_NB:_ WikiNames for given LoginNames can be changed via the 'View users list' panel.

<h4 class='actionTitle'></h4>
<div class='searchLine'>
  Search : <input type='text' value='' id='SearchBar' /> <button id='doSearch'>Go!</button> <br />
  <table>
  <tr><td>Case sensitive search:</td><td><input type='checkbox' id='caseSensitiveSearch' checked /></td></tr>
  <tr><td>Regular expression search:</td><td><input type='checkbox' id='regexSearch' /></td></tr>
  <tr class='userType'><td>User Type:</td><td><input type='radio' name='userType' value='wikiNames' checked> !WikiNames <input type='radio' name='userType' value='loginNames'> !LoginNames </td></tr>
  </table>
</div>
<img src='\%PUBURL%/\%SYSTEMWEB%/LdapContribAdminPlugin/gfx/spinner.gif' class='loader' />
<div class='dynamicContainer'></div>
  <div id='chart'></div>
</div>";
$html .= "\%JQPLOT{\"highlighter, cursor, dateAxisRenderer\"}%" if $JqPlotPluginInstalled;
  } else {
    $html = 'You must be an administrator to use this TWiki tag / variable.'
  }

  return $html;
}

sub _restDo {
  my ($session) = @_;

  my $view = CGI::param('view');
  
  return '' unless $isAdmin;

  eval {
      require JSON;
      JSON->import();
      1;
  };
  
  TWiki::Func::writeWarning("JSON could not be loaded: $@") and return if $@;
  
  eval {
      require CGI;
      CGI->import();
      1;
  };
  
  TWiki::Func::writeWarning("CGI could not be loaded: $@") and return if $@;

  my $ldap = TWiki::Contrib::LdapContrib::getLdapContrib($session);

  $search = CGI::param('searchString') || '';
  eval { qr/$search/ };
  return '' if $@ || length($search) == 0 && $view ne 'overview';

  $regexSearch = CGI::param('regexSearch');
  $regexSearch = 1 unless defined $regexSearch;
  $search = "\Q$search\E" unless $regexSearch; 

  $caseSensitiveSearch = CGI::param('caseSensitiveSearch');
  $caseSensitiveSearch = 1 unless defined $caseSensitiveSearch;

  $userType = CGI::param('userType');

  my %data; 

  if ($view eq 'users') {
    $data{'html'} = &__displayUsers($ldap);
  } elsif ($view eq 'changeWikiName') {
    my $loginName = CGI::param('loginName');
    my $oldWikiName = CGI::param('oldWikiName');
    my $newWikiName = CGI::param('newWikiName');
    
    my $error = 0;
    for ($loginName, $oldWikiName, $newWikiName) {
      unless (defined) {
        $error = 1;
        $data{'message'} = "Error: not enough parameters sent to backend when changing WikiName for user. Please consult your closest admin\n";
      }
    }
    
    unless ($error) {
      $data{'message'} .= "Error: The new WikiName is too short.\n" if length($newWikiName) == 0;
      $data{'message'} .= "Error: The new WikiName is not valid.\n" unless TWiki::Func::isValidWikiWord($newWikiName);
      $data{'message'} .= "Error: The new WikiName equals the old WikiName.\n" if $oldWikiName eq $newWikiName;
      $data{'message'} .= "Error: The new WikiName already exists.\n" if defined $ldap->getLoginOfWikiName($newWikiName);
      unless (defined $data{'message'}) {
        unless ($ldap->renameWikiName($loginName, $oldWikiName, $newWikiName)) {
          $data{'message'} .= "Error: An unknown error occured.\n" 
        }
      }
    }

    $data{'html'} = &__displayUsers($ldap);
  } elsif ($view eq 'deleteUser') {
    my $wikiName = CGI::param('wikiName');
    my $loginName = CGI::param('loginName');
    
    if (defined $wikiName || defined $loginName) {  
      unless ($ldap->removeUserFromCache($wikiName, $loginName)) {
        $data{'message'} = "Error deleting user $wikiName: An unknown error occured.\n"; 
      }
    } else {
      $data{'message'} = "Error: not enough parameters sent to backend when deleting user. Please consult your closest admin\n";
    }
    $data{'html'} = &__displayUsers($ldap);
  } elsif ($view eq 'ignoredUsers') {
    &__removeIgnoredUsers($ldap, CGI::param('remove')) if defined CGI::param('remove');
    $data{'html'} = &__displayIgnoredUsers($ldap);
  } elsif ($view eq 'groups') {
    $data{'html'} = &__displayGroups($ldap);
  } elsif ($view eq 'deleteGroup') {
    my $group = CGI::param('group');
    if (defined $group) {  
      unless ($ldap->removeGroupFromCache($group)) {
        $data{'message'} = "Error deleting group $group: An unknown error occured.\n"; 
      }
    } else {
      $data{'message'} = "Error: not enough parameters sent to backend when deleting group. Please consult your closest admin\n";
    }
    $data{'html'} = &__displayGroups($ldap);
  } elsif ($view eq 'ignoredGroups') {
    &__removeIgnoredGroups($ldap, CGI::param('remove')) if defined CGI::param('remove');
    $data{'html'} = &__displayIgnoredGroups($ldap);
  } elsif ($view eq 'overview') {
    my $overview = &__displayOverview($ldap);
    $data{'html'} = $overview->{'html'};
    $data{'createdSummary'} = $overview->{'createdSummary'};
  }

  return to_json(\%data, {latin1 => 1, pretty => 0})
}

sub __displayUsers {
  my ($ldap) = @_;

  if (!defined $userType || $userType !~ m/\A(?:loginNames|wikiNames)\z/) {
    die "Undefined or unknown \$userType in __displayUsers";
  }

  my $tie = $ldap->{locking}{mode};
  $ldap->getCacheTie('read');

  my $loginNames = $ldap->getAllLoginNames();
  my %loginNames = map {$_ => 1} @$loginNames;
  my $wikiNames = $ldap->getAllWikiNames(); 

  my @grepped;
  if ($userType eq 'loginNames') {
    @grepped = &grepSelection($loginNames);
  } elsif ($userType eq 'wikiNames') {
    @grepped = &grepSelection($wikiNames);
  }

  my $limit = 50;

  my $pubPath = TWiki::Func::getPubUrlPath();
  my $iconPath = "$pubPath/TWiki/LdapContribAdminPlugin/gfx";
  
  my $html = '';
  if (@grepped > 0) {
    $html = '<p class="description">';
    $html .= sprintf "Showing %d out of %d users matching the search.<br /><br />",
      $limit > scalar(@grepped) ? scalar(@grepped) : $limit,
      scalar(@grepped);
    $html .= 'Legend:<br />';
    $html .= "<img src='$iconPath/led-cup-green.gif' alt='Active user' /> Active user.<br />";
    $html .= "<img src='$iconPath/led-cup-yellow.gif' alt='Placeholder user' /> Placeholder user. Occurs when a user has it'a WikiName changed. This record prevents other user from taking the old WikiName";
    $html .= '(see $TWiki::cfg{Ldap}{PreserveWikiNames})';
    $html .= '<br />';
    $html .= "<img src='$iconPath/led-cup-red.gif' alt='Deleted user' /> User not found in LDAP based on {Ldap}{LoginAttrute}. Occurs when a user is removed from LDAP. This record prevents other users from taking the user's WikiName.";
    $html .= "<br /><img src='$iconPath/close.gif' alt='Delete user' /> Delete this user record permanently from the LdapContrib cache, freeing the WikiName.";
    $html .= "<br /><img src='$iconPath/edittopic.gif' alt='Delete user' /> Edit the WikiName for this user.";
    $html .= '</p>';

    if (@$loginNames != @$wikiNames) {
      $html .= "<strong style='color:red'>NB!:</strong> LdapContrib::getAllWikiNames() and LdapContrib::getAllLoginNames() does not report the same number! This is a logical error that has to be investigated. Please consult your closest admin.\n"
    }
    
    $html .= '<table class="mainTable" id="users"><tr>';
    $html .= '<th></th>';
    $html .= '<th>WikiName</th><th>LoginName</th><th>Status</th><th>Email</th><th>Date created</th>';
    $html .= '<th>Date updated</th><th>LDAP DN</th>';
    $html .= '</tr>';

    @grepped = splice @grepped, 0, $limit;

    for my $entity (@grepped) {

      my ($loginName, $wikiName);
      if ($userType eq 'loginNames') {
        $loginName = $entity;
        $wikiName = $ldap->getWikiNameOfLogin($loginName);
      } elsif ($userType eq 'wikiNames') {
        $wikiName = $entity;
        $loginName = $ldap->getLoginOfWikiName($wikiName);
      }

      my %monHumanReadable = (
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec'
      );
  
      my $created = $ldap->getDateOfLogin($loginName, 'created');
      if ($created) {
        my ($sec,$min,$hour,$mday,$mon,$year) = (localtime($created))[0 .. 5];
        $created = sprintf "%s %02d %d", $monHumanReadable{$mon + 1}, $mday, $year + 1900;
        if ($sec != 0 || $min != 0 || $hour != 0) {
          $created .= sprintf ", %02d:%02d:%02d", $hour,$min,$sec;
        }
      }

      my $emails = $ldap->getEmails($loginName);
      $emails = join("\n",@$emails);

      my $dn = $ldap->getDnOfLogin($loginName);

      my $updated;
      $updated = $ldap->getDateOfLogin($loginName, 'updated');
      if ($updated) {
        my ($sec,$min,$hour,$mday,$mon,$year) = (localtime($updated))[0 .. 5];
        $updated = sprintf "%s %02d %d", $monHumanReadable{$mon + 1}, $mday, $year + 1900;
        if ($sec != 0 || $min != 0 || $hour != 0) {
          $updated .= sprintf ", %02d:%02d:%02d", $hour,$min,$sec;
        }
      }

      my $status = "<img src='$iconPath/";
      if (!defined $dn) {
        if ($loginName =~ m/\A(.+)_\d{10}\z/ && defined $loginNames{$1}) {
          $status .= "led-cup-yellow.gif' alt='Placeholder user'";
        } else {
          $status .= "led-cup-red.gif' alt='Deleted user'";
        }
      } else {
        $status .= "led-cup-green.gif' alt='Active user'";
      }
      $status .= " />";

      my $firstCol = '';
      $firstCol .= "<td><img src='$iconPath/close.gif' alt='Delete user' class='del' />";
      if (defined $dn) {
        $firstCol .= " <img src='$iconPath/edittopic.gif' alt='Set new WikiName' class='edit' />";
      }
      $firstCol .= '</td>';

      $html .= sprintf "<tr>%s<td class='wikiName'>%s</td><td class='loginName'>%s</td><td>%s</td><td>%s</td><td>%s</td>",
        $firstCol,
        encode_entities($wikiName),
        encode_entities($loginName),
        $status,
        defined $emails ? encode_entities($emails)  : '',
        defined $created ? encode_entities($created) : '';

      $html .= sprintf "<td>%s</td><td>%s</td>",
      defined $updated ? encode_entities($updated) : '',
      defined $dn ? encode_entities($dn) : '';

      $html .= '</tr>';
    } 

    $html .= '</table>';
  } else {
    $html .= sprintf "<p class='red'>There are no users matching the search '%s'.</p>", encode_entities($search);
  } 
  
  $ldap->untieCache() if $tie eq 'none';

  return $html;
}

sub __displayIgnoredUsers {
  my ($ldap) = @_;
  return unless $isAdmin;
  $search ||= '.';

  my $ignoredUsers = $ldap->getAllIgnoredUsers(); 
  my $html = '<p class="description">Ignored users are values marked as users which has yielded no results when looked up in LDAP. To prevent spamming LDAP with invalid users, the ignored users list are taken into consideration to determine whether LDAP should be bothered or not.</p>';

  my @grepped = sort &grepSelection($ignoredUsers);

  $html .= "<p><button id='deleteAllIgnoredUsers'>Delete all ignored users</button></p>";

  $html .= "<table class='mainTable'><tr><th>Ignored user</th><th><button id='deleteIgnoredUsers'>Delete</button> :: <a href='#' id='selectAll'>Select all</a> :: <a href='#' id='selectNone'>Select none</a></th></tr>";
  if (@grepped > 0) {  
    @grepped = splice @grepped, 0, 500; 
    for my $user (@grepped) {
      $html .= sprintf "<tr><td>%s</td><td><input type='checkbox' name='deleteIgnoredUser' value='%s' /></td></tr>", encode_entities($user), encode_entities($user);
    }
    $html .= "</table>";
  } else {
    $html .= sprintf "<p class='red'>There are no ignored users matching the search '%s'.</p>", encode_entities($search);
  } 
  
  return $html;
}

sub __removeIgnoredUsers {
  my ($ldap, $removedUsers) = @_;
  return unless $isAdmin;
  unless ($removedUsers eq 'all') {
    my @removedUsers = split(/\s*,\s*/, $removedUsers);
    $ldap->removeIgnoredUsers(@removedUsers);
  } else {
    $ldap->emptyIgnoredUsers();
  }
}

sub __removeIgnoredGroups {
  my ($ldap, $removedGroups) = @_;
  return unless $isAdmin;
  unless ($removedGroups eq 'all') {
    my @removedGroups = split(/\s*,\s*/, $removedGroups);
    $ldap->removeIgnoredGroups(@removedGroups);
  } else {
    $ldap->emptyIgnoredGroups();
  }
}

sub __displayGroups {
  my ($ldap) = @_;
  return unless $isAdmin;

  my $tie = $ldap->{locking}{mode};
  $ldap->getCacheTie('read');

  my $groups = $ldap->getGroupNames();

  my $pubPath = TWiki::Func::getPubUrlPath();
  my $iconPath = "$pubPath/TWiki/LdapContribAdmin/gfx";
  
  my $limit = 25;

  my @grepped = &grepSelection($groups);

  my $html = '';
  if (@grepped > 0) {

    $html .= '<p class="description">';
    $html .= sprintf "Showing %d out of %d groups with any potential members matching the search.<br /><br />",
      $limit > scalar(@grepped) ? scalar(@grepped) : $limit,
      scalar(@grepped);
    $html .= '"No Members" means that there are no TWikiUsers in this group, not necessary that there is no LDAP members in this group.<br />';
    $html .= 'Legend: <br />';
    $html .= 'Entities <span class="green">marked in green</span>: Sub-groups. <br />';
    $html .= "<img src='$iconPath/close.gif' class='del' alt='Delete group' />: Delete group.";
    $html .= '</p>';

    $html .= '<table class="mainTable" id="groups"><tr><th></th><th>Group Name</th><th>LDAP DN</th><th>Members</th></tr>';
    @grepped = splice @grepped, 0, $limit;
    for my $group (@grepped) {
      my $dn = $ldap->getDnOfGroup($group);
      $html .= sprintf "<tr><td>%s</td><td class='groupName'>%s</td><td>%s</td><td>", "<img src='$iconPath/close.gif' class='del' alt='Delete group' />", encode_entities($group), encode_entities($dn);
      
      my $members = $ldap->getGroupMembers($group);
      unless (@$members) {
        $html .= 'No members.';
      } else {

        my $size = @$members;
        my $usersPerColumn = ceil($size / 4);

        my @columns;
        push @columns, [ splice @$members, 0, $usersPerColumn ] while @$members;

        $html .= '<table class="membershipTable">';
        for (my $i = 0; $i < $usersPerColumn; $i++) {
    
          $html .= "<tr>";
          for (my $j = 0; $j < scalar(@columns); $j++) {
            if (defined @{$columns[$j]}[$i]) {
              my $member = @{$columns[$j]}[$i];
              my $isGroup = $ldap->isGroup($member);
              my $wikiName;
              unless ($isGroup) {
                $wikiName = $ldap->getWikiNameOfLogin($member);
                unless ($wikiName) {
                  TWiki::Func::writeWarning("member $member in group $group is not a group, but does not have a WikiName either. Corrupt cache?");
                }
              }
              $html .= sprintf "<td>%s%s</td>", 
                ($isGroup ? '<span class="green">' : '<span>' ) . encode_entities($member) . '</span>', 
                ($wikiName ? " (<small>$wikiName</small>)" : '');
            }
          }
        }
        $html .= '</table>';
      }
      $html .= sprintf "</td></tr>", encode_entities($group);
    } 
    $html .= '</table>';

  } else {
    $html .= sprintf "<p class='red'>There are no groups matching the search '%s'.</p>", encode_entities($search);
  } 

  $ldap->untieCache() if $tie eq 'none';

  return $html;
}

sub __displayIgnoredGroups {
  my ($ldap) = @_;
  return unless $isAdmin;
  $search ||= '.';

  my $ignoredGroups = $ldap->getAllIgnoredGroups();
  my $html = '<p class="description">Ignored groups are values marked as users which has yielded no results when looked up in LDAP. To prevent spamming LDAP with invalid groups, the ignored groups list are taken into consideration to determine whether LDAP should be bothered or not.</p>';
  my @grepped = sort &grepSelection($ignoredGroups);
  $html .= "<table class='mainTable'><tr><th>Ignored group</th><th><button id='deleteIgnoredGroups'>Delete</button> :: <a href='#' id='selectAll'>Select all</a> :: <a href='#' id='selectNone'>Select none</a></th></tr>";
  if (@grepped > 0) {  
    $html .= "<p><button id='deleteAllIgnoredGroups'>Delete all ignored groups</button></p>";
    @grepped = splice @grepped, 0, 500;
    for my $group (@grepped) {
      if ($group =~ m/$search/) {
        $html .= sprintf "<tr><td>%s</td><td><input type='checkbox' name='deleteIgnoredGroup' value='%s' /></td></tr>", encode_entities($group), encode_entities($group);
      }
    }
    $html .= "</table>";
  } else {
    $html .= sprintf "<p class='red'>There are no ignored groups matching the search '%s'.</p>", encode_entities($search);
  } 
  return $html;
}

sub __displayOverview {
  my ($ldap) = @_;
  return unless $isAdmin;

  my $tie = $ldap->{locking}{mode};
  $ldap->getCacheTie('read');

  my @loginNames = @{$ldap->getAllLoginNames()}; 
  my $numberOfLoginNames = @loginNames;

  my @groups = @{$ldap->getGroupNames()};
  my $numberOfGroups = @groups;

  my $numberOfWikiNames = @{$ldap->getAllWikiNames()};
  my $numberOfIgnoredUsers = @{$ldap->getAllIgnoredUsers()};
  my $numberOfIgnoredGroups = @{$ldap->getAllIgnoredGroups()};
  
  # performance-heavy variables: 
  #my $numberOfStoredEmails = 0;
  #$numberOfStoredEmails += scalar(@{$ldap->getEmails($_)}) for @loginNames;
  my $numberOfStoredEmails = 'disabled';

  #my $numberOfStoredGroupMembers = 0;
  #for my $group (@groups) {
  #  my $groupMembers = $ldap->getGroupMembers($group);
  #  if (ref($groupMembers) eq 'ARRAY') {
  #    $numberOfStoredGroupMembers += scalar(@$groupMembers);
  #  }
  #}
  my $numberOfStoredGroupMembers = 'disabled';

  my $createdSummary;
  for my $loginName (@loginNames) {
    next unless defined $ldap->{data}{"U2CREATED::$loginName"};
    my $created = $ldap->{data}{"U2CREATED::$loginName"};
    my ($mon,$year) = (localtime($created))[4,5];
    $year += 1900;
    $mon = sprintf "%02d", $mon + 1;
    $createdSummary->{"$year-$mon"}++;
  }

  $ldap->untieCache() if $tie eq 'none';

  my @rows;
  push @rows, "Number of LoginNames: $numberOfLoginNames";
  push @rows, "Number of WikiNames: $numberOfWikiNames";

  my $ok = $numberOfLoginNames == $numberOfWikiNames;
  push @rows, "<span class='red bold'>ERROR:</span> Number of LoginNames does not match number of WikiNames!" unless $ok;

  push @rows, "Number of stored emails: $numberOfStoredEmails";
  push @rows, "Number of groups: $numberOfGroups";
  push @rows, "Number of stored group members: $numberOfStoredGroupMembers";
  push @rows, "Number of ignored LoginName values: $numberOfIgnoredUsers";
  push @rows, "Number of ignored group values: $numberOfIgnoredGroups";
  my $rows = join '', map { "<li>$_</li>" } @rows;
  my $html = "<ul>$rows</ul>";

  $html .= "<h5>Settings</h5>The following settings are set by the TWiki administrators:\n<ul>";
  for my $key (sort keys %{$TWiki::cfg{Ldap}}) {
    my $val = $TWiki::cfg{Ldap}->{$key};
    if (ref $val eq 'ARRAY') {
      $val = "[ '" . join("', '", @$val) . "' ]";
    } elsif (ref $val eq 'HASH') {
      $val = '{ ' . join ( ', ',  map { "$_ => '$val->{$_}'" }keys %$val ) . ' }';
    }

    $html .= sprintf "<li>%s = %s</li>", "\$TWiki::cfg{Ldap}{" . encode_entities($key) . "}", encode_entities($val);
  }
  $html .= '</ul>';
  
  my $res;
  $res->{'html'} = $html if defined $html;
  $res->{'createdSummary'} = $createdSummary if defined $createdSummary;

  return $res;
}

sub grepSelection {
  my ($selection) = @_;

  my @grepped;
  if ($caseSensitiveSearch) {
    @grepped = sort { lc($a) cmp lc($b) } grep { m/$search/o } @$selection;
  } else {
    @grepped = sort { lc($a) cmp lc($b) } grep { m/$search/io } @$selection;
  }  

  return @grepped;
}

sub addHead {
  if ($isAdmin) {

    my $base = '%PUBURLPATH%/%SYSTEMWEB%/LdapContribAdminPlugin';

    my $head = "<script src='$base/LdapContribAdminPlugin.js'></script>";
    $head .= "<link rel='stylesheet' href='$base/LdapContribAdminPlugin.css' type='text/css' media='all' />";

    TWiki::Func::addToHEAD('LDAPCONTRIBGUI', $head);
  }
}

1;
