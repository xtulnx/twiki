%META:TOPICINFO{author=""TWikiContributor"" date="1394142006" format="1.1" version="$Rev$"}%
<!-- Do _not_ attempt to edit this topic; it is auto-generated. Please add comments/questions/remarks to the feedback topic on twiki.org instead. -->
<!-- This topic includes a sample auto-attached spreadsheet; do not save the master copy with the attachment -->
<!-- **  -->
<!-- Copyright 2014 Timothe Litt.  See EmbedExcelPlugin for license -->
---+!! !EmbedExcelPlugin
<!--
One line description, required for extensions repository catalog.
   * Set SHORTDESCRIPTION = Embeds an Excel format spreadsheet in a %WIKITOOLNAME% page
-->
%SHORTDESCRIPTION%
<style>
hr.foot {
    margin-top:2em;
    border: thin solid;
    width: 10em;
}
div.foot {
    font-style: italic;
    font-size: smaller;
}
</style>
%TOC%<div style="float:right;margin:0px 0px 10px 10px;padding:5px;border:2px solid #808080;border-radius:5px;"><a href="%ATTACHURL%/SampleRendering.png" target="_blank"><img src="%ATTACHURL%/SampleRenderingPreview.png" alt="Colorful thumbnail of rendered spreadsheet"></a></div>
The %TOPIC% retrieves and displays an Excel<a href="#foot1"><sup>1</sup></a> spreadsheet as HTML embedded in the %WIKITOOLNAME%
page.  This enables a read-only view of a spreadsheet that is maintained as an attachment
to a %WIKITOOLNAME%  topic or as a file on another server.  Unlike other methods, no manual conversion steps are
required; if the spreadsheet changes, the topic view will reflect them.  Depending on where the spreadsheet is
maintained, it may not be necessary for the user to upload anything to the %WIKITOOLNAME%.  For example, a spreadsheet might
be maintained in a directory that is served by a remote webserver.  In that case, the %WIKITOOLNAME% topic
simply refers to the URL.

Files retrieved by a URL are automatically attached to the topic in which they are embedded.  This supports
the expectation that _if you see it on the %WIKITOOLNAME%, the version that you saw can be retrieved._


The client only needs a web browser; the spreadsheet viewing program (E.G. Microsoft Excel, !OpenOffice)
is not required.

The rendering of the spreadsheet is close to that of the source document, but limitations of
HTML and the underlying conversion tool prevent it from being exact.  Constructs such as
advanced cell formatting are especially problematic.  It is a static view: autofilter,
graphs and the like are not supported.

However, it *is* efficient.  It caches the rendering (and for remote files, the spreadsheet)
making views quick.  The caching is transparent and does not require manual attention. When possible,
the rendered data is dynamically loaded into the page, allowing the browser to cache it as well.

The spreadsheet does not become a %WIKITOOLNAME% object; its cells are not addressable with the SpreadSheetPlugin or other tools.
For that, consider [[http://twiki.org/cgi-bin/view/Plugins/ExcelImportExportPlugin][ExcelImportExportPlugin]].

<hr align="left" class="foot"><a name="foot1">
<div class="foot"><sup>1</sup></a> As used here, "Excel" means the spreadsheet binary file format known as _Excel Binary File Format_ that was initially produced by the Microsoft Excel spreadsheet product.  The file format can be produced by other tools (*inter alios* !OpenOffice, !LibreOffice and !NeoOffice) that have no connection to Microsoft.  Currently, %TOPIC% supports the Excel 97-2003 file format.</div>

---++ Usage
| =%<nop>EMBEDXLS{ _"SampleWorkbook.xls"_ options }%= | Display a file attached to the current topic. |
| =%<nop>EMBEDXLS{ _"SampleWorkbook.xls"_ topic="%WEB%.%TOPIC%"  options... }%= | Specify another topic. |
|=%<nop>EMBEDXLS{ _"<u>%ATTACHURL%SampleWorkbook.xls</u>"_ options... }%= | Specify remote source |

The spreadsheets may be maintained entirely within the wiki, as traditional topic attachments.  In this case, when the spreadsheet is changed, the attachment must be manually updated with the new version; all topics that view the spreadsheet will automatically show the new version.

Alternatively, the spreadsheets may be maintained on another server.  In this case, the %TOPIC% will automatically fetch them from the remote server, attach them to the topic, and keep the attachment up-to-date with changes made at the source.

Remote spreadsheets may be acquired using the http, https, or ftp protocols.  (https client authentication is not supported.)  When viewing these spreadsheets, the remote source is always queried but the spreadsheet is only transfered and re-rendered when necessary. These files are maintained as attachments to the viewed topic.  Like all %WIKITOOLNAME% attachments, changes cause a new version to be created.  

When viewing the spreadsheet, the user can:
   * Click tab buttons to slect the visible worksheet
   * Hover over any cell to see its coordinates (row,col)
   * Click on aa data cell to toggle visibility of the gridlines.
   * Click on any row/column label (or the tab bar if they aren't visible) to toggle the grid labels.
   * Use hyperlinks in the data cells
   * Hover on the intersection of the row and column labels to see who made the last change.

Note that relative hyperlinks will refer to the %WIKITOOLNAME% environment, not the source system's.  Absolute hyperlinks and cell references within a workbook will work reliably.

---+++ Options

<!-- The current table with footnotes and additional text is in Core.pm so it stays consistent with the code.
     What's here is intended to provide enough information to encourage a more complete evaluation. -->
%IF{"context EmbedExcelPluginEnabled"
    then='$percntEMBEDXLS{action="optdoc"}$percnt'
    else='<div style="display:inline-block; padding:10px;border:double;background-color:#E7E1E1;margin-bottom:20px;"> __Because $percntTOPIC$percnt is not installed, the most current options documentation is not available.__ <p>The following is list is incomplete, may be out-of-date and/or contain errors.<p>The current documentation is displayed here when $percntTOPIC$percnt is installed.</div><div style="border:double;background-color:#E7E1E1;padding:10px;">
These options are specified as =option="value"= in the =$percnt<nop>EMBEDXLS$percnt= macro call.  The =file=
option is the default option; the =file== keyword may be omitted.  Do not use commas between
options.

For example, to open =Sample.xls= worksheets =2= and =3,= with =3= initially open, use:
<div style="margin-left:5em;"> =$percnt<nop>EMBEDXLS{ "Sample.xls" sheets="2,3" opento="3" }$percnt= </div>

$percntTABLE{sort="off" headerrows="1" valign="top" databg="none"}$percnt
 | *Option* | *Values * | *Default* | *Function* |
 | =topic= | topic<br /> web.topic | _current_ | Specify topic that contains the spreadsheet (as an attached file) |
 | =file= | attachment name<br />URL | | Name of *.xls* file to embed.<br />  May be an http,https, or ftp URL. |
 | =filename= | attachment name | <i>from =file= </i> | Name of attachment |
 | =sheets= | omitted<br />sheet names | _all sheets_ | Specify name(s) or number(s)<a href="#foot4"><sup>4</sup></a> of sheets to be rendered.  Separate with comma. |
 | =opento= | auto <br />name or number | auto | Name or number<a href="#foot4"><sup>4</sup></a> of sheet to be opened when workbook is rendered.<a href="#foot3"><sup>3</sup></a> <br /> *auto* opens the sheet that was active when the workbook was last saved. |
 | =emptyok= | boolean | false | When true, won&#39;t report a warning for empty worksheets |
 | =gridlines= | auto<br />boolean | auto | When true, grid lines are rendered.<a href="#foot3"><sup>3</sup></a><br /> *auto* uses page setup in worksheet |
 | =gridlabels= | auto<br />boolean | auto | When true, grid labels (Column and row headings) are rendered.<a href="#foot3"><sup>3</sup></a><br /> *auto* uses page setup in worksheet|
 | =height= | auto<br />cssUnit | auto | Fixed height for workbook.  Any CSS unit is OK (typically, =px= or =%= is used) |
 | =width= | auto<br />cssUnit | auto | Fixed width for workbook.  Any CSS unit is OK (typically, =px= or =%= is used) |
 | =username= | string<a href="#foot2"><sup>2</sup></a> | | $percntPURPLE$percntUsername for accessing remote file$percntENDCOLOR$percnt |
 | =password= | string<a href="#foot2"><sup>2</sup></a> | | $percntPURPLE$percntPassword for accessing remote file$percntENDCOLOR$percnt |
 | =account= | string<a href="#foot2"><sup>2</sup></a> | | $percntPURPLE$percntAccount for accessing remote file (FTP)$percntENDCOLOR$percnt |
 | =realm= | string | | $percntPURPLE$percntAuthentication realm for accessing remote file$percntENDCOLOR$percnt |
 | =Password= | string<a href="#foot2"><sup>2</sup></a> | | $percntPURPLE$percntPassword used to open spreadsheet$percntENDCOLOR$percnt |
 | =errors= | detail <br />brief | detail | "detail" includes full webserver response for errors accessing remote content<br />&#39;brief&#39; includes only the response code & text. |
 | =style= | _URL_ | | Specifies the URL of a custom CSS stylesheet (See [[#AdvancedFormatting][Advanced Formatting]] for usage.) |
 | =showstatus= | boolean | false | Displays rendering status for each sheet, including the *first* issue encountered.  |
 | =percellcss= | boolean | false | Rendering includes a unique CSS class for each cell.  Rarely needed by custom CSS; slows web browser |
 | =lineheight= | number | 1.5 | Line height in CSS stylesheets, in units of em.  |
 | =mode= | fast<br />inline<a href="#foot2"><sup>2</sup></a> | inline | Normally a spreadsheet will be fetched by the browser independently of the topic content.  This allows the browser to cache the spreadsheet and improves performance.  Alternatively, the spreadsheet can be rendered inline, which is recommended only if *fast* is not supported by your webserver.  =inline= prevents browser caching. |
 | =nocache= | boolean | false | When true, any cached data will be discarded and refreshed <br />The browser will not be permitted to use cached data.|
 | =id= | string | hash | Unique identifier assigned to each rendering/options/pageposition.<br /> Can be used to target CSS, but be aware that changing an Id creates a new cache instance. |
</div>'
 }%

---++ Examples

Several sample renderings are available on this page.  In each case, the source of the data is indicated; you can navigate to the links that are provided to see the files with a native application.

The actual TML markup used is also shown.  The different appearances are due to options selected in the spreadsheets.

<div style="margin-left:40px;margin-top:10px;">
<style type="text/css">
input.xls[type="radio"] {
    display: none;
}
input.xls[type="radio"]+label {
    display: inline-block;
    margin: -2px;
    padding: 4px 12px;
    background-color: #e7e7e7;
    border: 2px outset #4B4950;
}
input.xls[type="radio"]:checked+label {
    background-color: #d0d0d0;
    font-style: italic;
    font-weight: bold;
    border-style: inset;
}
input.xls[type="radio"]+label.xlsleft {
    border-top-left-radius: 12px;
    border-bottom-left-radius: 12px;
}
input.xls[type="radio"]+label.xlsright {
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
}
div.xls {
    display:inline-block;
    padding: 2px;
    margin: 20px;
    border: 2px dotted gray;
}
</style>
<script>
function xlsClick(target) {
    var all = '#liveAttachmentData,#liveHumorData,#liveRemoteData,#pdfData,#screenData';
    $(all).hide();
    $(target).show();
    if(target.indexOf('live') >= 0) {
        XLS.ssDisplay(target);
    }
    return true;
}    
</script>
<span style="margin-right:20px;">Select an example:</span>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#none');" onload="$(this).click();" checked="checked"  id="noexample" /><label class="xlsleft" for="noexample">None</label>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#liveAttachmentData');" id="liveattach" /><label for="liveattach">Live Attachment</label>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#liveHumorData');" id="livehumor" /><label for="livehumor">Live Humor</label>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#liveRemoteData');" id="liveremote" /><label for="liveremote">Live Remote</label>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#pdfData');" id="pdfexample" /><label for="pdfexample">PDF view</label>
<input type="radio" class="xls" autocomplete="off" name="example" onclick="return xlsClick('#screenData');" id="screenexample" /><label class="xlsright" for="screenexample">Screen capture</label>
</div>
<div id="liveAttachmentData" style="display:none;">
---+++!! Live rendering - topic attachment
 <div style="margin-left:20;">
What you see is a workbook that is attached to this topic.  The workbook has hidden worksheet, which (of course) is not displayed.

The odd formatting of some of the cells in this sample is used for testing the rendering engine, and is not an artifact of the rendering.
|   *Data&nbsp;source* | [[%ATTACHURLPATH%/SampleWorkbook.xls][%WEB%.%TOPIC% (attachment)]] =SampleWorkbook.xls= |
|   *Markup* | =%<nop>EMBEDXLS{"SampleWorkbook.xls"}%= |
   <div class="xls">%IF{"context EmbedExcelPluginEnabled"
    then='$percntEMBEDXLS{"SampleWorkbook.xls"}$percnt'
    else='<div style="display:inline-block; padding:10px;"> __Because the %TOPIC% is not installed, the live demonstration is not available.__ <p>Please install %TOPIC% or select the screen capture.</div>' }%

   </div>
  </div>
</div>
<div id="liveHumorData" style="display:none;">
---+++!! Live humor - topic attachment
 <div style="margin-left:20;">
What you see is a workbook that is attached to this topic.  This humorous view of corporate America was provided by John Wallenbach in the style of similar automata created for at least 5 decades..

You will notice that it is a macro-based workbook.  The __Press F9 to refresh__ instructions won't work here, but the tabs will.

|   *Data&nbsp;source* | [[%ATTACHURLPATH%/MeetingBingo.xls][%WEB%.%TOPIC% (attachment)]] =MeetingBingo.xls= |
|   *Markup* | =%<nop>EMBEDXLS{"MeetingBingo.xls" opento="2"}%= |
   <div class="xls">%IF{"context EmbedExcelPluginEnabled"
    then='$percntEMBEDXLS{"MeetingBingo.xls" opento="2"}$percnt'
    else='<div style="display:inline-block; padding:10px;"> __Because the %TOPIC% is not installed, the live demonstration is not available.__ <p>Please install %TOPIC% or select the screen capture.</div>' }%
   </div>
  </div>
</div>
<div id="liveRemoteData" style="display:none;">
---+++!! Live rendering - remote file
 <div style="margin-left:20;">
_This requires_ =svn.twiki.org= _to be available._  What you see was retrieved by your %WIKITOOLNAME% server via *http*.

|   *Data&nbsp;source* | [[http://svn.twiki.org/svn/twiki/trunk/ExcelImportExportPlugin/pub/Sandbox/IssueExcelExportImport/IssueExcelExportImport.xls]] |
|   *Markup* | =%<nop>EMBEDXLS{"http://svn.twiki.org/svn/twiki/trunk/ExcelImportExportPlugin/pub/Sandbox/IssueExcelExportImport/IssueExcelExportImport.xls" opento="2"}%= |
   <div class="xls">
   %IF{"context EmbedExcelPluginEnabled"
    then='$percntEMBEDXLS{"http://svn.twiki.org/svn/twiki/trunk/ExcelImportExportPlugin/pub/Sandbox/IssueExcelExportImport/IssueExcelExportImport.xls" opento="2"}$percnt'
    else='<div style="display:inline-block; padding:10px;"> __Because the %TOPIC% is not installed, the live demonstration is not available.__ <p>Please install %TOPIC% or select the screen capture.</div>'
    }%
   </div>
  </div>
</div>
<div id="pdfData" style="display:none;">
---+++!! PDF of sample workbook
_This output is simulated._ This is a PDF printout of the sample workbook used for the *Live Attachment.*  It shows all four (visible and non-empty) worksheets in the workbook.

One sheet is too large to fit on one page, and is segmented (by Excel) in the PDF printout.

The odd formatting of some of the cells in the sample is used for testing the rendering engine.

|   *Data&nbsp;source* | [[%ATTACHURLPATH%/SampleWorkbook.xls][%WEB%.%TOPIC% (attachment)]] =SampleWorkbook.xls= |
|   *Markup* | _Printed to a PDF writer from Microsoft Excel_ |
 <div style="margin-left:20;">

     Click <a href="%ATTACHURL%/SampleWorkbook.pdf" target="_blank">SampleWorkbook.pdf</a> to view the file in a new window or tab.  
  </div>
</div>
<div id="screenData" style="display:none;">
---+++!! Screen capture
_This output is simulated._ It is a screen capture of the *Live Attachment* sample from a system with %TOPIC% installed.

The odd formatting of some of the cells in the sample is used for testing the rendering engine, and is not an artifact of the rendering.

|   *Data&nbsp;source* | [[%ATTACHURLPATH%/SampleWorkbook.xls][%WEB%.%TOPIC% (attachment)]] =SampleWorkbook.xls= |
|   *Markup* | =%<nop>EMBEDXLS{"SampleWorkbook.xls"}%= |
 <div style="margin-left:20;">
     <img src="%ATTACHURL%/SampleRendering.png"></a>
  </div>
</div>
<hr>


#AdvancedFormatting
---++ Advanced formatting
The rendering of each spreadsheet can be controlled by providing customized CSS for the elements of the spreadsheet.
This is generally not necessary.  

%TWISTY{
 mode="div"
 showlink="Show details %ICONURL{toggleopen}%"
 hidelink="Hide details %ICONURL{toggleclose}%"
}%
The spreadsheet is rendered using CSS-styled HTML elements in a table.
The base CSS can be viewed [[%PUBURL%/%SYSTEMWEB%/%TOPIC%/ExcelToHtml.css][Here]].

Note that the class names and descriptions match what is in the spreadsheet; the rendering may be different (e.g. when it is infeasible to render as specified.)

%IF{"context EmbedExcelPluginEnabled"
 then='$percntTABLE{dataalign="left" headeralign="center" headerrows="1" sort="off"}$percnt
$percntEMBEDXLS{action="cssdoc"}$percnt'
 else='<div style="background-color:#E7E1E1;border:2px double;padding:20px;display:inline-block;">Detailed documentation is available when %TOPIC% is installed and enabled.</div>'
}%
%ENDTWISTY%

---++ Installation and Configuration

You do not need to install anything on the browser to use this plugin. These instructions are for the administrator who installs the plugin on the %WIKITOOLNAME% server.

%TWISTY{
 mode="div"
 showlink="Show details %ICONURL{toggleopen}%"
 hidelink="Hide details %ICONURL{toggleclose}%"
}%
__1. Plugin installation__

There are three methods available for installing this plugin.:
   * Use the [[%SCRIPTURL%/configure%SCRIPTSUFFIX%][configure]] script's extension manager.
      * Simplest, but your webserver must have write access to the %WIKITOOLNAME% code.
   * Run the installation script manually
      * Automated, works in most environments
   * Do a manual installation
      * If you don't trust automation, or have an unusual environment

%ICON{"tip"}% No matter which method you choose, if you run selinux you will probably have to correct
security context assignments for the new files and/or any intermediate directories.

%JQTABPANE%
%JQTAB{"Using Configure"}%
   * Click on this link: [[%SCRIPTURL%/configure%SCRIPTSUFFIX%][configure]]
   * Click on *Extensions*, *Find more extensions*
   * Locate <noautolink> *%TOPIC%* </noautolink>
   * Click on the link and follow the directions
%JQENDTAB%
%JQTAB{"Runing the installation script manually"}%<noautolink>
   * From your %WIKITOOLNAME% root directory:
      * Download the =%TOPIC%_installer= script and the %TOPIC%.md5 file from the Plugin Home (see below)
      * Verify that the files were downloaded correctly: =md5sum -c %TOPIC%.md5=
      * Run the installer: =perl %TOPIC%_installer= </noautolink>*
%JQENDTAB%
%JQTAB{"Manual installation"}%<noautolink>
   * Install the prerequisite plugins.  Be sure that they are at or above the minimum requred revisions.
   * Download the =%TOPIC%.zip= or =%TOPIC%.tar.gz= and =%TOPIC%.md5= files from the Plugin Home (see below)
   * Verify that the files were downloaded correctly: =md5sum -c %TOPIC%.md5=
   * Expand the file that you selected in your <noautolink>%WIKITOOLNAME%</noautolink> installation directory. Content:
   * Correct file ownership, permissions, (and selinux contexts) for your environment.
   * %ICON{"tip"}%: by default, the =tar= and =unzip= commands will set the permissions of each directory to \
those in the archive.  This can disrupt your wiki.  This can be avoided by explicitly extracting each of the files.</noautolink>*
%$MANIFEST%
%JQENDTAB%
%JQENDTABPANE%

__2. Plugin configuration and testing__

   * Run the [[%SCRIPTURL%/configure%SCRIPTSUFFIX%][configure]] script to configure the plugin.
      * If you will fetch documents from remote websites, check the {PROXY}{HOST} and {PROXY}{PORT} settings.
       * ={Plugins}{EmbedExcelPlugin}{MaximumAge}= - Maximum retention period for renderings
   * Return to this page; the sample spreadsheet should match the pre-rendered version.
   * By default, this plugin uses the JQueryPlugin's  JQuery and JQuery-UI.  You can change this in the [[%SCRIPTURL%/configure%SCRIPTSUFFIX%][configure]] script, e.g. to use a Content Delivery Network.

__2. Plugin maintenance__
   * %TOPIC%'s cache is kept in the %WIKITOOLNAME%'s ={WorkingDir}= directory, specifically in ={WorkingDir}/%TOPIC%/=.  <br />These files may accumulate over time, so the =tools/EmbedExcelCleanup= script is provided.  This script will delete cached renderings that are older than ={Plugins}{EmbedExcelPlugin}{MaximumAge}= days.  It also removes cache data for attachments that have been deleted or moved. <br/><br/>We recommend that you install a =crontab= entry (or equivalent) for =tools/EmbedExcelCleanup= to run \
under the webserver user.  This will enable automatic maintenance of the %TOPIC% cache.  Use =-q=1= if you want the script to only report errors; the default is to print a message for every action.


__3. Debugging__
   * If the rendering is not what you expect, re-check the arguments to the %<nop>EMBEDXLS% macro.  The %WIKITOOLNAME% argumentsyntax is not always obvious.
   * If you have access to the %WIKITOOLNAME% log files, turn on ={Plugins}{EmbedExcelPlugin}{Debug}}= in the [[%SCRIPTURL%/configure%SCRIPTSUFFIX%][configure]] script.  This is an *Expert* setting.  <br /> *Note:* Certain sensitive information is edited out of the debug log; in particular, remote authtentication credentials are replaced by placeholders.
   * The plugin is not able to render all Excel constructs perfectly due to limitations of HTML, CSS and browsers.  Try changing the alignment setup in the Excel worksheet - especially when rotated text is involved.

%ENDTWISTY%

---++ Plugin Info

|  Plugin Author(s): | TWiki:Main.TimotheLitt |
|  Copyright: | &copy; 2014 TWiki:Main.TimotheLitt TWiki:Main.TimotheLitt |
|  License: | [[http://www.gnu.org/licenses/gpl.html][GPL (Gnu General Public License)]] |
|  Plugin Version: | %$VERSION% |
%TWISTY{
 mode="div"
 showlink="Show Change History %ICONURL{toggleopen}%"
 hidelink="Hide Change History %ICONURL{toggleclose}%"
}%
%TABLE{ tablewidth="100%" columnwidths="170," }%
|  2014-02-14 | Technology preview |
%ENDTWISTY%
%TABLE{ tablewidth="100%" columnwidths="170," }%
|  Dependencies: | %$DEPENDENCIES% |
|  Perl Version: | 5.008 |
|  [[TWiki:Plugins/Benchmark][Benchmarks]]: | %SYSTEMWEB%.GoodStyle nn%, %SYSTEMWEB%.FormattedSearch nn%, %TOPIC% nn% |
|  Plugin Home: | http://TWiki.org/cgi-bin/view/Plugins/%TOPIC% |
|  Feedback: | http://TWiki.org/cgi-bin/view/Plugins/%TOPIC%Dev |
|  Appraisal: | http://TWiki.org/cgi-bin/view/Plugins/%TOPIC%Appraisal |

__Related Topics:__ %SYSTEMWEB%.ExcelImportExportPlugin %SYSTEMWEB%.TWikiPlugins, %SYSTEMWEB%.DeveloperDocumentationCategory, %SYSTEMWEB%.AdminDocumentationCategory

%META:FILEATTACHMENT{name="SampleWorkbook.xls" attachment="SampleWorkbook.xls" attr="h" comment="Demonstration workbook" date="1392689855" size="61440" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="SampleWorkbook.pdf" attachment="SampleWorkbook.pdf" attr="h" comment="Demonstration workbook" date="1392689855" size="27371" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="SampleRendering.png" attachment="SampleRendering.png" attr="h" comment="Screenshot of demonstration workbook" date="1392689855" size="43384" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="SampleRenderingPreview.png" attachment="SampleRenderingPreview.png" attr="h" comment="Reduced screenshot of demonstration workbook" date="1392689855" size="4652" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="ExcelToHtml.css" attachment="ExcelToHtml.css" attr="h" comment="Style sheet for rendering spreadsheets" date="1392689855" size="22582" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="ExcelToHtml.js" attachment="ExcelToHtml.js" attr="h" comment="Javascript for rendering spreadsheets" date="1392689855" size="15341" user="TWikiContributor" version="1"}%
%META:FILEATTACHMENT{name="MeetingBingo.xls" attachment="MeetingBingo.xls" attr="h" comment="Demonstration workbook" date="1392689855" size="59392" user="TWikiContributor" version="1"}%

<!-- Do _not_ attempt to edit this topic; it is auto-generated. Please add comments/questions/remarks to the feedback topic on twiki.org instead. -->
