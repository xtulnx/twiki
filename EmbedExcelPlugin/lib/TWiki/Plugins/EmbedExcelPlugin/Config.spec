# ---+ Extensions
# ---++ EmbedExcelPlugin

# **BOOLEAN EXPERT**
# If the client browser accepts gzip-encoding, EmbedExcelPlugin will
# reply with gzip-encoded data.  Enable this if your webserver is not
# compressing output.  This would indicate a limitation or misconfiguration
# of the webserver.  This plugin must execute after any other plugin that
# uses modifyHeaderHandler or completePageHander.  (See {PluginsOrder} and
# the %SYSTEMWEB% InstalledPlugins topic.)
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{GzipEnabled} = 0;

# **NUMBER 40**
# Maximum age (days) of cached renderings.  Renderings older than this are deleted by the cleanup script.
# Zero will prevent the cleanup script from expiring cached renderings.
# This doesn't affect cached copies of attachments, which are retained for as long as the attachment
# stays attached to a topic.
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{MaximumAge} = 30;

# **OCTAL EXPERT**
# File permissions for cache directories. You may have to adjust these
# permissions to allow (or deny) users other than the webserver user access
# to files in the cache.  This might be necessary if backups are run under
# a different, non-privileged user.  This is an <b>octal</b> number
# representing the standard UNIX permissions (e.g. 0711 == rwx--x--x)
# The leading zero is required.
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{DirPermission}= 0711;

# **OCTAL EXPERT**
# File permissions for cache data. You may have to adjust these
# permissions to allow (or deny) users other than the webserver user access
# to files in the cache.  This might be necessary if backups are run under
# a different, non-privileged user.  This is an <b>octal</b> number
# representing the standard UNIX permissions (e.g. 0600 == rw-------)
# The leading zero is required.
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission}= 0600;

# **URL**
# Source for JQuery.  Defaults to <b><!-- JQUERY -->%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery.js<!-- /JQUERY --></b>.
# It is advisable to use a Content Distribution Network such as Google or Microsoft because they are widely used, location aware,
# and likely to be in browsers' caches.  Using another site will also reduce your page view times with many browsers.
# We recommend that you use the same version that we specify in the default.
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{JQuery} = '%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery.js';

# **URL**
# Source for JQueryUI.  Defaults to <b><!-- JQUERYUI -->%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery-ui.js<!-- /JQUERYUI --></b>.
# It is advisable to use a Content Distribution Network such as Google or Microsoft because they are widely used, location aware,
# and likely to be in browsers' caches.  Using another site will also reduce your page view times with many browsers.
# We recommend that you use the same version that we specify in the default.
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{JQueryUI} = '%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery-ui.js';

# **BOOLEAN EXPERT**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{EmbedExcelPlugin}{Debug} = 0;

1;
