# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 20114 Timothe Litt, litt [at] acm.org
# Plugin interface:
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

package TWiki::Plugins::EmbedExcelPlugin::Core;

use warnings;
use strict;

use Carp ();
use Error qw(:try);
use File::Basename;
use File::Path;
use TWiki::Func;
use Digest::MD5 ();
use Fcntl(qw/:flock/);
use POSIX;

# These are used when a full page is generated, e.g. by the REST service.
# jQueryUI is required - at this writing, the version shipped with TWiki is crippled and
# not recommended.  .min.js is replaced by .js when $debug is enabled.
#
# The ExcelToHtml javascript handles embedding of partial pages.

# N.B. Changes here auto-update the Config.spec file; just view a page. with $debug enabled.
my $JQuery =
  'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';
my $JQueryUI =
  'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js';

$JQuery   = '%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery.js';
$JQueryUI = '%PUBURL%/%SYSTEMWEB%/JQueryPlugin/jquery-ui.js';

# Changing this will cause cache invalidates.  Increment when
# rendering changes.
my $cacheVersion = 22;

# Overrides for external  page styles
# These ensure that rendering is accurate and that the CSS (including the
# inline-generated CSS isn't confused by the environment.

my $lineHeight = 1.5;

my $resetStyles = << "STYLES";
<style>
#patternPage div.ssWindow {
  font-size: 100%;
  line-height: ${lineHeight}em;
}
</style>
STYLES

my %validParams;

# The order here is what's used in documentation
# parameters from ExcelToHTML:: are appended.

our @validParams = (
    qw(topic filename rev height width errors style showrev showstatus nocache mode username password account realm  _DEFAULT _RAW action)
);

my $allowedProtocols = [qw(http https ftp)];
my $allowedProtocolsRE = ( '^(?:' . join( '|', @$allowedProtocols ) . ')://' );

# -----

my $debug;
{
    $debug = TWiki::Plugins::EmbedExcelPlugin->debug;

    my $spec = dirname(__FILE__) . '/Config.spec';

    if (   $debug
        && ( ( stat(__FILE__) )[9] || 0 ) > ( ( stat($spec) )[9] || 0 )
        && open( my $sh, '<', $spec ) )
    {

      # In debugging mode, fixup Config.spec with current defaults.
      # It's very uncommon to get here - the stat checks will almost always fail

        $spec =~ /^(.*)$/;
        $spec = $1;

        __PACKAGE__ =~ /^.*::([^:]+)::\w+$/;
        my $this = bless { plugin => $1, _indent => 0 }, __PACKAGE__;

        $this->_debug("Verifying $spec");
        local $/;
        my $txt = <$sh>;
        $txt =~ m/\A(.*)\z/s;
        $txt = $1;
        my $old = $txt;
        $txt =~
s/^(\$TWiki::cfg\{Plugins\}\{$this->{plugin}\}\{JQuery\}\s*=\s*')([^']*)(';\s*)$/$1$JQuery$3/gms;
        $txt =~ s{(<!-- JQUERY -->).*?(<!-- /JQUERY -->)}{$1$JQuery$2}gms;
        $txt =~
s/^(\$TWiki::cfg\{Plugins\}\{$this->{plugin}\}\{JQueryUI\}\s*=\s*')([^']*)(';\s*)$/$1$JQueryUI$3/gms;
        $txt =~ s{(<!-- JQUERYUI -->).*?(<!-- /JQUERYUI -->)}{$1$JQueryUI$2}gms;
        close $sh;
        undef $sh;

        if ( $txt eq $old ) {
            $this->_debug("No change required");
        }
        elsif ( open( my $nh, '>', $spec ) ) {
            print $nh ($txt);
            close $nh;
            $this->_debug("Updated $spec");
        }
        else {    # Read-only lib/ likely, can't do anything.
            $this->_debug("Failed to update $spec: $!");
        }
    }
}

my $gzipAvail = $TWiki::cfg{Plugins}{EmbedExcelPlugin}{GzipEnabled};
if ($gzipAvail) {
    eval { require IO::Compress::Gzip; };
    $gzipAvail = !$@;
}

# =========================
sub new {
    my $class = shift;

    my $this = { @_, _id => 0, _indent => 0 };

    $this->{_query} = TWiki::Func::getCgiQuery();

    bless $this, $class;

    $TWiki::cfg{Plugins}{ $this->{plugin} }{JQuery}   ||= $JQuery;
    $TWiki::cfg{Plugins}{ $this->{plugin} }{JQueryUI} ||= $JQueryUI;

    $JQuery   = $TWiki::cfg{Plugins}{ $this->{plugin} }{JQuery};
    $JQueryUI = $TWiki::cfg{Plugins}{ $this->{plugin} }{JQueryUI};

    if ($debug) {
        $_ =~ s/.min.js$/.js/ foreach ( $JQuery, $JQueryUI );
    }

    # N.B. The namespace of the module that's loaded
    # is simply ExcelToHtml - it is used in other applications.

    eval "require TWiki::Plugins::$this->{plugin}::ExcelToHtml";
    if ($@) {
        die
"Unable to load renderer TWiki::Plugins::$this->{plugin}::ExcelToHtml - $@";
    }

    unless ( keys %validParams ) {
        %validParams =
          map { $_ => 1 }
          grep { $_ !~ /^(?:cgi)$/ }
          ( @ExcelToHtml::validParams, @validParams );
    }

    $this->{_headers} = [
qq(<input type="hidden" name='ExcelToHtmlCSS' value="%PUBURL%/%SYSTEMWEB%/$this->{plugin}/ExcelToHtml.css" /><script src="$JQueryUI"></script><script src="%PUBURL%/%SYSTEMWEB%/$this->{plugin}/ExcelToHtml.js"></script>)
    ];

    my $context = TWiki::Func::getContext;
    if ( $context->{rest} ) {
        $this->{_rest} = 1;
        $this->{_headers}[0] =~
          s{<script}{<script src="$JQuery"></script><script};
    }
    if ( $context->{command_line} ) {
        $this->{_command_line} = 1;
    }

    $this->_addHeaders;

    if ($debug) {
        $this->_debug(
            "Initialized in ",
            ( $this->{_rest} ? 'REST' : 'topic' ),
            ' context for ',
            ( $this->{web} || "undef-web" ),
            '.',
            ( $this->{topic} || 'undef-topic' )
        );

        require IO::Handle;
        my $log = IO::Handle->new;
        tie *$log, 'TWiki::Plugins::EmbedExcelPlugin::Log', $this;
        $this->{_debug} = $log;

        ExcelToHtml->debug(1);
    }

    return $this;
}

sub _addHeaders {
    my $this = shift;

    TWiki::Func::addToHEAD( $this->{plugin} . 'Base', $this->{_headers}[0] );
}

# =========================
sub VarEMBEDXLS {
    my $this = shift;
    my ( $session, $params, $topic, $web, $meta, $textref ) = @_;

    local $SIG{__WARN__} = \&Carp::confess;

    if ($debug) {
        no warnings 'once';
        require Data::Dumper;
        local $Data::Dumper::Sortkeys = 1;
        $this->_debug(
            "Expanding EMBEDXLS in ",
            ( $this->{_rest} ? 'REST' : 'topic' ),
            " context with\n",
            Data::Dumper::Dumper( $web, $topic, $this->_obscure($params) )
        );
    }

    if ( ( my $error = $this->_validateParams( $params, $web, $topic ) ) ) {
        return $error;
    }

    if ( ( my $action = $params->{action} || '' ) ) {

        # Handle non-spreadsheet renderings

        if ( $action eq 'cssdoc' ) {    # Return documentation of CSS classes
            return $this->_cssdoc($params);
        }
        elsif ( $action eq 'optdoc' ) {    # Return documentation of options
            return $this->_optdoc($params);
        }
        return $this->_error(qq(Invalid action =$action= ));
    }
    my $file = $params->{file};

    $topic = $params->{topic} || qq($web.$topic);
    ( $web, $topic ) = TWiki::Func::normalizeWebTopicName( $web, $topic );

    my $mode = $params->{mode} || 'fetch';

    if (   $file =~ /\@/
        || $this->_isTrue( $params->{showstatus}, 0 )
        || exists $params->{Password}
        || exists $params->{password}
        || exists $params->{username}
        || exists $params->{account} )
    {
        $mode = 'inline';
    }

    my $style = $params->{style};
    if ( $style && !$this->{_css}{$style} ) {
        $this->{_css}{$style} = 1;
        push @{ $this->{_headers} },
          sprintf(
qq(<input type="hidden" name="ExcelToHtmlCSSu%04u" type="text/css" value="%%ENCODE{%s}%%" />),
            ( scalar keys %{ $this->{_css} } ), $style );
    }

    my $raw = $params->{_RAW};

    # Remove width and height as they apply to the div

    die "No _RAW parameters" unless ( defined $raw );

    foreach my $token (qw/width height/) {
        my $re = qq[$token=(['"]).*?\\];
        $raw =~ s{(?:^${re}1\s*)|(?:\s*${re}2)}{}gms;
    }

    # Over-encode to prevent / from looking like a path component,
    #  % from appearing as a variable, etc.
    $raw =~ s/([^0-9a-zA-Z.-])/'_'.sprintf('%02x',ord($1))/ge;

    # Handle user-specified fixed width, height

    my @dim = ( '', q(style=") );

    if ( $params->{width} ) {
        $dim[0] .= sprintf( qq(ssUserWidth="%s" ), $params->{width} );
        $dim[1] .= sprintf( qq(width:%s;),         $params->{width} );
    }
    if ( $params->{height} ) {
        $dim[0] .= sprintf( qq(ssUserHeight="%s" ), $params->{height} );
        $dim[1] .= sprintf( qq(height:%s;),         $params->{height} );
    }
    $dim[1] .= q(" );

    $dim[0] .= $dim[1] if ( $dim[0] );

    if ( $mode ne 'inline' ) {
        my $text =
qq(<noautolink><literal>$resetStyles</literal></noautolink><div $dim[0]class="ssRest">%ICON{processing-32}%<noautolink><literal>);
        $text .= sprintf(
qq(<input type="hidden" name="ssSrcURL" value="%s/%s/%s/%s/%s/ div.ssWrapper" />),
            TWiki::Func::getScriptUrl( undef, undef, 'rest' ),
            qq($this->{plugin}/xls), split( /\./, $params->{topic} ), $raw
        );
        $text .= qq(</literal></noautolink></div>);
        $this->_debug(qq(Rendered as REST request\n\t$text)) if ($debug);

        return $text;
    }

    # Inline rendering

    my $text = $this->_getRendering( $session, $params, $topic, $web );

    # Move status outside literal area

    my $status = '';
    if ( $$text =~ s/(<hr>.*)//msi ) {
        $status = $1;
    }

    $text =
        qq(<noautolink><literal>$resetStyles<div $dim[0]class="ssWindow">)
      . $$text
      . qq(</div></literal></noautolink>)
      . $status;

    $this->_debug(qq(Rendered inline)) if ($debug);
    return $text;
}

# =========================
# Handle REST requests
# o Gets rendering
# o Reply, handling conditional and byte range GETs

sub RestSERVER {
    my $this = shift;
    my ($session) = @_;

    local $SIG{__WARN__} = \&Carp::confess;

    require CGI;

    my $query = $this->{_query};

    my $pathInfo = $ENV{PATH_INFO};

    $this->{_session} = $session;

    $this->_debug("Entering REST server") if ($debug);

    my $requestMethod = $query->request_method;

    unless ( $requestMethod eq 'GET' ) {
        $this->_debug( "400: Bad request method ", ( $requestMethod || '??' ) )
          if ($debug);
        return $this->_restError( 400, "Bad request" );
    }

    $this->_debug("Processing GET request") if ($debug);

    # /Plugin/func/web/topic/params/tbd
    unless ( defined $pathInfo
        && $pathInfo =~
        m{/$this->{plugin}/xls/([^/]+)/([^/]+)/([^/]+)/(.*)$}ms )
    {
        $pathInfo = 'undefined' unless ( defined $pathInfo );
        $this->_debug(qq(400: Invalid pathinfo "$pathInfo")) if ($debug);
        return $this->_restError( 400, "Bad request" );
    }

    my ( $web, $topic, $params, $rest ) = ( $1, $2, $3, $4 );

    $params =~ s/_([0-9a-f]{2})/chr(oct("0x$1"))/gmse;

    if ( defined $topic ) {
        ( $web, $topic ) = TWiki::Func::normalizeWebTopicName( $web, $topic );

        $this->{web}   = $web;
        $this->{topic} = $topic;
    }

    unless (
           defined $web
        && defined $topic
        && TWiki::Func::checkAccessPermission(
            'VIEW', TWiki::Func::getWikiName(),
            undef, $topic, $web
        )
      )
    {
        $this->_debug("403: Missing web, topic or access permission")
          if ($debug);
        return $this->_restError( 403, "Access denied" );
    }

    my $context = TWiki::Func::getContext();
    unless ( $context->{authenticated} ) {
        $this->_debug("403: Authentication failure") if ($debug);
        return $this->_restError( 403, "Access denied" );
    }

    $params =
      { TWiki::Func::extractParameters( $params || "" ), _RAW => $params };

    if ( ( my $error = $this->_validateParams( $params, $web, $topic ) ) ) {
        return $this->_restError( 400, "Invalid request parameters", $error );
    }

    if ( $params->{action} ) {
        return $this->_restError(
            400,
            "Invalid request parameters",
            "=action= is not a REST service"
        );
    }

    my ( $html, $lastRendered ) =
      $this->_getRendering( $session, $params, $topic, $web );

    if ( !defined $lastRendered || $this->_isTrue( $params->{nocache}, 0 ) ) {
        $this->_debug("200: Cache miss") if ($debug);
        return $this->_restReply( [], $html );
    }

    my $lastmod = strftime( "%a, %d %b %Y %T GMT", gmtime($lastRendered) );
    my @headers = (
        -cache_control => 'public, max-age=0',
        -expires       => '-1d',
        -last_modified => $lastmod,
        -accept_ranges => 'bytes',
    );

    my $ifmod = $query->http('If-Modified-Since');
    if ( $ifmod && $ifmod eq $lastmod ) {
        push @headers, -status => '304 Not Modified';
        $this->_debug("304: Not modified") if ($debug);
        return $this->_restReply( \@headers );
    }

    $ifmod = $query->http('If-Range');
    my $range = $query->http('Range');

    my $filesize = length $$html;

    if (   $range
        && $range =~ s/^bytes=//
        && ( ( $ifmod && $ifmod eq $lastmod ) || !$ifmod ) )
    {
        my @ranges = split( /\s*,\s*/, $range );

        # Determine which range(s) can be satisfied
        # valid: m-n, m-, -n

        my @satisfied;
        foreach my $r (@ranges) {
            next if ( $r eq '-' || ( $r !~ /^(\d+)?-(\d+)?$/ ) );
            my ( $start, $end ) = ( $1, $2 );
            if ( defined($start) ) {
                next if ( $start >= $filesize );

                if ( defined($end) ) {    # m-n inclusive
                    next if ( $end < $start );
                    $end = $filesize - 1 if ( $end >= $filesize );
                }
                else {                    # m- to eof
                    $end = $filesize - 1;
                }
            }
            else {                        # -n last n bytes, n >= 0
                if ($end) {
                    $start = $filesize - $end;
                    $start = 0 if ( $start < 0 );
                    $end   = $filesize - 1;
                }
                else {                    # -0 abd - are invalid
                    next;
                }
            }
            push @satisfied, [ $start, $end ];
        }
        unless (@satisfied) {             # None can be satisfied
            push @headers,
              (
                -content_range => "*/$filesize",
                -status        => "416 Requested range not satisfiable",
              );
            $this->_debug("416: Range failure") if ($debug);
            return $this->_restReply( \@headers );
        }

        # Since request is now known to return 2xx or 412, it's OK to evaluate
        # an If-Unmodified-Since precondition.
        if ( ( $ifmod = $query->http('If-Unmodified-Since') )
            && $ifmod ne $lastmod )
        {
            push @headers, ( -status => "412 Precondition failed", );
            return $this->_restReply( \@headers );
        }

        # At least one range can be satisfied
        # Merge overlapping and adjacent ranges
        # Do not change request order unless part of a merge; client may prefer
        # to get an index from the end (e.g. of a PDF) before data.

        my @merged = shift @satisfied;
      RANGE:
        while (@satisfied) {
            my $r = shift @satisfied;    # Next request
            foreach my $m (@merged) {
                if (
                    $r->[0] <= $m->[0] && $r->[1] >= $m->[0] - 1
                    ||                   # Overlaps/adjacent from start
                    $r->[0] >= $m->[0] && $r->[0] <= $m->[1] + 1
                  )
                {                        # Overlaps/adjacent to end
                    $m->[0] = $r->[0] if ( $r->[0] < $m->[0] );   # Extend start
                    $m->[1] = $r->[1] if ( $r->[1] > $m->[1] );   # Extend end
                    unshift @satisfied,
                      @merged;  # Re-check in case new block can now merge again
                    @merged = shift @satisfied;
                    next RANGE;
                }
            }
            push @merged, $r;
        }
        @satisfied = @merged;
        @merged    = ();

        my $boundary =
            'THIS_STRING_SEPARATES-'
          . Digest::MD5::md5_hex(time)
          . '-THIS_STRING_SEPARATES';
        my $crlf = $CGI::CRLF;
        push @headers, -status => "206 Partial response";

        my $multipart = @satisfied > 1;
        if ($multipart) {
            push @headers,
              ( -content_type => "multipart/byteranges; boundary=$boundary", );
        }
        else
        { # MUST not return multipart if only one range requested (MAY if only one satisfiable)
            push @headers,
              (
                -content_type  => 'text/html',
                -content_range => sprintf( "%u-%u/%u",
                    $satisfied[0][0], $satisfied[0][1], $filesize ),
                -content_length => ( 1 + $satisfied[0][1] - $satisfied[0][0] ),
              );
        }
        my $data = '';
        foreach my $r (@satisfied) {
            if ($multipart) {
                $data .= sprintf(
                    qq($crlf$boundary${crlf}\
Content-Type: text/html$crlf\
Content-Range: bytes %u-%u/%u$crlf$crlf), $r->[0], $r->[1], $filesize
                );
            }
            $data .= substr( $$html, $r->[0], 1 + $r->[1] - $r->[0] );
            if ($multipart) {
                $data .= (qq($crlf));
            }
        }
        $this->_debug("206: Partial") if ($debug);
        return $this->_restReply( \@headers, \$data );
    }

    # No Range present or If-Range present, but not satisfied.

    if ( ( $ifmod = $query->http('If-Unmodified-Since') )
        && $ifmod ne $lastmod )
    {

        # For GET, this doesn't make much sense (return whole thing if
        # matches cached copy)  But we do it for ranges, so might as
        # well do it here. (Would make more sense for, say, 'delete'
        # or 'post'.)

        push @headers, ( -status => "412 Precondition failed", );
        $this->_debug("412: Precondition failed for If-Unmodified-Since")
          if ($debug);
        return $this->_restReply( \@headers );
    }

    push @headers, ( -content_type => 'text/html', );
    $this->_debug("200: Full cached reply") if ($debug);

    return $this->_restReply( \@headers, $html );
}

# =========================
sub _restReply {
    my $this = shift;
    my ( $headers, $html ) = @_;

    while (@$headers) {
        my $name  = lc shift @$headers;
        my $value = shift @$headers;

        $name =~ s/^-//;
        $name =~ s/_/-/g;

        $this->{_httpHdrs}{$name} = $value;
    }

    $this->{_session}->writeCompletePage(
        CGI::start_html( -title => qq($this->{plugin} REST Response), )
          . ( ref $html ? $$html : ( $html || '' ) )
          . CGI::end_html(),
        'view', 'text/html',
    );
    $this->_debug("Sent response") if ($debug);
    return undef;
}

# =========================
# Abort a REST request with an error page
sub _restError {
    my $this    = shift;
    my $status  = shift;
    my $message = shift;
##  my $content = join( '', @_ );

    require CGI;

    print(
        CGI::header(
            -status => sprintf( "%3u %s",
                ( $status  ||= 200 ),
                ( $message ||= "Error encountered" ) )
        )
    );
    print(
        CGI::start_html( -title => qq($this->{plugin} REST Error response), ) );

    print TWiki::Func::renderText(
        TWiki::Func::expandCommonVariables(
            join( '',
                qq(<div>$this->{plugin}: <span style="color:red;">),
                sprintf( "%3u %s", $status, $message ),
                qq(</span>), @_, qq(</div>) ),
            $this->{topic},
            $this->{web}
        )
    );
    print( CGI::end_html() );

    return undef;
}

# =========================
# Called when page is (almost) completely rendered
# Add any CSS requests

sub completePageHandler {
    my $this = shift;

##  my( $html, $httpHeaders) = @_;

    if ( $this->{_css} ) {
        my $base =
          TWiki::Func::expandCommonVariables( shift @{ $this->{_headers} },
            $this->{topic}, $this->{web} );
        my $found =
          $_[0] =~
s{$base}{join( '', $base, map {TWiki::Func::expandCommonVariables( $_, $this->{topic}, $this->{web} ) } @{$this->{_headers}} )}e;
        $this->_debug( "Added user CSS: ", ( $found ? 'Success' : 'Failed' ) )
          if ($debug);
    }

    if ( $this->{_gzip} ) {

        # It is too late to back out if gzip fails or lengthens the output.
        # The headers may already have been sent (e.g. by mod_perl).  In this
        # case, $httpHeaders will be null.

        my $zipped;
        unless ( IO::Compress::Gzip::gzip( \$_[0], \$zipped ) ) {
            no warnings 'once';
            Carp::confess("GZip failed: $IO::Compress::Gzip::GzipError");
        }

        if ($debug) {
            my $len = length( $_[0] );

            $_[0] = $zipped;
            $zipped = length($zipped);

            $this->_debug(
                sprintf(
                    "GZipped %u byte response, compression: %.2f%% (%u bytes)",
                    $len, 100 * ( 1.0 - ( $zipped / $len ) ), $zipped
                )
            );
        }
        else {
            $_[0] = $zipped;
        }
    }

    return;
}

# =========================
# Called when sending a REST reply
# Add any HTTP headers

sub modifyHeaderHandler {
    my $this = shift;
    my ( $headers, $query ) = @_;

    return unless ( $this->{_rest} );

    $this->_debug("Modify HTTP headers") if ($debug);

    if ( ( my $ours = $this->{_httpHdrs} ) ) {
        $headers->{$_} = $ours->{$_} foreach ( keys %$ours );
    }

    my $accept;
    if (
           $gzipAvail
        && !exists $headers->{'content-encoding'}
        && ( $ENV{'force-gzip'}
            || !$ENV{'no-gzip'}
            && defined( $accept = $this->{_query}->http('Accept-Encoding') )
            && $accept =~ /(?:^|,\s*)gzip(?:;|,|$)/ )
      )
    {
        $headers->{'content-encoding'} = 'gzip';
        if ( exists $headers->{vary} ) {
            $headers->{vary} .= ', Accept-Encoding';
        }
        else {
            $headers->{vary} = 'Accept-Encoding';
        }
        $this->{_gzip} = 1;
    }

    return;
}

# =========================
sub _validateParams {
    my $this = shift;

    my ( $params, $web, $topic ) = @_;

    foreach my $par ( keys %$params ) {
        unless ( $validParams{$par} ) {
            return $this->_error(qq(Unrecognized parameter =$par= ));
        }
    }

    foreach my $par (qw/gridlabels gridlines height opento width/) {
        delete $params->{$par}
          if ( exists $params->{$par} && $params->{$par} eq 'auto' );
    }

    return undef if ( $params->{action} );

    $params->{file} = $params->{_DEFAULT} unless ( exists $params->{file} );

    unless ( exists $params->{file}
        && defined $params->{file}
        && length $params->{file}
        || $params->{action} )
    {
        return $this->_error( qq(*file* _was not specified_.), );
    }

    if ( $params->{file} =~ m/$allowedProtocolsRE/o ) {
        require URI;
        require URI::Escape;

        my $uri  = URI->new( $params->{file} );
        my @path = $uri->path_segments;

        $params->{filename} = $path[-1] unless ( exists $params->{filename} );

        # Always attach remote files to viewed topic
        $params->{topic} = qq($this->{web}.$this->{topic});
    }
    else {
        my $fn = $params->{file};
        unless ( $fn eq ( TWiki::Func::sanitizeAttachmentName($fn) )[0] ) {
            return $this->_error(qq(Attachment name =$fn= is not valid));
        }
        $params->{filename} = $fn unless ( exists $params->{filename} );
    }
    my $fn = $params->{filename};
    unless ( defined $fn
        && length($fn)
        && $fn eq ( TWiki::Func::sanitizeAttachmentName($fn) )[0] )
    {
        $fn ||= '_was not specified_';
        return $this->_error(qq(Attachment name =$fn= is not valid));
    }

    unless ( exists $params->{topic} ) {
        $params->{topic} = $topic;
    }

    ( $web, $topic ) =
      TWiki::Func::normalizeWebTopicName( $web, $params->{topic} );
    $params->{topic} = "$web.$topic";

    unless ( TWiki::Func::topicExists( $web, $topic ) ) {
        return $this->_error(qq(!$web.$topic does not exist));
    }

    return undef;
}

# =========================
sub _isTrue {
    my $this = shift;
    my ( $value, $default ) = @_;

    return $default unless ( defined $value );

    # Treats 'off', 'false' and 'no' as false; trims spaces and case

    return TWiki::Func::isTrue( $value, $default );
}

# =========================
sub _sanitize {
    my $this = shift;
    my $content = join( '', @_ );

    $content =~ s{^(?:.*<body[^>]*>)?(.*>)(?:</body>)?.*$}{$1}msio;
    $content =~ s{^<?:<script[^>]*>.*?(?:</script>)?}{}gmsio;
    return qq(<div class="twikiSpreadsheet">$content</div>);
}

# =========================
sub _error {
    my $this = shift;

    return q(<div class="ssWrapper">)
      . TWiki::Func::renderText(
        TWiki::Func::expandCommonVariables(
            qq(\n*%RED%$this->{plugin}:* ) . join( '', @_ ) . qq(%ENDCOLOR%),
            $this->{topic}, $this->{web}
        )
      ) . q(</div>);
}

# =========================
sub _obscure {
    my $this = shift;

    my ($params) = @_;

    # Not foolproof, but attempt to keep credentials out of the
    # debug log.

    my $obscure = {%$params};

    foreach my $key ( keys %$obscure ) {
        if ( $key =~ /^(password|Password|username|account)$/ ) {
            $obscure->{$key} = qq(*$1*);
            if ( exists $obscure->{_RAW} ) {
                $obscure->{_RAW} =~ s/$key="[^"]*"/$key="*$key*"/gs;
                $obscure->{_RAW} =~ s/$key='[^']*'/$key='*$key*'/gs;
            }
        }
    }
    foreach my $key (qw(_RAW _DEFAULT file)) {
        next unless ( exists $obscure->{$key} );
        $obscure->{$key} =~ s{://([^@]*)\@}{://******\@}gs;
    }
    return $obscure;
}

# =========================
# Get the rendering for spreadsheet
# o Fetches remote documents
# o Consults/updates the cache
# o (Re)renders if miss

sub _getRendering {
    my $this = shift;

    my ( $session, $params, $topic, $web ) = @_;

    my $file     = $params->{file};
    my $filename = $params->{filename};

    my $style = $params->{style};
    my $realm = $params->{realm} || '';

    my ( $keepXLS, $keepRendered ) = ( 1, 1 );

    # Evaluate options that effect rendering.  These contribute to the
    # filenames used for caching the rendering.

    # _id is the sequence of rendered workbooks on page.  It ends up
    # in the HTML as IDs, which must be unique.

    my $opts = ++$this->{_id} . qq(/$cacheVersion/);
    my @opts;
    if ( $this->_isTrue( $params->{showstatus}, 0 ) ) {
        $opts .= 'ss/';
    }
    if ( $params->{rev} ) {
        $opts .= qq(r=$params->{rev}/);
    }

    $params->{lineheight} ||= $lineHeight;
    $opts .= sprintf( "lh(%u)", $params->{lineheight} );
    push @opts, lineheight => $params->{lineheight};

    if ( $params->{Password} ) {
        $keepRendered = 0;
        $opts .= qq(/$params->{Password});
        push @opts, Password => $params->{Password};
    }

    unless ( exists $params->{base} ) {
        my $base = TWiki::Func::getScriptUrl( split( /\./, $params->{topic}, 2 ),
            'viewfile', filename => '_REL_', );
        $base =~ s/filename=_REL_/filename=%REL%/;
        $params->{base} = $base;
    }
    if ( defined $params->{base} ) {
        $opts .= qq(B$params->{base}/);
        push @opts, base => $params->{base};
    }

    if ( $this->_isTrue( $params->{emptyok}, 0 ) ) {
        $opts .= 'eo/';
        push @opts, emptyok => 1;
    }

    if ( exists $params->{gridlines} ) {
        if ( $this->_isTrue( $params->{gridlines}, 0 ) ) {
            $opts .= 'g1/';
            push @opts, gridlines => 1;
        }
        else {
            $opts .= 'g0/';
            push @opts, gridlines => 0;
        }
    }
    if ( exists $params->{gridlabels} ) {
        if ( $this->_isTrue( $params->{gridlabels}, 1 ) ) {
            $opts .= 'gl1/';
            push @opts, gridlabels => 1;
        }
        else {
            $opts .= 'gl0/';
            push @opts, gridlabels => 0;
        }
    }
    if ( defined $params->{minrows} ) {
        $opts .= qq(mr=$params->{minrows}/);
        push @opts, minrows => $params->{minrows};
    }
    if ( defined $params->{mincols} ) {
        $opts .= qq(mc=$params->{mincols}/);
        push @opts, mincols => $params->{mincols};
    }
    if ( $this->_isTrue( $params->{percellcss}, 0 ) ) {
        $opts .= 'pcc/';
        push @opts, percellcss => 1;
    }
    if ( my $sheet = delete $params->{sheet} ) {
        $params->{sheets} = $sheet;
    }
    if ( $params->{sheets} ) {
        $opts .= qq(S$params->{sheets}/);
        push @opts, sheets => $params->{sheets};
    }
    if ( exists $params->{opento} && $params->{opento} ne 'auto' ) {
        $opts .= "is(" . ( $params->{opento} || 1 ) . ')';
        push @opts, opento => ( $params->{opento} || 1 );
    }
    if ( $params->{width} && $params->{width} ne 'auto' ) {
        $opts .= qq(w($params->{width}));
        push @opts, width => $params->{width};
    }
    if ( $params->{height} && $params->{height} ne 'auto' ) {
        $opts .= qq(w($params->{height}));
        push @opts, height => $params->{height};
    }
    if ( $this->_isTrue( $params->{showrev}, 0 ) ) {
        $opts .= qq(srev $params->{showrev}/);
    }
    if ( defined $params->{title} ) {
        $opts .= qq(t="$params->{title}"/);
    }

    $opts .= $style if ($style);

    push @opts, context => $this->{_context}
      if ( exists $this->{_context} && !$this->{_rest} );

    # Cache structure:
    #  WorkArea:
    #   web/topic/.lock - topic global lock (exclusive only for maintenance)
    #   web/topic/attach.filehash.{xls,lock} XLS file, per-file lock
    #   web/topic/attach.renderhash.html renderings for file,
    #   protected by per-XLS file lock.

    my $cache =
      join( '/', $this->_getWorkArea, split( /\./, $params->{topic}, 2 ) );

    unless ( -d $cache ) {
        my $umask = umask(0000);
        eval {
            mkpath(
                $cache, 0,
                (
                    $TWiki::cfg{Plugins}{EmbedExcelPlugin}{DirPermission}
                      || 0711
                )
            );
        };
        umask($umask);
        if ($@) {
            $this->_debug(qq(Failed to create cache directory $cache:$@))
              if ($debug);
            return $this->_error(qq(Unable to create cache directory: $@));
        }
        $this->_debug(qq(Created cache directory $cache)) if ($debug);
    }

    # Hash of filename + options for rendered data
    # Hash includes source if remote
    #
    # Revisions are tricky.  We don't know the revision until it is
    # actually created - but we need a filename first.
    # If an explicit rev is specified, that's appended to the filename
    # in the hash.  If not, a later request for that rev will have a
    # different hash.

    my $rhash = qq($filename.) . Digest::MD5::md5_hex( $file . $opts );

    # Hash of just filename for cache lock and remote files

    my $xhash =
      qq($filename.) . Digest::MD5::md5_hex( $file . ( $params->{rev} || '' ) );

    my $cacheLock;
    unless (
        sysopen( $cacheLock, qq($cache/.lock), O_RDWR | O_CREAT,
            ( $TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission} || 0600 ) )
        && flock( $cacheLock, LOCK_SH )
      )
    {
        close($cacheLock) if ( defined $cacheLock );
        $this->_debug("Failed to access $cache/.lock cache:$!") if ($debug);
        return $this->_error( qq(Unable to access cache: =$!= ), );
    }
    my $cacheFileLock;
    unless (
        sysopen( $cacheFileLock, qq($cache/$xhash.lock), O_RDWR | O_CREAT,
            ( $TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission} || 0600 ) )
        && flock( $cacheFileLock, LOCK_EX )
      )
    {
        $this->_debug("Failed to lock $cache/$xhash.lock cache:$!") if ($debug);
        my $error = $this->_error( qq(Unable to lock cache: =$!= ), );
        close($cacheFileLock) if ( defined $cacheFileLock );
        flock( $cacheLock, LOCK_UN );
        close($cacheLock);
        return $error;
    }
    my $unlockCache = sub {
        flock( $cacheFileLock, LOCK_UN );
        close($cacheFileLock);
        flock( $cacheLock, LOCK_UN );
        close($cacheLock);
        return;
    };

    my ( $data, $attrs );
    my $path = qq($cache/$xhash.xls);

    if ( $file =~ m/$allowedProtocolsRE/o ) {    # Remote request

        # Get a local copy of the attachment (if it exists)

        if ( $this->_isTrue( $params->{nocache}, 0 ) ) {
            unlink($path)
              && $this->_debug(
                qq(Attachment removed from cache *nocache* $path))
              if ($debug);
            $attrs = {};
        }
        else {
            ( $data, $attrs ) =
              $this->_getAttachment( $path, $params->{topic}, $filename,
                $params->{rev} );
            if ( ref $data ) {
                close $data;
            }
            undef $data;
        }

        require LWP::UserAgent;

        my $ua;
        {
            no warnings 'once';
            $ua = LWP::UserAgent->new(
                agent => __PACKAGE__ . '/'
                  . $TWiki::Plugins::EmbedExcelPlugin::RELEASE,
                protocols_allowed => $allowedProtocols,
            );
        }
        if ( $TWiki::cfg{PROXY}{HOST} ) {
            $ua->proxy->(
                $allowedProtocols,
                sprintf( "%s:%u",
                    $TWiki::cfg{PROXY}{HOST},
                    $TWiki::cfg{PROXY}{PORT} )
            );
        }
        else {
            $ua->env_proxy;
        }

        require URI;
        require URI::Escape;

        my $uri = URI->new($file);

        my ( $scheme, $host, $port ) = ( $uri->scheme, $uri->host, $uri->port );
        my $netloc = qq($host:$port);
        my $ui     = $uri->userinfo;
        if ($ui) {
            $uri->userinfo(undef);
            my ( $u, $p ) = URI::Escape::uri_unescape( split( ':', $ui ) );
            $ua->credentials( $netloc, $realm, $u, $p );
            $keepXLS      = 0;
            $keepRendered = 0;
        }
        else {
            if ( exists $params->{username} || exists $params->{password} ) {
                $ua->credentials(
                    $netloc, $realm,
                    ( $params->{username} || '' ),
                    ( $params->{password} || '' )
                );
                $keepXLS      = 0;
                $keepRendered = 0;
            }
        }
        $uri  = $uri->canonical;
        $file = "$uri";

        if ( defined $params->{account} ) {
            $ua->default_header( 'Account', $params->{account} );
            $keepXLS      = 0;
            $keepRendered = 0;
        }

        my $sentEtag = '';
        if ( defined $attrs->{Etag} && length $attrs->{Etag} ) {
            $sentEtag = $attrs->{Etag};
            $ua->default_header( 'If-None-Match', $sentEtag );
            $this->_debug("Sending ETag $sentEtag") if ($debug);
        }

        $this->_debug("Contacting $file") if ($debug);

        my $response;
        {
            my $umask = umask(0177);
            $response = $ua->mirror( $file, $path );
            umask($umask);
        }

        $this->_debug( "External result: ", $response->code, " cache=$path" )
          if ($debug);

        if ( $response->is_error ) {
            &$unlockCache;
            my $content = $this->_sanitize( $response->content || '' );
            return $this->_error(
                sprintf(
                    qq(Unable to access =$file= <br /> =%u %s= ),
                    $response->code, $response->message
                )
              )
              . (
                ( $params->{errors} || 'detail' ) eq 'detail'
                ? qq(<div><noautolink>$content</noautolink></div>)
                : ''
              );
        }

        my @stat;
        unless ( sysopen( $data, $path, O_RDONLY ) && ( @stat = stat($data) ) )
        {
            &$unlockCache;
            $this->_debug("Unable to open result $path:$!") if ($debug);
            return $this->_error(qq(Unable to open result $file:$!));
        }

        my $update   = '';
        my $saveOpts = {
            dontlog  => 1,
            filesize => $stat[7] || 0,
            filedate => $stat[9] || 0,
            filepath => $file,
        };

        # SVN returns a full (200) reply with the same Etag when nothing
        # has changed.  Since Etags are supposed to be unique, treat
        # that case as a 304 reply.

        my $rcode = $response->code;

        if ( defined( my $responseEtag = $response->header('ETag') ) ) {
            if ( $responseEtag eq $sentEtag ) {
                $this->_debug("Received same Etag") if ($debug);
                $rcode = 304 if ( $rcode == 200 );
            }
            else {
                if ( length $sentEtag ) {
                    $update = 'content';
                    $this->_debug(qq(Received new Etag, saving new contents))
                      if ($debug);
                }
                else {
                    $update = 'props';
                    $this->_debug(
                        qq(Received Etag, updating attachment properties))
                      if ($debug);
                }
            }
            $saveOpts->{comment} =
              ( defined( $attrs->{comment} ) && length( $attrs->{comment} ) )
              ? qq( $attrs->{comment} Etag:$responseEtag)
              : qq(Etag:$responseEtag);
        }
        elsif ( defined $attrs->{comment} ) {
            $saveOpts->{comment} = $attrs->{comment};
            $update ||= 'props' if ( $attrs->{Etag} );
        }

        unless ( defined $attrs->{size}
            && $attrs->{size} == $saveOpts->{filesize}
            && defined $attrs->{mtime}
            && $attrs->{mtime} == $saveOpts->{filedate}
            && ( $rcode == 304 ) )
        {
            $update = 'content';
        }
        if ($update) {
            if ( defined $attrs->{hidden} ) {
                $saveOpts->{hide} = $attrs->{hidden};
            }
            elsif ( !defined $attrs->{attached} ) {
                $saveOpts->{hide} = 1;
            }

            if ( $update eq 'content' ) {
                $saveOpts->{stream} = $data;
            }

            my ( $w, $t ) = split( /\./, $params->{topic}, 2 );

            if ($debug) {
                require Data::Dumper;
                $this->_debug(
                    "Saving $w.$t $filename\n",
                    Data::Dumper::Dumper(
                        $rcode, $w, $t,
                        $filename,
                        {
                            map { ref($_) =~ /GLOB|Fh/ ? 'GLOB' : $_ }
                              %$saveOpts
                        },
                        $attrs
                    )
                );
            }

            my $error =
              TWiki::Func::saveAttachment( $w, $t, $filename, $saveOpts );

            seek( $data, 0, SEEK_SET );
            if ($error) {
                close $data;
                &$unlockCache;
                $this->_debug(qq(saveAttachment returned error: $error))
                  if ($debug);
                return $this->_error(qq(Unable to update attachment: $error));
            }

            $attrs->{size}  = $saveOpts->{filesize};
            $attrs->{mtime} = $saveOpts->{filedate};
            ( undef, undef, my $newrev, undef ) =
              TWiki::Func::getRevisionInfo( $w, $t, undef, $filename );

            $newrev = 'unknown' unless ( defined $newrev );

            $this->_debug( qq(Updated attachment $update.  Old revision: )
                  . ( $attrs->{rev} || 'none' )
                  . qq( New revision: $newrev) )
              if ($debug);
            $attrs->{rev} = $newrev;
        }

        # Technically, this is correct.  If the host didn't say 'Not Modified',
        # our rendering cache is stale.  On the other hand, some hosts send
        # full responses despite our conditional GET.  And our date check
        # should catch that. For now, I guess I'll be conservative.

        if ( $update eq 'content' || $rcode != 304 ) {
            $this->_debug("Invalidating cache $rhash.html")
              if ( $debug && -f qq($cache/$rhash.html) );
            unlink(qq($cache/$rhash.html));
        }
    }
    else {
        ( $data, $attrs ) =
          $this->_getAttachment( $path, $params->{topic}, $filename,
            $params->{rev} );
        unless ( ref $data ) {
            &$unlockCache;
            return $this->_error(qq(<noautolink>$data</noautolink>));
        }
    }

    my $xtime = $attrs->{mtime};

    # Check cache for a rendered version of this request

    my $rcache;
    if (
        !$this->_isTrue( $params->{nocache}, 0 )
        && sysopen(
            $rcache,
            qq($cache/$rhash.html),
            O_RDWR | O_CREAT,
            ( $TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission} || 0600 )
        )
      )
    {
        my $rtime = ( stat($rcache) )[9];

        if ( defined $xtime && defined $rtime && -s _ && $rtime > $xtime ) {
            local $/;

            $this->_debug("Using cached rendering from $cache/$rhash.html")
              if ($debug);

            my $html = <$rcache>;
            close $rcache;
            &$unlockCache;
            close $data;
            return ( \$html, $rtime ) if (wantarray);
            return \$html;
        }
        $this->_debug(
            "Cached rendering present, but stale in $cache/$rhash.html")
          if ( $debug && -s _ );
    }

    my $title   = $params->{title};
    my $revdate = TWiki::Func::formatTime( $attrs->{mtime} );
    my $revtag  = qq(r$attrs->{rev});
    if ( $this->_isTrue( ( my $revtype = $params->{showrev} ), 0 ) ) {
        my $revtext;
        if ( $revtype eq 'date' ) {
            $revtext = $revdate;
        }
        elsif ( $revtype =~ /^rev(?:ision)?$/ ) {
            $revtext = $revtag;
        }
        else {
            $revtext = qq($revtag - $revdate);
        }
        my $revinfo = sprintf(
            qq(<span class="ssRevInfo"><a href="%s">%s %s</a></span>),
            TWiki::Func::getScriptUrl(
                split( /\./, $params->{topic}, 2 ), 'viewfile',
                filename => $filename,
                rev      => $attrs->{rev}
            ),
            $filename,
            $revtext
        );
        if ( defined $title ) {
            $title = qq(<div class="ssTitleText">$title</div> $revinfo);
        }
        else {
            $title = qq(<p>$revinfo</p>);
        }
    }
    elsif ( defined $title ) {
        my $revfile = sprintf(
            qq(<span class="ssRevInfo"><a href="%s">%s</a></span>),
            TWiki::Func::getScriptUrl(
                split( /\./, $params->{topic}, 2 ), 'viewfile',
                filename => $filename,
                rev      => $attrs->{rev}
            ),
            $filename
        );
        $title =~ s/\$rev/$revtag/g;
        $title =~ s/\$date/$revdate/g;
        $title =~ s/\$file/$revfile/g;
    }
    if ( defined $title ) {
        push @opts, title => $title;
    }

    push @opts, _debug => $this->{_debug} if ($debug);

    my $xls = ExcelToHtml->renderFile(
        @opts,
        id   => "XLS-$rhash",
        cgi  => $this->{_query},
        file => $data,
    );

    $this->_debug(
        "Rendered: (",
        ( defined $xls->errorCode ? $xls->errorCode : 'undef' ) . ') '
          . $xls->error
    ) if ($debug);

    close $data;

    unless ($keepXLS) {
        unlink($path);
        unlink(qq($path.etag));
        undef $xtime;
    }

    my $status = '';
    if ( $this->_isTrue( $params->{showstatus}, 0 ) ) {
        $status =
qq(<HR> __Rendering status__\n%TABLE{sort="off" headerrows="1" valign="top" dataalign="left"}%\n);
        my @status = $xls->getStatus;
        if (@status) {
            $status .=
qq(\n| Workheet | *First Row* | *First Column* | *Last Row* | *Last Column* | *Result* | \n);

            foreach my $sheet (@status) {
                $status .= sprintf(
                    qq(| *%s* | %u | %u | %u | %u | %s |\n),
                    $sheet->{name},     $sheet->{firstRow},
                    $sheet->{firstCol}, $sheet->{lastRow},
                    $sheet->{lastCol},  $sheet->{status}
                );
            }
        }
        else {
            $status .= qq(\nNo worksheets processed\n\n");
        }
    }

    if ( $xls->errorCode ) {
        close $rcache if ($rcache);
        unlink(qq($cache/$rhash.html));
        &$unlockCache;

        if ($status) {
            $xls->close;
            return \$status;
        }
        my $error = $this->_error( $xls->error );
        $xls->close;
        return $error;
    }

    $this->{_context} = $xls->saveContext unless ( $this->{_rest} );

    my $html = $xls->getHtml;
    $xls->close;

    if ($rcache) {
        if ($keepRendered) {
            $this->_debug("Caching rendering in $cache/$rhash.html")
              if ($debug);

            print $rcache ($$html);

            truncate( $rcache, tell($rcache) );
            $xtime = ( stat($rcache) )[9] || 0;
            close $rcache;
        }
        else {
            close $rcache;
            unlink(qq($cache/$rhash.html));
            $xtime = time;
        }
    }

    &$unlockCache;

    $this->_debug( qq(Rendered result: ),
        length($$html), ' ', ( scalar localtime($xtime) ) )
      if ($debug);

    $$html .= $status;
    return ( $html, $xtime ) if (wantarray);
    return $html;
}

# =========================
# Get an attachment and return a file handle (or error string)

sub _getAttachment {
    my $this = shift;
    my ( $path, $topic, $filename, $revWanted ) = @_;

    ( my $web, $topic ) = split( /\./, $topic, 2 );

    my $data;
    my $attrs = {};
    my $error = try {

        # What keeps these atomic?
        unless ( TWiki::Func::topicExists( $web, $topic ) ) {
            throw Error::Simple(qq(Topic does not exist));
        }
        unless ( TWiki::Func::attachmentExists( $web, $topic, $filename ) ) {
            throw Error::Simple(qq(Attachment does not exist));
        }
        $attrs->{attached} = 1;

        my ( $rtime, $user, $rev, $comment ) =
          TWiki::Func::getRevisionInfo( $web, $topic, $revWanted, $filename );
        unless ( defined $rtime ) {
            throw Error::Simple(qq(unable to get revision time));
        }
        $attrs = {
            attached => 1,
            rtime    => $rtime,
            user     => $user,
            rev      => $rev || 0,
            comment  => $comment,
        };
        if ( defined $comment && $comment =~ s/(?:^|\s+)Etag:(.*)\Z//sm ) {
            $attrs->{comment} = $comment;
            $attrs->{Etag}    = $1;
        }

        # $rtime is the time the revision was checked-in to RCS (or
        # whatever Store implementation.)  It seems that the only way to
        # get the actual file mtime is to use the topic metadata.  This
        # can mean iterating thru multiple versions of the topic to find
        # the topic rev with the metadata for the specified attachment
        # rev. sigh.
        #
        # From most recent to oldest, look in topic for attachment
        # metadata of desired attachment rev.  Use that date.

        # $meta-> TOPICINFO is not a reliable source for revision, so we
        # must use the (slow) getRevisionInfo to find the latest.

        my ( undef, undef, $trev, undef ) =
          TWiki::Func::getRevisionInfo( $web, $topic, undef );
        die("Corrupt topic: no rev") unless ( defined $trev );

        while (1) {
            my ( $meta, undef ) = TWiki::Func::readTopic( $web, $topic, $trev );
            last unless ($meta);

            my $fa = $meta->get( 'FILEATTACHMENT', $filename );

            if ( defined $fa && defined $fa->{version} ) {
                if ( $fa->{version} == ( $rev || $fa->{version} ) ) {
                    $attrs->{mtime} = $fa->{date};
                    $attrs->{size}  = $fa->{size};
                    if ( defined $fa->{attr} ) {
                        $attrs->{hidden} = ( $fa->{attr} =~ /h/ ) ? 1 : 0;
                    }
                    last;
                }
            }
            last unless ( --$trev < 1 );
        }

        $attrs->{mtime} = $rtime unless ( $attrs->{mtime} );

        # Open/create cache copy

        my $umask = umask(0177);
        unless (
            sysopen(
                $data, $path,
                O_RDWR | O_CREAT,
                (
                    $TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission}
                      || 0600
                )
            )
          )
        {
            umask($umask);
            throw Error::Simple(qq(Can't open cache data file: $!));
        }
        umask($umask);
        binmode $data;

        # Return cache copy if current

        my $mtime = $attrs->{mtime} || 0;

        my @stat = stat($data);
        if (   @stat
            && -f _
            && $stat[7]
            && $stat[7] == ( $attrs->{size} || 0 )
            && ( $stat[9] || 0 ) == $mtime )
        {
            $this->_debug(qq(Cache copy of $path is current)) if ($debug);
            return undef;
        }

        # Update/create cache copy

        truncate( $data, 0 );

        # Contrary to the documentation, this does NOT return undef if
        # the attachment doesn't exist.
        my $attachment =
          TWiki::Func::readAttachment( $web, $topic, $filename, $revWanted );
        unless ( defined $attachment ) {
            close($data);
            unlink($path);
            throw Error::Simple(
                qq(attachment does not exist or cannot be read));
        }

        $this->_debug(qq(Writing attachment data to cache $path)) if ($debug);
        print $data ($attachment);
        undef $attachment;

        $attrs->{size} = tell($data);
        seek( $data, 0, SEEK_SET );

        utime( $mtime, $mtime, $path );
        return undef;
    }
    catch Error::Simple with {
        my $exc = shift;

        return $exc->{-text};
    };
    if ($error) {
        $error =
          qq(Failed to read attachment: =$web.$topic $filename= - $error);
        $this->_debug($error) if ($debug);
        return ( $error, $attrs ) if (wantarray);
        return $error;
    }

    return ( $data, $attrs ) if (wantarray);
    return $data;
}

# =========================
# Periodic process to cleanup work area

sub cleanupWorkarea {
    my $this = shift;

    local $SIG{__WARN__} = \&Carp::confess;

    unless ( $this->{_command_line} ) {
        die "EmbedExcelCCleanup: Only runs from the command line\n";
    }

    # Protect against common user error - expecting -t to work.
    # It doesn't work with the V5+ Engine::CLI parser.

    foreach my $arg (@ARGV) {
        if ( $arg =~ /^-/ && $arg !~ /=/ ) {
            print STDERR "Switches require a value.  Specify  $arg=1\n";
            exit(1);
        }
    }
    my $query = $this->{_query};
    $this->{_verbose} =
      !( $query && ( $query->param('quiet') || $query->param('q') ) );
    $this->{_testmode} =
      ( $query && ( $query->param('test') || $query->param('t') ) );

    $debug ||= $this->{_verbose};
    $this->_debug("In test mode") if ( $this->{_testmode} && $debug );

    my $cache = $this->_getWorkArea;

    my $exitStatus = 0;

    my $expires = $TWiki::cfg{Plugins}{EmbedExcelPlugin}{MaximumAge} || 0;

    $expires = time - ( $expires * 24 * 60 * 60 ) if ($expires);

    $this->{_expires} = $expires;

    my $webRegex   = TWiki::Func::getRegularExpression('webNameRegex');
    my $topicRegex = TWiki::Func::getRegularExpression('wikiWordRegex');

    if ( opendir( my $cdir, $cache ) ) {
        $this->_debug("Scanning $cache") if ($debug);
        $this->{_indent}++;

        while ( defined( my $webName = readdir($cdir) ) ) {
            next if ( $webName =~ /^\.\.?\z/ );

            if ( $webName =~ /^($webRegex)\z/ ) {
                $webName = $1;
            }
            else {
                next;
            }

            next unless ( -d qq($cache/$webName) );

            if ( TWiki::Func::webExists($webName) ) {

                if ( opendir( my $wdir, qq($cache/$webName) ) ) {
                    $this->_debug("Scanning $webName/") if ($debug);
                    $this->{_indent}++;

                    while ( defined( my $topicName = readdir($wdir) ) ) {
                        next if ( $topicName =~ /^\.(?:\.|lock)?\z/ );

                        if ( $topicName =~ /^($topicRegex)\z/ ) {
                            $topicName = $1;
                        }
                        else {
                            next;
                        }
                        next unless ( -d qq($cache/$webName/$topicName) );

                        my $cacheLock;
                        unless (
                            sysopen(
                                $cacheLock,
                                qq($cache/.lock),
                                O_RDWR | O_CREAT,
                                (
                                    $TWiki::cfg{Plugins}{EmbedExcelPlugin}
                                      {FilePermission} || 0600
                                )
                            )
                            && flock( $cacheLock, LOCK_EX )
                          )
                        {
                            close($cacheLock) if ( defined $cacheLock );
                            $this->_debug("Unable to lock cache: $!")
                              if ($debug);
                            next;
                        }

                        if ( TWiki::Func::topicExists( $webName, $topicName ) )
                        {

                            if (
                                opendir(
                                    my $tdir, qq($cache/$webName/$topicName)
                                )
                              )
                            {
                                $this->_debug("Scanning $webName/$topicName")
                                  if ($debug);
                                $this->{_indent}++;

                                while (
                                    defined( my $attachName = readdir($tdir) ) )
                                {
                                    next if ( $attachName =~ /^\.\.?\z/ );

                                    my $path = join( '/',
                                        $cache,     $webName,
                                        $topicName, $attachName );

                                    next unless ( -f $path );

                                    my $status = 0;
                                    if ( $attachName =~
                                        /^(.*)\.([[:xdigit:]]{32})\.xls\z/ )
                                    {
                                        $status =
                                          $this->_processXLS( $webName,
                                            $topicName, $cache, $1, $2 );
                                    }
                                    elsif ( $attachName =~
                                        /^(.*)\.([[:xdigit:]]{32})\.html\z/ )
                                    {
                                        $status =
                                          $this->_processHTML( $webName,
                                            $topicName, $cache, $1, $2 );
                                    }
                                    elsif ( $attachName =~
                                        /^(.*)\.([[:xdigit:]]{32})\.lock$/ )
                                    {
                                        $status =
                                          $this->_processLock( $webName,
                                            $topicName, $cache, $1, $2 );
                                    }
                                    $exitStatus ||= $status;
                                }
                                closedir($tdir);
                                $this->{_indent}--;
                                $this->_debug("End of $webName/$topicName")
                                  if ($debug);
                            }
                            else {
                                print STDERR (
"Unable to scan $cache/$webName/$topicName: $!\n"
                                );
                                $exitStatus ||= 1;
                            }
                        }
                        else {
                            $this->_debug(
"Topic $webName.$topicName does not exist, removing cache"
                            ) if ($debug);
                            eval {
                                rmtree( qq($cache/$webName$topicName), 0, 0 )
                                  unless ( $this->{_testmode} );
                            };
                            if ($@) {
                                print STDERR (
"Unable to remove $webName.$topicName cache: $@\n"
                                );
                                $exitStatus ||= 1;
                            }
                        }

                        if ( defined $cacheLock ) {
                            flock( $cacheLock, LOCK_UN );
                            close($cacheLock);
                        }
                    }
                    closedir($wdir);
                    $this->{_indent}--;
                    $this->_debug("End of $webName/") if ($debug);
                }
                else {
                    print STDERR ("Unable to scan $cache/$webName: $!\n");
                    $exitStatus ||= 1;
                }
            }
            else {
                $this->_debug("Web $webName does not exist, removing cache")
                  if ($debug);
                eval {
                    rmtree( qq($cache/$webName), 0, 0 )
                      unless ( $this->{_testmode} );
                };
                if ($@) {
                    print STDERR ("Unable to remove $webName cache: $@\n");
                    $exitStatus ||= 1;
                }
            }
        }
        closedir($cdir);
        $this->{_indent}--;
        $this->_debug("End of $cache") if ($debug);
    }
    else {
        print STDERR ("Unable to scan $cache: $!\n");
        $exitStatus ||= 1;
    }

    return $exitStatus;
}

# =========================
# Web/Topic/attachment moves
#
# Because filenames are hashed and the hashes usually
# contain location information, we simply flush the
# cache.  As little of it as is possible.

sub afterRenameHandler {
    my $this = shift;

    my ( $oldWeb, $oldTopic, $oldAttachment, $newWeb, $newTopic,
        $newAttachment ) = @_;

    my $cache = $this->_getWorkArea;

    if ( $oldTopic eq '' && $oldAttachment eq '' ) {

        # web rename

        if ( length $oldWeb ) {
            $this->_debug(qq(Rename $oldWeb => $newWeb, flushing cache))
              if ($debug);
            eval {
                rmtree( qq($cache/$oldWeb), 0, 0 )
                  unless ( $this->{_testmode} );
            };
            if ($@) {
                TWiki::Func::writeWarning(
" - $this->{plugin}\[$$]: Unable to flush $oldWeb cache - $@"
                );
            }
        }
        return;
    }

    if ( $oldAttachment eq '' ) {

        # topic rename

        if ( length $oldWeb && length $oldTopic ) {
            $this->_debug(
qq(Rename $oldWeb.$oldTopic => $newWeb/$newTopic, flushing cache)
            ) if ($debug);
            eval {
                rmtree( qq($cache/$oldWeb/$oldTopic), 0, 0 )
                  unless ( $this->{_testmode} );
            };
            if ($@) {
                TWiki::Func::writeWarning(
" - $this->{plugin}\[$$]: Unable to flush $oldWeb.$oldTopic cache - $@"
                );
            }
        }
        return;
    }

    return
      unless ( length $oldWeb && length $oldTopic && length $oldAttachment );

    # attachment rename

    # hashes in names make it pointless to save the files.
    # Just delete them (& they'll be re-fetched/created on reference)

    $this->_debug(
qq(Rename $oldWeb.$oldTopic $oldAttachment => $newWeb.$newTopic $newAttachment, flushing cache)
    ) if ($debug);

    my $cacheLock;
    if (
        sysopen( $cacheLock, qq(/.lock), O_RDWR | O_CREAT,
            ( $TWiki::cfg{Plugins}{EmbedExcelPlugin}{FilePermission} || 0600 ) )
        && flock( $cacheLock, LOCK_EX )
      )
    {

        if ( opendir( my $dh, qq($cache/$oldWeb/$oldTopic) ) ) {
            while ( defined( my $attachName = readdir($dh) ) ) {
                next if ( $attachName =~ /^\.\.?\z/ );

                next unless ( -f $attachName );

                if ( $attachName =~
                    m/^($oldAttachment\.[[:xdigit:]]{32}\.(?:xls|html|lock))\z/
                  )
                {
                    unless ( $this->_unlink(qq($cache/$oldWeb/$oldTopic/$1)) ) {
                        TWiki::Func::writeWarning(
" - $this->{plugin}\[$$]: Unable to flush $oldWeb.$oldTopic $oldAttachment cache - $!"
                        );
                    }
                }
            }
        }

        flock( $cacheLock, LOCK_UN ) if ( defined $cacheLock );
        close($cacheLock);
    }

    return;
}

# =========================
# Cleanup cache entry

sub _processXLS {
    my $this = shift;

    my ( $web, $topic, $cache, $fileName, $hash ) = @_;

    return 0 if ( TWiki::Func::attachmentExists( $web, $topic, $fileName ) );

    $this->_debug(
        "Removing cached copy of $fileName - no longer attached to topic")
      if ($debug);
    unless ( $this->_unlink(qq($cache/$web/$topic/$fileName.$hash.xls)) ) {
        $this->_debug(" - Failed: $!");
        return 1;
    }
    return 0;
}

# =========================
# Cleanup cache entry

sub _processHTML {
    my $this = shift;

    my ( $web, $topic, $cache, $fileName, $hash ) = @_;

    my @stat = stat(qq($cache/$web/$topic/$fileName.$hash.html));
    return 1 unless (@stat);

    if ( !TWiki::Func::attachmentExists( $web, $topic, $fileName )
        || ( $this->{_expires} && $stat[9] < $this->{_expires} ) )
    {
        $this->_debug(
            "Removing expired rendering of $fileName - $fileName.$hash.html")
          if ($debug);
        unless ( $this->_unlink(qq($cache/$web/$topic/$fileName.$hash.html)) ) {
            $this->_debug("    - Failed: $!");
            return 1;
        }
    }

    return 0;
}

# =========================
# Cleanup cache entry

sub _processLock {
    my $this = shift;

    my ( $web, $topic, $cache, $fileName, $hash ) = @_;

    # Since the global lock is held, the .lock file isn't in use
    # Simply delete it - it's created when needed.

    $this->_debug("Removing lockfile $fileName.$hash.lock") if ($debug);
    unless ( $this->_unlink(qq($cache/$web/$topic/$fileName.$hash.lock)) ) {
        $this->_debug("    - Failed: $!");
        return 1;
    }
    return 0;
}

# =========================
sub _getWorkArea {
    my $this = shift;

    my $umask = umask(
        ~( $TWiki::cfg{Plugins}{EmbedExcelPlugin}{DirPermission} || 0711 ) );

    my $cache = $this->{cacheddir}
      || ( $this->{cachedir} = TWiki::Func::getWorkArea( $this->{plugin} ) );
    umask($umask);

    return $cache;
}

# =========================
sub _unlink {
    my $this = shift;

    return 0;
    return scalar @_ if ( $this->{_testmode} );

    return unlink(@_);
}

# =========================
sub _debug {
    my $this = shift;

    return unless ($debug);

    if ( $this->{_command_line} ) {
        foreach my $line ( split /\n/ms, join( '', @_ ) ) {
            print STDERR (
                join( '', "Debug: ", ( ' ' x $this->{_indent} ), $line, "\n" )
            );
        }
    }
    else {
        foreach my $line ( split /\n/ms, join( '', @_ ) ) {

            # Note that writeDebug does NOT want a \n

            TWiki::Func::writeDebug(
                join( '',
                    "- ", $this->{plugin}, "[", $$, "]: ",
                    ( ' ' x $this->{_indent} ), $line )
            );
        }
    }

    return;
}

# =========================
# Return formatted options documentation for plugin topic

sub _optdoc {
    my $this = shift;

    my ($params) = @_;

    my $loc = tell(DATA);

    my $doc = '';
    my %doc = ( _PREFIX => '', _SUFFIX => '' );
    my $tableSeen;
    while (<DATA>) {
        next if (m{^\s*#});
        last if (m{^__END__$});
        if (/^\s*?\|\s*?[=*]?(\w+)[=*]?\s*?\|/) {    # Table data
            $tableSeen = 1;
            $doc{$1} = $_;
        }
        elsif ($tableSeen) {
            $doc{_SUFFIX} .= $_;
        }
        else {
            $doc{_PREFIX} .= $_;
        }
    }
    seek( DATA, $loc, SEEK_SET );
    foreach
      my $item ( '_PREFIX', 'Option', @validParams, @ExcelToHtml::validParams,
        '_SUFFIX' )
    {
        $doc .= $doc{$item} if ( exists $doc{$item} );
    }
    return $doc;
}

# =========================
# Return formatted CSS class documentation for plugin topic

sub _cssdoc {
    my $this = shift;

    my ($params) = @_;

    my $doc  = ExcelToHtml->cssdoc;
    my $item = $params->{_DEFAULT};

    if ($item) {
        my %idx = map { ( $_->[0] => $_->[1] ) } @$doc;

        my $text = $idx{$item};
        return qq(%RED%No information for "$item"%ENDCOLOR%)
          unless ( defined $text );

        chomp $text;
        $text = "| $item | $text |";
        $text =~ s/\n/\\\n/gms;
        return $text . "\n";
    }

    my $table =
qq(%TABLE{sort="off" headerrows="1" valign="top" dataalign="left"}%\n| *Class* | *Usage* |\n);

    foreach my $class (@$doc) {
        my $text = $class->[0];
        $table .= qq(| $text |  );

        $text = $class->[1];
        chomp $text;
        $text =~ s/\n/\\\n/gms;
        $table .= qq($text |\n);
    }

    return $table;
}

# =========================
sub DESTROY {
    my $this = shift;

    delete $this->{$_} foreach ( keys %$this );

    return;
}

package TWiki::Plugins::EmbedExcelPlugin::Log;

# Logging class for ExcelToHtml's debug output
# Send to TWiki log

sub TIEHANDLE {
    my $class = shift;
    my ($core) = @_;

    return bless { core => $core }, $class;
}

sub PRINT {
    my $self = shift;
    return $self->{core}->_debug(@_);
}

sub PRINTF {
    my $self = shift;
    my $fmt  = shift;

    return $self->PRINT( sprintf( $fmt, @_ ) );
}

package TWiki::Plugins::EmbedExcelPlugin::Core;

1;
__DATA__
# Please keep this in sync with the list of valid parameters at the top of this file, and the
# options supported by ExcelToHtml.  The presentation order is determined by those lists, not
# the order seen here.
#
These options are specified as =option="value"= in the =%<nop>EMBEDXLS%= macro call.  The =file=
option is the default option; the =file== keyword may be omitted.  Do not use commas between
options.

For example, to open =Sample.xls= worksheets =2= and =3,= with =3= initially open, use:
<div style="margin-left:5em;"> =%<nop>EMBEDXLS{ "Sample.xls" sheets="2,3" opento="3" }%= </div>

%TABLE{sort="off" headerrows="1" valign="top"}%
 | *Option* | *Values * | *Default* | *Function* |
 | =topic= | topic<br /> web.topic | _current_ | Specify topic that contains the spreadsheet (as an attached file) |
 | =file= | attachment name<br />URL | | Name of *.xls* file to embed.<br />  May be an http,https, or ftp URL. |
 | =filename= | attachment name | <i>from =file= </i> | Name of attachment as stored in %WIKITOOLNAME% |
 | =title= | string | | Use string as a title for the spreadsheet. HTML (but not TML) markup can be included.<br /> The tokens =$file=, =$date= &amp; =$rev= will be replaced if =showrev= is not used. |
 | =showrev= | boolean<br /> =date= <br /> =revision= | false | Include the filename, date and revision of the xls file (at the end of the title, if title is present)<br /> =date= includes only the revision date<br /> =revision= includes only the revision number |
 | =rev= | number |_latest rev_ | Revision number of attachment to render |
 | =sheets= | omitted<br />sheet names | _all sheets_ | Specify name(s) or number(s)<a href="#foot4"><sup>4</sup></a> of sheets to be rendered.  Separate with comma. |
 | =opento= | auto <br />name or number | auto | Name or number<a href="#foot4"><sup>4</sup></a> of sheet to be opened when workbook is rendered.<a href="#foot3"><sup>3</sup></a> <br /> *auto* opens the sheet that was active when the workbook was last saved. |
 | =emptyok= | boolean | false | When true, won&#39;t report a warning for empty worksheets |
 | =gridlines= | auto<br />boolean | auto | When true, grid lines are rendered.<a href="#foot3"><sup>3</sup></a><br /> *auto* uses page setup in worksheet |
 | =gridlabels= | auto<br />boolean | auto | When true, grid labels (Column and row headings) are rendered.<a href="#foot3"><sup>3</sup></a><br /> *auto* uses page setup in worksheet|
 | =mincols= | number | 0 | Minimum number of columns to display |
 | =minrows= | number | 0 | Minimum number of rows to display |
 | =height= | auto<br />cssUnit | auto | Fixed height for workbook.  Any CSS unit is OK (typically, =px= or =%= is used) |
 | =width= | auto<br />cssUnit | auto | Fixed width for workbook.  Any CSS unit is OK (typically, =px= or =%= is used) |
 | =username= | string<a href="#foot2"><sup>2</sup></a> | | %PURPLE%Username for accessing remote file%ENDCOLOR% |
 | =password= | string<a href="#foot2"><sup>2</sup></a> | | %PURPLE%Password for accessing remote file%ENDCOLOR% |
 | =account= | string<a href="#foot2"><sup>2</sup></a> | | %PURPLE%Account for accessing remote file (FTP)%ENDCOLOR% |
 | =realm= | string | | %PURPLE%Authentication realm for accessing remote file%ENDCOLOR% |
 | =Password= | string<a href="#foot2"><sup>2</sup></a> | | %PURPLE%Password used to open spreadsheet%ENDCOLOR% |
 | =errors= | detail <br />brief | detail | "detail" includes full webserver response for errors accessing remote content<br />&#39;brief&#39; includes only the response code & text. |
 | =style= | _URL_ | | Specifies the URL of a custom CSS stylesheet (See [[#AdvancedFormatting][Advanced Formatting]] for usage.) |
 | =showstatus= | boolean | false | Displays rendering status for each sheet, including the *first* issue encountered.  |
 | =percellcss= | boolean | false | Rendering includes a unique CSS class for each cell.  Rarely needed by custom CSS; slows web browser |
 | =lineheight= | number | 1.5 | Line height in CSS stylesheets, in units of em.  |
 | =mode= | fast<br />inline<a href="#foot2"><sup>2</sup></a> | inline | Normally a spreadsheet will be fetched by the browser independently of the topic content.  This allows the browser to cache the spreadsheet and improves performance.  Alternatively, the spreadsheet can be rendered inline, which is recommended only if *fast* is not supported by your webserver.  =inline= prevents browser caching. |
 | =nocache= | boolean | false | When true, any cached data will be discarded and refreshed <br />The browser will not be permitted to use cached data.|
 | =id= | string | hash | Unique identifier assigned to each rendering/options/pageposition.<br /> Can be used to target CSS, but be aware that changing an Id creates a new cache instance. |

# Undocumented: cgi, action

=boolean= values follow the %WIKITOOLNAME% convention that 'false', 'off', 'no', 0, and '' all mean false.

Note that in several cases, the absence of a paramenter is not the same as setting it to a false value or empty string.

The %TOPIC% caches files retrieved from external sources, and also caches the rendered version.  This is normally transparent,
but =nocache="1"= can be used to force the file to be re-acquired and/or re-rendered.

*%PURPLE%Security considerations:%ENDCOLOR%* Use of the username and password parameters is not recommended unless their values are dynamic - e.g. come from user input at the time of viewing.  Passwords are secrets; wikis are public places.

The spreadsheet password only supports the default Excel encryption scheme, which is =Office 97/2000 Compatible encryption=.

If a password is used to fetch a remote file, the file will not be cached locally.  If a password is used to open a file, the rendering will not be cached.  

The =realm= parameter is usually the title of the pop-up window that prompts you for credentials when visiting the file with a web browser.  E.g. "Bernie's Special Accounts Server"
<hr align="left" class="foot">
<div class="foot">
<a name="foot2"><sup>2</sup></a>This will also force =inline= rendering, which will increase page view times.
</div>
<div class="foot">
<a name="foot3"><sup>3</sup></a>These determine the initial rendering of the worksheet.  The person viewing the topic can change the presentation by clicking on a tab, cell, or label.
</div>
<div class="foot">
<a name="foot4"><sup>4</sup></a>Worksheets are numbered starting with *1*.  Numbers *include* any hidden worksheets, and reflect the order in which sheets appear on the tabs bar.  Use of sheet names is prefered.
</div>

__Other considerations:__
Workbooks are variably sized elements.  This is generally transparent, but if a workbook is displayed in a hidden element (e.g. in a TWISTY), you must call XLS.ssDisplay('#elementID') when it is visible in order for it to display properly.
This is typically done witn an _onclick_ attribute on the surrounding div or the button that controls it.  Alternatively, you can set a fixed size with the =width= and =height= options.
__END__
