#!/usr/bin/perl -T

# Copyright (C) 2014 Timothe Litt, Southborough, Massachusetts.
# ALL RIGHTS RESERVED.
# Provided under license.  See accompanying package.
#
# Present a spreadsheet as HTML table(s)
#
# For Spreadsheet::ParseExcel ambiguities, see http://download.microsoft.com/download/0/B/E/0BE8BDD7-E5E8-422A-ABFD-4342ED7AD886/Excel97-2007BinaryFileFormat(xls)Specification.xps
#
# Formatting is controlled by the spreadsheet.  Some print dialog options are obeyed:
#  Print gridlines
#  Print row and column headers
#
# As much as possible is done in the CSS; in cases where that's really awkward,
# style attributes are added.
#
# LOOSE ENDs:
# text shrink-to-fit
# Diagonal lines as 'borders'
# non-solid patterns for fill

package ExcelToHtml;

use strict;
use warnings;

my $debug;

use Carp;
use Fcntl(qw/:seek/);
use HTML::Entities ();

sub renderFile;

sub _base26;
my $base26 = join( '', ( 'A' .. 'Z' ) );

my $PI = atan2( 1, 1 ) * 4;
my $PI_180 = $PI / 180;

my $lineHeight = 1.5;    # em

# Assumed DPI for pt -> pixel conversions

my $dpi = 96;

# Fudge factors for calculating text overhang.
# It takes a big library to really solve the problem.
# This assumes (wrongly) that adding the widths of
# characters gives the width of a string; it doesn't
# account for kerning, font changes, or other typography.
#
# Doing nothing seems to grossly overestimate, This may be
# better.  To avoid the issue, set the last column of your
# spreadsheet to something with 'wrap'.
#
# These were measured with 12pt Arial in Firefox by populating
# a span with 100 characters and measuring the width with
# javascript..

my @widest = (
    10,      4.4500,  5.6800,  8.9000,  8.9000,  11.1000, 10.6700, 3.0500,
    5.3300,  5.3300,  6.2300,  9.3500,  4.4500,  5.3300,  4.4500,  4.4500,
    8.9000,  7.7300,  8.9000,  8.9000,  8.9000,  8.9000,  8.9000,  8.9000,
    8.9000,  8.9000,  4.4500,  4.4500,  9.3500,  9.3500,  9.3500,  8.9000,
    16.2500, 10.6700, 10.6700, 11.5500, 11.5500, 10.6700, 9.7700,  12.4500,
    11.5500, 4.4500,  8.0000,  10.6700, 8.9000,  13.3300, 11.5500, 12.4500,
    10.6700, 12.4500, 11.5500, 10.6700, 9.7700,  11.5500, 10.6700, 15.1000,
    10.6700, 10.6700, 9.7700,  4.4500,  4.4500,  4.4500,  7.5000,  8.9000,
    5.3300,  8.9000,  8.9000,  8.0000,  8.9000,  8.9000,  4.1500,  8.9000,
    8.9000,  3.5500,  3.5500,  8.0000,  3.5500,  13.3300, 8.9000,  8.9000,
    8.9000,  8.9000,  5.3300,  8.0000,  4.4500,  8.9000,  8.0000,  11.5500,
    8.0000,  8.0000,  8.0000,  5.3500,  4.1500,  5.3500,  9.3500,  12.0000
);

sub _estimateStringWidth {
    my ( $fontSize, $string ) = @_;

    my $width = 0;
    foreach my $char ( split( m//, $string ) ) {
        my $ch = ord $char;
        if ( $ch >= 32 && $ch <= 127 ) {
            $width += $widest[ $ch - 32 ] * $fontSize / 12;
        }
    }
    return $width;
}

sub _print;
sub _printf;
sub getHtml;
sub error;
sub errorCode;
sub _decodefont;
sub _encode_value;
sub close;
sub DESTROY;

# Special table for format strings - doesn't match @aColor in ParseExcel
# Found at http://dmcritchie.mvps.org/excel/colors.htm

my %formatColors = (
    '[Black]'   => 1,
    '[White]'   => 2,
    '[Red]'     => 3,
    '[Green]'   => 4,
    '[Blue]'    => 5,
    '[Yellow]'  => 6,
    '[Magenta]' => 7,
    '[Cyan]'    => 8,
);

# The order here is used in documentation
our @validParams = (
    qw(Password file sheets sheet opento emptyok title gridlines gridlabels mincols minrows percellcss lineheight cgi id base)
);

my $id = 0;

my %validParams = map { $_ => 1 } @validParams;

=pod

 my $xls = renderFile( file => xlsfile,  # File name,  handle, in-memory scalar
                     Password => 'secret', # Low security XLS password (optional)
                     cgi => cgi,       # CGI object (optional)
                     emptyok => 1      # No message if worksheet is empty
                     gridlines => 0/1    # Print gridlines
                     gridlabels => 0/1   # Print grid labels
                     sheets => name or list # Render specified sheet(s)
                     percellcss => 1     # Generate a unique class for each cell
                     opento => tab name or number # To open in rendering.  Undef uses WB.
                     id => string       # Unique id for each file on a page.  Default 'ExcelToHtml-1'
                     lineheight => float # line height (in em) used in CSS
                     title => string  # Add a title div above the table
                     minrows => #     # Minimum rows to display
                     mincols => #     # Minimum columns to display
                     base => string   # Url base for relative hyperlinks %REL% is replaced with link

0/1 items default if not present:
  gridlines:    print if page setup specifies printers should include gridlines
  gridlabels:   print if page setup specifies printers should include grid labels

if( $xls->errorCode ) {
   print $xls->error (or test $xls->errorCode)
} else {
  print $xls->getHtml
}

Codes:
  0 - no error detected
  1 - unable to open file
  2 - No Excel data in file (corrupt, unsupported version, or not an Excel file)
  3 - File is encrypted
100 - unable to initialize XLS reader
101 - File opened, but contains no worksheets
102 - Specified worksheet is not in workbook

$status = $xls->getStatus( 'sheetname' ) # Optional
returns detailed status of rendering for named sheet. If no name provided, list of all.
$status-> {
    name -> sheetname,
    firstRow First row rendered (1-based)
    firstCol
    lastRow
    lastCol
    status first unsupported feature, warning or error encountered String starts with 'OK:' if nothing unexpected happened.
}

ExcelToHtml->cssdoc - return css class documentation as a hash

$xls->close
  Does not close file, but does cleanup memory.

=cut

sub renderFile {
    my $class = shift;

    my $this;

    $this = {@_};

    my $log = delete $this->{_debug};
    if ($debug) {
        $log = \*STDERR unless ( ref $log );

        require Data::Dumper;

        no warnings 'once';
        local $Data::Dumper::Sortkeys = 1;
        print $log (
            '',
            ( scalar localtime ),
            qq([$$]: ExcelToHtml called with\n),
            Data::Dumper::Dumper(
                [ map { ref($_) =~ /GLOB|Fh/ ? 'GLOB' : $_ } @_ ]
            )
        );

        croak("Invalid argument list") if ( @_ % 2 );

        my $file = $this->{file};
        croak("No input specified") unless ( defined $file );

        my $type = ref $file;
        if ( ref($type) eq "SCALAR" ) {
            $type = 'scalar variable';
        }
        elsif ( ( ref($type) =~ /GLOB/ ) || ( ref($type) eq 'Fh' ) ) {
            $type = 'file handle ';
        }
        elsif ( ref($type) eq 'ARRAY' ) {
            $type = 'internal stream';
        }
        else {
            $type = qq(file: $file);
        }
        print $log (qq([$$]: Input is from $type\n));
    }
    else {
        croak("Invalid argument list") if ( @_ % 2 );
    }

    croak('No file specified') unless ( defined $this->{file} );

    {
        my $context = delete $this->{context};

        foreach my $par ( keys %$this ) {
            unless ( $validParams{$par} ) {
                croak(qq(Unrecognized parameter $par));
            }
        }

        if ( ref $context eq 'ExcelToHtml::Context' ) {
            $this->{_context} = $context;
        }
        elsif ($context) {
            croak('Invalid context handle');
        }
        else {
            $this->{_context} = ExcelToHtml::Context->new( extracss => {} );
        }

        $this->{_debug} = $log if ( defined $log );
    }

    $this->{_html} = '';
    $this->{_error} = [ 0, 'No errors detected' ];

    $this->{id} ||= sprintf( 'ExcelToHtml-%u', ++$id );

    # Remove convenience values that mean 'not specified'

    foreach my $par (qw/gridlabels gridlines height intital width/) {
        delete $this->{$par}
          if ( exists $this->{$par} && $this->{$par} eq 'auto' );
    }

    if ( my $sheet = delete $this->{sheet} ) {
        $this->{sheets} = $sheet;
    }

    eval {
        require Spreadsheet::ParseExcel;
        require Spreadsheet::ParseExcel::Utility;
    };
    if ($@) {
        return
          bless { _error => [ 100, "Failed to load Spreadsheet::ParseExcel" ] },
          $class;
    }

    my $q;
    if ( exists $this->{cgi} && $this->{cgi}->isa('CGI') ) {
        $q = $this->{cgi};
    }
    else {
        require CGI;
        $q = CGI::->new;
    }

    my $parser = Spreadsheet::ParseExcel->new(
        ( exists $this->{Password} ? ( Password => $this->{Password} ) : () ) );
    unless ($parser) {
        return
          bless {
            _error => [ 100, "Failed to initialize Spreadsheet::ParseExcel" ] },
          $class;
    }

    bless $this, $class;

    my $workbook;
    if ( ref $this->{file} eq 'SCALAR' ) {
        open( my $file, '<', $this->{file} )
          or return bless { _error => [ 1, "Unable to open stream: $!" ] },
          $class;
        binmode $file;
        $workbook = $parser->parse($file);

        CORE::close($file);
    }
    else {
        $workbook = $parser->parse( $this->{file} );
    }

    unless ($workbook) {
        $this->{_error} = [ $parser->error_code, $parser->error ];
        return $this;
    }

    if ($debug) {
        print $log (
            "[$$]: Worksheets: ",
            join(
                ', ',
                map {
                    sprintf( "(%u) %s",
                        $_->sheet_num + 1,
                        $this->_get_name($_) )
                  } $workbook->worksheets
            ),
            "\n"
        );
        my @hidden = grep { $_->is_sheet_hidden } $workbook->worksheets;
        print $log (
            "[$$]: Hidden worksheets: ",
            join( ', ', map { $_->sheet_num + 1 } @hidden ), "\n"
        ) if (@hidden);
    }

    my @worksheets = grep { !$_->is_sheet_hidden } $workbook->worksheets;
    unless (@worksheets) {
        $this->{_error} = [ 101, "File contains no (visible) worksheets" ];
        return $this;
    }

    # If sheet list specified, validate and create list

    if ( $this->{sheets} ) {
        my @sheets =
          ( ref $this->{sheets} )
          ? @{ $this->{sheets} }
          : split( /\s*,\s*/, $this->{sheets} );

        # Find specified worksheets by name or by number
        #  Can't be hidden.
        # $workbook->worksheet almost works, but its numbers are 0-based

        my %selected;
      SHEET:
        foreach my $sheet (@sheets) {
            foreach my $wsheet (@worksheets) {
                if ( $this->_get_name($wsheet) eq $sheet ) {
                    $selected{ $wsheet->sheet_num } = $wsheet;
                    next SHEET;
                }
            }
            if (   $sheet =~ /^\d+$/
                && $sheet
                && defined( my $wsheet = $workbook->worksheet( $sheet - 1 ) ) )
            {
                unless ( $wsheet->is_sheet_hidden ) {
                    $selected{ $wsheet->sheet_num } = $wsheet;
                    next SHEET;
                }
                $this->{_error} = [ 102, qq(Worksheet "$sheet" is hidden) ];
                return $this;
            }
            $this->{_error} =
              [ 102, qq(Worksheet "$sheet" is not in workbook) ];
            return $this;
        }
        unless (%selected) {
            $this->{_error} =
              [ 101, qq(No specified worksheets exist in workbook) ];
            return $this;
        }
        @worksheets = map { $selected{$_} } sort keys %selected;
    }

    # Decide which tab is initially open and convert to 0-based index.

    my $iTab = (
        defined $this->{opento}
        ? $this->{opento}
        : $workbook->get_active_sheet + 1 || 1
    );

    foreach my $wsheet (@worksheets) {
        if ( $this->_get_name($wsheet) eq $iTab ) {
            $iTab = $wsheet;
            last;
        }
    }
    if ( ref $iTab ) {
        $iTab = $iTab->sheet_num;
    }
    else {
        my $wsheet;
        if (   $iTab =~ /^\d+$/
            && $iTab
            && defined( $wsheet = $workbook->worksheet( $iTab - 1 ) )
            && !$wsheet->is_sheet_hidden )
        {
            $iTab = $wsheet->sheet_num;
        }
        else {
            if ( defined $wsheet && $wsheet->is_sheet_hidden ) {
                $this->{_error} = [ 102, qq(Worksheet "$iTab" is hidden) ];
            }
            else {
                $this->{_error} =
                  [ 102, qq(Worksheet "$iTab" is not in workbook) ];
            }
            return $this;
        }
    }
    {
        my $invisible = 0;
        foreach my $wsheet ( $workbook->worksheets ) {
            last if ( $wsheet->sheet_num >= $iTab );
            $invisible++ if ( $wsheet->is_sheet_hidden );
        }
        $iTab -= $invisible;
    }

    # Build list of (visible & selected) tabs

    my $id = $this->{id};

    my $tabs = '';
    my $n    = 0;

    foreach my $wks (@worksheets) {
        my $wsname = $this->_get_name($wks);

        my $color = $wks->get_tab_color;
        if ( defined $color ) {
            $color = lc( $workbook->color_idx_to_rgb($color) );
            $color = sprintf(
qq( ssTabColor="#%s" style="background-color:#%s;border-bottom-color:#%s;border-left-color:#%s;border-right-color:#%s;"),
                $color, $color, $color, $color, $color );
        }
        else {
            $color = '';
        }
        $n++;
        $tabs .= qq(<li$color><a href="#$id-$n">$wsname</a></li>);
    }
    $tabs .=
qq(<li class="ssRemaining" title="Click here to make grid labels appear or disappear">Labels</li>);

    my $dim = '';
    $dim .= sprintf( qq(ssUserWidth="%s" width="%s" ),
        $this->{width}, $this->{width} )
      if ( $this->{width} );

    $dim .= sprintf( qq(ssUserHeight="%s height="%s"" ),
        $this->{height}, $this->{height} )
      if ( $this->{height} );

    $this->_print(qq(<div ${dim}class="ssWrapper">));
    $this->_print(qq(<div class="ssTitle">$this->{title}</div>))
      if ( defined $this->{title} );
    $this->_print( qq(<div ssInitialTab="$iTab" class="ssWorkbook"><ul>),
        $tabs, qq(</ul>) );
    undef $tabs;

    # Find cells in this workbook that are hyperlink targets

    my %linkTargets;

    foreach my $wks (@worksheets) {
        next unless ( $wks->{HyperLinks} );

        foreach my $link ( @{ $wks->{HyperLinks} } ) {
            for ( my $row = $link->[3] ; $row <= $link->[4] ; $row++ ) {
                for ( my $col = $link->[5] ; $col <= $link->[6] ; $col++ ) {
                    my $target = $link->[1];
                    if ( $target =~ s/^#// ) {
                        $target =~
                          s/%([[:xdigit:]]{2})/chr( oct( '0x' . $1 ) )/gmse;
                        $linkTargets{ $id . '-' . $target } = 1;
                    }
                }
            }
        }
    }

    # Generate each worksheet
    #
    # Trim empty/hidden rows

    $n = 0;
    foreach my $wks (@worksheets) {
        my $wsname = $this->_get_name($wks);

        $n++;
        $this->_print(qq(<div id="$id-$n">));

        my ( $firstRow, $lastRow ) = $wks->row_range;
        my ( $firstCol, $lastCol ) = $wks->col_range;

        $this->{_wsp}{$wsname} = {
            name     => $wsname,
            number   => $n,
            firstRow => $firstRow + 1,
            lastRow  => $lastRow + 1,
            firstCol => $firstCol + 1,
            lastCol  => $lastCol + 1,
            status   => '',
        };
        my $maxCol = $lastCol;
      ROW:

        for ( my $row = $lastRow ; $row >= $firstRow ; $row-- ) {
            next if ( $wks->is_row_hidden($row) );

            for ( my $col = $firstCol ; $col <= $lastCol ; $col++ ) {
                next if ( $wks->is_col_hidden($col) );

                my $cell = $wks->get_cell( $row, $col );

                if ( defined $cell ) {
                    my $value = $cell->value;
                    my $format;

                    if (
                        defined $value
                        && (
                            $value ne ''
                            || (
                                ( $format = $cell->get_format )
                                && (
                                    (
                                        $format->{BdrStyle}
                                        && grep { $_ } # Exclude 0 (none) and undef (unspec)
                                        @{ $format->{BdrStyle} }[ 0 .. 3 ]
                                    )
                                    || $format->{Fill} && $format->{Fill}[0]
                                    || $format->{BdrDiag}
                                    && $format->{BdrDiag}[0]
                                )
                            )
                        )
                      )
                    {
                        $lastRow = $row;
                        last ROW;
                    }
                }
            }
            $lastRow--;
        }
        my $mergedAreas = $wks->get_merged_areas || [];

        foreach my $mergedArea (@$mergedAreas) {
            if (   $mergedArea->[0] <= $lastRow
                && $mergedArea->[2] > $lastRow
                && $mergedArea->[1] <= $lastCol )
            {
                $lastRow = $mergedArea->[2];
            }
        }

        if ( $lastRow < $firstRow ) {

            # Must output empty table for navigation
            $this->_print(
qq(<table class="spreadsheet ssEmpty" summary="Empty worksheet $wsname"><tr><td>&nbsp;</table>)
            );
            $this->_print(
qq(<div class="infotext"><span class="infotext">Worksheet is empty</span></div>)
            ) unless ( $this->{emptyok} );
            $this->_print(qq(\n<!-- End of $wsname (empty) -->)) if ($debug);
            $this->_print(q(</div>));
            $this->{_wsp}{$wsname}{status} = 'Empty, no data found';
            next;
        }

        if ( defined $this->{minrows} && $lastRow < $this->{minrows} - 1 ) {
            $lastRow = $this->{minrows} - 1;
        }

        # Setup formatting globals

        my $rowHdrClass = sprintf( q(ssRowHeader%u), length("$lastRow") );

        my ( $defRowHeight, $defColWidth ) =
          ( $wks->get_default_row_height, $wks->get_default_col_width );
        my ( $stdRowHeight, $stdColWidth ) =
          ( 12.75, 8.43 );    # chars of default font

        my $stdColWidthP = $this->_widthToPixels( $stdColWidth, $stdColWidth );
        my $stdRowHeightP =
          $this->_heightToPixels( $stdRowHeight, $stdRowHeight );

        my @rowHeights = $wks->get_row_heights;
        my @colWidths  = $wks->get_col_widths;

        # lastCol doesn't account for overhanging cell data, sigh.
        # This may add more (or fewer) cols than needed due to font size
        # approximation and hidden columns..

        for ( my $row = $firstRow ; $row <= $lastRow ; $row++ ) {
            for ( my $col = $lastCol ; $col >= $firstCol ; $col-- ) {
                my ( $cell, $value );

                if (   defined( $cell = $wks->get_cell( $row, $col ) )
                    && defined( $value = $cell->value )
                    && $value ne '' )
                {
                    my $fontSize = 10;
                    if ( ( my $format = $cell->get_format ) ) {
                        last if ( $format->{Wrap} );

                        if ( $format->{Font} ) {
                            $fontSize = $format->{Font}{Height} || 10;
                        }
                    }
                    my $width = _estimateStringWidth( $fontSize, $value );

                    # Allow some margin.
                    $width += 32;

                    # Oddly enough, unallocated columns may have width.
                    my $avail =
                      $this->_widthToPixels( $stdColWidth,
                        @colWidths[ $col .. $lastCol ] );
                    while (1) {
                        last if ( $width <= $avail );

                        $avail +=
                          $this->_widthToPixels( $stdColWidth,
                            $colWidths[ ++$lastCol ] );
                    }
                    last;
                }
            }
        }

        if ( defined $this->{mincols} && $lastCol < $this->{mincols} - 1 ) {
            $lastCol = $this->{mincols} - 1;
        }

        # Column styles & Formats

        my @colStyles;

        for ( my $col = 0 ; $col <= $lastCol ; $col++ ) {

            my $cWidth = $colWidths[$col];
            $cWidth = $stdColWidth unless ( defined $cWidth );
            my $cWidthP = $this->_widthToPixels( $stdColWidth, $cWidth );

            if ( $cWidthP == $stdColWidthP ) {
                push @colStyles, '';
            }
            else
            { # Convert chars (Arial 10pt) to px - see _convert_col_width() in ParseExcel.pm
                push @colStyles,
                  sprintf( "width:%spx;max-width:%spx;", $cWidthP, $cWidthP );
            }

            next if ( $wks->is_col_hidden($col) );

            # Apply any column format, materializing as required

            my $colFmtNo = $wks->{ColFmtNo}[$col];

            if ( defined $colFmtNo
                && ( my $colFmt = $workbook->{Format}[$colFmtNo] ) )
            {
                for ( my $row = 0 ; $row <= $lastRow ; $row++ ) {
                    my $cell = $wks->get_cell( $row, $col );
                    unless ($cell) {
                        $cell = Spreadsheet::ParseExcel::Cell->new(
                            Val      => '',
                            FormatNo => $colFmtNo,
                            Code     => undef,
                            Format   => $colFmt,
                            Type     => 'Text',
                            _Kind    => 'BLANK',
                            _Value   => '',
                        );
                        $wks->{Cells}[$row][$col] = $cell;
                        next;
                    }
                    my $format = $cell->get_format;
                    unless ($format) {
                        $cell->{Format} = $colFmt;
                        next;
                    }

                    # Merging is not required; cells with any format have
                    # all formatting.
                }
            }
        }

        # Start table

        my $pHdrs =
          exists $this->{gridlabels}
          ? $this->{gridlabels}
          : $wks->is_print_row_col_headers;

        $this->_print(
            $q->start_table(
                {
                    class => (
                        (
                            exists $this->{gridlines}
                            ? $this->{gridlines}
                            : $wks->is_print_gridlines
                        ) ? "ssWithGrid" : "spreadsheet"
                      )
                      . ( $pHdrs ? ' ssWithLabels' : '' ),
                    summary =>
qq(This table is the contents of the "$wsname" worksheet.),
                }
            ),
        );

        # Generate grid label row

        $this->_print(
            $q->start_thead(
                {
                    -title =>
"Click here to make row and column labels disappear.  Click tab bar to restore.&#10;Click anywhere on the data to make the gridlines appear or disappear."
                },
            ),
            $q->start_Tr,
            $q->td(
                {
                    -class => "ssColHeader ssRowHeader $rowHdrClass",
                    -title => "Last saved by: "
                      . (
                        $workbook->{Author} ? $workbook->{Author} : 'Unknown' )
                },
                '&middot;'
            )
        );

        for ( my $col = 0 ; $col <= $lastCol ; $col++ ) {
            next if ( $wks->is_col_hidden($col) );

            $this->_print(
                $q->td(
                    {
                        -class => 'ssColHeader',
                        (
                            $colStyles[$col] ? ( -style => $colStyles[$col] )
                            : ()
                        )
                    },
                    $q->div(
                        {
                            -class => 'ssCell',
                            (
                                $colStyles[$col]
                                ? ( -style => $colStyles[$col] )
                                : ()
                            )
                        },
                        _base26($col)
                    ),
                )
            );
        }

        # Generate the main table

        $this->_print( $q->end_thead, $q->start_tbody );

        my $firstVisibleRow;
        for ( my $row = 0 ; $row <= $lastRow ; $row++ ) {
            next if ( $wks->is_row_hidden($row) );
            $firstVisibleRow = $row unless ( defined $firstVisibleRow );

            my $tr = sprintf( qq(<tr class="%s"),
                ( $row & 1 ) ? 'ssOddRow' : 'ssEvenRow' );

            my $rowHeight = $rowHeights[$row];
            my $rowHeightP =
              $this->_heightToPixels( $stdRowHeight, $rowHeight );

            if ( $rowHeightP == $stdRowHeightP ) {
                $this->_print("$tr>");
            }
            else {
                $this->_printf( qq(%s style="height:%spx;">), $tr,
                    $rowHeightP );
            }

            $this->_print(
                $q->td(
                    { -class => "ssRowHeader $rowHdrClass", },
                    $q->div( { class => 'ssCell', }, $row + 1 )
                )
            );

            # Data columns

          COL:
            for ( my $col = 0 ; $col <= $lastCol ; $col++ ) {
                next if ( $wks->is_col_hidden($col) );

                my $cell = $wks->get_cell( $row, $col );
                my $format;

                my ( $cWidth, $cStyle ) =
                  ( $colWidths[$col], $colStyles[$col] );
                my $cWidthP = $this->_widthToPixels( $stdColWidth, $cWidth );

                my $span =
                  sprintf( qq(title="%s" ), _cellref( undef, $row, $col ) );
                if ( $cell && $cell->is_merged ) {
                    foreach my $mergeArea (@$mergedAreas) {    # RS CS RE CE
                        next
                          unless ( $row >= $mergeArea->[0]
                            && $row <= $mergeArea->[2]
                            && $col >= $mergeArea->[1]
                            && $col <= $mergeArea->[3] );

                        if (   $row == $mergeArea->[0]
                            && $col == $mergeArea->[1] )
                        {                                      # Top left cell
                            my $wid = 1 + $mergeArea->[3] - $mergeArea->[1];
                            my $h   = 1 + $mergeArea->[2] - $mergeArea->[0];
                            $span .= sprintf( qq(colspan="%u" ), $wid )
                              if ( $wid > 1 );
                            $span .= sprintf( qq(rowspan="%u" ), $h )
                              if ( $h > 1 );

                            my $endCell =
                              $wks->get_cell( $mergeArea->[2],
                                $mergeArea->[3] );    # Bottom right cell
                            if (   defined $endCell
                                && defined( $format = $cell->get_format )
                                && defined( my $endFormat =
                                      $endCell->get_format ) )
                            {

                                $format->{BdrColor} =
                                  [ @{ $format->{BdrColor} } ];
                                $format->{BdrStyle} =
                                  [ @{ $format->{BdrStyle} } ];
                                @{ $format->{BdrColor} }[ 1,      3 ] =
                                  @{ $endFormat->{BdrColor} }[ 1, 3 ]
                                  ;    # Right, bottom
                                @{ $format->{BdrStyle} }[ 1,      3 ] =
                                  @{ $endFormat->{BdrStyle} }[ 1, 3 ];
                            }

                            my $cWidthP =
                              $this->_widthToPixels( $stdColWidth,
                                @colWidths[ $col .. $col + $wid - 1 ] );

                            if (
                                $cWidthP == $stdColWidthP

                              )
                            {
                                $cStyle = '';
                            }
                            else {
                                $cStyle = sprintf( "width:%spx;max-width:%spx;",
                                    $cWidthP, $cWidthP );
                            }
                            my $rHeightP =
                              $this->_heightToPixels( $stdRowHeight,
                                @rowHeights[ $row .. $row + $h - 1 ] );
                            if ( $rHeightP != $stdRowHeightP ) {
                                $cStyle .= sprintf( "height:%spx;", $rHeightP );
                            }
                            last;
                        }
                        next COL;
                    }
                }
                $this->_print(qq(<td ${span}class="ssCell));
                $this->_print( q( ssId), _cellref( $wsname, $row, $col ) )
                  if ( $this->{percellcss} );

                my $anchor = $id . '-' . _cellref( $wsname, $row, $col, 1 );
                if ( $linkTargets{$anchor} ) {
                    $anchor =
                      sprintf( q(<a name="%s" id="%s"></a>), $anchor, $anchor );
                }
                else {
                    $anchor = '';
                }

                unless ( defined $cell ) {

                    # Cell doesn't exist, not even formatting
                    # Force the width for row 0 so table doesn't shrink
                    # when the grid labels are hidden.

                    $this->_print(q( ssEmptyCell"));
                    $this->_printf( qq( style="width:%spx;max-width:%spx;"),
                        $cWidthP, $cWidthP )
                      if ( $row == $firstVisibleRow );
                    $this->_printf( qq(>%s), $anchor );

                    next;
                }
                my $value = $cell->value;
                unless ( defined $value && $value ne '' ) {    # Exists
                    $this->_print(qq( ssEmptyCell));
                }
                $value = '' unless ( defined $value );

                $format = $cell->get_format unless ($format);
                my $x = $format->{AlignH};
                $this->_print(
                    {

##                      0 => ' ssAlignNone', # Default
                        1 => ' ssAlignLeft',
                        2 => ' ssAlignCenter',
                        3 => ' ssAlignRight',
                        4 => ' ssAlignFill',
                        5 => ' ssAlignJustify',
                        6 => ' ssAlignCenterAcross',
                        7 => ' ssAlignEqualSpace',
                    }->{ $x || '' }
                      || ''
                );

                my $vAlign = $format->{AlignV};
                $vAlign = 2 if ( !defined $vAlign );
                $this->_print(
                    {
                        0 => ' ssAlignTop',
                        1 => ' ssAlignMiddle',

##                      2 => ' ssAlignBottom',  # Default
                        3 => ' ssAlignVJustify',
                        4 => ' ssAlignVEqualSpace',
                    }->{$vAlign}
                      || ''
                );

                $this->_print(' ssJustLast')   if ( $format->{JustLast} );
                $this->_print(' ssCellLocked') if ( $format->{Locked} );
                $this->_print(' ssCellHidden') if ( $format->{Hidden} );
                $this->_print(
                    {

##                      0 => ' ssCellDirection', # Context
                        1 => ' ssCellDirectionLtR',
                        2 => ' ssCellDirectionRtL',
                    }->{ $format->{ReadDir} || '' }
                      || ''
                );

                my $cellType = $cell->type() || 'Text';
                $this->_print(qq( ssType$cellType))
                  unless ( $cellType eq 'Text' );

                $x = $format->{Indent} || 0;
                $this->_print(qq( ssIndent$x)) if ($x);

                if ( $format->{Wrap} ) {
                    $this->_print(qq( ssWrappedText));
                }
                else {
                    my ( $nextCell, $nextValue );
                    if (
                           $col < $lastCol
                        && $value ne ''
                        && (   !( $nextCell = $wks->get_cell( $row, $col + 1 ) )
                            || !defined( $nextValue = $nextCell->value )
                            || $nextValue eq '' )
                      )
                    {
                        $this->_print(qq( ssOverflowOK));
                    }
                }

                $this->_print(qq( ssShrinkToFit)) if ( $format->{Shrink} );

                $x = $format->{BdrStyle};
                if ($x) {
                    my @names = (qw/Left Right Top Bottom/);
                    for ( my $s = 0 ; $s < @names ; $s++ ) {
                        my $type = {
                            0  => '',
                            1  => 'Thin',
                            2  => 'Medium',
                            3  => 'Dashed',
                            4  => 'Dotted',
                            5  => 'Thick',
                            6  => 'Double',
                            7  => 'Hairline',
                            8  => 'MediumDashed',
                            9  => 'DashDot',
                            10 => 'MediumDashDot',
                            11 => 'DashDotDot',
                            12 => 'MediumDashDotDot',
                            13 => 'SlantedDashDot',
                          }->{ $x->[$s] || '' }
                          || '';
                        $this->_printf( q( ssBorder%s%s), $names[$s], $type )
                          if ($type);
                    }
                }
                my $rowStyles = '';
                $x = $format->{BdrColor};
                if ($x) {
                    my @names = (qw/left right top bottom/);
                    for ( my $s = 0 ; $s < @names ; $s++ ) {
                        my $color = $x->[$s];
                        if ( defined $color ) {
                            $color =
                              (      $color == 8
                                  || $color ==
                                  Spreadsheet::ParseExcel::AutoColor() )
                              ? '000000'
                              :    # Bug in ParseExcel for black & auto
                              lc( $workbook->color_idx_to_rgb($color) );
                            $rowStyles .= sprintf( q(border-%s-color:#%s;),
                                $names[$s], $color )
                              if ( $color !~ /^0+$/ );
                        }
                    }
                }

                $x = $format->{Fill};
                if ($x) {          # [ Pattern #, Pattern FG, Pattern BG
                    if ( $x->[0] == 1 ) {    # Solid fill (Others more complex)
                        my $color = $x->[1];    # Pattern FG
                        if ( defined $color ) {
                            if ( $color == 8 ) {    # Black
                                $color = '000000';
                            }
                            elsif (
                                $color == Spreadsheet::ParseExcel::AutoColor() )
                            {                       # auto
                                undef $color;
                            }
                            else {
                                $color =
                                  lc( $workbook->color_idx_to_rgb($color) );
                            }
                            $rowStyles .=
                              sprintf( q(background-color:#%s;), $color )
                              if ( defined $color );
                        }
                    }
                }

                # Background diagonal lines - need a stretchable
                # image in all colors.
                #    BdrDiag  [k, s, color] k: 0 none, 1 r-dn, 2,
                # r-up, 3 both

                $this->{_wsp}{$wsname}{status} ||=
                  'Diagonal line(s) used in worksheet but not rendered'
                  if ( $format->{BdrDiag}[0] );

                # Style ??

                # Wrap text for truncation/wrap & rotation
                my $div = qq($anchor<div class="ssCell);

                if ( ( my $rotation = $format->{Rotate} ) ) {

                    # Rotation is applied to table cells.  The origin is
                    # established and may be offset to account for
                    # alignment. There are too many (180) rotations x 3
                    # alignments x sizes to include these in the static CSS.
                    # Only
                    # those used will be generated in-line.  Doesn't make
                    # sense to enable user styling of rotations..
                    # Class name is ssRotate . sign . degrees - alignment
                    # ssRotateUp and ssRotateDown are also added for the CSS

                    my $lines = ( $value =~ tr/\n// ) + 1;

                    my $class = sprintf( q(ssRotate%d-%d-%s-%s-%u),
                        $rotation, $vAlign, $cWidthP, $rowHeightP, $lines );
                    $class =~ s/\./_/gms;
                    my $rule = qq(TD.ssCell DIV.ssCell.$class);
                    my $direction = ( '', 'Up', 'Down' )[ $rotation <=> 0 ];

                    unless ( exists $this->{_context}{extracss}{$rule} ) {
#<<<
                        # Generate custom CSS for this rotation

                        # hash key is {AlignV} code / rotation direction
                        my $params = {
                                      # Top
                                      qq(0/Up)   => [ 'left top', 'translatey(%hpx)', 'translatex(-%xem)' ],
                                      qq(0/Down) => [ 'left top', 'translatex(%lem)', 'translatey(%x%)' ],

                                      # Middle
                                      qq(1/Up)   => [ '50% 50%', '', '' ],
                                      qq(1/Down) => [ '50% 50%', '', '' ],

                                      # Bottom
                                      qq(2/Up)   => [ 'left top', 'translatey(100%)', 'translate(%xem,-%y%)' ],
                                      qq(2/Down) => [ 'left bottom', 'translate(-%xem,-%hpx)', '' ],

                                      # VJustify (treat as middle)
                                      qq(3/Up)   => [ '50% 50%', '', '' ],
                                      qq(3/Down) => [ '50% 50%', '', '' ],

                                      # VEqualSpace (treat as middle)
                                      qq(4/Up)   => [ '50% 50%', '', '' ],
                                      qq(4/Down) => [ '50% 50%', '', '' ],

                                      # Temporary (?) too hard to compute middle, make it bottom for now
                                      qq(1/Up)   => [ 'left top', 'translatey(100%)', 'translate(%xem,-%y%)' ],
                                      qq(1/Down) => [ 'left bottom', 'translate(-%xem,-%hpx) translatey(-100%)', '' ],
                                      qq(3/Up)   => [ 'left top', 'translatey(100%)', 'translate(%xem,-%y%)' ],
                                      qq(3/Down) => [ 'left bottom', 'translate(-%xem,-%hpx) translatey(-100%)', '' ],
                                      qq(4/Up)   => [ 'left top', 'translatey(100%)', 'translate(%xem,-%y%)' ],
                                      qq(4/Down) => [ 'left bottom', 'translate(-%xem,-%hpx) translatey(-100%)', '' ],

                                     }->{qq($vAlign/$direction)} || [];
#>>>
                        my ( $origin, $before, $after, $genprop ) = @$params;

                        # Apply width to DIV using CSS.

                        $genprop = qq(overflow:visible;) .    # Hack
                          q(width:%dpx;\nmax-width:%dpx;)
                          unless ( defined $genprop );

                        $genprop = qq(width:%dpx;\nmax-width:%dpx;)
                          unless ( defined $genprop );

                        my $absR = abs($rotation);
                        my $cosR = sprintf( "%.4f", cos( $PI_180 * $absR ) );
                        my $x    = $lines * $lineHeight * $cosR;
                        my $y    = 100 * $cosR;

                        my ( $Rwid, $cosRwid, $d );
                        if ( $absR > 45 ) {
                            $Rwid = 90 - $absR;
                            $cosRwid =
                              sprintf( "%.4f", cos( $PI_180 * $Rwid ) );
                            $d =
                              ( $cosRwid != 0 )
                              ? $rowHeightP / $cosRwid
                              : $rowHeightP;

                            # 90 gets no added width, but we must use the
                            # Excel width if it's smaller

                            if ( $absR == 90 ) {
                                $d = (
                                      $rowHeightP < $cWidthP
                                    ? $rowHeightP
                                    : $cWidthP
                                );
                            }
                            $d = sprintf( "%.4f", $d );
                        }
                        else {
                            $Rwid = $absR;
                            $cosRwid =
                              sprintf( "%.4f", cos( $PI_180 * $Rwid ) );
                            $d =
                              ( $cosRwid != 0 )
                              ? $cWidthP / $cosRwid
                              : $cWidthP;
                        }

                        foreach my $clause ( $before, $after, $genprop ) {
                            $clause = '' unless ( defined $clause );

                            # Lines of text (x 1.5em ~~height)
                            $clause =~ s/%l/$lines * $lineHeight/gmse;

                            # x offset (em) due to rotation
                            $clause =~ s/%x/${x}/gms;

                            # y offset (%) due to rotation
                            $clause =~ s/%y/$y/gms;

                            # cell width (px)
                            $clause =~ s/%w/$cWidthP/gms;

                            # cell height (px)
                            $clause =~ s/%h/$rowHeightP/gms;

                            # width of text, increased by rotation
                            # Doesn't acct for reduction with mult lines
                            $clause =~ s/%d/$d/gms;
                        }

                        my $css = << 'CSS';
-moz-transform: $transform;
-o-transform: $transform;
-ms-transform: $transform;
-webkit-transform: $transform;
transform: $transform;
-moz-transform-origin: $origin;
-o-transform-origin: $origin;
-webkit-transform-origin: $origin;
-ms-transform-origin: $origin;
transform-origin: $origin;
CSS
                        $css =~ s/\$transform/join( ' ',
                                                    grep { $_ } (
                                                                 $before,
                                                                 sprintf( 'rotate(%ddeg)',
                                                                          -$rotation ),
                                                                 $after ) )/gmsex;
                        $css =~ s/\$origin/$origin/gms;
                        $css .= qq($genprop\n) if ($genprop);

                        # peephole optimizations for transforms:
                        # o Convert -0 to 0
                        # o Remove null translations
                        # o convert translate( 0, * ) to translatey
                        # o convert translate( *, 0 ) to translatex
                        # o Remove default transform-orgin

                        $css =~ s/-0/0/gms;
                        $css =~
                          s/\s+translate[xy]\s*\(\s*0(?:em|px|%)\s*\)//gms;
                        $css =~
s/\s+translate\s*\(\s*0(?:em|px|%),\s*0(?:em|px|%)\s*\)//gms;
                        $css =~
s/\s+translate\s*\(\s*0(?:em|px|%),\s*([^)]*)\)/ translatey($1)/gms;
                        $css =~
s/\s+translate\s*\(\s*([^,]*),\s*0(?:em|px|%)/ translatex($1)/gms;
                        $css =~
s/^\s*(?:-\w+-)?transform-origin:\s*(?:50\%|center)\s+(?:50\%|center)\s*;\s*\n//gms;

                        # Install new rule
                        $this->{_context}{extracss}{$rule} = $css;
                    }
                    $div .= qq( $class);
                    $div .= sprintf( qq( ssRotate%s), $direction )
                      if ($direction);
                }
                $div .= qq(");    # Close div class="

                # Close TD with decodefont

                $this->_decodefont( $workbook, $format->{Font},
                    $cStyle . $rowStyles );

                $this->_print($div);

                # Suppress DIV STYLE= width and height when rotated
                # since the CSS has set them (and they don't match
                # the TD size.  Height on div is problematic too.
                # (it breaks centering..exactly why tbd.)
                # WARNING: if other styling is ever added to $cStyle,
                # this needs to be more careful.

                if ( $cStyle && !$format->{Rotate} ) {
                    $cStyle =~ s/height:\s*\d+px;//;
                    $this->_print(qq( style="$cStyle"))
                      if ($cStyle);
                }
                $this->_print('>');

                my $hyperlink = $cell->get_hyperlink;
                if ($hyperlink) {
                    my $href = $hyperlink->[1];

                    # Qualify cell refs with ID.
                    $href =~ s/^#/#$id-/;

                    #  Apply base to relative file: URIs.
                    if ( $this->{base} ) {
                        if ( $href =~ s/^.*?%REL%// ) {
                            my $url = $this->{base};
                            $url =~ s/%REL%/$href/;
                            $href = $url;
                        }
                    }
                    $this->_printf( q(<a href="%s"), $href );
                    $this->_printf( q( target="%s"), $hyperlink->[2] )
                      if ( defined $hyperlink->[2] );
                    $this->_print(q(>));
                }

                # Rich text block
                $x = $cell->get_rich_text;
                if ( $x && @$x ) {
                    my $begin =
                      _encode_value( substr( $value, 0, $x->[0][0] ) );
                    $this->_print( $q->span( { -class => 'ssText', }, $begin ) )
                      if ( length $begin );

                    for ( my $i = 0 ; $i < @$x ; $i++ ) {
                        my $str   = $x->[$i];
                        my $start = $str->[0];
                        my $text;
                        if ( $i == $#$x ) {
                            $text = substr( $value, $start );
                        }
                        else {
                            $text =
                              substr( $value, $start,
                                $x->[ $i + 1 ][0] - $start );
                        }
                        $text = _encode_value($text);

                        $this->_print(qq(<span class="ssText));
                        $this->_decodefont( $workbook, $str->[1] );

                        $this->_print( $text, q(</span>) );
                    }
                }
                elsif ($cellType =~ /^(Numeric|Date)$/
                    && $format->{FmtIdx} )
                {

                    # Formatted number or date
                    # 1 is 'want color, locale'
                    #
                    # Color numbers are 7 less than rgb_indices
                    # [Color1] - [Color56] are defined.

                    my $formatStr =
                      $workbook->{FmtClass}->FmtString( $cell, $workbook );

                    my ( $dval, $color, $locale ) =
                      Spreadsheet::ParseExcel::Utility::ExcelFmt( $formatStr,
                        $cell->unformatted, $workbook->using_1904_date,
                        $cell->{Type}, 1 );

                    $this->_printf( qq(<span class="ssType%s"), $cellType );

                    if ( defined $color && $color ne '' ) {
                        my $idx;
                        $idx = $formatColors{$color} + 7
                          if ( exists $formatColors{$color} );
                        unless ( defined $idx ) {
                            if ( $color =~ /^\[Color(\d+)\]$/ ) {
                                $idx = $1 + 7;
                            }
                        }
                        if ( defined $idx ) {
                            $color = lc( $workbook->color_idx_to_rgb($idx) );
                            $this->_printf( q( style="color:#%s;"), $color );
                        }
                    }

                    $this->_print(q(>));

                    $dval = _encode_value($dval);
                    $this->_print( $dval, q(</span>) );
                }
                else {
                    $value = _encode_value($value);
                    $this->_print( $q->span( { -class => 'ssText' }, $value ) );
                }
                $this->_print(q(</a>)) if ($hyperlink);
                $this->_print(qq(</div>));
            }
        }
        $this->_print( $q->end_table );
        $this->{_wsp}{$wsname}{status} ||= 'OK: Everything rendered';
        $this->_print(qq(\n<!-- End of $wsname -->)) if ($debug);
        $this->_print(q(</div>));
    }

    if ($debug) {
        $this->_print( qq(\n<!-- End of Workbook $id --></div>), );
    }
    else {
        $this->_print(qq(</div>));
    }
    if ( $this->{_context}{extracss} ) {
        $this->_print(qq(<style type="text/css">\n));
        foreach my $extra ( sort keys %{ $this->{_context}{extracss} } ) {

            # Output once, keep around in case another rendering needs.

            if ( $this->{_context}{extracss}{$extra} ) {
                $this->_printf( qq(%s {\n%s}\n),
                    $extra, $this->{_context}{extracss}{$extra} );
                $this->{_context}{extracss}{$extra} = '';
            }
        }
        $this->_print(qq(</style>\n));
    }
    if ($debug) {
        $this->_print(qq(\n<!-- End of Wrapper $id --></div>));
    }
    else {
        $this->_print(qq(</div>));
    }

    return $this;
}

sub _get_name {
    my $this = shift;

    my ($worksheet) = @_;

    my $name = $worksheet->get_name;
    return $name if ( defined $name && length $name );

    $name = sprintf( "Sheet%u", $worksheet->sheet_num );
    return $name;
}

sub _heightToPixels {
    my $this         = shift;
    my $stdRowHeight = shift;

    my $height = 0;
    foreach my $rh (@_) {
        my $rowHeight = $rh;
        $rowHeight = $stdRowHeight unless ( defined $rowHeight );
        $height += ( $rowHeight * $dpi ) / 72;
    }
    $height = sprintf( "%f", $height );
    $height =~ s/0*$//;
    $height =~ s/\.$//;

    return $height;
}

sub _widthToPixels {
    my $this        = shift;
    my $stdColWidth = shift;

    my $width = 0;
    foreach my $cw (@_) {
        my $cWidth = $cw;
        $cWidth = $stdColWidth unless ( defined $cWidth );
        $width += ( $cWidth <= 1.0 ) ? $cWidth * 12 : ( $cWidth * 7 ) + 5;
    }
    $width = sprintf( "%f", $width );
    $width =~ s/0*$//;
    $width =~ s/\.$//;

    return $width;
}

sub _print {
    my $this = shift;

    $this->{_html} .= join( '', @_ );
    return;
}

sub _printf {
    my $this = shift;
    my $fmt  = shift;

    $this->{_html} .= sprintf( $fmt, @_ );

    return;
}

sub getHtml {
    my $this = shift;

    my $parser = $this->{_parser};

    return \$this->{_html};
}

sub id {
    my $this = shift;

    return $this->{id};
}

sub debug {
    my $class = shift;

    $debug = $_[0] if (@_);
    return $debug;
}

sub getStatus {
    my $this = shift;
    my ($sheet) = @_;

    if ($sheet) {
        return $this->{_wsp}{$sheet};
    }
    return sort { $a->{number} <=> $b->{number} } values %{ $this->{_wsp} };
}

sub error {
    my $this = shift;

    return $this->{_error}[1];
}

sub errorCode {
    my $this = shift;

    return $this->{_error}[0];
}

sub _cellref {
    my ( $ws, $row, $col, $html ) = @_;

    return sprintf( q(%s%u), _base26($col), $row + 1 ) unless ($ws);
    return sprintf( q(%s!%s%u), $ws, _base26($col), $row + 1 ) if ($html);
    return sprintf( q(%s\\!%s%u), $ws, _base26($col), $row + 1 );
}

sub _base26 {
    my $col = 1 + shift;

    my $string = '';
    while ($col) {
        $string = substr( $base26, ( $col - 1 ) % 26, 1 ) . $string;
        $col = int( ( $col - 1 ) / 26 );
    }
    $string ||= 'A';
}

# Excel Font object
# Some element class=" is open
# Closed on return

sub _decodefont {
    my $this = shift;
    my ( $workbook, $font, $styles ) = @_;

    unless ($font) {
        $this->_print(qq(">));
        return;
    }

    $this->_print(' ssBold')      if ( $font->{Bold} );
    $this->_print(' ssItalic')    if ( $font->{Italic} );
    $this->_print(' ssStrikeout') if ( $font->{Strikeout} );

    if ( $font->{Underline} ) {
        $this->_print(
            {
                1  => ' ssUnderSingle',
                2  => ' ssUnderDouble',
                33 => ' ssUnderSingleAcct',
                34 => ' ssUnderDoubleAcct',
            }->{ $font->{UnderlineStyle} || '' }
              || ''
        );
    }
    $this->_print(
        {
            1 => ' ssAlignSuperscript',
            2 => ' ssAlignSubscript',
        }->{ $font->{Super} || '' }
          || ''
    );

    $this->_print(qq("));

    my $color = $font->{Color};
    if ( defined $color ) {
        if ( $color == 8 ) {    # Black
            undef $color;       # Default
        }
        elsif ( $color == Spreadsheet::ParseExcel::AutoColor() ) {    # Auto
            undef $color;
        }
        else {
            $color = $workbook->color_idx_to_rgb($color);
            undef $color if ( $color =~ /^0+$/ );                     # Black
        }
    }
    my $style = ( $styles || $color || $font->{Name} ) ? 1 : 0;
    if ($styles) {
        $this->_print(q( style=")) if ( $style == 1 );
        $style = 2;
        $this->_print($styles);
    }
    if ($color) {
        $this->_print(q( style=")) if ( $style == 1 );
        $style = 2;
        $this->_print(qq(color:#$color;));
    }
    if ( $font->{Name} && lc( $font->{Name} ) ne 'arial' ) {
        $this->_print(q( style=")) if ( $style == 1 );
        $style = 2;
        my $name = $font->{Name};
        $name = qq('$name') if ( $name =~ /\s+/ );
        $this->_print( q(font-family:), $name, ';' );
    }
    my $h = sprintf( "%f", $font->{Height} || 10 );
    if ( $h != 10 ) {
        $this->_print(q( style=")) if ( $style == 1 );
        $style = 2;
        $h =~ s/0*$//;
        $h =~ s/\.$//;
        $this->_print(qq(font-size:${h}pt;));
    }
    $this->_print(q(")) if ( $style == 2 );

    $this->_print('>');

    return;
}

# Encode a value from a spreadsheet so it's safe for HTML
#
# Support TWiki's usage by encoding %
#
# Also turn \n into <br />

sub _encode_value {
    my $result = HTML::Entities::encode_entities( $_[0] );

    $result =~ s/(\r*\n|\r)/<br \/>/gos;

    # Inhibit macro/variable expansion

    $result =~ s/\%/&#37;/gos;

    return $result;
}

# saveContext can be provided to a renderFile when multiple Workbooks
# are rendered on a page.  This enables cross-workbook optimization
# of the generated HTML, but is not required.

sub saveContext {
    my $this = shift;

    return $this->{_context};
}

# Close frees all resources
#
# For debugging, records caller and ensures that any attempt to reference
# a closed object is trapped and reported.

sub close {
    my $this = shift;

    %$this = ( _caller => [] );

    my $level = 1;
    while ( ( my @frame = caller( $level++ ) ) ) {
        push @{ $this->{_caller} }, [@frame];
    }

    bless $this, __PACKAGE__ . '::Closed';

    return;
}

sub DESTROY {
    my $this = shift;

    $this->close;

    return;
}

package ExcelToHtml::Context;

sub new {
    my $class = shift;

    return bless {@_}, $class;
}

package ExcelToHtml::Closed;

# Trap and report references to closed objects

sub AUTOLOAD {
    my $this = shift;

    our $AUTOLOAD;

    return if ( $AUTOLOAD =~ /::DESTROY$/ );

    croak("Corrupt closed handle") unless ( $this->{_caller} );

    my $message = "Reference to $AUTOLOAD in handle closed by:";
    my $trace   = $this->{_caller};

    my $level = 1;
    while (@$trace) {
        my $frame = shift @$trace;

        # ( pkg, file, line, subroutine, details... )

        $message .= sprintf( "\n%4u: ", $level++ );
        $message .= $frame->[0] . '::'   if ( defined $frame->[0] );
        $message .= $frame->[3]          if ( defined $frame->[3] );
        $message .= " in $frame->[1]"    if ( defined $frame->[1] );
        $message .= " line  $frame->[2]" if ( defined $frame->[2] );
    }

    croak("$message\nwas made");
}

package ExcelToHtml;

# Return CSS documentation

sub cssdoc {
    my $class = shift;
    my ($item) = @_;

    my $loc = tell(DATA);
    my @doc;

    while (<DATA>) {
        last if (/^__END__$/);
        next if (/^#/);
        if (/^\s+(.*)$/) {
            $doc[-1][1] .= $1 if (@doc);
        }
        else {
            my @fields = split( /\s*\|\s*/, $_, 2 );

            if ( @fields == 1 || !length $fields[0] ) {
                $doc[-1][1] = $fields[0] if (@doc);
            }
            else {
                push @doc, [@fields];
            }
        }
    }
    seek( DATA, $loc, SEEK_SET );

    return \@doc;
}

1;
__DATA__
# This describes the CSS clases for user documentation
# key | value
# These are returned in order.
# Lines starting with space are continuation, as are lines
# without a |.
errortext | Display of errors detected in formatting spreadsheet.
#
ssWindow | div controlling loads and size For the javascript to work, the rendering <b>must</b> be inside a DIV with this class.
ssWrapper  | Exterior div used to limit floats, define content for partial loads.
ssWorkbook | Workbook div, used for installing tab panes
ssTitle | Title div
ssRevInfo | Revision information
#
spreadsheet | Table when grid is omitted
ssWithGrid | Table when grid is displayed
ssWithLabels | Table when grid labels are displayed
#
ssOddRow | Odd numbered rows of the spreadsheet
ssEvenRow | Even numbered rows of the spreadsheet
#
ssCell | Data cells
ssColHeader | Column header cells (A B C...)
ssRowHeader | Row header cells (1 2 3 )
#
ssTypeNumeric | Cells with a numeric value
ssTypeText | Cells with a text value
ssTypeDate | Cells with a date value
#
ssAlignLeft | Left-aligned cells
ssAlignRight | Right-aligned cells
ssAlignCenter | Center-aligned cells
ssAlignFill | Filled cells (Excel repeats contents)
ssAlignJustify | Justified cells
ssAlignCenterAcross | Centered across multiple cells
ssAlignEqualSpace | Equal-spaced cells
#
ssAlignTop | Cells with data aligned to the top of the cell
ssAlignMiddle | Cells with data aligned to the middle of the cell
ssAlignBottom | Cells with data aligned to the bottom of the cell
ssAlignVJustify | Cells justified vertically (Excel adjusts row height)
ssAlignVEqalSpace | Cells with each line padded to consume equal space
ssAlignSuperscript | Superscript text segment
ssAlignSubscript | Subscript text segment
#
ssRotate<b>N</b>umber | Rotated cell <b>N</b> degrees (CCW)
#
ssBold | Bold text
ssItalic | Italic text
ssUnderSingle | Text with a single underline
ssUnderSingleAcct | Text with a single accounting underline
ssUnderDouble | Text with a double underline
ssUnderDoublAcct | Text with a double accounting underline
ssStrikeout | Struck-out text (lined through)
#
ssJustLast | Justify last
ssCellLocked | Locked cells
ssCellHidden | Hidden cells
ssWrappedText | Cell with wrapped text
ssShrinkToFit | Cell with text shrunk to fit cell size
ssOverflowOK | Cell can expand into adjacent cell
#
ssCellDirectionLtR | Cell whose text reading direction is Left to Right
ssCellDirectionRtL | Cell whose text reading direction is Right to Left
#
ssRowHeader<b>N</b> | Row heading (number) when the number of digits in the maximum row number is N. (e.g. 99 uses ssRowHeader2).
#
ssIndent<b>N</b> | Cells with text indented N characters (1-15)
#
ssBorder<b>S</b>ide<b>T</b>ype | Add border to a side of a cell.
 <br /><b>S</b>ide is one of Left, Right, Top or Bottom.
 <br />Each combination of <b>S</b>ide and <b>T</b>ype has a separate class.
ssBorder<b>S</b>ide</b>Thin | Thin solid border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Medium | Medium solid border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Dashed | Dashed border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Dotted | Dotted border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Thick | Thick solid border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Double | Double solid border on <b>S</b>ide
ssBorder<b>S</b>ide</b>Hairline | Hairline border on <b>S</b>ide
ssBorder<b>S</b>ide</b>MediumDashed | Thin dashed border on <b>S</b>ide
ssBorder<b>S</b>ide</b>DashDot | Thin dash-dot border on <b>S</b>ide
ssBorder<b>S</b>ide</b>MediumDashDot | Medium dash-dot border on <b>S</b>ide
ssBorder<b>S</b>ide</b>SlantedDashDot | Thin slanted border on <b>S</b>ide
ssBorder<b>S</b>ide</b>DashDotDot | Thin dash-dot-dot border on <b>S</b>ide
ssBorder<b>S</b>ide</b>MediumDashDotDot| Medium dash-dot-dot border on <b>S</b>ide
#
ssId<b>C</b>ellref | When enabled, each cell has its own class, named <b>W</b>orkbook\!<b>C</b>ol<b>R</b>ow, where Workbook, Col & Row are the cell's address.  Note that the '<b>\</b>' is required syntax for CSS identifiers containing unusual characters.
#
ssRemaining | Left-over space on tabs bar; clickable surface

__END__
