# Plugin for TWiki Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 20114 Timothe Litt, litt [at] acm.org
# Plugin interface:
# Copyright (C) 2013-2014 Peter Thoeny, peter[at]thoeny.org
# Copyright (C) 2013-2014 TWiki Contributors
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html

=pod

---+ package TWiki::Plugins::EmbedExcelPlugin


=cut

use warnings;
use strict;

package TWiki::Plugins::EmbedExcelPlugin;

# Name of this Plugin, only used in this module
our $pluginName = 'EmbedExcelPlugin';

require TWiki::Func;    # The plugins API

our $VERSION = '$Rev$';

our $RELEASE = '2014-02-16';

our $SHORTDESCRIPTION =
  'Embeds an Excel format spreadsheet in a %WIKITOOLNAME% page';

our $NO_PREFS_IN_TOPIC = 1;

use Carp ();

our $debug;
our $trace;

my $core;

sub debug {
    my $class = shift;

    $debug = $_[0] if (@_);
    return $debug;
}

# Handlers defined in Core:: that are registered with TWiki for this plugin.
# Do not list un-registered functions (e.g. Tag or REST handlers); they are
# autoloaded on demand.  Set the value to 0 if the handler is needed only
# if another event (typically a Tag) has activated the plugin.  Set the
# value to 1 if the handler is to be invoked unconditionally (typically
# when monitoring actions of others - e.g. rename).
#

my %handlers = (
    completePageHandler => 0,
    modifyHeaderHandler => 0,
    afterRenameHandler  => 1,
);

sub initPlugin {
    my ( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if ( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning(
            "Version mismatch between $pluginName and Plugins.pm");
        return 0;
    }

    {
        no warnings 'once';
        $debug = $TWiki::cfg{Plugins}{$pluginName}{Debug} || 0;
    }

    undef $core;

    TWiki::Func::registerTagHandler( 'EMBEDXLS', \&VarEMBEDXLS );

    TWiki::Func::registerRESTHandler( 'xls', \&RestSERVER );

    # Register plugin handlers (must be defined to be called)
    # When called, the corresponding Core:: method will be invoked.

    foreach my $handler ( keys %handlers ) {
        no strict 'refs';
        *{ __PACKAGE__ . "::$handler" } = sub {
            print STDERR (
                scalar localtime(),
                "[$$]: $pluginName - Invoking  $handler core ",
                ( $core ? "is" : 'not' ),
                " loaded\n"
            ) if ( $trace && ( $core || $handlers{$handler} ) );
            unless ($core) {
                return
                  unless ( $handlers{$handler} )
                  ;    # Don't activate unless configured
                $core = _loadCore();
            }
            unshift @_, $core;
            goto &{ __PACKAGE__ . "::Core::$handler" };
          }
          unless ( defined &{ __PACKAGE__ . "::$handler" } );
    }

    return 1;
}

# All functions are dynamically loaded and invoked as object methods.

# =========================
sub AUTOLOAD {
    our $AUTOLOAD;

    return if ( $AUTOLOAD =~ /::DESTROY$/ );

    print STDERR (
        scalar localtime() . "[$$]: $pluginName - Autoloading $AUTOLOAD\n" )
      if ($trace);

    my $method = $AUTOLOAD;
    $method =~ s/^.*:://;

    no strict 'refs';

    *$AUTOLOAD = sub {
        print STDERR (
            scalar localtime() . "[$$]: $pluginName - Invoking $method core ",
            ( $core ? "is" : 'not' ),
            " loaded\n"
        ) if ($trace);

        unless ($core) {
            $core = _loadCore();
        }

        my $methodSub = $core->can($method) || $core->can('AUTOLOAD');

        Carp::confess("EmbedExcelPlugin: $method is not implemented")
          unless ($methodSub);

        unshift @_, $core;
        goto &$methodSub;
    };
    goto &$AUTOLOAD;
}

# =========================
sub _loadCore {
    eval "require " . __PACKAGE__ . "::Core;";
    Carp::confess("$pluginName - failed to load Core: $@")
      if ($@);

    no warnings 'once';
    my $session = $TWiki::Plugins::SESSION;
    no strict 'refs';
    return ( __PACKAGE__ . "::Core" )->new(
        plugin => $pluginName,
        topic  => $session->{SESSION_TAGS}{TOPIC}
          || $session->{SESSION_TAGS}{BASETOPIC},
        web => $session->{SESSION_TAGS}{WEB} || $session->{SESSION_TAGS}{WEB},
        @_
    );
}

1;

# EOF
