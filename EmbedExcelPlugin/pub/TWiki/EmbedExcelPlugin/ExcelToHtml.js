/* Copyright (C) 2014 Timothe Litt litt at acm dot org
 */

// There is one XLS object per frame.  This reduces namespace pollution

var XLS = {
    // Because we identify objects based on classes, DOM ready is
    // too soon.  Document ready has the classes applied.

    uiver: 0,
    vpWidth:0,
    vpHeight:0,
    cssReady: false,
    stylesPending: [],
    tries: 0,
    stylesLoaded: {},
    uitimerrunning:0,

    load: function($) {
        // Horrible hack so we only start when the styles are applied.
        // Can be called for late-loaded documents, so we keep track of
        // what's loaded and don't request it again.  For that reason,
        // we also can't use IDs.  But order only matters within a
        // document, so names should be OK.

        if( !$.ui ) {
            if( uitimerrunning++ ) {
                return;
            }
            var uitimer = setInterval(function() {
                                          if( $.ui ) {
                                              clearInterval(uitimer);
                                              XLS.uitimerrunning = 0;
                                              XLS.load(jQuery);
                                              return;
                                          }
                                          return;
                                      }, 10);
            return;
        }

        if( !this.cssReady ) {
            var u = 1;
            var head = document.getElementsByTagName('head')[0];
            var timerRunning = this.stylesPending.length != 0;

            for( var css =  $('[name="ExcelToHtmlCSS"]'); css.length > 0; u++ ) {
                css.each( function () {
                              var cssVal = $(this).val();
                              if( !XLS.stylesLoaded[cssVal] ) {
                                  var style = document.createElement('style');
                                  style.textContent = '@import "' + cssVal + '"';
                                  head.appendChild(style);
                                  XLS.stylesPending.push(style);
                                  XLS.stylesLoaded[cssVal] = true;
                              }
                              return true;
                          });
                var n = u + '';
                while( n.length < 4 ) {
                    n = '0' + n;
                }
                css = $('[name="ExcelToHtmlCSSu' + n +'"]');
            }

            if( this.stylesPending.length == 0 ) {
                this.cssReady = true;
                XLS.tries = 0;
            } else {
                if( timerRunning ) {
                    return;
                }
                var timer = setInterval(function () {
                                            try {
                                                while( XLS.stylesPending.length ) {

                                                    XLS.stylesPending[0].sheet.cssRules; // Exception until loaded
                                                    XLS.stylesPending.shift();
                                                }
                                                clearInterval(timer);
                                                XLS.cssReady = true;
                                                XLS.tries = 0;
                                                XLS.load(jQuery);
                                                return;
                                            } catch (e) {
                                                if( ++XLS.tries >= 6000 ) { // 1 min is long enough!
                                                    clearInterval(timer);
                                                    throw( { name: 'CSSTimeout', message: 'CSS failed to load' } );
                                                }
                                                return;
                                            };
                                        }, 10);
                    return;
                }
        }

        // CSS is ready, handle load event

        // The jQuery tabs UI has evolved.  Identify and account for
        // differences.  We know about 1.8.x, 1.9.x and 1.10.x
        // We'll assume (hope) that future versions won't be
        // incompatible, again.

        this.uiver = $.ui.version.match(/^(\d+)\.(\d+)\.(\d+)$/);

        if( this.uiver.length != 4 || this.uiver[1] == 1 && this.uiver[2] < 8 ) {
            throw { name   :"ExecToHtml.unsupported",
                    message: "The running version of jQueryUI ( " + $.ui.version + ") is not a supported version" };
        }
        switch( parseInt(this.uiver[2], 10) ) {
            case 8:
            this.api = { selectOption: 'selected',
                         openedEvent : 'tabsshow',
                         openedTab   : 'tab' };
            break;
            case 9:
            this.api = { selectOption: 'active',
                         openedEvent : 'tabsactivate',
                         openedTab   :  'newTab' };
            break;
            case 10:
            default:
            this.api = { selectOption: 'active',
                         openedEvent : 'tabsactivate',
                         openedTab   : 'newTab' };
            break;
        }

        // Use AJAX to load any embedded spreadsheets

        XLS.fetchSS($);

        // Use the initial width and height as a default limit for sizes.

        var ruler = document.createElement('div');
        ruler.style.position = 'fixed';
        ruler.style.width = '100%';
        ruler.style.height = '100%';

        document.body.appendChild(ruler);
        this.vpWidth = ruler.offsetWidth;
        this.vpHeight = ruler.offsetHeight;
        document.body.removeChild(ruler);

        // Apply tab classes and event handlers.

        var so = this.api.selectOption;
        var $newWbs = $( 'div.ssWindow div.ssWorkbook' ).not('.ui-widget').
            each(function () {
                     var $wb = $(this);
                     var i = $wb.attr('ssInitialTab');
                     if( i === undefined ) {
                         $wb.tabs();
                     } else {
                         var opts = {};
                         opts[so] = parseInt( i, 10 );
                         $wb.tabs( opts );
                     }
                     $(this).find('table.spreadsheet,table.ssWithGrid').
                         find('thead,td.ssRowHeader').bind('click',
                                            function (e) {
                                                $(this).closest('table').toggleClass('ssWithLabels');
                                                e.preventDefault();
                                                XLS.resizeFrame( XLS.findFrame($wb) );
                                                return false;
                                            }).end().
                         find('td.ssCell').bind('click',
                                                function (e) {
                                                    var $this = $(this);
                                                    if( $this.find('div a[href]').length ) {
                                                        return true;
                                                    }
                                                    e.preventDefault;
                                                    $this.closest('table').toggleClass('spreadsheet ssWithGrid');
                                                    XLS.resizeFrame( XLS.findFrame($wb));
                                                    return false;
                                                });
                     var selected = 0;
                     $(this).find('.ui-tabs-nav li.ssRemaining').bind('click',
                                             function (e) {
                                                 var $wb =
                                                     $(this).closest('.ssWorkbook').
                                                          find('ul.ui-tabs-nav li').not('.ssRemaining').
                                                             each( function (index) {
                                                                       if( $(this).hasClass('ui-state-active') ||
                                                                           $(this).hasClass('ui-tabs-selected') ) {
                                                                           selected = index;
                                                                           return false;
                                                                       }
                                                                       return true;
                                                                   }).end().end(). // .not .find => Workbook
                                                     find('table.spreadsheet,table.ssWithGrid').eq( selected ).
                                                         toggleClass('ssWithLabels');
                                                 e.preventDefault();
                                                 XLS.resizeFrame( XLS.findFrame($wb));
                                                 return false;
                                             });
                     return true;
                 }
             );

        // Convert classes to bottom tabs by moving rounded corners
        // to the bottom

        $newWbs.find( '.ui-tabs-nav, .ui-tabs-nav *' )
            .removeClass( "ui-corner-all ui-corner-top" )
            .addClass( "ui-corner-bottom" );

        // Move tab navigation bar to bottom of enclosing div.
        // If in a FRAME, setup activation handlers to resize it when
        // spreadsheets open.

        var oe = this.api.openedEvent;
        var ot = this.api.openedTab;
        $newWbs.
            each( function () {
                      $wb = $(this);
                      $wb.find( ".ui-tabs-nav" ).appendTo( this );
                      var frame = XLS.findFrame($wb);

                      // new API gives both old and new tabs as jQuery objects, but can't count on that.
                      // Old gives the dom element.

                      var resize = function( event, ui ) {
                          var $tab = $(ui[ot]).closest('li');
                          var $ref = $tab.closest('div.ssWorkbook').
                                          find('thead td.ssColHeader.ssRowHeader').first();

                          var $li = $tab.css('background-color',
                                        $ref.css('background-color')).
                                            closest('ul').find('li').not($tab);
                          var defbg = $li.filter('.ssRemaining').first().css('background-color');
                          $li.not('.ssRemaining').
                                   each(
                                        function () {
                                            var $tab = $(this);
                                            var bg = $tab.attr('ssTabColor');
                                            if( bg === undefined ) {
                                                bg = defbg;
                                            }
                                            $tab.css('background-color', bg);
                                            return true;
                                        });
                          XLS.resizeFrame(frame);
                          return true;
                      };

                      var ui = {};
                      ui[ot] = $(this).find( ".ui-tabs-nav .ui-state-active" ).eq(0);
                      resize( {}, ui );
                      $(this).bind(oe, resize);
                      // $(window).resize(resize);
                      return true;
                  }
            );
    },
    offsetLeft: function(element) {
        if( !element ) {
            return 0;
        }
        var offset = element.offsetLeft;

        for( var parent = element.offsetParent; parent; parent = parent.offsetParent ) {
            offset += parent.offsetLeft;
        }
        return offset;
    },
    resizeFrame: function (f) {
          var $f,
          wid,
          h;

          if( !f ) {
              return true;
          }
          $f = $(f);

          if( !$f.filter(':visible').length ) {
              return true;
          }

          // Allow the content to fill a reasonable space

          wid = $f.attr('ssUserWidth');
          h   = $f.attr('ssUserHeight');
          if( !wid || ! h ) {
              var $c = $f.find("div.ssWrapper");
              if( !$c.length ) {
                  $c = $f;
              }

              if( !wid ) {
                  $f.css('max-width', 'none').css('width', '100000px');

                  wid = Math.min( window.innerWidth, (XLS.vpWidth - this.offsetLeft(f)) ) -32;
                  $f.css('max-width', wid ).css('width', $c.outerWidth() +32);
              }
              if( !h ) {
                  $f.css('max-height', 'none').css('height', '100000px');

                  h = Math.min( window.innerHeight, XLS.vpHeight ) -32;
                  $f.css('max-height', h).css('height', $c.outerHeight() +32);
              }
          }

          return true;
    },
    ssDisplay: function ( selector ) {
        // When an entity containing a Workbook becomes visible, frames
        // containing workbooks need to be resized.

        $(selector).find('div.ssWindow').
        each( 
             function () { 
                 XLS.resizeFrame(this);

                 return true;
             });

        return true;
    },
    findFrame: function( $element ) {
        return $element.closest('.ssWindow').get(0);
    },
    fetchSS: function ($) {

        $('div.ssRest').toggleClass('ssRest ssRestLoading').
            each( function () {
                      var $div = $(this);
                      var restServer = $div.find('INPUT[type="hidden"][name="ssSrcURL"]').val();
                      if( restServer === undefined || !restServer.length ) {
                          alert( "Invalid TML: No server");
                          return true;
                      }
                      $div.load(restServer,
                                function (rsp, status, xhr) {
                                    var $div = $(this);
                                    $div.removeClass('ssRestLoading');
                                    if( status == 'success' || status == 'notmodified' ) {
                                        $div.addClass('ssWindow');
                                        XLS.load(jQuery);
                                    } else {
                                        rsp = rsp.replace( /\n/mg,'<newline>' );
                                        rsp = rsp.replace( /^.*<html[^>]*?>/mi, '' );
                                        rsp = rsp.replace( /<\/html>.*$/mi, '' );
                                        rsp = rsp.replace( /<head>/gmi, '<div>');
                                        rsp = rsp.replace( /<body[^>]*?>/gmi, '<div>');
                                        rsp = rsp.replace( /<\/head>/gmi, '</div>');
                                        rsp = rsp.replace( /<\/body>/gmi, '</div>');
                                        rsp = rsp.replace( /<title[^>]*>.*?<\/title>/mgi, '' );
                                        rsp = rsp.replace( /<script[^>]*>.*?<\/script>/mgi, '' );
                                        rsp = rsp.replace( /<newline>/mg, "\n" );

                                        $div.html(rsp);
                                    }
                                    return true;
                                });
                      return true;
                  });
        return true;
    }
};

jQuery(window).load( function() {

            XLS.load(jQuery);
  }
);
