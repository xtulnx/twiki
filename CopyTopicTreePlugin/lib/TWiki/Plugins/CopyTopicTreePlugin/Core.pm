# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2014 Alba Power Quality Solutions
# Copyright (C) 2014 Peter Thoeny, peter[at]thoeny.org 
# Copyright (C) 2014 TWiki Contributors. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::CopyTopicTreePlugin::Core;

use strict;
use Error qw( :try );
require File::Copy;
require File::Path;
require File::Basename;

# =========================
sub new {
    my ( $class, $debug ) = @_;

    my $this = {
          Debug         => $debug,
          CopyRevisions => $TWiki::cfg{Plugins}{CopyTopicTreePlugin}{CopyRevisions} || 0,
          WorkAreaDir   => TWiki::Func::getWorkArea( 'CopyTopicTreePlugin' ),
        };
    bless( $this, $class );
    $this->_writeDebug( "new: constructor" );

    return $this;
}

# =========================
sub VarCOPYTOPICTREE
{
    my ( $this, $session, $params, $theTopic, $theWeb ) = @_;
    $this->{Session} = $session;
    my $action  = $params->{_DEFAULT} || $params->{action} || '';
    $this->_writeDebug( "VarCOPYTOPICTREE($action)" );
    my $text = '';
    if( $action eq 'copytopictree' ) {
        $text = $this->_copyTopicTree( $params, $theWeb, $theTopic );
    } elsif( $action ) {
        $text = "COPYTOPICTREE ERROR: Unrecogized action parameter $action";
    } else {
        $text = '';
    }
    $this->_writeDebug( "VarCOPYTOPICTREE($action) return: $text" );
    return $text;
}

# =========================
sub _copyTopicTree
{
    my ( $this, $params, $theWeb, $theTopic ) = @_;

    my $from         = $params->{from};
    my $to           = $params->{to};
    my $includeRegex = $params->{includes}   || '';
    my $excludeRegex = $params->{excludes}   || '';
    my $substRegex   = $params->{substitute} || '';
    $this->{BaseParent}     = $params->{baseparent}     || '';
    $this->{BaseFormFields} = $params->{baseformfields} || '';

    $this->_writeDebug( "_copyTopicTree($from, $to, $includeRegex, $excludeRegex, $substRegex)" );
    unless( $from && $to ) {
        return 'COPYTOPICTREE ERROR: Copy topic tree not done, from="" and to="" parameters are missing';
    }
    unless( TWiki::Func::getContext()->{authenticated} ) {
        return 'COPYTOPICTREE ERROR: Copying a topic tree can only be done once authenticated';
    }
    $includeRegex = '^.*$' unless( $includeRegex );
    $excludeRegex = '^something-impossible$' unless( $excludeRegex );
    my ( $fromWeb, $fromTopic ) = TWiki::Func::normalizeWebTopicName( $theWeb, $from );
    my ( $toWeb, $toTopic )     = TWiki::Func::normalizeWebTopicName( $theWeb, $to );
    my @topicList =
      grep { !/^$fromTopic$/ }
      grep { !/$excludeRegex/ }
      grep { /$includeRegex/ }
      TWiki::Func::getTopicList( $fromWeb );
    my $result = TWiki::Func::searchInWebContent( '.', $fromWeb, \@topicList,
                                        { type => 'regex', files_without_match => 1 } );
    my $topicParents;
    foreach my $aTopic ( keys %$result ) {
        my( $aMeta ) = TWiki::Func::readTopic( $fromWeb, $aTopic );
        $topicParents->{$aTopic} = $aMeta->getParent();
    }
    # use Data::Dumper;
    # $msg .= Dumper( $topicParents ) . '<br />';
    if( $substRegex =~ /^ *\/(.*)\/(.*)\/ *$/ ) {
        $this->{SubstRegex}   = $1;
        $this->{SubstReplace} = $2;
    } else {
        $this->{SubstRegex}   = undef;
        $this->{SubstReplace} = undef;
    }
    my $seen;
    my $msg = '';
    $this->{FromBaseTopic} = $fromTopic;
    $this->{ToBaseTopic}   = $toTopic;
    $this->_copyTopicAndChildren( $fromWeb, $fromTopic, $toWeb, $toTopic, $topicParents, $seen, \$msg );
    return $msg;
}

# =========================
sub _copyTopicAndChildren
{
    my ( $this, $fromWeb, $fromTopic, $toWeb, $toTopic, $topicParents, $seen, $msgRef ) = @_;

    return if( $seen->{$fromTopic} ); # break out of circular reference
    $seen->{$fromTopic} = 1;

    $this->_writeDebug( "_copyTopicAndChildren($fromWeb, $fromTopic, $toWeb, $toTopic)" );
    # copy this topic
    $this->_copyTopic( $fromWeb, $fromTopic, $toWeb, $toTopic, $msgRef );

    # recursively copy all children
    foreach my $aChild ( keys %{$topicParents} ) {
        if( $topicParents->{$aChild} eq $fromTopic ) {
            # create name of child topic
            my $toChild = $aChild;
            if( $this->{SubstRegex} && $toChild =~ s/$this->{SubstRegex}/$this->{SubstReplace}/ ) {
                my ( $arg1, $arg2, $arg3, $arg4 ) = ( $1, $2, $3, $4 );
                $toChild =~ s/\$1/$arg1/g if( defined $arg1 );
                $toChild =~ s/\$2/$arg2/g if( defined $arg2 );
                $toChild =~ s/\$3/$arg3/g if( defined $arg3 );
                $toChild =~ s/\$4/$arg4/g if( defined $arg4 );
            }
            # recursively copy child and grand children
            $this->_copyTopicAndChildren( $fromWeb, $aChild, $toWeb, $toChild, $topicParents, $seen, $msgRef );
        }
    }
}

# =========================
sub _copyTopic
{
    my ( $this, $fromWeb, $fromTopic, $toWeb, $toTopic, $msgRef ) = @_;

    $this->_writeDebug( "_copyTopic($fromWeb, $fromTopic, $toWeb, $toTopic)" );
    $$msgRef .= "- $fromTopic ";

    if( TWiki::Func::topicExists( $toWeb, $toTopic ) ) {
        # no action
        $$msgRef .= "not copied, destination $toTopic alerady exists <br />";
        return;
    }

    my $user = TWiki::Func::getWikiName(); # WikiName of login user

    # target topic does not exist, but we read it anyway to check for permissions of web
    my ( $meta, $text ) = TWiki::Func::readTopic( $toWeb, $toTopic );
    unless( TWiki::Func::checkAccessPermission( 'VIEW', $user, $text, $toTopic, $toWeb, $meta ) ) {
        $$msgRef .= "not copied, no permission to view $toWeb web <br />";
        return;
    }
    unless( TWiki::Func::checkAccessPermission( 'CHANGE', $user, $text, $toTopic, $toWeb, $meta ) ) {
        $$msgRef .= "not copied, no permission to change $toWeb web <br />";
        return;
    }

    # check access control of source topic
    ( $meta, $text ) = TWiki::Func::readTopic( $fromWeb, $fromTopic );
    unless( TWiki::Func::checkAccessPermission( 'VIEW', $user, $text, $fromTopic, $fromWeb, $meta ) ) {
        $$msgRef .= "not copied, no read permission on source topic <br />";
        return;
    }
    unless( TWiki::Func::checkAccessPermission( 'CHANGE', $user, $text, $fromTopic, $fromWeb, $meta ) ) {
        $$msgRef .= "not copied, no change permission on source topic <br />";
        return;
    }

    my $needSave = 0;
    my $saveAttrs = { forcenewrevision => 1, minor => 1 };
    if( $this->{CopyRevisions} ) {
        # clone new topic from source topic, including all revisions
        my $ok = $this->_cloneTopic( $fromWeb, $fromTopic, $toWeb, $toTopic );
        return unless( $ok );
        $needSave = 1 if( $this->{CopyRevisions} > 1 );

    } else {
        # copy attachments to new topic
        if( $this->_copyAttachments( $meta, $text, $fromWeb, $fromTopic, $toWeb, $toTopic ) ) {
            # reload topic because attachment meta data changed
            ( $meta, $text ) = TWiki::Func::readTopic( $toWeb, $toTopic );
            delete $saveAttrs->{forcenewrevision};
        }
        # save topic after substitution of form fields
        $needSave = 1;
    }

    # fix parent
    my $parent = $meta->get( 'TOPICPARENT' );
    $parent = $parent->{name} if( $parent );
    if( $fromTopic eq $this->{FromBaseTopic} && $this->{BaseParent} ) {
        # set parent of destination base topic if toparent parameter is specified
        $meta->put( 'TOPICPARENT', { name => $this->{BaseParent} } );
        $needSave = 1;

    } elsif( $parent && $this->{SubstRegex} ) {
        # need to fix parent if topic substitution is specified
        if( $parent eq $this->{FromBaseTopic} ) {
            # this topic is an immediate child of the base topic
            $meta->put( 'TOPICPARENT', { name => $this->{ToBaseTopic} } );
            $needSave = 1;

        } elsif( $fromTopic ne $this->{FromBaseTopic} &&
                 $parent =~ s/$this->{SubstRegex}/$this->{SubstReplace}/ ) {
            # this topic is a grand child or below
            my ( $arg1, $arg2, $arg3, $arg4 ) = ( $1, $2, $3, $4 );
            $parent =~ s/\$1/$arg1/g if( defined $arg1 );
            $parent =~ s/\$2/$arg2/g if( defined $arg2 );
            $parent =~ s/\$3/$arg3/g if( defined $arg3 );
            $parent =~ s/\$4/$arg4/g if( defined $arg4 );
            $meta->put( 'TOPICPARENT', { name => $parent } );
            $needSave = 1;
        }
    }

    my $session = $this->{Session};
    my $request = $session->{request} if( $session );
    if( $fromTopic eq $this->{FromBaseTopic} && $this->{BaseFormFields} ) {
        # set form fields of destination base topic
        foreach my $key ( split( /, */, $this->{BaseFormFields}) ) {
            my $value = '';
            if( $key =~ s/^(.*?)=(.*)$/$1/ ) {
                # set based on key=value
                $value = $2;
            } elsif( $request ) {
                # set based on URL parameter
                $value = $request->param( $key );
                $value = '' unless( defined $value );
            }
            my $field = $meta->get( 'FIELD', $key );
            if( $field ) {
                $field->{value} = $value;
                $meta->putKeyed( 'FIELD', $field );
                $needSave = 1;
            }
        }
    }

    # FIXME substitution of descendant form fields

    # finally save new topic, forcing new revision
    if( $needSave ) {
        # save new topic, forcing new revision
        TWiki::Func::saveTopic( $toWeb, $toTopic, $meta, $text, $saveAttrs );
    }
    $$msgRef .= "==> $toWeb.$toTopic copied <br />";
}

# =========================
sub _copyAttachments
{
    my ( $this, $meta, $text, $fromWeb, $fromTopic, $toWeb, $toTopic ) = @_;

    $this->_writeDebug( "_createTopic(meta, text, $fromWeb, $fromTopic, $toWeb, $toTopic)" );

    # check if there are attachments
    my @attachments = $meta->find('FILEATTACHMENT');
    return 0 unless( scalar @attachments );

    $meta->remove( 'FILEATTACHMENT' );
    my $saveAttrs = { dontlog => 1, minor => 1 };
    # save topic first so that we can attach files
    TWiki::Func::saveTopic( $toWeb, $toTopic, $meta, $text, $saveAttrs );

    # copy top rev of all attachments to new topic
    my $workDir = TWiki::Func::getWorkArea( 'CopyTopicTreePlugin' );
    foreach my $aAttachment ( @attachments ) {
        my $name = $aAttachment->{name};
        my $tmpFile = "$workDir/$name";
        try {
            $this->_writeDebug( " -- copy attachment $name" );
            TWiki::Func::saveFile(
              $tmpFile,
              TWiki::Func::readAttachment( $fromWeb, $fromTopic, $name )
            );
            my @fileStats = stat( $tmpFile );
            my $fileSize  = $fileStats[7];
            my $result = TWiki::Func::saveAttachment(
              $toWeb, $toTopic, $name,
              {
                file     => $tmpFile,
                filepath => $name,
                filesize => $fileSize,
                filedate => time(),
                comment  => $aAttachment->{comment} || '',
                hide     => ( $aAttachment->{attr} || '' ) =~ /h/ ? 1 : 0,
                dontlog  => 1
              },
              1
            );
            # $this->_writeDebug( "=== error: $result" ) if( $result );
            # FIXME error handling
            unlink( $tmpFile );
        } catch TWiki::AccessControlException with {
            # $this->_writeDebug( "=== error: " . shift->{-text} );
            # FIXME error handling
        };
    }
    return 1;
}

# =========================
sub _cloneTopic
{
    my ( $this, $fromWeb, $fromTopic, $toWeb, $toTopic ) = @_;

    $this->_writeDebug( "_cloneTopic($fromWeb, $fromTopic, $toWeb, $toTopic)" );
    my $ok = 1;

    # FIXME: No TWiki::Func API exists to clone a topic
    if( $TWiki::Plugins::VERSION >= 6.0 ) {
        # do TWiki internal call and pray that the API does not change
        my $session = $this->{Session};
        my $store = $session->{store};
        try {
            $store->copyTopic( $session->{user}, $fromWeb, $fromTopic, $toWeb, $toTopic );
        } catch Error::Simple with {
            # FIXME error handling
            #throw TWiki::OopsException( 'attention', web => $fromWeb, topic => $fromTopic,
            #                            def => 'copy_error',
            #                            params => [ shift->{-text}, $toWeb, $toTopic ] );
            return 0;
        };

    } else {
        # assume file backend and copy topic and attachments on file level
        my $dataDir = $TWiki::cfg{DataDir};
        my $pubDir = $TWiki::cfg{PubDir};
        $fromWeb =~ s/\./\//g;
        $toWeb =~ s/\./\//g;
        my $fromFile = "$dataDir/$fromWeb/$fromTopic.txt";
        my $toFile = "$dataDir/$toWeb/$toTopic.txt";
        $this->_copyFile( $fromFile, $toFile );
        $fromFile .= ',v';
        $toFile .= ',v';
        $this->_copyFile( $fromFile, $toFile ) if( -e $fromFile );
        my $fromDir = "$pubDir/$fromWeb/$fromTopic";
        my $toDir = "$pubDir/$toWeb/$toTopic";
        if( -e $fromDir ) {
            if( opendir( my $DIR, $fromDir )) {
                my @files = sort grep { !/^\./ } readdir $DIR;
                closedir $DIR;
                for my $file ( @files ) {
                    $file = TWiki::Sandbox::untaintUnchecked( $file );
                    $this->_copyFile( "$fromDir/$file", "$toDir/$file" );
                }
            }
        }
    }
    return $ok;
}

# =========================
sub _copyFile {
    my( $this, $from, $to ) = @_;

    $this->_writeDebug( "_copyFile($from, $to)" );

    $this->_mkPathTo( $to );
    unless( File::Copy::copy( $from, $to ) ) {
        # FIXME error "$!"
    }
    return;
}

# =========================
sub _mkPathTo {
    my( $this, $dir ) = @_;

    $dir = TWiki::Sandbox::untaintUnchecked( $dir );
    my $path = File::Basename::dirname( $dir );
    eval {
        File::Path::mkpath( $path, 0, $TWiki::cfg{RCS}{dirPermission} );
    };
    if ($@) {
        # FIXME error "$!"
    }

    return;
}

# =========================
sub _writeDebug
{
    my ( $this, $msg ) = @_;
    return unless( $this->{Debug} );
    TWiki::Func::writeDebug( "- CopyTopicTreePlugin::Core::$msg" );
}

# =========================
1;
