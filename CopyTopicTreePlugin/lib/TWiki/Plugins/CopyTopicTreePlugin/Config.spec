# ---+ Extensions
# ---++ Copy Topic Tree Plugin
# This is the configuration of the <b>CopyTopicTreePlugin</b>.

# **STRING 80**
# Copy revisions:
# <br />- Specify 0 to ignore the topic revisions of the source topic, e.g. start 
# destination topic with revision 1 based on latest revision of source topic.
# <br />- Specify 1 to copy all revisions, e.g. to clone topic.
# <br />- Specify 2 to copy all revisions and create a new revision on top of it,
# with author set to person who does the copy topic tree action.
$TWiki::cfg{Plugins}{CopyTopicTreePlugin}{CopyRevisions} = 0;

# **BOOLEAN**
# Debug plugin. See output in data/debug.txt
$TWiki::cfg{Plugins}{CopyTopicTreePlugin}{Debug} = 0;

1;
