# Plugin for TWiki Enterprise Collaboration Platform, http://TWiki.org/
#
# Copyright (C) 2014 Alba Power Quality Solutions
# Copyright (C) 2014 Peter Thoeny, peter[at]thoeny.org 
# Copyright (C) 2014 TWiki Contributors. All Rights Reserved.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read LICENSE in the root of this distribution.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, published at
# http://www.gnu.org/copyleft/gpl.html
#
# As per the GPL, removal of this notice is prohibited.

package TWiki::Plugins::CopyTopicTreePlugin;

# =========================
our $VERSION = '$Rev$';
our $RELEASE = '2014-03-29';
our $SHORTDESCRIPTION = 'Copy a topic tree recursively to a new tree';
our $NO_PREFS_IN_TOPIC = 1;

# =========================
our $debug = $TWiki::cfg{Plugins}{CopyTopicTreePlugin}{Debug} || 0;
our $web;
our $topic;
our $core;

# =========================
sub initPlugin
{
    ( $topic, $web, $user, $installWeb ) = @_;

    # check for Plugins.pm versions
    if( $TWiki::Plugins::VERSION < 1.2 ) {
        TWiki::Func::writeWarning( "Version mismatch between CopyTopicTreePlugin and Plugins.pm" );
        return 0;
    }

    TWiki::Func::registerTagHandler( 'COPYTOPICTREE', \&_COPYTOPICTREE );

    # Plugin correctly initialized
    TWiki::Func::writeDebug( "- TWiki::Plugins::CopyTopicTreePlugin::initPlugin( $web.$topic ) is OK" ) if $debug;

    return 1;
}

# =========================
sub _COPYTOPICTREE
{
    my ( $session, $params, $theTopic, $theWeb ) = @_;

    # Lazy loading, e.g. compile core module only when required
    unless( $core ) {
        require TWiki::Plugins::CopyTopicTreePlugin::Core;
        $core = new TWiki::Plugins::CopyTopicTreePlugin::Core( $debug );
    }
    return $core->VarCOPYTOPICTREE( $session, $params, $theTopic, $theWeb );
}

# =========================
1;
